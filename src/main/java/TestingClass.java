import org.apache.log4j.PropertyConfigurator;

import com.banquetin.action.BookingDisplayAction;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dab.userdab.UserDABFactory;
import com.banquetin.dataobjects.PropertyInfoData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.userpab.UserPAB;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;

public class TestingClass {

	public static void main(String[] args) {

		PropertyConfigurator.configure("C:\\temp\\log4j.properties");
		BaseDAB.USE_DIRECT_CONNECTION = true;

		// testPropertyData();
		// testAmentiesDab();
		// testBookingData();
		// testBlockDatesData();
		// testRatesData();
		// testVenueData();
		// testPropertyAmenities();
		// testUserData();
		// testVendorData();
		// testFilterVenue();
		//testEmailId();
		testPhone();
	}

	private static void testEmailId()
	{
		System.out.println(PropertyPABFactory.getPABInstance().validateEmailExist("puneet4sdfdsf1824@gmail.com"));
	}
	
	private static void testPhone()
	{
		System.out.println(PropertyPABFactory.getPABInstance().validatePhoneExist("9632968050"));
	}
	
	
	private static void testFilterVenue() {
//		String startDate = "2018-03-28";
//		String location = "city";
//		String pax = "3000";
//		VenuePABFactory.getPABInstance().getFilteredVenueList(startDate, location, pax);
	}

	private static void testPropertyAmenities() {

		// PropertyPABFactory.getPABInstance().addAmenitiesToProperty("2", "1");
	}

	private static void testVendorData() {
		// VendorData vendorData = new VendorData();
		// vendorData.setVendorName("Dhruva2");
		// vendorData.setVendorDescription(" A big supplier car");
		// vendorData.setImageId(null);
		// StaticData vendorType = new StaticData();
		// vendorType.setId("1");
		// vendorData.setVendorType(vendorType);
		// vendorData.setVendorId("4");
		// String vendorId =
		// VendorPABFactory.getPABInstance().addVendorData(vendorData);
		// System.out.println(vendorId);
		// boolean check =
		// VendorPABFactory.getPABInstance().updateVendorData(vendorData);
		// System.out.print(check);
		// System.out.println( VendorPABFactory.getPABInstance().getVendorData("4"));
		// System.out.println(VendorPABFactory.getPABInstance().getVendorList());
		// System.out.println(VendorPABFactory.getPABInstance().deleteVendorData("5"));
	}

	private static void testUserData() {

		// UserData userData = new UserData();
		// userData.setUserName("Dhruva1");
		// userData.setUserPassword("test");
		// userData.setUserEmail("dhruva@test.com");
		// StaticData userType = new StaticData();
		// userType.setId("1");
		// userData.setUserRoleType(userType);
		// userData.setUserMobile("9538723390");
		// userData.setUserId("1");
		// boolean userId = UserPABFactory.getPABInstance().updateUserData(userData);
		// System.out.println( UserPABFactory.getPABInstance().getUserData("1"));
		// System.out.println( UserPABFactory.getPABInstance().getUserList());
		// System.out.println( UserPABFactory.getPABInstance().deleteUserData("2"));
		// System.out.println(userId);
		// System.out.println(UserPABFactory.getPABInstance().addPropertyToUser("1", "5"));
		// System.out.println(UserPABFactory.getPABInstance().deletePropertyFromUser("1", "3"));
		// System.out.println(UserPABFactory.getPABInstance().getUserPropertyData("1", "2"));
		// System.out.println(UserPABFactory.getPABInstance().getUserPropertyList("14").get(0));
		
	}

	private static void testVenueData() {
		VenueData venueData = new VenueData();
		//
		// venueData.setHeight(100);
		// venueData.setLength(100);
		// venueData.setMinBookingAmount(25000);
		// venueData.setVenueDescription("VenueDesc1");
		// //
		// StaticData venueType = new StaticData();
		// venueType.setId("1");
		// venueData.setVenueType(venueType);
		// //
		// venueData.setWidth(10);
		// // venueData.setVenueId("4");
		//
		// String venueId = VenuePABFactory.getPABInstance().addVenueData(venueData);
		//
		// System.out.println(VenuePABFactory.getPABInstance().getVenueData("2"));
		// System.out.println(VenuePABFactory.getPABInstance().getVenueList());
		// boolean check = VenuePABFactory.getPABInstance().updateVenueData(venueData);
		// System.out.println(check);
		// System.out.println(VenuePABFactory.getPABInstance().deleteVenueData("2"));
		// System.out.println(VenuePABFactory.getPABInstance().deleteEventTypeFromVenue("2","1"));
		// System.out.println(VenuePABFactory.getPABInstance().deleteSeatingTypeFromVenue("2","2"));
		// System.out.print(check);
		// System.out.print(venueId);

		// //
		// SeatingTypeData seatingTypeData = new SeatingTypeData();
		// seatingTypeData.setMaxCapacity("1000");
		// seatingTypeData.setMinCapacity("200");
		// seatingTypeData.setSeatingTypeId("1");
		//
		// VenuePABFactory.getPABInstance().addSeatingTypeToVenue(venueId,
		// seatingTypeData);
		//
		// VenuePABFactory.getPABInstance().addEventTypeToVenue(venueId, "1", "1");

		// PropertyPABFactory.getPABInstance().addVenueToProperty("2", "2", "1");
		// System.out.println(VenuePABFactory.getPABInstance().getEventTypeFromVenue("2"));
		// System.out.println(VenuePABFactory.getPABInstance().getSeatingTypeFromVenue("2"));

	}

	private static void testPropertyData() {
		// PropertyData propertyData = new PropertyData();
		// propertyData.setOnlineInd("Y");
		// propertyData.setPropertyDescription("propDesc");
		// propertyData.setPropertyName("propName123");
		// propertyData.setPropertyId("2");
		// StaticData propertyType = new StaticData();
		// propertyType.setId("1");
		// propertyData.setPropertyType(propertyType);
		// propertyData.setStarRating("5");
//		String oldTitle = "<b>Updated Cancellation Policy</b>";
//		String newTitle = "Updated Cancellation Policy";
//		String value = "<h3>Lorem Ipsum is simply dummy</h3>\n"
//				+ "<p>text of the printing and typesetting industry. Lorem Ipsum has been the"
//				+ "<h3>Lorem Ipsum is simply dummy</h3>\n"
//				+ "<p>text of the printing and typesetting industry. Lorem Ipsum has been the"
//				+ "<h3>Lorem Ipsum is simply dummy</h3>\n"
//				+ "<p>text of the printing and typesetting industry. Lorem Ipsum has been the";
//		PropertyInfoData propertyInfo = new PropertyInfoData();
//		propertyInfo.setPropertyId("5");
//		propertyInfo.setTitle(oldTitle);
//		propertyInfo.setValue(value);
//		PropertyInfoData newPropertyInfo = new PropertyInfoData();
//		newPropertyInfo.setPropertyId("5");
//		newPropertyInfo.setTitle(newTitle);
//		newPropertyInfo.setValue(value);

		// System.out.println(PropertyPABFactory.getPABInstance().addImageToProperty("2",
		// "5", 0));
		// System.out.println(PropertyPABFactory.getPABInstance().updateImageOfProperty("2",
		// "5", 1));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteImageFromProperty("2",
		// null, true));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteImageFromProperty("2",
		// null, false));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteImageFromProperty("2",
		// "5", true));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteImageFromProperty("2",
		// "5", false));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteImageFromProperty("2",
		// "6", true));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteImageFromProperty("2",
		// "6", false));
		// System.out.println(PropertyPABFactory.getPABInstance().getImageFromProperty("2",
		// "5"));
		// System.out.println(PropertyPABFactory.getPABInstance().getImageFromProperty("2",
		// "6"));
		// System.out.println(PropertyPABFactory.getPABInstance().getAllImageFromProperty("2"));

		// System.out.println(PropertyPABFactory.getPABInstance().addInfoToProperty(propertyInfo));
		// System.out.println(PropertyPABFactory.getPABInstance().updateInfoForPropertyUtil(propertyInfo,
		// newPropertyInfo));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteInfoFromProperty("2",
		// title));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteInfoFromProperty("2",
		// null));
		// System.out.println(PropertyPABFactory.getPABInstance().getInfoFromProperty("3",
		// title));
		// System.out.println(PropertyPABFactory.getPABInstance().getAllInfoFromProperty("2"));
		// PropertyPABFactory.getPABInstance().addPropertyData(propertyData);
		// PropertyPABFactory.getPABInstance().updatePropertyData(propertyData);
		// System.out.println(PropertyPABFactory.getPABInstance().getPropertyData("2"));
		// System.out.println(PropertyPABFactory.getPABInstance().getPropertyList());
		// System.out.println(PropertyPABFactory.getPABInstance().getPropertyData("2"));
		// System.out.println(PropertyPABFactory.getPABInstance().deletePropertyData("3"));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteVenueFromProperty("2","2"));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteAmenitiesFromProperty("2","1"));
		// System.out.println(PropertyPABFactory.getPABInstance().getVenueFromProperty("2"));
		// System.out.println(PropertyPABFactory.getPABInstance().getAmenitiesFromProperty("2"));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteAddressFromProperty("3","1"));
		// System.out.println(PropertyPABFactory.getPABInstance().getAddressFromProperty("3"));
		// System.out.println(PropertyPABFactory.getPABInstance().addVendorToProperty("4","1"));
		// System.out.println(PropertyPABFactory.getPABInstance().deleteVendorFromProperty("4","1"));
		// System.out.println(PropertyPABFactory.getPABInstance().getVendorFromProperty("5"));
		// AddressData addressData = new AddressData();
		// addressData.setAddress1("a1");
		// addressData.setAddress2("a2");
		// addressData.setAddressTypeId("1");
		// addressData.setCity("city");
		// addressData.setCountryId("1");
		// addressData.setStateId("1");
		// addressData.setPincode("12345");
		//
		// PropertyPABFactory.getPABInstance().addAddressToProperty("1", addressData);

		// EmailData emailData = new EmailData();
		// emailData.setEmailAddress("abc@xyz.com");
		// emailData.setEmailTypeId("1");

		// PropertyPABFactory.getPABInstance().addEmailToProperty("1", emailData);

	}

	private static void testAmentiesDab() {
		// AmenitiesData amenitiesData = new AmenitiesData();
		//
		// amenitiesData.setAmenitiesDescription("Desc");
		// amenitiesData.setAmenitiesIcon("01");
		// amenitiesData.setAmenitiesText("SPA");
		// amenitiesData.setSortOrder(1);
		// amenitiesData.setShowInFilter("1");
		// String amenetiesId = null;
		//
		// amenetiesId =
		// AmenitiesPABFactory.getPABInstance().addAmenitiesData(amenitiesData);
		// // AmenitiesPABFactory.getPABInstance().updateAmenitiesData(amenitiesData);
		// // AmenitiesPABFactory.getPABInstance().deleteAmenitiesData(amenetiesId);
	}

	private static void testBookingData() {
		// BookingData bookingData = new BookingData();
		// bookingData.setBookingId("16");
		// bookingData.setVenueId("2");
		// java.util.Date startDate = new java.util.Date();
		// bookingData.setStartDate(startDate);
		// java.util.Date endDate = new java.util.Date();
		// bookingData.setEndDate(endDate);
		// bookingData.setAmount(100000);
		// bookingData.setTransactionid("102");
		// StaticData paymentType = new StaticData();
		// paymentType.setId("1");
		// paymentType.setText("CASH");
		// bookingData.setPaymentType(paymentType);
		// StaticData bookingStatus = new StaticData();
		// bookingStatus.setId("1");
		// bookingData.setBookingStatus(bookingStatus);
		// bookingData.setUserId("1");
		// bookingData.setEventType("1");
		// SlotTypeData slotTypeData = new SlotTypeData();
		// slotTypeData.setSlotTypeId("1");
		// bookingData.setStartSlotTypeData(slotTypeData);
		// slotTypeData.setSlotTypeId("2");
		// bookingData.setEndSlotTypeData(slotTypeData);
		//
		// String date = "2018-03-21";
		// String startDate = "2018-03-19";
		// String endDate = "2018-03-31";
		// System.out.println(BookingPABFactory.getPABInstance().addBookingData(bookingData));
		// System.out.println(BookingPABFactory.getPABInstance().updateBookingData(bookingData));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingList("2" ,
		// "1", java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate)));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingList("2" ,
		// null, java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate)));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingList(null ,
		// "1", java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate)));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingList(null ,
		// null, java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate)));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingList(null ,
		// null, null, null));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingsForProperty("2",
		// java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(endDate)));
		// System.out.println(BookingPABFactory.getPABInstance().getBookingData("16"));
		// System.out.println(BookingPABFactory.getPABInstance().deleteBookingData("10"));
	}

	private static void testBlockDatesData() {
		// BlockDatesData blockDatesData = new BlockDatesData();
		// blockDatesData.setBlockDatesId("1");
		// blockDatesData.setVenueId("3");
		// String date = "2018-03-22";
		// String startDate = "2018-03-22";
		// String endDate = "2018-03-30";
		// blockDatesData.setStartDate(java.sql.Date.valueOf(date));
		// blockDatesData.setEndDate(java.sql.Date.valueOf(date));
		// SlotTypeData startSlotTypeData = new SlotTypeData();
		// startSlotTypeData.setSlotTypeId("1");
		// blockDatesData.setStartSlotTypeData(startSlotTypeData);
		// SlotTypeData endSlotTypeData = new SlotTypeData();
		// endSlotTypeData.setSlotTypeId("2");
		// blockDatesData.setEndSlotTypeData(endSlotTypeData);

		// System.out.println(BlockDatesPABFactory.getPABInstance().addBlockDatesData(blockDatesData));
		// System.out.println(BlockDatesPABFactory.getPABInstance().updateBlockDatesData(blockDatesData));
		// System.out.println(BlockDatesPABFactory.getPABInstance().getBlockDatesData("1"));
		// System.out.println(BlockDatesPABFactory.getPABInstance().getBlockDatesList("3",
		// java.sql.Date.valueOf(date), java.sql.Date.valueOf(date)));
		// System.out.println(BlockDatesPABFactory.getPABInstance().getBlockDatesForProperty("2",java.sql.Date.valueOf(startDate),
		// java.sql.Date.valueOf(endDate)));
		// System.out.println(BlockDatesPABFactory.getPABInstance().deleteBlockDatesData("1"));
	}

	private static void testRatesData() {
		// RatesData ratesData = new RatesData();
		// ratesData.setRatesId("3");
		// ratesData.setVenueId("2");
		// String date = "2018-03-22";
		// ratesData.setStartDate(java.sql.Date.valueOf(date));
		// ratesData.setEndDate(java.sql.Date.valueOf(date));
		// ratesData.setRate(10000);
		// StaticData ratesType = new StaticData();
		// ratesType.setId("2");
		// ratesData.setRatesType(ratesType);
		// ratesData.setBookingAmount(20000);
		// System.out.println(RatesPABFactory.getPABInstance().addRatesData(ratesData));
		// System.out.println(RatesPABFactory.getPABInstance().updateRatesData(ratesData));
		// System.out.println(RatesPABFactory.getPABInstance().deleteRatesData("1"));
		// System.out.println(RatesPABFactory.getPABInstance().getRatesData("2"));
		// System.out.println(RatesPABFactory.getPABInstance().getRatesList("2"));
	}

}
