package com.banquetin.pab.eventpab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IEventPAB{

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addEventData(EventData eventData);
	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updateEventData(EventData eventData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<EventData> getEventList();

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public EventData getEventData(String eventTypeId);

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deleteEventrData(String eventId);
	
	public List<ImageData> getAllImageFromEvent();

}
