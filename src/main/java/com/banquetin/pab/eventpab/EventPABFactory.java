package com.banquetin.pab.eventpab;

import com.banquetin.pab.BasePABFactory;

public class EventPABFactory extends BasePABFactory {
	public static IEventPAB getPABInstance() {
		return EventPAB.getInstance();
	}
}
