package com.banquetin.pab.eventpab;

import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.eventdab.EventDABFactory;
import com.banquetin.dab.eventdab.IEventDAB;
import com.banquetin.dab.userdab.IUserDAB;
import com.banquetin.dab.userdab.UserDABFactory;
import com.banquetin.dataobjects.EventData;
import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.pab.BasePAB;

public class EventPAB extends BasePAB implements IEventPAB {

	// Singleton Instance of the class
	private static IEventPAB INSTANCE = new EventPAB();
	private static IEventDAB DABINSTANCE =  EventDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(EventPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private EventPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IEventPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addEventData(EventData eventData) {

		logger.debug("Entry");

		//TODO:Implement Logic
		String returnData = DABINSTANCE.addEventData(eventData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updateEventData(EventData eventData) {

		logger.debug("Entry");

		//TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateEventData(eventData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<EventData> getEventList() {

		logger.debug("Entry");

		//TODO:Implement Logic
		List<EventData> returnData = DABINSTANCE.getEventList();
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public EventData getEventData(String eventTypeId) {

		logger.debug("Entry");

		//TODO:Implement Logic
		EventData returnData = DABINSTANCE.getEventData(eventTypeId);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deleteEventData(String eventTypeId) {

		logger.debug("Entry");

		//TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteEventData(eventTypeId);
		logger.debug("Exit");
		return returnData;
	}
	public List<ImageData> getAllImageFromEvent() {
		
		logger.debug("Entry");

		//TODO:Implement Logic
		List<ImageData> returnData = DABINSTANCE.getAllImageFromEvent();
		return returnData;
		
	}

	@Override
	public boolean deleteEventrData(String eventId) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
