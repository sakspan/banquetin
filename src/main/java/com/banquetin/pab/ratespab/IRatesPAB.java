package com.banquetin.pab.ratespab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IRatesPAB{

	/**
	 * This method is used to Create a new Rates
	 *
	 * @return
	 */
	public String addRatesData(RatesData ratesData);

	/**
	 * This method is used to Update the Rates details
	 *
	 * @return
	 */
	public boolean updateRatesData(RatesData ratesData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<RatesData> getRatesList(String venueId);

	/**
	 * This method is used to Get details for a particular Rates
	 *
	 * @return
	 */
	public RatesData getRatesData(String ratesId);

	/**
	 * This method is used to Delete the existing Rates details
	 *
	 * @return
	 */
	public boolean deleteRatesData(String ratesId);

	public List<SlotTypeData> getSlotTypes();

}
