package com.banquetin.pab.ratespab;

import com.banquetin.pab.BasePABFactory;

public class RatesPABFactory extends BasePABFactory {

	public static IRatesPAB getPABInstance() {
		return RatesPAB.getInstance();
	}

}
