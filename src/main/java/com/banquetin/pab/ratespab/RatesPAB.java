package com.banquetin.pab.ratespab;

import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.genericdab.GenericDABFactory;
import com.banquetin.dab.ratesdab.IRatesDAB;
import com.banquetin.dab.ratesdab.RatesDABFactory;
import com.banquetin.dataobjects.RatesData;
import com.banquetin.dataobjects.SlotTypeData;
import com.banquetin.pab.BasePAB;
import com.banquetin.util.CacheUtil;

public class RatesPAB extends BasePAB implements IRatesPAB {

	// Singleton Instance of the class
	private static IRatesPAB INSTANCE = new RatesPAB();
	private static IRatesDAB DABINSTANCE = RatesDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(RatesPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private RatesPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IRatesPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Rates
	 *
	 * @return
	 */
	public String addRatesData(RatesData ratesData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.addRatesData(ratesData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.RATES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the Rates details
	 *
	 * @return
	 */
	public boolean updateRatesData(RatesData ratesData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateRatesData(ratesData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.RATES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.RATES_DATA, ratesData.getRatesId());
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<RatesData> getRatesList(String venueId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.RATES_LIST, venueId);

		List<RatesData> returnData = (List<RatesData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getRatesList(venueId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular Rates
	 *
	 * @return
	 */
	public RatesData getRatesData(String ratesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.RATES_DATA, ratesId);

		RatesData returnData = (RatesData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getRatesData(ratesId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing Rates details
	 *
	 * @return
	 */
	public boolean deleteRatesData(String ratesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteRatesData(ratesId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.RATES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.RATES_DATA, ratesId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	public List<SlotTypeData> getSlotTypes() {
		logger.debug("Entry");
		List<SlotTypeData> slotTypeList = GenericDABFactory.getDABInstance().getSlotTypes();
		logger.debug("Exit");
		return slotTypeList;
	}

}
