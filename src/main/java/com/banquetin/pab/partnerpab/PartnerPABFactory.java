package com.banquetin.pab.partnerpab;

import com.banquetin.pab.BasePABFactory;

public class PartnerPABFactory extends BasePABFactory {
	public static IPartnerPAB getPABInstance() {
		return PartnerPAB.getInstance();
	}
}
