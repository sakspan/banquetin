package com.banquetin.pab.partnerpab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IPartnerPAB{

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addPartnerData(PartnerData partnerData);

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updatePartnerData(PartnerData partnerData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PartnerData> getPartnerList();

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public PartnerData getPartnerData(String partnerId);

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deletePartnerData(String partnerId);

}
