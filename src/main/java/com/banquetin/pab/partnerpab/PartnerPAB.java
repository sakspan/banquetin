package com.banquetin.pab.partnerpab;

import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.partnerdab.IPartnerDAB;
import com.banquetin.dab.partnerdab.PartnerDABFactory;
import com.banquetin.dab.userdab.IUserDAB;
import com.banquetin.dab.userdab.UserDABFactory;
import com.banquetin.dataobjects.PartnerData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.pab.BasePAB;

public class PartnerPAB extends BasePAB implements IPartnerPAB {

	// Singleton Instance of the class
	private static IPartnerPAB INSTANCE = new PartnerPAB();
	private static IPartnerDAB DABINSTANCE =  PartnerDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(PartnerPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private PartnerPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IPartnerPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addPartnerData(PartnerData partnerData) {

		logger.debug("Entry");

		//TODO:Implement Logic
		String returnData = DABINSTANCE.addPartnerData(partnerData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updatePartnerData(PartnerData partnerData) {

		logger.debug("Entry");

		//TODO:Implement Logic
		boolean returnData = DABINSTANCE.updatePartnerData(partnerData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PartnerData> getPartnerList() {

		logger.debug("Entry");

		//TODO:Implement Logic
		List<PartnerData> returnData = DABINSTANCE.getPartnerList();
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public PartnerData getPartnerData(String partnerId) {

		logger.debug("Entry");

		//TODO:Implement Logic
		PartnerData returnData = DABINSTANCE.getPartnerData(partnerId);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deletePartnerData(String partnerId) {

		logger.debug("Entry");

		//TODO:Implement Logic
		boolean returnData = DABINSTANCE.deletePartnerData(partnerId);
		logger.debug("Exit");
		return returnData;
	}


	
}
