package com.banquetin.pab.amenitiespab;

import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.amenitiesdab.AmenitiesDABFactory;
import com.banquetin.dab.amenitiesdab.IAmenitiesDAB;
import com.banquetin.dataobjects.AmenitiesData;
import com.banquetin.pab.BasePAB;
import com.banquetin.util.CacheUtil;

public class AmenitiesPAB extends BasePAB implements IAmenitiesPAB {

	// Singleton Instance of the class
	private static IAmenitiesPAB INSTANCE = new AmenitiesPAB();
	private static IAmenitiesDAB DABINSTANCE = AmenitiesDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(AmenitiesPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private AmenitiesPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IAmenitiesPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Amenities
	 *
	 * @return
	 */
	public String addAmenitiesData(AmenitiesData amenitiesData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.addAmenitiesData(amenitiesData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the Amenities details
	 *
	 * @return
	 */
	public boolean updateAmenitiesData(AmenitiesData amenitiesData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateAmenitiesData(amenitiesData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_DATA, amenitiesData.getAmenitiesId());
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesList() {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_LIST);

		List<AmenitiesData> returnData = (List<AmenitiesData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getAmenitiesList();
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular Amenities
	 *
	 * @return
	 */
	public AmenitiesData getAmenitiesData(String amenitiesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_DATA, amenitiesId);

		AmenitiesData returnData = (AmenitiesData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getAmenitiesData(amenitiesId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing Amenities details
	 *
	 * @return
	 */
	public boolean deleteAmenitiesData(String amenitiesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteAmenitiesData(amenitiesId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.AMENITIES_DATA, amenitiesId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

}
