package com.banquetin.pab.amenitiespab;

import com.banquetin.pab.BasePABFactory;

public class AmenitiesPABFactory extends BasePABFactory {

	public static IAmenitiesPAB getPABInstance() {
		return AmenitiesPAB.getInstance();
	}

}
