package com.banquetin.pab.blockdatespab;

import com.banquetin.pab.BasePABFactory;

public class BlockDatesPABFactory extends BasePABFactory {

	public static IBlockDatesPAB getPABInstance() {
		return BlockDatesPAB.getInstance();
	}

}
