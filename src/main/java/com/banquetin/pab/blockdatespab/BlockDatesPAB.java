package com.banquetin.pab.blockdatespab;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.blockdatesdab.BlockDatesDABFactory;
import com.banquetin.dab.blockdatesdab.IBlockDatesDAB;
import com.banquetin.dataobjects.BlockDatesData;
import com.banquetin.pab.BasePAB;
import com.banquetin.util.CacheUtil;

public class BlockDatesPAB extends BasePAB implements IBlockDatesPAB {

	// Singleton Instance of the class
	private static IBlockDatesPAB INSTANCE = new BlockDatesPAB();
	private static IBlockDatesDAB DABINSTANCE = BlockDatesDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(BlockDatesPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private BlockDatesPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IBlockDatesPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new BlockDates
	 *
	 * @return
	 */
	public String addBlockDatesData(BlockDatesData blockDatesData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.addBlockDatesData(blockDatesData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the BlockDates details
	 *
	 * @return
	 */
	public boolean updateBlockDatesData(BlockDatesData blockDatesData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateBlockDatesData(blockDatesData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_DATA, blockDatesData.getBlockDatesId());
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<BlockDatesData> getBlockDatesList(String venueId, Date startDate, Date endDate) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_LIST);

		List<BlockDatesData> returnData = (List<BlockDatesData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getBlockDatesList(venueId, startDate, endDate);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular BlockDates
	 *
	 * @return
	 */
	public BlockDatesData getBlockDatesData(String blockDatesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_DATA, blockDatesId);

		BlockDatesData returnData = (BlockDatesData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getBlockDatesData(blockDatesId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing BlockDates details
	 *
	 * @return
	 */
	public boolean deleteBlockDatesData(String blockDatesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteBlockDatesData(blockDatesId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATES_DATA, blockDatesId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	public List<BlockDatesData> getBlockDatesForProperty(String propertyId, Date startDate, Date endDate) {
		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.BLOCKDATESFORPROPERTY_LIST);

		List<BlockDatesData> returnData = (List<BlockDatesData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getBlockDatesForProperty(propertyId, startDate, endDate);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

}
