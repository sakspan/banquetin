package com.banquetin.pab.venuepab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.lookupdab.ILookupDAB;
import com.banquetin.dab.lookupdab.LookupDABFactory;
import com.banquetin.dab.venuedab.IVenueDAB;
import com.banquetin.dab.venuedab.VenueDABFactory;
import com.banquetin.dataobjects.SeatingTypeData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.BasePAB;
import com.banquetin.pab.propertypab.IPropertyPAB;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.util.CacheUtil;
import com.banquetin.util.Constants;

public class VenuePAB extends BasePAB implements IVenuePAB {

	// Singleton Instance of the class
	private static IVenuePAB INSTANCE = new VenuePAB();
	private static IVenueDAB DABINSTANCE = VenueDABFactory.getDABInstance();
	private static IPropertyPAB PROP_PABINSTANCE = PropertyPABFactory.getPABInstance();
	private static ILookupDAB LOOKUPDABINSTANCE = LookupDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(VenuePAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private VenuePAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IVenuePAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Venue
	 *
	 * @return
	 */
	public String addVenueData(VenueData venueData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String venueId = DABINSTANCE.addVenueData(venueData);

		List<SeatingTypeData> seatingTypeList = venueData.getSeatingType();

		for (int i = 0; i < seatingTypeList.size(); i++) {
			SeatingTypeData seatingTypeData = seatingTypeList.get(i);
			addSeatingTypeToVenue(venueId, seatingTypeData);
		}

		List<String> eventTypeList = new ArrayList(Arrays.asList(venueData.getEventTypeId()));

		for (int i = 0; i < eventTypeList.size(); i++) {
			addEventTypeToVenue(venueId, eventTypeList.get(i), "1");
		}

		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.VENUE_LIST);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return venueId;
	}

	/**
	 * This method is used to Update the Venue details
	 *
	 * @return
	 */
	public boolean updateVenueData(VenueData venueData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateVenueData(venueData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.VENUE_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.VENUE_DATA, venueData.getVenueId());
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VenueData> getVenueList() {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.VENUE_LIST);
		List<VenueData> returnData = (List<VenueData>) CacheUtil.getDataFromCache(cacheKey);
		if (returnData == null) {
			returnData = DABINSTANCE.getVenueList();
			for (VenueData venueDataItr : returnData) {
				venueDataItr.setPropertyData(PROP_PABINSTANCE.getPropertyData(venueDataItr.getPropertyId()));
				venueDataItr.setVenueImageList(DABINSTANCE.getAllImageFromVenue(venueDataItr.getVenueId()));
				venueDataItr.setSeatingType(DABINSTANCE.getSeatingTypeFromVenue(venueDataItr.getVenueId()));
				venueDataItr.setEventTypeList(DABINSTANCE.getEventTypeFromVenue(venueDataItr.getVenueId()));
			}
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular Venue
	 *
	 * @return
	 */
	public VenueData getVenueData(String venueId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.VENUE_DATA, venueId);

		VenueData returnData = (VenueData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getVenueData(venueId);
			returnData.setPropertyData(PROP_PABINSTANCE.getPropertyData(returnData.getPropertyId()));
			returnData.setVenueImageList(DABINSTANCE.getAllImageFromVenue(returnData.getVenueId()));
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing Venue details
	 *
	 * @return
	 */
	public boolean deleteVenueData(String venueId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteVenueData(venueId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.VENUE_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.VENUE_DATA, venueId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addEventTypeToVenue(String venueId, String eventTypeId, String sortOrder) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.addEventTypeToVenue(venueId, eventTypeId, sortOrder);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.EVENT_TYPE_LIST, venueId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteEventTypeFromVenue(String venueId, String eventTypeId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteEventTypeFromVenue(venueId, eventTypeId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.EVENT_TYPE_LIST, venueId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<StaticData> getEventTypeFromVenue(String venueId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.EVENT_TYPE_LIST, venueId);

		List<StaticData> returnData = (List<StaticData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getEventTypeFromVenue(venueId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addSeatingTypeToVenue(String venueId, SeatingTypeData seatingTypeData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.addSeatingTypeToVenue(venueId, seatingTypeData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.SEATING_TYPE_LIST, venueId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteSeatingTypeFromVenue(String venueId, String seatingTypeId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteSeatingTypeFromVenue(venueId, seatingTypeId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.SEATING_TYPE_LIST, venueId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<SeatingTypeData> getSeatingTypeFromVenue(String venueId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		List<SeatingTypeData> returnData = DABINSTANCE.getSeatingTypeFromVenue(venueId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.SEATING_TYPE_LIST, venueId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method returns all the type of venues.
	 * 
	 * @return
	 */
	public List<StaticData> getVenueType() {
		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.VENUE_TYPE);

		List<StaticData> returnData = (List<StaticData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = LOOKUPDABINSTANCE.getLookUpData(Constants.TABLE_VENUETYPE);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method returns all the type of seatings.
	 * 
	 * @return
	 */
	public List<StaticData> getSeatingType() {
		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.SEATING_TYPE);

		List<StaticData> returnData = (List<StaticData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = LOOKUPDABINSTANCE.getLookUpData(Constants.TABLE_SEATINGTYPE);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public List<VenueData> getFilteredVenueList(String startDate, String location, String pax) {
		logger.debug("Entry");
		List<VenueData> returnData = new ArrayList<VenueData>();
		returnData = DABINSTANCE.getFilteredVenueList(startDate, location, pax);
		logger.debug("Exit");
		return returnData;
	}

}
