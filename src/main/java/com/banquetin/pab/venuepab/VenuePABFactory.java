package com.banquetin.pab.venuepab;

import com.banquetin.pab.BasePABFactory;

public class VenuePABFactory extends BasePABFactory {

	public static IVenuePAB getPABInstance() {
		return VenuePAB.getInstance();
	}

}
