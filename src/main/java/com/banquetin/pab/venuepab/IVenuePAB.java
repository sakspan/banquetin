package com.banquetin.pab.venuepab;

import java.util.List;

import com.banquetin.dataobjects.SeatingTypeData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.VenueData;

public interface IVenuePAB {

	/**
	 * This method is used to Create a new Venue
	 *
	 * @return
	 */
	public String addVenueData(VenueData venueData);

	/**
	 * This method is used to Update the Venue details
	 *
	 * @return
	 */
	public boolean updateVenueData(VenueData venueData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VenueData> getVenueList();

	/**
	 * This method is used to Get details for a particular Venue
	 *
	 * @return
	 */
	public VenueData getVenueData(String venueId);

	/**
	 * This method is used to Delete the existing Venue details
	 *
	 * @return
	 */
	public boolean deleteVenueData(String venueId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addEventTypeToVenue(String venueId, String eventTypeId, String sortOrder);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteEventTypeFromVenue(String venueId, String eventTypeId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<StaticData> getEventTypeFromVenue(String venueId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addSeatingTypeToVenue(String venueId, SeatingTypeData seatingTypeData);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteSeatingTypeFromVenue(String venueId, String seatingTypeId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<SeatingTypeData> getSeatingTypeFromVenue(String venueId);
	
	/**
	 * This method returns all the type of venues.
	 * @return
	 */
	public List<StaticData> getVenueType();
	
	/**
	 * This method returns all the type of seatings.
	 * @return
	 */
	public List<StaticData> getSeatingType();

	public List<VenueData> getFilteredVenueList(String startDate, String location, String pax);

}
