package com.banquetin.pab.bookingpab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IBookingPAB {

	/**
	 * This method is used to Create a new Booking
	 *
	 * @return
	 */
	public String addBookingData(BookingData bookingData);

	/**
	 * This method is used to Update the Booking details
	 *
	 * @return
	 */
	public String updateBookingData(BookingData bookingData, UserData userData, AddressData addressData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<BookingData> getBookingList(String venueId, String userId, Date startDate, Date endDate);

	/**
	 * This method is used to Get details for a particular Booking
	 *
	 * @return
	 */
	public BookingData getBookingData(String bookingId);

	/**
	 * This method is used to Delete the existing Booking details
	 *
	 * @return
	 */
	public boolean deleteBookingData(String bookingId);

	public List<BookingData> getBookingsForProperty(String propertyId, Date startDate, Date endDate);
	
	public List<BookingData> getBlockedDatesList(String venueId);

}
