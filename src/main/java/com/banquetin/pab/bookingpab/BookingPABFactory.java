package com.banquetin.pab.bookingpab;

import com.banquetin.pab.BasePABFactory;

public class BookingPABFactory extends BasePABFactory {

	public static IBookingPAB getPABInstance() {
		return BookingPAB.getInstance();
	}

}
