package com.banquetin.pab.bookingpab;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.banquetin.dab.bookingdab.BookingDABFactory;
import com.banquetin.dab.bookingdab.IBookingDAB;
import com.banquetin.dab.genericdab.GenericDABFactory;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.dataobjects.SlotTypeData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.pab.BasePAB;

public class BookingPAB extends BasePAB implements IBookingPAB {

	// Singleton Instance of the class
	private static IBookingPAB INSTANCE = new BookingPAB();
	private static IBookingDAB DABINSTANCE = BookingDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(BookingPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private BookingPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IBookingPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Booking
	 *
	 * @return
	 */
	public String addBookingData(BookingData bookingData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.addBookingData(bookingData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the Booking details
	 *
	 * @return
	 */
	public String updateBookingData(BookingData bookingData, UserData userData, AddressData addressData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.updateBookingData(bookingData,userData,addressData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<BookingData> getBookingList(String venueId, String userId, Date startDate, Date endDate) {

		logger.debug("Entry");

		// TODO:Implement Logic
		List<BookingData> returnData = DABINSTANCE.getBookingList(venueId, userId, startDate, endDate);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular Booking
	 *
	 * @return
	 */
	public BookingData getBookingData(String bookingId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		BookingData returnData = DABINSTANCE.getBookingData(bookingId);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing Booking details
	 *
	 * @return
	 */
	public boolean deleteBookingData(String bookingId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteBookingData(bookingId);
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public List<BookingData> getBookingsForProperty(String propertyId, Date startDate, Date endDate) {
		logger.debug("Entry");

		// TODO:Implement Logic
		List<BookingData> returnData = DABINSTANCE.getBookingsForProperty(propertyId, startDate, endDate);

		logger.debug("Exit");
		return returnData;
	}
	
	@Override
	public List<BookingData> getBlockedDatesList(String venueId) {
		logger.debug("Entry");

		List<BookingData> returnData = DABINSTANCE.getBlockedDatesList(venueId);

		logger.debug("Exit");
		return returnData;
	}


}
