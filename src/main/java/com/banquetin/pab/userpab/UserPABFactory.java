package com.banquetin.pab.userpab;

import com.banquetin.pab.BasePABFactory;

public class UserPABFactory extends BasePABFactory {
	public static IUserPAB getPABInstance() {
		return UserPAB.getInstance();
	}
}
