package com.banquetin.pab.userpab;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.userdab.IUserDAB;
import com.banquetin.dab.userdab.UserDABFactory;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.CustomerData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.pab.BasePAB;
import com.banquetin.pab.propertypab.PropertyPABFactory;

public class UserPAB extends BasePAB implements IUserPAB {

	// Singleton Instance of the class
	private static IUserPAB INSTANCE = new UserPAB();
	private static IUserDAB DABINSTANCE = UserDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(UserPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private UserPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IUserPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to create a new customer record
	 * 
	 * @return
	 */
	public boolean createNewCustomer(CustomerData customerData) {

		logger.debug("Entry");
		boolean isSuccess = false;

		boolean customerExist = UserDABFactory.getDABInstance().checkIfCustomerExist(customerData);

		if (!customerExist)
			isSuccess = UserDABFactory.getDABInstance().createNewCustomer(customerData);

		logger.debug("Exit");
		return isSuccess;
	}


	/**
	 * This method is used to update existing customer record
	 * 
	 * @return
	 */
	public boolean updateCustomer(CustomerData customerData) {

		logger.debug("Entry");
		boolean isSuccess = false;
		isSuccess = UserDABFactory.getDABInstance().updateCustomer(customerData);
		logger.debug("Exit");
		return isSuccess;
	}

	/**
	 * This method is used to validate the login details of the customer
	 * 
	 * @return
	 */
	public boolean customerLogin(CustomerData customerData) {

		logger.debug("Entry");
		boolean isSuccess = false;
		isSuccess = UserDABFactory.getDABInstance().customerLogin(customerData);
		logger.debug("Exit");
		return isSuccess;
	}

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addUserData(UserData userData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.addUserData(userData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updateUserData(UserData userData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateUserData(userData);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<UserData> getUserList() {

		logger.debug("Entry");

		// TODO:Implement Logic
		List<UserData> returnData = DABINSTANCE.getUserList();
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public UserData getUserData(String userId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		UserData returnData = DABINSTANCE.getUserData(userId);
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deleteUserData(String userId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteUserData(userId);
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public List<UserData> getFeedBackList() {
		logger.debug("Entry");

		// TODO:Implement Logic
		List<UserData> returnData = DABINSTANCE.getFeedBackList();
		logger.debug("Exit");
		return returnData;
	}

	public String userLogin(UserData userData) {
		logger.debug("Entry");

		String returnData = DABINSTANCE.userLogin(userData);
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public boolean addPropertyToUser(String userId, String propertyId) {
		logger.debug("Entry");
		boolean returnData = DABINSTANCE.addPropertyToUser(userId, propertyId);
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public boolean updateUserProperty(String userId, String propertyId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public PropertyData getUserPropertyData(String userId, String propertyId) {
		logger.debug("Entry");

		// TODO:Implement Logic
		PropertyData returnData = DABINSTANCE.getUserPropertyData(userId, propertyId);

		logger.debug("Exit");
		return returnData;
	}

	@Override
	public List<PropertyData> getUserPropertyList(String userId) {
		logger.debug("Entry");

		// TODO:Implement Logic
		List<PropertyData> returnData = DABINSTANCE.getUserPropertyList(userId);
		List<PropertyData> propertyDataList = new ArrayList<PropertyData>();

		for (int i = 0; i < returnData.size(); i++) {

			PropertyData inputPropertyData = returnData.get(i);
			PropertyData outputPropertyData = PropertyPABFactory.getPABInstance()
					.getPropertyData(inputPropertyData.getPropertyId());
			propertyDataList.add(outputPropertyData);
		}

		logger.debug("Exit");
		return propertyDataList;
	}

	@Override
	public boolean deletePropertyFromUser(String userId, String propertyId) {
		logger.debug("Entry");

		boolean returnData = DABINSTANCE.deletePropertyFromUser(userId, propertyId);

		logger.debug("Exit");
		return returnData;
	}
}
