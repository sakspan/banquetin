package com.banquetin.pab.vendorpab;

import com.banquetin.pab.BasePABFactory;

public class VendorPABFactory extends BasePABFactory {

	public static IVendorPAB getPABInstance() {
		return VendorPAB.getInstance();
	}

}
