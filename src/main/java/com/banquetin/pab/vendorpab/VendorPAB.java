package com.banquetin.pab.vendorpab;

import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.vendordab.IVendorDAB;
import com.banquetin.dab.vendordab.VendorDABFactory;
import com.banquetin.dataobjects.VendorData;
import com.banquetin.pab.BasePAB;
import com.banquetin.util.CacheUtil;

public class VendorPAB extends BasePAB implements IVendorPAB {

	// Singleton Instance of the class
	private static IVendorPAB INSTANCE = new VendorPAB();
	private static IVendorDAB DABINSTANCE = VendorDABFactory.getDABInstance();

	private static final Logger logger = Logger.getLogger(VendorPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private VendorPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IVendorPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Vendor
	 *
	 * @return
	 */
	public String addVendorData(VendorData vendorData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String returnData = DABINSTANCE.addVendorData(vendorData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_LIST);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the Vendor details
	 *
	 * @return
	 */
	public boolean updateVendorData(VendorData vendorData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updateVendorData(vendorData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_DATA, vendorData.getVendorId());
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VendorData> getVendorList() {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_LIST);

		List<VendorData> returnData = (List<VendorData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getVendorList();
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular Vendor
	 *
	 * @return
	 */
	public VendorData getVendorData(String vendorId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_DATA, vendorId);

		VendorData returnData = (VendorData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getVendorData(vendorId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Delete the existing Vendor details
	 *
	 * @return
	 */
	public boolean deleteVendorData(String vendorId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteVendorData(vendorId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.VENDOR_DATA, vendorId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

}
