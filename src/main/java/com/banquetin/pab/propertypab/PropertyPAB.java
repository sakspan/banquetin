package com.banquetin.pab.propertypab;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.amenitiesdab.AmenitiesDABFactory;
import com.banquetin.dab.amenitiesdab.IAmenitiesDAB;
import com.banquetin.dab.genericdab.GenericDABFactory;
import com.banquetin.dab.genericdab.IGenericDAB;
import com.banquetin.dab.lookupdab.ILookupDAB;
import com.banquetin.dab.lookupdab.LookupDABFactory;
import com.banquetin.dab.propertydab.IPropertyDAB;
import com.banquetin.dab.propertydab.PropertyDABFactory;
import com.banquetin.dab.venuedab.IVenueDAB;
import com.banquetin.dab.venuedab.VenueDABFactory;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.AmenitiesData;
import com.banquetin.dataobjects.EmailData;
import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.PhoneData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.PropertyInfoData;
import com.banquetin.dataobjects.SeatingTypeData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.VendorData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.BasePAB;
import com.banquetin.pab.venuepab.IVenuePAB;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.CacheUtil;
import com.banquetin.util.Constants;

public class PropertyPAB extends BasePAB implements IPropertyPAB {

	// Singleton Instance of the class
	private static IPropertyPAB INSTANCE = new PropertyPAB();
	private static IPropertyDAB DABINSTANCE = PropertyDABFactory.getDABInstance();
	private static IAmenitiesDAB ADABINSTANCE = AmenitiesDABFactory.getDABInstance();
	private static ILookupDAB LOOKUPDABINSTANCE = LookupDABFactory.getDABInstance();
	private static IGenericDAB GENERICDABINSTANCE = GenericDABFactory.getDABInstance();
	private static IVenueDAB VENUEDABINSTANCE = VenueDABFactory.getDABInstance();
	private static IVenuePAB VENUEPABINSTANCE = VenuePABFactory.getPABInstance();

	private static final Logger logger = Logger.getLogger(PropertyPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private PropertyPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IPropertyPAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Property
	 *
	 * @return
	 */
	public String addPropertyData(PropertyData propertyData) {

		logger.debug("Entry");

		// Add basic property details
		String propertyId = DABINSTANCE.addPropertyData(propertyData);

		// Add address to property
		List<AddressData> addressList = propertyData.getAddressList();

		for (int i = 0; i < addressList.size(); i++) {
			AddressData addressData = (AddressData) addressList.get(i);
			String addressId = GENERICDABINSTANCE.addAddressData(addressData);

			List<PhoneData> phoneList = addressData.getPhoneList();

			if (phoneList != null) {
				for (int j = 0; j < phoneList.size(); j++) {
					PhoneData phoneData = (PhoneData) phoneList.get(j);
					String phoneId = GENERICDABINSTANCE.addPhoneData(phoneData);
					GENERICDABINSTANCE.addPhoneToAddress(addressId, phoneId);
				}
			}

			DABINSTANCE.addAddressToProperty(propertyId, addressId);
		}

		List<EmailData> emailList = propertyData.getEmailList();

		for (int i = 0; i < emailList.size(); i++) {
			EmailData emailData = (EmailData) emailList.get(i);
			String emailId = GENERICDABINSTANCE.addEmailData(emailData);
			GENERICDABINSTANCE.addEmailToProperty(emailId, propertyId);
		}

		// Add Venue to property
		List<VenueData> venueList = propertyData.getVenueList();

		for (int i = 0; i < venueList.size(); i++) {
			VenueData venueData = (VenueData) venueList.get(i);
			String venueId = VENUEPABINSTANCE.addVenueData(venueData);
			DABINSTANCE.addVenueToProperty(propertyId, venueId, String.valueOf(i));
		}

		// Add amenities to property
		List<String> amenitiesList = propertyData.getAmenitiesIdList();

		for (int i = 0; i < amenitiesList.size(); i++) {
			String amenitiesId = (String) amenitiesList.get(i);
			DABINSTANCE.addAmenitiesToProperty(propertyId, amenitiesId);
		}

		// Add images to property
		List<String> imageList = propertyData.getImageIdList();
		for (int i = 0; i < imageList.size(); i++) {
			String imageId = (String) imageList.get(i);
			DABINSTANCE.addImageToProperty(propertyId, imageId, Constants.IS_PRIMARY_YES);
		}

		List<PropertyInfoData> propertyInfoDatas = propertyData.getPropertyInfo();
		for (int i = 0; i < propertyInfoDatas.size(); i++) {
			PropertyInfoData propertyInfoData = (PropertyInfoData) propertyInfoDatas.get(i);
			propertyInfoData.setPropertyId(propertyId);
			propertyInfoData.setIcon("icon-term-condition");
			DABINSTANCE.addInfoToProperty(propertyInfoData);
		}

		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_LIST);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return propertyId;
	}

	/**
	 * This method is used to Update the Property details
	 *
	 * @return
	 */
	public boolean updatePropertyData(PropertyData propertyData) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.updatePropertyData(propertyData);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_DATA, propertyData.getPropertyId());
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Update the Property details
	 *
	 * @return
	 */
	public boolean updatePropertyContact(PropertyData propertyData) {

		logger.debug("Entry");

		String propertyId = propertyData.getPropertyId();
		DABINSTANCE.deleteAllAddressFromProperty(propertyId);
		DABINSTANCE.deleteAllEmailFromProperty(propertyId);
		// Add address to property
		List<AddressData> addressList = propertyData.getAddressList();

		for (int i = 0; i < addressList.size(); i++) {
			AddressData addressData = (AddressData) addressList.get(i);
			String addressId = GENERICDABINSTANCE.addAddressData(addressData);

			List<PhoneData> phoneList = addressData.getPhoneList();

			if (phoneList != null) {
				for (int j = 0; j < phoneList.size(); j++) {
					PhoneData phoneData = (PhoneData) phoneList.get(j);
					String phoneId = GENERICDABINSTANCE.addPhoneData(phoneData);
					GENERICDABINSTANCE.addPhoneToAddress(addressId, phoneId);
				}
			}

			DABINSTANCE.addAddressToProperty(propertyId, addressId);
		}

		List<EmailData> emailList = propertyData.getEmailList();

		for (int i = 0; i < emailList.size(); i++) {
			EmailData emailData = (EmailData) emailList.get(i);
			String emailId = GENERICDABINSTANCE.addEmailData(emailData);
			GENERICDABINSTANCE.addEmailToProperty(emailId, propertyId);
		}

		logger.debug("Exit");
		return true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PropertyData> getPropertyList() {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_LIST);

		List<PropertyData> returnData = (List<PropertyData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getPropertyList();
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to Get details for a particular Property
	 *
	 * @return
	 */
	public PropertyData getPropertyData(String propertyId) {

		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_DATA, propertyId);

		PropertyData returnData = (PropertyData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getPropertyData(propertyId);
			returnData.setAddressList(DABINSTANCE.getAddressFromProperty(propertyId));
			returnData.setAmenitiesList(mergeAmenetiesData(ADABINSTANCE.getAmenitiesList(),
					DABINSTANCE.getAmenitiesFromProperty(propertyId)));

			List<VenueData> venueDataList = DABINSTANCE.getVenueFromProperty(propertyId);

			for (int i = 0; i < venueDataList.size(); i++) {
				VenueData venueData = (VenueData) venueDataList.get(i);
				List<SeatingTypeData> seatingTypeList = VenueDABFactory.getDABInstance()
						.getSeatingTypeFromVenue(venueData.getVenueId());
				venueData.setSeatingType(seatingTypeList);
				List<StaticData> eventTypeList = VenueDABFactory.getDABInstance()
						.getEventTypeFromVenue(venueData.getVenueId());
				venueData.setEventTypeList(eventTypeList);
			}

			returnData.setVenueList(venueDataList);

			returnData.setPropertyInfo(DABINSTANCE.getAllInfoFromProperty(propertyId));
			returnData.setPropertyImageList(DABINSTANCE.getAllImageFromProperty(propertyId));
			returnData.setEmailList(DABINSTANCE.getEmailForProperty(propertyId));
			returnData.setPhoneList(DABINSTANCE.getPhoneNumbersForProperty(propertyId));
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * 
	 */
	public List<AmenitiesData> mergeAmenetiesData(List<AmenitiesData> completeData, List<AmenitiesData> propertyData) {
		List<AmenitiesData> finalList = new ArrayList<AmenitiesData>();
		for (Iterator<AmenitiesData> iterator = completeData.iterator(); iterator.hasNext();) {
			AmenitiesData amenitiesData = (AmenitiesData) iterator.next();
			for (Iterator<AmenitiesData> itr = propertyData.iterator(); itr.hasNext();) {
				AmenitiesData amenitiesData1 = (AmenitiesData) itr.next();
				if (amenitiesData.getAmenitiesId().contentEquals(amenitiesData1.getAmenitiesId())) {
					amenitiesData.setEnabled(true);
				}
			}
			finalList.add(amenitiesData);
		}
		return finalList;
	}

	/**
	 * This method is used to Delete the existing Property details
	 *
	 * @return
	 */
	public boolean deletePropertyData(String propertyId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deletePropertyData(propertyId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_LIST);
		CacheUtil.putDataToCache(cacheKey, null);
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_DATA, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVenueToProperty(String propertyId, String venueId, String sortOrder) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.addVenueToProperty(propertyId, venueId, sortOrder);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_VENUE, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVenueFromProperty(String propertyId, String venueId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteVenueFromProperty(propertyId, venueId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_VENUE, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VenueData> getVenueFromProperty(String propertyId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_VENUE, propertyId);

		List<VenueData> returnData = (List<VenueData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getVenueFromProperty(propertyId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addAmenitiesToProperty(String propertyId, String amenitiesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.addAmenitiesToProperty(propertyId, amenitiesId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_AMENITIES, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAmenitiesFromProperty(String propertyId, String amenitiesId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteAmenitiesFromProperty(propertyId, amenitiesId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_AMENITIES, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesFromProperty(String propertyId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_AMENITIES, propertyId);

		List<AmenitiesData> returnData = (List<AmenitiesData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getAmenitiesFromProperty(propertyId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVendorToProperty(String propertyId, String vendorId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.addVendorToProperty(propertyId, vendorId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_VENDOR, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to add address to a property data
	 *
	 * @return
	 */
	public boolean addAddressToProperty(String propertyId, AddressData addressData) {

		logger.debug("Entry");

		String addressId = GENERICDABINSTANCE.addAddressData(addressData);
		boolean returnData = DABINSTANCE.addAddressToProperty(propertyId, addressId);

		logger.debug("Exit");
		return returnData;
	}

	public boolean deleteAddressFromProperty(String propertyId, String addressId) {
		logger.debug("Entry");

		// TODO:Implement Cache Logic
		boolean returnData = DABINSTANCE.deleteAddressFromProperty(propertyId, addressId);

		logger.debug("Exit");
		return returnData;
	}

	public List<AddressData> getAddressFromProperty(String propertyId) {
		logger.debug("Entry");

		// TODO:Implement Cache Logic
		List<AddressData> returnData = (List<AddressData>) (DABINSTANCE.getAddressFromProperty(propertyId));

		logger.debug("Exit");
		return returnData;
	}

	public List<AddressData> getUniqueAddressFromProperty() {
		logger.debug("Entry");

		// TODO:Implement Cache Logic
		List<AddressData> returnData = (List<AddressData>) (DABINSTANCE.getUniqueAddressFromProperty());

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to add email to a property data
	 *
	 * @return
	 */
	public boolean addEmailToProperty(String propertyId, EmailData emailData) {

		logger.debug("Entry");

		String emailId = GENERICDABINSTANCE.addEmailData(emailData);
		boolean returnData = DABINSTANCE.addEmailToProperty(propertyId, emailId);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVendorFromProperty(String propertyId, String vendorId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		boolean returnData = DABINSTANCE.deleteVendorFromProperty(propertyId, vendorId);
		String cacheKey = null;
		cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_VENDOR, propertyId);
		CacheUtil.putDataToCache(cacheKey, null);

		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VendorData> getVendorFromProperty(String propertyId) {

		logger.debug("Entry");

		// TODO:Implement Logic
		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_VENDOR, propertyId);

		List<VendorData> returnData = (List<VendorData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getVendorFromProperty(propertyId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	/**
	 * This method returns all the type of properties.
	 * 
	 * @return
	 */
	public List<StaticData> getPropertyType() {
		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_TYPE);

		List<StaticData> returnData = (List<StaticData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = LOOKUPDABINSTANCE.getLookUpData(Constants.TABLE_PROPERTYTYPE);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public boolean addInfoToProperty(PropertyInfoData propertyInfo) {
		logger.debug("Entry");

		boolean returnData = DABINSTANCE.addInfoToProperty(propertyInfo);

		logger.debug("Exit");
		return returnData;
	}

	@Override
	public boolean deleteInfoFromProperty(String propertyInfoId) {
		logger.debug("Entry");

		boolean returnData = DABINSTANCE.deleteInfoFromProperty(propertyInfoId);

		logger.debug("Exit");
		return returnData;
	}

	@Override
	public PropertyInfoData getInfoFromProperty(String propertyInfoId) {
		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_INFO);
		PropertyInfoData returnData = (PropertyInfoData) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getInfoFromProperty(propertyInfoId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public List<PropertyInfoData> getAllInfoFromProperty(String propertyId) {
		logger.debug("Entry");

		String cacheKey = CacheUtil.getKey(CacheUtil.PROPERTY_INFO_LIST);
		List<PropertyInfoData> returnData = (List<PropertyInfoData>) CacheUtil.getDataFromCache(cacheKey);

		if (returnData == null) {
			returnData = DABINSTANCE.getAllInfoFromProperty(propertyId);
			CacheUtil.putDataToCache(cacheKey, returnData);
		}
		logger.debug("Exit");
		return returnData;
	}

	@Override
	public boolean updateInfoForProperty(String propertyInfoId, PropertyInfoData propertyInfo) {
		boolean returnData = DABINSTANCE.updateInfoForProperty(propertyInfoId, propertyInfo);
		return returnData;
	}

	@Override
	public boolean addImageToProperty(String propertyId, String imageId, int isPrimary) {
		boolean returnData = DABINSTANCE.addImageToProperty(propertyId, imageId, isPrimary);
		return returnData;
	}

	@Override
	public boolean updateImageOfProperty(String propertyId, String imageId, int isPrimary) {
		boolean returnData = DABINSTANCE.updateImageOfProperty(propertyId, imageId, isPrimary);
		return returnData;
	}

	@Override
	public boolean deleteImageFromProperty(String propertyId, String imageId, boolean deletePrimaryFlag) {
		boolean returnData = DABINSTANCE.deleteImageFromProperty(propertyId, imageId, deletePrimaryFlag);
		return returnData;
	}

	@Override
	public ImageData getImageFromProperty(String propertyId, String imageId) {
		// TODO: Cache
		ImageData returnData = DABINSTANCE.getImageFromProperty(propertyId, imageId);
		return returnData;
	}

	@Override
	public List<ImageData> getAllImageFromProperty(String propertyId) {
		// TODO: Cache
		List<ImageData> returnData = DABINSTANCE.getAllImageFromProperty(propertyId);
		return returnData;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllAmenitiesFromProperty(String propertyId) {

		boolean returnData = DABINSTANCE.deleteAllAmenitiesFromProperty(propertyId);
		return returnData;
	}

	/**
	 * This method is used to validate email exist
	 * 
	 * @return
	 */
	public boolean validateEmailExist(String emailId) {
		boolean emailExist = GenericDABFactory.getDABInstance().validateEmailExist(emailId);
		return emailExist;
	}

	/**
	 * This method is used to validate phone exist
	 * 
	 * @return
	 */
	public boolean validatePhoneExist(String phoneNum) {
		boolean phoneExist = GenericDABFactory.getDABInstance().validatePhoneExist(phoneNum);
		return phoneExist;
	}
}
