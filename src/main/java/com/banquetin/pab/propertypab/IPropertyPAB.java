package com.banquetin.pab.propertypab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IPropertyPAB {

	/**
	 * This method is used to Create a new Property
	 *
	 * @return
	 */
	public String addPropertyData(PropertyData propertyData);

	/**
	 * This method is used to Update the Property details
	 *
	 * @return
	 */
	public boolean updatePropertyData(PropertyData propertyData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PropertyData> getPropertyList();

	/**
	 * This method is used to Get details for a particular Property
	 *
	 * @return
	 */
	public PropertyData getPropertyData(String propertyId);

	public List<AmenitiesData> mergeAmenetiesData(List<AmenitiesData> completeData, List<AmenitiesData> propertyData);

	/**
	 * This method is used to Delete the existing Property details
	 *
	 * @return
	 */
	public boolean deletePropertyData(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVenueToProperty(String propertyId, String venueId, String sortOrder);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVenueFromProperty(String propertyId, String venueId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VenueData> getVenueFromProperty(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addAmenitiesToProperty(String propertyId, String amenitiesId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAmenitiesFromProperty(String propertyId, String amenitiesId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesFromProperty(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVendorToProperty(String propertyId, String vendorId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVendorFromProperty(String propertyId, String vendorId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VendorData> getVendorFromProperty(String propertyId);

	/**
	 * This method is used to add address to a propertydata
	 *
	 * @return
	 */
	public boolean addAddressToProperty(String propertyId, AddressData addressData);

	/**
	 * This method is used to add email to a property data
	 *
	 * @return
	 */
	public boolean addEmailToProperty(String propertyId, EmailData emailData);

	public boolean deleteAddressFromProperty(String propertyId, String addressId);

	public List<AddressData> getAddressFromProperty(String propertyId);

	public boolean addInfoToProperty(PropertyInfoData propertyInfo);

	public boolean updateInfoForProperty(String propertyInfoId, PropertyInfoData propertyInfo);

	public boolean deleteInfoFromProperty(String propertyInfoId);

	public PropertyInfoData getInfoFromProperty(String propertyInfoId);

	public List<PropertyInfoData> getAllInfoFromProperty(String propertyId);

	public boolean addImageToProperty(String propertyId, String imageId, int isPrimary);

	public boolean updateImageOfProperty(String propertyId, String imageId, int isPrimary);

	public boolean deleteImageFromProperty(String propertyId, String imageId, boolean deletePrimaryFlag);

	public ImageData getImageFromProperty(String propertyId, String imageId);

	public List<ImageData> getAllImageFromProperty(String propertyId);

	/**
	 * This method returns all the type of properties.
	 * 
	 * @return
	 */
	public List<StaticData> getPropertyType();

	public List<AddressData> getUniqueAddressFromProperty();

	/**
	 * This method is used to Update the Property details
	 *
	 * @return
	 */
	public boolean updatePropertyContact(PropertyData propertyData);
	
	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllAmenitiesFromProperty(String propertyId);
	
	/**
	 * This method is used to validate email exist
	 * 
	 * @return
	 */
	public boolean validateEmailExist(String emailId);
	
	/**
	 * This method is used to validate phone exist
	 * 
	 * @return
	 */
	public boolean validatePhoneExist(String phoneNum);
	
}
