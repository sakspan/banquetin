package com.banquetin.pab.propertypab;

import com.banquetin.pab.BasePABFactory;

public class PropertyPABFactory extends BasePABFactory {

	public static IPropertyPAB getPABInstance() {
		return PropertyPAB.getInstance();
	}

}
