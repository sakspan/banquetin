package com.banquetin.pab.lookuppab;

import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.lookupdab.LookupDABFactory;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.pab.BasePAB;
import com.banquetin.util.CacheUtil;
import com.banquetin.util.Constants;

public class LookUpPAB extends BasePAB implements ILookUpPAB {

	// Singleton Instance of the class
	private static ILookUpPAB INSTANCE = new LookUpPAB();

	private static final Logger logger = Logger.getLogger(LookUpPAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private LookUpPAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 * 
	 * @return
	 */
	static ILookUpPAB getInstance() {
		return INSTANCE;
	}

	private List<StaticData> getStaticList(String cacheKey, String lookup) {
		logger.debug("Entry");

		List<StaticData> staticList = (List<StaticData>) CacheUtil.getDataFromCache(cacheKey);

		if (staticList == null) {
			staticList = LookupDABFactory.getDABInstance().getLookUpData(lookup);
			CacheUtil.putDataToCache(cacheKey, staticList);
		}
		logger.debug("Exit");
		return staticList;

	}

	/**
	 * This method is used to get the different phone types
	 * 
	 * @return
	 */
	public List<StaticData> getPhoneList() {
		logger.debug("Entry");

		List<StaticData> phoneTypeList = getStaticList(CacheUtil.PHONE_LIST, Constants.PHONE_TYPE);

		logger.debug("Exit");
		return phoneTypeList;
	}

	/**
	 * This method is used to get the list of states
	 * 
	 * @return
	 */
	public List<StaticData> getPaymentTypeList() {
		logger.debug("Entry");
		// List<StaticData> stateList = LookupDABFactory.getDABInstance()
		// .getLookUpData(Constants.STATES);

		List<StaticData> stateList = getStaticList(CacheUtil.PAYMENT_TYPE_LIST, Constants.PAYMENTTYPE);
		logger.debug("Exit");
		return stateList;
	}

	/**
	 * This method is used to get the list of states
	 * 
	 * @return
	 */
	public List<StaticData> getStateList() {
		logger.debug("Entry");
		// List<StaticData> stateList = LookupDABFactory.getDABInstance()
		// .getLookUpData(Constants.STATES);

		List<StaticData> stateList = getStaticList(CacheUtil.STATE_LIST, Constants.STATES);
		logger.debug("Exit");
		return stateList;
	}


	/**
	 * This method is used to get the list of countries
	 * 
	 * @return
	 */
	public List<StaticData> getCountryList() {
		logger.debug("Entry");
		List<StaticData> countryList = getStaticList(CacheUtil.COUNTRY_LIST, Constants.COUNTRIES);
		// List<StaticData> countryList =
		// LookupDABFactory.getDABInstance().getLookUpData(Constants.COUNTRIES);
		logger.debug("Exit");
		return countryList;
	}

}
