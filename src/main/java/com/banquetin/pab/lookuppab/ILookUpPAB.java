package com.banquetin.pab.lookuppab;

import java.util.List;

import com.banquetin.dataobjects.StaticData;

public interface ILookUpPAB {

	/**
	 * This method is used to get the different phone types
	 *
	 * @return
	 */
	public List<StaticData> getPhoneList();

	/**
	 * This method is used to get the list of states
	 *
	 * @return
	 */
	public List<StaticData> getStateList();

	/**
	 * This method is used to get the list of countries
	 *
	 * @return
	 */
	public List<StaticData> getCountryList();

	/**
	 * This method is used to get the list of states
	 * 
	 * @return
	 */
	public List<StaticData> getPaymentTypeList();
}
