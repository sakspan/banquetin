package com.banquetin.pab.lookuppab;

import com.banquetin.pab.BasePABFactory;

public class LookUpPABFactory extends BasePABFactory {

	public static ILookUpPAB getPABInstance() {
		return LookUpPAB.getInstance();
	}

}
