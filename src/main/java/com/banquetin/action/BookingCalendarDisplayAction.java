package com.banquetin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class BookingCalendarDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(BookingCalendarDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
	
		logger.debug("Exit");

		return mapping.findForward("success");
	}

	@Override
	protected boolean isAdminLoginRequired() {
		return true;
	}

}