package com.banquetin.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.RatesData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.form.RatesForm;
import com.banquetin.pab.ratespab.RatesPABFactory;
import com.banquetin.util.CommonUtil;
import com.banquetin.util.Constants;

public class AddRatesSubmitAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(AddRatesSubmitAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");

		RatesForm inputForm = (RatesForm) form;
		if (Constants.OPERATION_DELETE.equals(inputForm.getOperationType())) {
			RatesPABFactory.getPABInstance().deleteRatesData(inputForm.getRatesId());
		} else {
			RatesData ratesData = convertFormToData(inputForm);
			if (Constants.OPERATION_ADD.equals(inputForm.getOperationType())) {
				if(validateFormRate(ratesData, request)) {
					RatesPABFactory.getPABInstance().addRatesData(ratesData);
				}
			}
			else
				RatesPABFactory.getPABInstance().updateRatesData(ratesData);
		}
		
		logger.debug("Exit");

		return mapping.findForward("success");
	}

	private RatesData convertFormToData(RatesForm ratesForm) {

		RatesData ratesData = new RatesData();

		ratesData.setRatesId(ratesForm.getRatesId());
		ratesData.setBookingAmount(ratesForm.getBookingAmount());
		ratesData.setEndDate(CommonUtil.convertStringToDate(ratesForm.getEndDate()));
		ratesData.setStartDate(CommonUtil.convertStringToDate(ratesForm.getStartDate()));
		ratesData.setRate(ratesForm.getRate());
		ratesData.setSlotTypeId(ratesForm.getSlotTypeId());
		ratesData.setVenueId(ratesForm.getVenueId());

		StaticData ratesType = new StaticData();
		ratesType.setId(ratesForm.getRateType());

		ratesData.setRatesType(ratesType);

		return ratesData;

	}
	
	private boolean validateFormRate(RatesData formData, HttpServletRequest request) {
		HttpSession session = request.getSession();
		List<RatesData> ratesList =  new ArrayList<RatesData>();
		ratesList = (List<RatesData>) session.getAttribute("rateList");
		
		boolean flag = false;
		for (RatesData ratesData : ratesList) {
			flag = false;
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd");  
			
			String startDate1 = formatter.format(ratesData.getStartDate()); 
			String endDate1 = formatter.format(ratesData.getEndDate()); 
			
		    String startDate2 = formatter.format(formData.getStartDate());  
			String endDate2 = formatter.format(formData.getEndDate());  
			
			if(startDate2.compareTo(startDate1)>=0 && startDate2.compareTo(endDate1)<=0) {
				flag=true;
				session.setAttribute("StartDateError", "Start Date already exists.");
			}
			if(endDate2.compareTo(startDate1)>=0 && endDate2.compareTo(endDate1)<=0) {
				flag=true;
				session.setAttribute("EndDateError", "End Date already exists.");
			}
			if(flag) break;
		}
		
		return !flag;	
	}

}