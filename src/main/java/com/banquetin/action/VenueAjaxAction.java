package com.banquetin.action;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.form.VenueForm;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.Constants;
import com.google.gson.Gson;

public class VenueAjaxAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(VenueAjaxAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//
		logger.debug("Entry");
		String operation = (String) request.getParameter("operationType");
		VenueData venueData = null;

		if (Constants.OPERATION_DELETE.equals(operation)) {
			String venueId = (String) request.getParameter("venueId");
			venueData = new VenueData();
			venueData.setVenueId(venueId);
		} else {
			String jsonVenueString = (String) request.getParameter("venueData");
			Gson gson = new Gson();

			System.out.println(jsonVenueString);
			VenueData[] arr = gson.fromJson(jsonVenueString, VenueData[].class);

			venueData = arr[0];
		}
		System.out.println("VenueData : " + venueData);

		boolean result = false;

		List<PropertyData> propertyList = (List<PropertyData>) request.getSession()
				.getAttribute(Constants.SESSION_PROPERTY_LIST);

		PropertyData propertyData = (PropertyData) propertyList.get(0);

		if (Constants.OPERATION_DELETE.equals(operation) || Constants.OPERATION_UPDATE.equals(operation)) {
			result = VenuePABFactory.getPABInstance().deleteVenueData(venueData.getVenueId());
			if (result) {
				PropertyPABFactory.getPABInstance().deleteVenueFromProperty(propertyData.getPropertyId(),
						venueData.getVenueId());
			}
		}

		if (Constants.OPERATION_ADD.equals(operation) || Constants.OPERATION_UPDATE.equals(operation)) {
			String venueId = VenuePABFactory.getPABInstance().addVenueData(venueData);
			if (venueId != null) {
				PropertyPABFactory.getPABInstance().addVenueToProperty(propertyData.getPropertyId(), venueId, "1");
				result = true;
			}
		}

		// else if (Constants.OPERATION_DELETE.equals(operation)) {
		// result =
		// VenuePABFactory.getPABInstance().deleteVenueData(venueData.getVenueId());
		// if (result) {
		// PropertyPABFactory.getPABInstance().deleteVenueFromProperty(propertyData.getPropertyId(),
		// venueData.getVenueId());
		// }
		// } else if (Constants.OPERATION_UPDATE.equals(operation)) {
		// // result = VenuePABFactory.getPABInstance().updateVenueData(venueData);
		// result =
		// VenuePABFactory.getPABInstance().deleteVenueData(venueData.getVenueId());
		// if (result) {
		// PropertyPABFactory.getPABInstance().deleteVenueFromProperty(propertyData.getPropertyId(),
		// venueData.getVenueId());
		// }
		// String venueId = VenuePABFactory.getPABInstance().addVenueData(venueData);
		// if (venueId != null)
		// result = true;
		// }

		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();

		if (result) {
			out.print(Constants.SUCCESS);
		} else {
			out.print(Constants.FAILURE);
		}

		out.flush();

		logger.debug("Exit");
		return null;
	}
}