package com.banquetin.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.CustomerData;
import com.banquetin.form.RegisterForm;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.util.Constants;

public class RegisterUserAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(RegisterUserAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.debug("Entry");

		RegisterForm registerForm = (RegisterForm) form;

		CustomerData customerData = getCustomerData(registerForm);

		boolean userCreated = UserPABFactory.getPABInstance().createNewCustomer(customerData);

		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");

		PrintWriter out = response.getWriter();

		if (userCreated) {
			request.getSession().setAttribute(Constants.SESSION_CUSTOMER_DATA, customerData);
			out.print(Constants.SUCCESS);
		} else {
			request.getSession().setAttribute(Constants.SESSION_CUSTOMER_DATA, null);
			out.print(Constants.FAILURE);
		}

		out.flush();

		logger.debug("Exit");
		return null;
	}

	/**
	 * This method is used to convert the input form object to a bean object
	 * 
	 * @param registerForm
	 * @return
	 */
	private CustomerData getCustomerData(RegisterForm registerForm) {

		CustomerData customerData = new CustomerData();
		customerData.setEmailId(registerForm.getEmailId());
		customerData.setFirstName(registerForm.getFirstName());
		customerData.setLastName(registerForm.getLastName());
		customerData.setTelephone(registerForm.getMobile());
		customerData.setPassword(registerForm.getPassword());

		return customerData;
	}

}