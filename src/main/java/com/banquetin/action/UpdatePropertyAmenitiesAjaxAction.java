package com.banquetin.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.util.Constants;
import com.google.gson.Gson;

public class UpdatePropertyAmenitiesAjaxAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(UpdatePropertyAmenitiesAjaxAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//
		logger.debug("Entry");

		String amenitiesList = (String) request.getParameter("amenities");

		Gson gson = new Gson();
		String[] arr = gson.fromJson(amenitiesList, String[].class);

		logger.debug(amenitiesList);

		List<PropertyData> propertyList = (List<PropertyData>) request.getSession()
				.getAttribute(Constants.SESSION_PROPERTY_LIST);

		PropertyData propertyData = (PropertyData) propertyList.get(0);

		List<String> amenities = new ArrayList<String>();

		if (amenitiesList != null) {
			amenities = new ArrayList<String>(Arrays.asList(amenitiesList.split(",")));
		}

		PropertyPABFactory.getPABInstance().deleteAllAmenitiesFromProperty(propertyData.getPropertyId());

		for (int i = 0; i < arr.length; i++) {
			PropertyPABFactory.getPABInstance().addAmenitiesToProperty(propertyData.getPropertyId(), arr[i]);
		}

		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();

		// if (result) {
		// out.print(Constants.SUCCESS);
		// } else {
		// out.print(Constants.FAILURE);
		// }

		out.flush();

		logger.debug("Exit");
		return null;
	}

}