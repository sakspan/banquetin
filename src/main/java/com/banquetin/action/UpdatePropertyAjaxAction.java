package com.banquetin.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.EmailData;
import com.banquetin.dataobjects.PhoneData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.form.PropertyForm;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.util.Constants;

public class UpdatePropertyAjaxAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(UpdatePropertyAjaxAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//
		logger.debug("Entry");

		PropertyForm propertyForm = (PropertyForm) form;

		PropertyData propertyData = getPropertyDataFromForm(propertyForm);

		boolean result = false;

		System.out.println(propertyData);

		// if (propertyData.getPropertyName() != null) {
		result = PropertyPABFactory.getPABInstance().updatePropertyData(propertyData);
		// } else {
		result = PropertyPABFactory.getPABInstance().updatePropertyContact(propertyData);
		// }

		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();

		if (result) {
			out.print(Constants.SUCCESS);
		} else {
			out.print(Constants.FAILURE);
		}

		out.flush();

		logger.debug("Exit");
		return null;
	}

	private PropertyData getPropertyDataFromForm(PropertyForm form) {

		PropertyData propertyData = new PropertyData();

		propertyData.setPropertyId(form.getPropertyId());
		propertyData.setOnlineInd(form.getOnlineInd());
		propertyData.setPropertyDescription(form.getPropertyDescription());
		propertyData.setPropertyName(form.getPropertyName());
		propertyData.setPropertyTypeId(form.getPropertyType());
		propertyData.setStarRating(form.getStarRating());

		AddressData addressData = new AddressData();
		addressData.setAddress1(form.getAddress1());
		addressData.setAddress2(form.getAddress2());
		addressData.setCity(form.getCity());
		addressData.setPincode(form.getZipCode());
		addressData.setCountryId(Constants.COUNTRY_INDIA);
		addressData.setStateId(form.getState());

		List<PhoneData> phoneList = new ArrayList<PhoneData>();

		if (form.getPhoneNumber() != null) {
			PhoneData phoneData = new PhoneData();
			phoneData.setPhoneNumber(form.getPhoneNumber());
			phoneData.setPhoneTypeId(Constants.PHONE_PRIMARY);
			phoneList.add(phoneData);
		}

		if (form.getAltPhoneNumber1() != null) {
			PhoneData phoneData = new PhoneData();
			phoneData.setPhoneNumber(form.getAltPhoneNumber1());
			phoneData.setPhoneTypeId(Constants.PHONE_ALTERNATE);
			phoneList.add(phoneData);
		}

		addressData.setPhoneList(phoneList);

		List<AddressData> addressList = new ArrayList<AddressData>();
		addressList.add(addressData);
		propertyData.setAddressList(addressList);

		// List<VenueData> venueList = new
		// ArrayList<VenueData>(Arrays.asList(venueDataArray));
		// propertyData.setVenueList(venueList);
		//
		// List<String> amenitiesIdList = new
		// ArrayList<String>(Arrays.asList(amenitiesDataArray));
		// propertyData.setAmenitiesIdList(amenitiesIdList);
		//
		// List<PropertyInfoData> propertyInfoDataList = new
		// ArrayList<PropertyInfoData>(Arrays.asList(propertyInfoData));
		// propertyData.setPropertyInfo(propertyInfoDataList);

		EmailData emailData = new EmailData();
		emailData.setEmailAddress(form.getEmailId());
		emailData.setEmailTypeId(Constants.EMAIL_TYPE_PRIMARY);
		List<EmailData> emailList = new ArrayList<EmailData>();
		emailList.add(emailData);

		propertyData.setEmailList(emailList);

		return propertyData;
	}

}