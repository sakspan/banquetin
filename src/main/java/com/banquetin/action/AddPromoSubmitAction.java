package com.banquetin.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.banquetin.dab.genericdab.GenericDABFactory;
import com.banquetin.dataobjects.PromoData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.form.PromoForm;
import com.banquetin.util.Constants;

public class AddPromoSubmitAction extends BaseAction{

	private static final Logger logger = Logger.getLogger(AddRatesSubmitAction.class.getName());

	@Override
	protected ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.debug("Entry");
		List<PropertyData> propertyList = (List<PropertyData>) request.getSession().getAttribute(Constants.SESSION_PROPERTY_LIST);
		String propertyId = ((PropertyData)propertyList.get(0)).getPropertyId();
		PromoForm promoForm = (PromoForm) form;
		if (Constants.OPERATION_DELETE.equals(promoForm.getOperationType())) {
			GenericDABFactory.getDABInstance().deleteConfiguration(promoForm.getPromoId());
		}else {
			PromoData promoData = convertFormToData(promoForm);
			if (Constants.OPERATION_ADD.equals(promoForm.getOperationType())) {
				GenericDABFactory.getDABInstance().addConfiguration(propertyId, promoData);
			}else {
				GenericDABFactory.getDABInstance().updateConfiguration(promoData);
			}
		}
		logger.debug("Exit");
		return mapping.findForward("success");
	}

	private PromoData convertFormToData(PromoForm promoForm) {

		PromoData ratesData = new PromoData();
		ratesData.setPromoId(promoForm.getPromoId());
		ratesData.setPromoName(promoForm.getPromoName());
		ratesData.setPromoDescription(promoForm.getPromoDescription());
		ratesData.setPromoValue(promoForm.getPromoValue());

		return ratesData;

	}

}
