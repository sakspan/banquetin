package com.banquetin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.AmenitiesData;
import com.banquetin.dataobjects.EventData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.pab.amenitiespab.AmenitiesPABFactory;
import com.banquetin.pab.eventpab.EventPABFactory;
import com.banquetin.pab.lookuppab.LookUpPABFactory;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.Constants;

public class AddPropertyDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(AddPropertyDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");

		ActionForward forward = null;

		List<PropertyData> propertyList = (List<PropertyData>) request.getSession()
				.getAttribute(Constants.SESSION_PROPERTY_LIST);

		if (!propertyList.isEmpty()) {
			request.setAttribute("propertyList", propertyList);
			forward = mapping.findForward("updateProperty");
		} else {
			forward = mapping.findForward("addProperty");
		}

		List<StaticData> propertyTypeList = PropertyPABFactory.getPABInstance().getPropertyType();
		List<StaticData> venueTypeList = VenuePABFactory.getPABInstance().getVenueType();
		List<StaticData> seatingTypeList = VenuePABFactory.getPABInstance().getSeatingType();
		List<AmenitiesData> amenitiesList = AmenitiesPABFactory.getPABInstance().getAmenitiesList();
		List<StaticData> statesList = LookUpPABFactory.getPABInstance().getStateList();
		List<EventData> eventList = EventPABFactory.getPABInstance().getEventList();

		request.setAttribute("propertyTypeList", propertyTypeList);
		request.setAttribute("venueTypeList", venueTypeList);
		request.setAttribute("seatingTypeList", seatingTypeList);
		request.setAttribute("amenitiesList", amenitiesList);
		request.setAttribute("statesList", statesList);
		request.setAttribute("eventTypeList", eventList);

		logger.debug("Exit");

		return forward;
	}

	@Override
	protected boolean isAdminLoginRequired() {
		return true;
	}

}