package com.banquetin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.util.Constants;

public class LogoutAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(TestPageAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");

		request.getSession().setAttribute(Constants.SESSION_USER_DATA, null);
		request.getSession().setAttribute(Constants.SESSION_CUSTOMER_DATA, null);
		request.getSession().setAttribute(Constants.SESSION_PROPERTY_LIST, null);
		request.getSession().setAttribute(Constants.SESSION_ERROR_MSG, null);
		
		
		logger.debug("Exit");

		return mapping.findForward("success");
	}

}