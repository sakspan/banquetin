package com.banquetin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.util.Constants;

public abstract class BaseAction extends Action {

	private static final Logger logger = Logger.getLogger(BaseAction.class.getName());

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.debug("Entry");

		ActionForward forward = null;

		
		
		try {

			if (isAdminLoginRequired() && !isAdminLoggedIn(request)) {

				forward = mapping.findForward("adminLogin");
			} else {
				getPropertyDataForUser(request);
				forward = doExecute(mapping, form, request, response);
			}
		}

		catch (Exception e) {
			logger.error("Exception Occurred. Redirected user to error page", e);
		}
		logger.debug("Exit");

		return forward;
	}

	protected void clearePropertyCache(HttpServletRequest request) {
		request.getSession().setAttribute(Constants.SESSION_PROPERTY_LIST, null);
	}

	private void getPropertyDataForUser(HttpServletRequest request) {

		UserData userData = (UserData) request.getSession().getAttribute(Constants.SESSION_USER_DATA);
		List<PropertyData> propertyList = (List<PropertyData>) request.getSession()
				.getAttribute(Constants.SESSION_PROPERTY_LIST);
		if (userData != null && (propertyList == null || propertyList.isEmpty())) {
			propertyList = UserPABFactory.getPABInstance().getUserPropertyList(userData.getUserId());
			request.getSession().setAttribute(Constants.SESSION_PROPERTY_LIST, propertyList);
		}
	}

	private ActionForward checkMobileForward(ActionMapping mapping, ActionForward forward, HttpServletRequest request) {
		return forward;
	}

	protected boolean isRequestFromMobile(HttpServletRequest request) {
		if (request != null && request.getHeader("User-Agent") != null
				&& request.getHeader("User-Agent").indexOf("Mobile") != -1) {
			return true;
		} else {
			return false;
		}

	}

	private boolean isAdminLoggedIn(HttpServletRequest request) {
		UserData userData = (UserData) request.getSession().getAttribute(Constants.SESSION_USER_DATA);
		return userData == null ? false : true;
	}

	protected boolean isAdminLoginRequired() {
		return false;
	}

	private boolean isUserLoggedIn(HttpServletRequest request) {
		return true;
	}

	protected boolean isUserLoginRequired() {
		return false;
	}

	protected abstract ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception;

}