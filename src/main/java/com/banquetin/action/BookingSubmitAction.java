package com.banquetin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dab.genericdab.GenericDABFactory;
import com.banquetin.dab.genericdab.IGenericDAB;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.form.BookingForm;
import com.banquetin.pab.bookingpab.BookingPABFactory;
import com.banquetin.pab.bookingpab.IBookingPAB;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.util.CommonUtil;
import com.banquetin.util.Constants;

public class BookingSubmitAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(BookingSubmitAction.class.getName());
	private static final IBookingPAB PABINSTANCE = BookingPABFactory.getPABInstance();
	private static final IGenericDAB GENDABINSTANCE = GenericDABFactory.getDABInstance();

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");

		BookingForm bookingForm = (BookingForm) form;
		String operationType = request.getParameter("operationType");
		if(Constants.OPERATION_DELETE.equals(operationType)){
			BookingPABFactory.getPABInstance().deleteBookingData(bookingForm.getBookingId());
		}
		else if(Constants.OPERATION_UPDATE.equals(operationType)){
			String bookingId = request.getParameter("bookingId");
			BookingData bookingData = BookingPABFactory.getPABInstance().getBookingData(bookingId);
			UserData userData = UserPABFactory.getPABInstance().getUserData(bookingData.getUserId());
			
			AddressData addressData = GENDABINSTANCE.getAddressData(bookingData.getUserId());
			request.setAttribute("editUserData", userData);
			request.setAttribute("editAddressData", addressData);
			request.setAttribute("bookingData", bookingData);
			
		}
		else if("edit".equals(operationType)){
			String bookingId = bookingForm.getBookingId();
			BookingData bookingData = BookingPABFactory.getPABInstance().getBookingData(bookingId);
			UserData userData = UserPABFactory.getPABInstance().getUserData(bookingData.getUserId());
			AddressData addressData = GENDABINSTANCE.getAddressData(bookingData.getUserId());
			editBooking(bookingData,userData,addressData,bookingForm);
			String Id = PABINSTANCE.updateBookingData(bookingData,userData,addressData);
			request.setAttribute("bookingId", Id);
		}
		else {
		bookingForm.setTransactionId("12345");
		BookingData bookingData = convertFormToData(bookingForm);

		UserData userData = getUserData(bookingForm);
		
		AddressData addressData = getAddressData(bookingForm);
		
		String userId = UserPABFactory.getPABInstance().addUserData(userData);
		
		String addressId = GENDABINSTANCE.addAddressData(addressData);

		bookingData.setUserId(userId);
		bookingData.setAddressId(addressId);
		
		GENDABINSTANCE.addUserAddressData(bookingData);
		String bookingId = PABINSTANCE.addBookingData(bookingData);

		request.setAttribute("bookingId", bookingId);
		}
		logger.debug("Exit");

		return mapping.findForward("success");
	}

	private UserData getUserData(BookingForm bookingform) {
		UserData userData = new UserData();

		userData.setUserEmail(bookingform.getEmailId());
		userData.setUserName(bookingform.getGuestName());
		userData.setUserPassword(bookingform.getGuestName());
		userData.setUserRoleTypeId(Constants.USER_TYPE_GUEST);
		userData.setUserMobile(bookingform.getPhoneNum());

		return userData;
	}

	private BookingData convertFormToData(BookingForm bookingform) {

		BookingData bookingData = new BookingData();

		bookingData.setAmount(Double.parseDouble(bookingform.getBookingAmount()));
		bookingData.setEndDate(CommonUtil.convertStringToDate(bookingform.getBookingDate()));
		bookingData.setBookingStatusId(Constants.BOOKING_STATUS_BOOKED);
		bookingData.setEndSlotTypeId(bookingform.getSlotType());
		bookingData.setEventType(bookingform.getEventType());
		bookingData.setPaymentTypeId(bookingform.getPaymentType());
		bookingData.setStartDate(CommonUtil.convertStringToDate(bookingform.getBookingDate()));
		bookingData.setStartSlotTypeId(bookingform.getSlotType());
		bookingData.setTransactionid(bookingform.getTransactionId());
		bookingData.setUserId("");
		bookingData.setVenueId(bookingform.getVenueId());
		bookingData.setPaxCount(bookingform.getPaxCount());
		bookingData.setAdvanceAmount(Double.parseDouble(bookingform.getAdvancePaid()));

		return bookingData;

	}
	
	private AddressData getAddressData(BookingForm bookingform) {
		AddressData addressData = new AddressData();

		addressData.setAddress1(bookingform.getAddress1());
		addressData.setAddress2(bookingform.getAddress2());
		addressData.setCity(bookingform.getCity());
		addressData.setStateId(bookingform.getState());
		addressData.setCountryId(Constants.COUNTRY_INDIA);
		addressData.setAddressTypeId(Constants.ADDRESS_TYPE_BOOKING);
		addressData.setPincode(bookingform.getPincode());

		return addressData;
	}
	
	private void editBooking(BookingData bookingData,UserData userData,AddressData addressData,BookingForm bookingForm) {
		userData.setUserName(bookingForm.getGuestName());
		userData.setUserEmail(bookingForm.getEmailId());
		userData.setUserMobile(bookingForm.getPhoneNum());
		addressData.setAddress1(bookingForm.getAddress1());
		addressData.setAddress2(bookingForm.getAddress2());
		addressData.setCity(bookingForm.getCity());
		addressData.setStateId(bookingForm.getState());
		addressData.setPincode(bookingForm.getPincode());
		bookingData.setAmount(Double.parseDouble(bookingForm.getEstAmount()));
		bookingData.setAdvanceAmount(Double.parseDouble(bookingForm.getAdvancePaid()));
		
	}
}