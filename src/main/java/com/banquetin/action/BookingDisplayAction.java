package com.banquetin.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.bookingpab.BookingPABFactory;
import com.banquetin.pab.lookuppab.LookUpPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.Constants;
import com.google.gson.Gson;

public class BookingDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(BookingDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
		String startDate = (String)request.getParameter("selectedDate");
		String location = null;
		String pax = (String)request.getParameter("paxCount");
		StringBuffer bookedDatesList = new StringBuffer();
		Map<String,String> bookingDateMap = new HashMap<>();

		List<PropertyData> propertyList = (List<PropertyData>) request.getSession().getAttribute(Constants.SESSION_PROPERTY_LIST);
		PropertyData propertyData = propertyList.get(0);

		location = propertyData.getAddressList().get(0).getCity();
		List<VenueData> propertyVenueList = propertyData.getVenueList();

		//List<VenueData> allVenueList = VenuePABFactory.getPABInstance().getVenueList();

		List<VenueData> filterVenueList = VenuePABFactory.getPABInstance().getFilteredVenueList(startDate, location, pax);
		List<VenueData> venueList = new ArrayList<VenueData>();
		for (VenueData pVenueData : propertyVenueList) {
			for (VenueData fVenueData : filterVenueList) {
				if(fVenueData.getVenueId().compareTo(pVenueData.getVenueId())==0) {
					venueList.add(fVenueData);
					break;
				}
			}
		}

		Calendar cal = Calendar.getInstance();
		//TODO: This changes has to be reverted once testing is done!
		cal.add(Calendar.DATE, -180);
		Date today = cal.getTime();
		cal.add(Calendar.YEAR, 2);
		Date nextYear = cal.getTime();

		List<BookingData> bookingList = BookingPABFactory.getPABInstance().getBookingsForProperty(propertyData.getPropertyId(), today, nextYear);
		List<BookingData> blockedBookingList = BookingPABFactory.getPABInstance().getBlockedDatesList(request.getParameter("venueId"));

		List<StaticData> statesList = LookUpPABFactory.getPABInstance().getStateList();
		List<StaticData> paymentTypeList = LookUpPABFactory.getPABInstance().getPaymentTypeList();

		request.setAttribute("bookingList", bookingList);
		request.getSession().setAttribute("venueList", venueList);
		request.getSession().setAttribute("propertyVenueList", propertyVenueList);
		request.getSession().setAttribute("statesList", statesList);
		request.getSession().setAttribute("paymentTypeList", paymentTypeList);
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat formatter1 = new SimpleDateFormat("_MMddyyyy");

		for (BookingData bookingData : bookingList) {
			if(bookingData.getVenueId().equals(request.getParameter("venueId"))) {
				bookedDatesList
				//.append("'").append((formatter.format(bookingData.getStartDate()))).append("',")
				.append("'").append(formatter.format(bookingData.getEndDate())).append("',");
				request.setAttribute("filteredVenueId", request.getParameter("venueId"));
				if(bookingDateMap.containsKey((formatter1.format(bookingData.getEndDate())))) {
					bookingDateMap.put((formatter1.format(bookingData.getEndDate())),
							bookingDateMap.get((formatter1.format(bookingData.getEndDate())))+"\\n"
									+bookingData.getVenueText()+","+(bookingData.getStartSlotTypeData().getSlotTypeId().equals("1")?"Booked Morning Slot":" Booked Evening Slot"));
				}else {
					bookingDateMap.put((formatter1.format(bookingData.getEndDate())),
							bookingData.getVenueText()+","+(bookingData.getStartSlotTypeData().getSlotTypeId().equals("1")?"Booked Morning Slot":" Booked Evening Slot"));
				}
			}
		}

		for (BookingData bookingData : blockedBookingList) {
			if(bookingData.getVenueId().equals(request.getParameter("venueId"))) {
				bookedDatesList
				//.append("'").append((formatter.format(bookingData.getStartDate()))).append("',")
				.append("'").append(formatter.format(bookingData.getEndDate())).append("',");

				request.setAttribute("filteredVenueId", request.getParameter("venueId"));

				if(bookingDateMap.containsKey((formatter1.format(bookingData.getEndDate())))) {
					bookingDateMap.put((formatter1.format(bookingData.getEndDate())),
							bookingDateMap.get((formatter1.format(bookingData.getEndDate())))+"\\n"
									+(bookingData.getStartSlotTypeData().getSlotTypeId().equals("1")?"Blocked Morning Slot":"Blocked Evening Slot"));
				}else {
					bookingDateMap.put((formatter1.format(bookingData.getEndDate())),
							(bookingData.getStartSlotTypeData().getSlotTypeId().equals("1")?"Blocked Morning Slot":"Blocked Evening Slot"));
				}
			}
		}

		if(bookedDatesList.length()>0) {
			request.setAttribute("bookedDatesList","["+bookedDatesList.append("[", 0, 0).substring(0, bookedDatesList.length()-1).toString()+"]");
			System.out.println("Booking Date List--->["+bookedDatesList.append("[", 0, 0).substring(0, bookedDatesList.length()-1).toString()+"]");
			System.out.println("Booking Date Map ---> "+bookingDateMap.toString());
			Gson gson = new Gson();
			System.out.println("Booking Date JSon---> "+gson.toJson(bookingDateMap)+"\n");
			request.setAttribute("bookedDatesMap",gson.toJson(bookingDateMap));
		}

		/*if(blockedDatesList.length()>0) {
			request.setAttribute("blockedDatesList","["+blockedDatesList.append("[", 0, 0).substring(0, blockedDatesList.length()-1).toString()+"]");
			System.out.println("Blocked Date List--->["+blockedDatesList.append("[", 0, 0).substring(0, blockedDatesList.length()-1).toString()+"]");
			System.out.println("Blocked Date Map ---> "+blockedBookingDateMap.toString());
			Gson gson = new Gson();
			System.out.println("Blocked Date JSon---> "+gson.toJson(blockedBookingDateMap));
			request.setAttribute("blockedBookingDateMap",gson.toJson(blockedBookingDateMap));
		}*/

		logger.debug("Exit");
		return mapping.findForward("success");
	}

	@Override
	protected boolean isAdminLoginRequired() {
		return true;
	}

	public static void main(String[] args) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("_MMddyyyy");
		System.out.println(formatter1.format(new Date()));
	}
}