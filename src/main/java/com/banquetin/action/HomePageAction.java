package com.banquetin.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.PartnerData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.EventData;
import com.banquetin.pab.eventpab.EventPABFactory;
import com.banquetin.pab.partnerpab.PartnerPABFactory;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.userpab.UserPABFactory;

public class HomePageAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(HomePageAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
		List<EventData> eventData = EventPABFactory.getPABInstance().getEventList();
		List<ImageData> eventImageData = new ArrayList<ImageData>();
		eventImageData = EventPABFactory.getPABInstance().getAllImageFromEvent();
		request.setAttribute("eventDataList", eventData);
		request.setAttribute("imageData", eventImageData);
		List<UserData> feedbackData = new ArrayList<UserData>();
		feedbackData = UserPABFactory.getPABInstance().getFeedBackList();
		request.setAttribute("feedBackList", feedbackData);
		List<PartnerData> partnerData = PartnerPABFactory.getPABInstance().getPartnerList();
		request.setAttribute("partnerList", partnerData);
		List<AddressData> addressdata = PropertyPABFactory.getPABInstance().getUniqueAddressFromProperty();
		request.setAttribute("locationList", addressdata);

		logger.debug("Exit");

		return mapping.findForward("success");
	}

}