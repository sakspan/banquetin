package com.banquetin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.venuepab.VenuePABFactory;

public class VenueDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(VenueDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
		String venueId = (String)request.getParameter("venueId");
		
		VenueData venueData = VenuePABFactory.getPABInstance().getVenueData(venueId);
		request.setAttribute("venueData", venueData);
		logger.debug("Exit");

		return mapping.findForward("success");
	}

}