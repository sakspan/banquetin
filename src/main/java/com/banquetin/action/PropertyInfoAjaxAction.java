package com.banquetin.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.EmailData;
import com.banquetin.dataobjects.PhoneData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.PropertyInfoData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.form.PropertyForm;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.Constants;
import com.google.gson.Gson;

public class PropertyInfoAjaxAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(PropertyInfoAjaxAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.debug("Entry");

		Gson gson = new Gson();

		String operation = (String) request.getParameter("operationType");
		boolean result = false;

		if (Constants.OPERATION_DELETE.equals(operation)) {
			String propertyInfoId = (String) request.getParameter("propertyInfoId");
			result = PropertyPABFactory.getPABInstance().deleteInfoFromProperty(propertyInfoId);
		} else {
			String jsonInfoString = (String) request.getParameter("propertyInfoData");
			logger.debug("PropertyData" + jsonInfoString);

			PropertyInfoData[] propertyInfoList = gson.fromJson(jsonInfoString, PropertyInfoData[].class);

			List<PropertyData> propertyList = (List<PropertyData>) request.getSession()
					.getAttribute(Constants.SESSION_PROPERTY_LIST);

			PropertyData propertyData = (PropertyData) propertyList.get(0);

			PropertyInfoData propertyInfoData = propertyInfoList[0];
			propertyInfoData.setIcon("icon-term-condition");
			propertyInfoData.setPropertyId(propertyData.getPropertyId());

			System.out.println("propertyInfoData : " + propertyInfoData);

			if (Constants.OPERATION_ADD.equals(operation)) {
				result = PropertyPABFactory.getPABInstance().addInfoToProperty(propertyInfoData);
			} else if (Constants.OPERATION_UPDATE.equals(operation)) {
				result = PropertyPABFactory.getPABInstance()
						.deleteInfoFromProperty(propertyInfoData.getPropertyInfoId());
				result = PropertyPABFactory.getPABInstance().addInfoToProperty(propertyInfoData);
			}
		}
		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();

		if (result) {
			out.print(Constants.SUCCESS);
		} else {
			out.print(Constants.FAILURE);
		}

		out.flush();

		logger.debug("Exit");
		return null;

	}

}