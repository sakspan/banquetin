package com.banquetin.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.form.BookingForm;
import com.banquetin.pab.bookingpab.BookingPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

public class DownloadFileAction extends BaseAction {

	@Override
	public ActionForward doExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		//return an application file instead of html page
		response.reset();
		response.setContentType("application/force-download");
		response.setHeader("Content-Disposition","attachment;filename=booking.pdf");
		FileInputStream inStream = null;
		OutputStream outStream =  null;
		BookingForm bookingForm = (BookingForm)form;
		try 
		{
			String home = System.getProperty("user.home");
			String path = new String(home+"/Downloads/" + "booking" + ".pdf"); 
			String bookingId = (String) request.getAttribute("printBookingId");
			if(bookingId==null) {
				bookingId = bookingForm.getBookingId();
			}
			BookingData bookingData = BookingPABFactory.getPABInstance().getBookingData(bookingId);

			// Creating an ImageData object
			String imageFile = "C:\\Users\\KASHYAP\\git\\banquetin\\src\\main\\webapp\\images\\logo.png";       
			ImageData data = ImageDataFactory.create(imageFile); 

			PdfWriter pdfWriter = new PdfWriter(path);
			PdfDocument pdfDocument = new PdfDocument(pdfWriter);
			pdfDocument.addNewPage();
			pdfDocument.setDefaultPageSize(PageSize.A4);
			Document document = new Document(pdfDocument);
			document.setTextAlignment(TextAlignment.CENTER);
			document.setBold();

			// Creating a table
			//row1
			float [] pointColumnWidths2 = {174f, 346f};
			Table table1 = new Table(pointColumnWidths2);

			Cell cell1_1 = new Cell();              
			Image img = new Image(data);              
			cell1_1.add(img.setWidth(85));
			//cell1_1.setTextAlignment(TextAlignment.LEFT);
			//cell1.setBorder(Border.NO_BORDER);
			table1.addCell(cell1_1);

			Cell cell1_2 = new Cell();
			cell1_2.add("Hotel Name and Address");
			//cell2.setBorder(Border.NO_BORDER);
			//cell1_2.setTextAlignment(TextAlignment.RIGHT);
			table1.addCell(cell1_2);

			//row2
			Cell cell2 = new Cell(0,2);
			cell2.add("Booking Confirmation Sheet");
			cell2.setTextAlignment(TextAlignment.CENTER);
			table1.addCell(cell2);
			document.add(table1);

			//row3
			float [] pointColumnWidths3 = {87f,87f,196f,150f};
			Table table3 = new Table(pointColumnWidths3);
			Cell cell3_1 = new Cell();
			cell3_1.add("Prepared by");
			table3.addCell(cell3_1);

			Cell cell3_2 = new Cell();
			cell3_2.add("Banquetin Team");
			table3.addCell(cell3_2);


			Cell cell3_3 = new Cell();
			table3.addCell(cell3_3);

			Cell cell3_4 = new Cell();
			//nested table begin
			float [] pointColumnWidths3_4 = {75f, 75f};       
			Table table3_1 = new Table(pointColumnWidths3_4);                

			Cell cell3_4_1 = new Cell();       
			cell3_4_1.add("Date"); 
			table3_1.addCell(cell3_4_1);                   

			Cell cell3_4_2 = new Cell();       
			cell3_4_2.add(java.time.LocalDate.now().toString());       
			table3_1.addCell(cell3_4_2);                   

			Cell cell3_4_3 = new Cell();       
			cell3_4_3.add("Time");
			table3_1.addCell(cell3_4_3);                    

			Cell cell3_4_4 = new Cell();       
			cell3_4_4.add(java.time.LocalTime.now().getHour()+":"+java.time.LocalTime.now().getMinute()+":"+java.time.LocalTime.now().getSecond());
			table3_1.addCell(cell3_4_4);

			cell3_4.add(table3_1);
			table3.addCell(cell3_4);

			Cell cell5_ = new Cell(0,5);
			cell5_.add("Contact Details");
			cell5_.setTextAlignment(TextAlignment.CENTER);
			table3.addCell(cell5_);
			document.add(table3);

			Table table4 = new Table(pointColumnWidths2);
			//row4
			Cell cell4_1 = new Cell();
			cell4_1.add("Primary Contact Name");

			Cell cell4_2 = new Cell();
			cell4_2.add(bookingForm.getGuestName());
			table4.addCell(cell4_1);
			table4.addCell(cell4_2);

			//row5
			Cell cell5_1 = new Cell();
			cell5_1.add("Company / Event");

			Cell cell5_2 = new Cell();
			cell5_2.add("");
			table4.addCell(cell5_1);
			table4.addCell(cell5_2);

			//row6
			Cell cell6_1 = new Cell();
			cell6_1.add("Address");

			Cell cell6_2 = new Cell();
			cell6_2.add(bookingForm.getAddress1()+", "+bookingForm.getCity()+", "+bookingForm.getPincode());
			table4.addCell(cell6_1);
			table4.addCell(cell6_2);

			//row7
			Cell cell7_1 = new Cell();
			cell7_1.add("Mobile Number");

			Cell cell7_2 = new Cell();
			cell7_2.add(bookingForm.getPhoneNum());
			table4.addCell(cell7_1);
			table4.addCell(cell7_2);

			//row8
			Cell cell8_1 = new Cell();
			cell8_1.add("Fax");

			Cell cell8_2 = new Cell();
			cell8_2.add("Fax Number goes here");
			table4.addCell(cell8_1);
			table4.addCell(cell8_2);

			//row9
			Cell cell9_1 = new Cell();
			cell9_1.add("Email");

			Cell cell9_2 = new Cell();
			cell9_2.add(bookingForm.getEmailId());
			table4.addCell(cell9_1);
			table4.addCell(cell9_2);

			//row10
			Cell cell10 = new Cell(0,2);
			cell10.add("Event Details");
			cell10.setTextAlignment(TextAlignment.CENTER);
			table4.addCell(cell10);

			//row11
			Cell cell11_1 = new Cell();
			cell11_1.add("Venue Name");

			Cell cell11_2 = new Cell();
			cell11_2.add(String.valueOf(bookingData.getVenueText()));
			table4.addCell(cell11_1);
			table4.addCell(cell11_2);

			//row12
			Cell cell12_1 = new Cell();
			cell12_1.add("Date Of The Event");

			Cell cell12_2 = new Cell();
			cell12_2.add(bookingForm.getBookingDate());
			table4.addCell(cell12_1);
			table4.addCell(cell12_2);

			document.add(table4);

			//row13
			Table table5 = new Table(pointColumnWidths3);
			Cell cell13_1 = new Cell();
			cell13_1.add("Start Time");

			Cell cell13_2 = new Cell();
			cell13_2.add("Morning/evening slot's start time - " +bookingData.getStartDate());

			Cell cell13_3 = new Cell();
			cell13_3.add("End Time");

			Cell cell13_4 = new Cell();
			cell13_4.add("Morning/evening slot's end time - "+bookingData.getEndDate());
			table5.addCell(cell13_1);
			table5.addCell(cell13_2);
			table5.addCell(cell13_3);
			table5.addCell(cell13_4);
			document.add(table5);

			Table table6 = new Table(pointColumnWidths2);
			//row14
			Cell cell14_1 = new Cell();
			cell14_1.add("Status of the booking");

			Cell cell14_2 = new Cell();
			cell14_2.add(String.valueOf(bookingData.getBookingStatus()));
			table6.addCell(cell14_1);
			table6.addCell(cell14_2);

			//row15
			Cell cell15_1 = new Cell();
			cell15_1.add("Type of Event");

			Cell cell15_2 = new Cell();
			cell15_2.add(String.valueOf(bookingData.getEventType()));
			table6.addCell(cell15_1);
			table6.addCell(cell15_2);

			//row16
			Cell cell16_1 = new Cell();
			cell16_1.add("Rate Per Pax");

			Cell cell16_2 = new Cell();
			cell16_2.add(String.valueOf(bookingForm.getRate()));
			table6.addCell(cell16_1);
			table6.addCell(cell16_2);

			//row17
			Cell cell17_1 = new Cell();
			cell17_1.add("Seating Arrangement");

			Cell cell17_2 = new Cell();
			cell17_2.add(VenuePABFactory.getPABInstance().getSeatingTypeFromVenue(bookingForm.getVenueId()).toString());
			table6.addCell(cell17_1);
			table6.addCell(cell17_2);

			//row18
			Cell cell18_1 = new Cell();
			cell18_1.add("Media Requirements");

			Cell cell18_2 = new Cell();
			cell18_2.add("Media Requirements goes here");
			table6.addCell(cell18_1);
			table6.addCell(cell18_2);
			document.add(table6);

			//row19
			Table table7 = new Table(pointColumnWidths3);
			Cell cell19_1 = new Cell();
			cell19_1.add("Guaranteed Pax");

			Cell cell19_2 = new Cell();
			cell19_2.add(String.valueOf(bookingData.getPaxCount()));

			Cell cell19_3 = new Cell();
			cell19_3.add("Maximum Pax");

			Cell cell19_4 = new Cell();
			cell19_4.add("max pax");
			table7.addCell(cell19_1);
			table7.addCell(cell19_2);
			table7.addCell(cell19_3);
			table7.addCell(cell19_4);
			document.add(table7);

			Table table8 = new Table(pointColumnWidths2);
			//row20
			Cell cell20_1 = new Cell();
			cell20_1.add("Menu");

			Cell cell20_2 = new Cell();
			cell20_2.add("Package1/Package2/Package3");
			table8.addCell(cell20_1);
			table8.addCell(cell20_2);

			//row21
			Cell cell21_1 = new Cell();
			cell21_1.add("Other Comments");

			Cell cell21_2 = new Cell();
			cell21_2.add(String.valueOf(bookingForm.getComments()));
			table8.addCell(cell21_1);
			table8.addCell(cell21_2);
			document.add(table8);

			document.close();

			System.out.println("Created!!");

			// reads input file from an absolute path
			File downloadFile = new File(path);
			inStream = new FileInputStream(downloadFile);

			response.setContentLength((int) downloadFile.length());

			outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally {
			inStream.close();
			outStream.flush();
			outStream.close();
		}

		return null;
	}

	/*public static void main(String[] args) {
		try 
		{
			String home = System.getProperty("user.home");
			String path = new String(home+"/Downloads/" + "booking" + ".pdf"); 

			// Creating an ImageData object
			String imageFile = "C:\\Users\\KASHYAP\\git\\banquetin\\src\\main\\webapp\\images\\logo.png";       
			ImageData data = ImageDataFactory.create(imageFile); 

			PdfWriter pdfWriter = new PdfWriter(path);
			PdfDocument pdfDocument = new PdfDocument(pdfWriter);
			pdfDocument.addNewPage();
			pdfDocument.setDefaultPageSize(PageSize.A4);
			Document document = new Document(pdfDocument);
			document.setTextAlignment(TextAlignment.CENTER);
			document.setBold();

			// Creating a table
			//row1
			float [] pointColumnWidths2 = {260f, 260f};
			Table table1 = new Table(pointColumnWidths2);

			Cell cell1_1 = new Cell();              
			Image img = new Image(data);              
			cell1_1.add(img.setWidth(85));
			//cell1_1.setTextAlignment(TextAlignment.LEFT);
			//cell1.setBorder(Border.NO_BORDER);
			table1.addCell(cell1_1);

			Cell cell1_2 = new Cell();
			cell1_2.add("Hotel Name and Address");
			//cell2.setBorder(Border.NO_BORDER);
			//cell1_2.setTextAlignment(TextAlignment.RIGHT);
			table1.addCell(cell1_2);

			//row2
			Cell cell2 = new Cell(0,2);
			cell2.add("Booking Confirmation Sheet");
			cell2.setTextAlignment(TextAlignment.CENTER);
			table1.addCell(cell2);
			document.add(table1);

			//row3
			float [] pointColumnWidths3 = {130f,130f,130f,130f};
			Table table3 = new Table(pointColumnWidths3);
			Cell cell3_1 = new Cell();
			cell3_1.add("Prepared by");
			table3.addCell(cell3_1);

			Cell cell3_2 = new Cell();
			cell3_2.add("Banquetin Team");
			table3.addCell(cell3_2);


			Cell cell3_3 = new Cell();
			table3.addCell(cell3_3);

			Cell cell3_4 = new Cell();
			//nested table begin
			float [] pointColumnWidths3_4 = {65f, 65f};       
			Table table3_1 = new Table(pointColumnWidths3_4);                

			Cell cell3_4_1 = new Cell();       
			cell3_4_1.add("Date"); 
			table3_1.addCell(cell3_4_1);                   

			Cell cell3_4_2 = new Cell();       
			cell3_4_2.add("");       
			table3_1.addCell(cell3_4_2);                   

			Cell cell3_4_3 = new Cell();       
			cell3_4_3.add("Time");
			table3_1.addCell(cell3_4_3);                    

			Cell cell3_4_4 = new Cell();       
			cell3_4_4.add("");
			table3_1.addCell(cell3_4_4);

			cell3_4.add(table3_1);
			table3.addCell(cell3_4);

			Cell cell5_ = new Cell(0,5);
			cell5_.add("Contact Details");
			cell5_.setTextAlignment(TextAlignment.CENTER);
			table3.addCell(cell5_);
			document.add(table3);

			Table table4 = new Table(pointColumnWidths2);
			//row4
			Cell cell4_1 = new Cell();
			cell4_1.add("Primary Contact Name");

			Cell cell4_2 = new Cell();
			cell4_2.add("");
			table4.addCell(cell4_1);
			table4.addCell(cell4_2);

			//row5
			Cell cell5_1 = new Cell();
			cell5_1.add("Company / Event");

			Cell cell5_2 = new Cell();
			cell5_2.add("");
			table4.addCell(cell5_1);
			table4.addCell(cell5_2);

			//row6
			Cell cell6_1 = new Cell();
			cell6_1.add("Address");

			Cell cell6_2 = new Cell();
			cell6_2.add("");
			table4.addCell(cell6_1);
			table4.addCell(cell6_2);

			//row7
			Cell cell7_1 = new Cell();
			cell7_1.add("Mobile Number");

			Cell cell7_2 = new Cell();
			cell7_2.add("");
			table4.addCell(cell7_1);
			table4.addCell(cell7_2);

			//row8
			Cell cell8_1 = new Cell();
			cell8_1.add("Fax");

			Cell cell8_2 = new Cell();
			cell8_2.add("");
			table4.addCell(cell8_1);
			table4.addCell(cell8_2);

			//row9
			Cell cell9_1 = new Cell();
			cell9_1.add("Email");

			Cell cell9_2 = new Cell();
			cell9_2.add("");
			table4.addCell(cell9_1);
			table4.addCell(cell9_2);

			//row10
			Cell cell10 = new Cell(0,2);
			cell10.add("Event Details");
			cell10.setTextAlignment(TextAlignment.CENTER);
			table4.addCell(cell10);

			//row11
			Cell cell11_1 = new Cell();
			cell11_1.add("Venue Name");

			Cell cell11_2 = new Cell();
			cell11_2.add("");
			table4.addCell(cell11_1);
			table4.addCell(cell11_2);

			//row12
			Cell cell12_1 = new Cell();
			cell12_1.add("Date Of The Event");

			Cell cell12_2 = new Cell();
			cell12_2.add("");
			table4.addCell(cell12_1);
			table4.addCell(cell12_2);

			document.add(table4);

			//row13
			Table table5 = new Table(pointColumnWidths3);
			Cell cell13_1 = new Cell();
			cell13_1.add("Start Time");

			Cell cell13_2 = new Cell();
			cell13_2.add("");

			Cell cell13_3 = new Cell();
			cell13_3.add("End Time");

			Cell cell13_4 = new Cell();
			cell13_4.add("");
			table5.addCell(cell13_1);
			table5.addCell(cell13_2);
			table5.addCell(cell13_3);
			table5.addCell(cell13_4);
			document.add(table5);

			Table table6 = new Table(pointColumnWidths2);
			//row14
			Cell cell14_1 = new Cell();
			cell14_1.add("Status of the booking");

			Cell cell14_2 = new Cell();
			cell14_2.add("Tentative/Confirmed");
			table6.addCell(cell14_1);
			table6.addCell(cell14_2);

			//row15
			Cell cell15_1 = new Cell();
			cell15_1.add("Type of Event");

			Cell cell15_2 = new Cell();
			cell15_2.add("");
			table6.addCell(cell15_1);
			table6.addCell(cell15_2);

			//row16
			Cell cell16_1 = new Cell();
			cell16_1.add("Rate Per Pax");

			Cell cell16_2 = new Cell();
			cell16_2.add("");
			table6.addCell(cell16_1);
			table6.addCell(cell16_2);

			//row17
			Cell cell17_1 = new Cell();
			cell17_1.add("Seating Arrangement");

			Cell cell17_2 = new Cell();
			cell17_2.add("");
			table6.addCell(cell17_1);
			table6.addCell(cell17_2);

			//row18
			Cell cell18_1 = new Cell();
			cell18_1.add("Media Requirements");

			Cell cell18_2 = new Cell();
			cell18_2.add("");
			table6.addCell(cell18_1);
			table6.addCell(cell18_2);
			document.add(table6);

			//row19
			Table table7 = new Table(pointColumnWidths3);
			Cell cell19_1 = new Cell();
			cell19_1.add("Guaranteed Pax");

			Cell cell19_2 = new Cell();
			cell19_2.add("");

			Cell cell19_3 = new Cell();
			cell19_3.add("Maximum Pax");

			Cell cell19_4 = new Cell();
			cell19_4.add("");
			table7.addCell(cell19_1);
			table7.addCell(cell19_2);
			table7.addCell(cell19_3);
			table7.addCell(cell19_4);
			document.add(table7);

			Table table8 = new Table(pointColumnWidths2);
			//row20
			Cell cell20_1 = new Cell();
			cell20_1.add("Menu");

			Cell cell20_2 = new Cell();
			cell20_2.add("Package1/Package2/Package3");
			table8.addCell(cell20_1);
			table8.addCell(cell20_2);

			//row21
			Cell cell21_1 = new Cell();
			cell21_1.add("Other Comments");

			Cell cell21_2 = new Cell();
			cell21_2.add("");
			table8.addCell(cell21_1);
			table8.addCell(cell21_2);
			document.add(table8);


			document.close();

			System.out.println("Created!!");

		}catch(Exception e){
			e.printStackTrace();
		}
	}*/

}