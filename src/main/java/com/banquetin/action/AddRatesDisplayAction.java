package com.banquetin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.RatesData;
import com.banquetin.dataobjects.SlotTypeData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.form.RatesForm;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.ratespab.RatesPABFactory;
import com.banquetin.util.Constants;

public class AddRatesDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(AddRatesDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
		RatesForm ratesForm = (RatesForm) form;

		List<PropertyData> propertyList = (List<PropertyData>) request.getSession().getAttribute(Constants.SESSION_PROPERTY_LIST);

		PropertyData propertyData = propertyList.get(0);

		List<VenueData> venueList = PropertyPABFactory.getPABInstance()
				.getVenueFromProperty(propertyData.getPropertyId());
		List<RatesData> rateList = null;

		if (ratesForm != null && ratesForm.getVenueId() != null) {
			rateList = RatesPABFactory.getPABInstance().getRatesList(ratesForm.getVenueId());
		}

		List<SlotTypeData> slotTypeDataList = RatesPABFactory.getPABInstance().getSlotTypes();

		request.setAttribute("slotTypeList", slotTypeDataList);
		request.setAttribute("venueList", venueList);
		request.setAttribute("rateList", rateList);

		HttpSession session = request.getSession();
		session.setAttribute("rateList", rateList);
		logger.debug("Exit");

		return mapping.findForward("success");
	}

	@Override
	protected boolean isAdminLoginRequired() {
		return true;
	}

}