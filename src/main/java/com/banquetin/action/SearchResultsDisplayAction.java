package com.banquetin.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.Constants;


public class SearchResultsDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(SearchResultsDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
		String eventType = (String)request.getParameter("eventtype");
		String startDate = (String)request.getParameter("date");
		String location = (String)request.getParameter("location");
		String pax = (String)request.getParameter("pax");

		List<VenueData> filterVenueList = VenuePABFactory.getPABInstance().getFilteredVenueList(startDate, location, pax);
		
		request.setAttribute("venueDataList", filterVenueList);
		request.setAttribute("eventType", eventType);
		request.setAttribute("startDate", startDate);
		request.setAttribute("location", location);
		request.setAttribute("pax", pax);
		logger.debug("Exit");

		return mapping.findForward("success");
	}

}