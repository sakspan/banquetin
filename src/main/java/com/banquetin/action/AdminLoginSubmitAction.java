package com.banquetin.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.form.LoginForm;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.util.Constants;

public class AdminLoginSubmitAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(AdminLoginSubmitAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");

		LoginForm loginForm = (LoginForm) form;
		ActionForward forward = null;

		UserData userData = getUserDataFromForm(loginForm);
		userData.setUserId(UserPABFactory.getPABInstance().userLogin(userData));
		boolean isValidUser = false;
		if (userData.getUserId() != null) {
			isValidUser = true;
		}
		if (isValidUser) {
			request.getSession().setAttribute(Constants.SESSION_USER_DATA, userData);
			List<PropertyData> propertyList = UserPABFactory.getPABInstance().getUserPropertyList(userData.getUserId());
			request.getSession().setAttribute(Constants.SESSION_PROPERTY_LIST, propertyList);
			
			forward = mapping.findForward("success");
		} else {
			request.getSession().setAttribute(Constants.SESSION_ERROR_MSG, "Invalid user name or password");
			forward = mapping.findForward("failure");
		}

		logger.debug("Exit");

		return forward;
	}

	private UserData getUserDataFromForm(LoginForm loginForm) {

		UserData userData = new UserData();
		userData.setUserName(loginForm.getUserName());
		userData.setUserPassword(loginForm.getPassword());

		return userData;
	}

}