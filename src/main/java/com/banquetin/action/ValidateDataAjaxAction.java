package com.banquetin.action;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.form.VenueForm;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.venuepab.VenuePABFactory;
import com.banquetin.util.Constants;
import com.google.gson.Gson;

public class ValidateDataAjaxAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(ValidateDataAjaxAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//
		logger.debug("Entry");
		String data = (String) request.getParameter("data");
		String type = (String) request.getParameter("type");
		boolean result = false;

		if (data == null || data.equals("")) {
			result = false;
		} else {
			if (Constants.TYPE_EMAIL.equals(type)) {
				result = PropertyPABFactory.getPABInstance().validateEmailExist(data);
			} else if (Constants.TYPE_PHONE.equals(type)) {
				result = PropertyPABFactory.getPABInstance().validatePhoneExist(data);
			}
		}

		response.setContentType("text/text;charset=utf-8");
		response.setHeader("cache-control", "no-cache");
		PrintWriter out = response.getWriter();

		if (result) {
			out.print(Constants.SUCCESS);
		} else {
			out.print(Constants.FAILURE);
		}

		out.flush();

		logger.debug("Exit");
		return null;
	}
}