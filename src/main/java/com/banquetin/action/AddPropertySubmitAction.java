package com.banquetin.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.EmailData;
import com.banquetin.dataobjects.PhoneData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.PropertyInfoData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.form.PropertyForm;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.util.Constants;
import com.google.gson.Gson;

public class AddPropertySubmitAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(AddPropertySubmitAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");

		Gson gson = new Gson();

		PropertyForm propertyForm = (PropertyForm) form;

		String jsonInfoString = propertyForm.getInfoListArray();
		PropertyInfoData[] propertyInfoList = gson.fromJson(jsonInfoString, PropertyInfoData[].class);
		logger.debug("Property Info : " + jsonInfoString + " PropertyInfoList : " + propertyInfoList);

		String jsonAmenitiesString = propertyForm.getAmenitiesListArray();
		String[] amenitiesIdList = gson.fromJson(jsonAmenitiesString, String[].class);
		logger.debug("Ameneties List " + amenitiesIdList);

		String jsonVenueString = propertyForm.getVenueListArray();

		// TODO : Remove after frontend passes the data as per below format.
//		jsonVenueString = jsonVenueString.replaceAll("venueType", "venueTypeId");
//		jsonVenueString = jsonVenueString.replaceAll("eventType", "eventTypeId");
//		jsonVenueString = jsonVenueString.replaceAll("seatingTypeList", "seatingList");
//		jsonVenueString = jsonVenueString.replaceAll("seatingType", "seatingTypeId");
//		jsonVenueString = jsonVenueString.replaceAll("seatingList", "seatingTypeList");

		VenueData[] arr = gson.fromJson(jsonVenueString, VenueData[].class);
		logger.debug("Venue Array: " + arr);

		PropertyData propertyData = getPropertyDataFromForm(propertyForm, arr, amenitiesIdList, propertyInfoList);
		logger.debug("Property Data : " + propertyData);

		String propertyId = PropertyPABFactory.getPABInstance().addPropertyData(propertyData);

		UserData userData = (UserData) request.getSession().getAttribute(Constants.SESSION_USER_DATA);
		UserPABFactory.getPABInstance().addPropertyToUser(userData.getUserId(), propertyId);

		clearePropertyCache(request);
		
		logger.debug("Exit");

		return mapping.findForward("success");
	}

	private PropertyData getPropertyDataFromForm(PropertyForm form, VenueData[] venueDataArray,
			String[] amenitiesDataArray, PropertyInfoData[] propertyInfoData) {

		PropertyData propertyData = new PropertyData();

		propertyData.setOnlineInd(form.getOnlineInd());
		propertyData.setPropertyDescription(form.getPropertyDescription());
		propertyData.setPropertyName(form.getPropertyName());
		propertyData.setPropertyTypeId(form.getPropertyType());
		propertyData.setStarRating(form.getStarRating());

		AddressData addressData = new AddressData();
		addressData.setAddress1(form.getAddress1());
		addressData.setAddress2(form.getAddress2());
		addressData.setCity(form.getCity());
		addressData.setPincode(form.getZipCode());
		addressData.setCountryId(Constants.COUNTRY_INDIA);
		addressData.setStateId(form.getState());

		List<PhoneData> phoneList = new ArrayList<PhoneData>();

		if (form.getPhoneNumber() != null) {
			PhoneData phoneData = new PhoneData();
			phoneData.setPhoneNumber(form.getPhoneNumber());
			phoneData.setPhoneTypeId(Constants.PHONE_PRIMARY);
			phoneList.add(phoneData);
		}

		if (form.getAltPhoneNumber1() != null) {
			PhoneData phoneData = new PhoneData();
			phoneData.setPhoneNumber(form.getAltPhoneNumber1());
			phoneData.setPhoneTypeId(Constants.PHONE_ALTERNATE);
			phoneList.add(phoneData);
		}

		addressData.setPhoneList(phoneList);

		List<AddressData> addressList = new ArrayList<AddressData>();
		addressList.add(addressData);
		propertyData.setAddressList(addressList);

		List<VenueData> venueList = new ArrayList<VenueData>(Arrays.asList(venueDataArray));
		propertyData.setVenueList(venueList);

		List<String> amenitiesIdList = new ArrayList<String>(Arrays.asList(amenitiesDataArray));
		propertyData.setAmenitiesIdList(amenitiesIdList);

		List<PropertyInfoData> propertyInfoDataList = new ArrayList<PropertyInfoData>(Arrays.asList(propertyInfoData));
		propertyData.setPropertyInfo(propertyInfoDataList);

		EmailData emailData = new EmailData();
		emailData.setEmailAddress(form.getEmailId());
		emailData.setEmailTypeId(Constants.EMAIL_TYPE_PRIMARY);
		List<EmailData> emailList = new ArrayList<EmailData>();
		emailList.add(emailData);

		propertyData.setEmailList(emailList);

		return propertyData;
	}

}