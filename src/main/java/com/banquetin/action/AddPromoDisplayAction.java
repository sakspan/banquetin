package com.banquetin.action;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.banquetin.dab.genericdab.GenericDABFactory;
import com.banquetin.dataobjects.PromoData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.form.PromoForm;
import com.banquetin.util.Constants;


public class AddPromoDisplayAction extends BaseAction {

	private static final Logger logger = Logger.getLogger(AddPromoDisplayAction.class.getName());

	public ActionForward doExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		logger.debug("Entry");
		PromoForm promoForm = (PromoForm) form;
		List<PropertyData> propertyList = (List<PropertyData>) request.getSession().getAttribute(Constants.SESSION_PROPERTY_LIST);
		String propertyId = ((PropertyData)propertyList.get(0)).getPropertyId();
		List<PromoData> promoList = null;

		if (propertyId != null && !propertyId.isEmpty()) {
			promoList = GenericDABFactory.getDABInstance().getConfigurations(propertyId);
		}
		request.setAttribute("promoList", promoList);

		logger.debug("Exit");
		return mapping.findForward("success");
	}

	@Override
	protected boolean isAdminLoginRequired() {
		return true;
	}

}