package com.banquetin.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class CacheUtil {

	private static final Logger logger = Logger.getLogger(CacheUtil.class.getName());

	public static final String PROPERTY_LIST = "PROPERTY_LIST";
	public static final String PROPERTY_DATA = "PROPERTY_DATA";
	public static final String PROPERTY_VENUE = "PROPERTY_VENUE";
	public static final String PROPERTY_AMENITIES = "PROPERTY_AMENITIES";
	public static final String PROPERTY_VENDOR = "PROPERTY_VENDOR";
	public static final String PROPERTY_INFO = "PROPERTY_INFO";
	public static final String PROPERTY_INFO_LIST = "PROPERTY_INFO_LIST";
	public static final String VENUE_LIST = "VENUE_LIST";
	public static final String VENUE_DATA = "VENUE_DATA";
	public static final String EVENT_TYPE_LIST = "EVENT_TYPE_LIST";
	public static final String SEATING_TYPE_LIST = "SEATING_TYPE_LIST";
	public static final String AMENITIES_LIST = "AMENITIES_LIST";
	public static final String AMENITIES_DATA = "AMENITIES_DATA";
	public static final String VENDOR_LIST = "VENDOR_LIST";
	public static final String VENDOR_DATA = "VENDOR_DATA";
	public static final String RATES_LIST = "RATES_LIST";
	public static final String RATES_DATA = "RATES_DATA";
	public static final String BLOCKDATES_LIST = "BLOCKDATES_LIST";
	public static final String BLOCKDATES_DATA = "BLOCKDATES_DATA";
	public static final String BLOCKDATESFORPROPERTY_LIST = "BLOCKDATESFORPROPERTY_LIST";

	public static final String STATE_LIST = "STATE_LIST";
	public static final String PHONE_LIST = "PHONE_LIST";
	public static final String COUNTRY_LIST = "COUNTRY_LIST";
	public static final String PAYMENT_TYPE_LIST = "PAYMENT_TYPE_LIST";
	
	public static final String PROPERTY_TYPE = "PROPERTY_TYPE";
	public static final String VENUE_TYPE = "VENUE_TYPE";
	public static final String SEATING_TYPE = "SEATING_TYPE";

	public static final boolean USE_CACHE = false;

	private static Map<String, Object> cacheMap = new HashMap<String, Object>();

	public static void putDataToCache(String key, Object obj) {
		logger.debug("Entry");
		cacheMap.put(key, obj);
		logger.debug("Exit");
	}

	public static Object getDataFromCache(String key) {
		logger.debug("Entry");

		Object cacheObject = null;

		if (USE_CACHE)
			cacheObject = (Object) cacheMap.get(key);

		logger.debug("Exit");
		return cacheObject;
	}

	public static Map<String, Object> getCache() {
		return cacheMap;
	}

	public static void clearCache() {
		cacheMap = new HashMap<String, Object>();
	}

	public static String getKey(String... inputs) {
		String key = null;
		for (String s : inputs) {
			if (key == null)
				key = s;
			else
				key = key + ":" + s;
		}

		return key;
	}

}
