package com.banquetin.util;

import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.log4j.Logger;

public class PasswordEncryptionService {

	private static final Logger logger = Logger.getLogger(PasswordEncryptionService.class.getName());

	public static boolean authenticate(String attemptedPassword, String encryptedPassword) {
		// Encrypt the clear-text password using the same salt that was used to
		// encrypt the original password
		String encryptedAttemptedPassword = getEncryptedPassword(attemptedPassword);

		// Authentication succeeds if encrypted password that the user entered
		// is equal to the stored hash
		return encryptedAttemptedPassword.equals(encryptedPassword);
	}

	public static String getEncryptedPassword(String password) {
		// PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
		// specifically names SHA-1 as an acceptable hashing algorithm for
		// PBKDF2
		String algorithm = "PBKDF2WithHmacSHA1";
		// SHA-1 generates 160 bit hashes, so that's what makes sense here
		int derivedKeyLength = 160;
		// Pick an iteration count that works for you. The NIST recommends at
		// least 1,000 iterations:
		// http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
		// iOS 4.x reportedly uses 10,000:
		// http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
		int iterations = 1000;
		String salt = generateSalt();
		byte[] encryptPassword = null;
		try {
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), iterations, derivedKeyLength);

			SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

			encryptPassword = f.generateSecret(spec).getEncoded();
		} catch (Exception e) {
			logger.error("Exception Generated", e);
		}
		String strPassword = new String(encryptPassword);

		return strPassword;

	}

	private static String generateSalt() {
		// VERY important to use SecureRandom instead of just Random
		// SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		String saltValue = "xUIzTQLA";
		// byte[] salt = saltValue.getBytes();
		// Generate a 8 byte (64 bit) salt as recommended by RSA PKCS5
		// byte[] salt = new byte[8];
		// random.nextBytes(salt);

		return saltValue;
	}

	public static void main(String[] args) {

		String abc = "lcadmin";

		try {
			String encryptedPassword = getEncryptedPassword(abc);

			System.out.println(encryptedPassword);
			System.out.println(authenticate(abc, encryptedPassword));
			System.out.println(authenticate("ac", encryptedPassword));

		} catch (Exception e) {
			logger.error("Exception Generated", e);
		}
	}
}
