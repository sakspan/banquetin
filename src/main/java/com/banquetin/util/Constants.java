package com.banquetin.util;

public class Constants {

	public static String FAILURE = "failure";
	public static String SUCCESS = "success";

	public static int IS_ACTIVE_YES = 1;
	public static int IS_ACTIVE_NO = 0;

	public static int IS_PRIMARY_YES = 1;
	public static int IS_PRIMARY_NO = 0;

	public static String OPERATION_ADD = "add";
	public static String OPERATION_UPDATE = "update";
	public static String OPERATION_DELETE = "delete";

	/** LOOK UP TABLES **/
	public static String TABLE_PROPERTYTYPE = "propertyType";
	public static String TABLE_VENUETYPE = "venueType";
	public static String TABLE_SEATINGTYPE = "seatingType";

	// Lookup Tables
	public static final String PHONE_TYPE = "phonetype";
	public static final String STATES = "state";
	public static final String COUNTRIES = "country";
	public static final String TAXES = "tax";
	public static final String SIZES = "size";
	public static final String COLORS = "color";
	public static final String MANUFACTURER = "manufacturer";
	public static final String BRAND = "brand";
	public static final String PRODUCTSTATUS = "productstatus";
	public static final String PAYMENTTYPE = "paymenttype";
	

	// USER DATA
	public static final String SESSION_USER_DATA = "userDataSession";
	public static final String SESSION_PROPERTY_LIST = "propertyListSession";
	public static final String SESSION_ERROR_MSG = "sessionErrorMsg";

	public static final String SESSION_CUSTOMER_DATA= "customerDataSession";
	// Booking Status
	public static final String BOOKING_STATUS_BOOKED = "1";
	public static final String BOOKING_STATUS_CANCELLED = "2";
	
	// Event Type
	public static final String EVENT_TYPE_BIRTHDAY = "1";

	// Payment Type
	public static final String PAYMENT_TYPE_CARD = "1";

	// Role Type
	public static final String USER_TYPE_GUEST = "1";

	// Country
	public static final String COUNTRY_INDIA = "1";
	
	public static final String PHONE_PRIMARY = "1";
	public static final String PHONE_ALTERNATE = "2";
	
	public static final String EMAIL_TYPE_PRIMARY = "1";
	
	// Validate data
	public static final String TYPE_PHONE = "phone";
	public static final String TYPE_EMAIL="email";
	
	//Address type
	public static final String ADDRESS_TYPE_CORPORATE="1";
	public static final String ADDRESS_TYPE_BOOKING="2";
	
	public static final int MORNING_SLOT = 1;
	public static final int EVENING_SLOT = 2;
	
}
