package com.banquetin.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CommonUtil {

	public static Timestamp getTimeStamp(Date inputDate) {
		return new java.sql.Timestamp(inputDate.getTime());
	}

	public static Date getShiftDate(int hour, int minute) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.YEAR, 2000);

		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date d = cal.getTime();

		return d;
	}

	public static Date getBreakDate(int hour, int minute) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.YEAR, 2000);

		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date d = cal.getTime();

		return d;
	}

	public static java.sql.Date getDefaultSQLDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.YEAR, 2000);

		cal.set(Calendar.HOUR_OF_DAY, date.getHours());
		cal.set(Calendar.MINUTE, date.getMinutes());
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date d = cal.getTime();

		return new java.sql.Date(d.getTime());
	}

	public static Date getDefaultDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.YEAR, 2000);

		cal.set(Calendar.HOUR_OF_DAY, date.getHours());
		cal.set(Calendar.MINUTE, date.getMinutes());
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		Date d = cal.getTime();

		return d;
	}

	public static java.sql.Date getSQLDate(Date d) {
		return new java.sql.Date(d.getTime());
	}

	public static Date convertStringToDate(String strDate) {
		SimpleDateFormat srcFormatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			Date srcDate = srcFormatter.parse(strDate);
			String date1 = formatter.format(srcDate);
			date = formatter.parse(date1);
		} catch (ParseException e) {
			System.out.println(e);
		}

		return date;
	}
	
	public static String convertStringTDate(String strDate) {
		SimpleDateFormat srcFormatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String date = "";
		try {
			Date srcDate = srcFormatter.parse(strDate);
			date = formatter.format(srcDate);
		} catch (ParseException e) {
			System.out.println(e);
		}

		return date;
	}

}
