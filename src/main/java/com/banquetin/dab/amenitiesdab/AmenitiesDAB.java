package com.banquetin.dab.amenitiesdab;

import java.util.*;
import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.util.Constants;

public class AmenitiesDAB extends BaseDAB implements IAmenitiesDAB {

	// Singleton Instance of the class
	private static IAmenitiesDAB INSTANCE = new AmenitiesDAB();
	private static final Logger logger = Logger.getLogger(AmenitiesDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private AmenitiesDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IAmenitiesDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Amenities
	 *
	 * @return
	 */
	public String addAmenitiesData(AmenitiesData amenitiesData) {

		logger.debug("Entry");

		String amenitiesId = null;
		String query = "INSERT INTO Amenities(AmenitiesText,AmenitiesIcon,AmenitiesDescription,SortOrder,ShowInFilter,IsActive) values(?,?,?,?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {
			stmt.setString(1, amenitiesData.getAmenitiesText());
			stmt.setString(2, amenitiesData.getAmenitiesIcon());
			stmt.setString(3, amenitiesData.getAmenitiesDescription());
			stmt.setInt(4, amenitiesData.getSortOrder());
			stmt.setString(5, amenitiesData.getShowInFilter());
			stmt.setInt(6, Constants.IS_ACTIVE_YES);
			
			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				amenitiesId = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");

		return amenitiesId;
	}

	/**
	 * This method is used to Update the Amenities details
	 *
	 * @return
	 */
	public boolean updateAmenitiesData(AmenitiesData amenitiesData) {

		logger.debug("Entry");

		String query = "UPDATE Amenities SET AmenitiesText = ? ,AmenitiesIcon = ? ,AmenitiesDescription = ? ,SortOrder = ? ,ShowInFilter = ?  where AmenitiesId = ? AND IsActive = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, amenitiesData.getAmenitiesText());
			stmt.setString(2, amenitiesData.getAmenitiesIcon());
			stmt.setString(3, amenitiesData.getAmenitiesDescription());
			stmt.setInt(4, amenitiesData.getSortOrder());
			stmt.setString(5, amenitiesData.getShowInFilter());
			stmt.setString(6, amenitiesData.getAmenitiesId());
			stmt.setInt(7, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesList() {

		logger.debug("Entry");

		String query = "SELECT * FROM Amenities where IsActive = ?;";
		List<AmenitiesData> amenitiesDataList = new ArrayList<AmenitiesData>();

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);

			while (rs.next()) {
				AmenitiesData amenitiesData = new AmenitiesData();
				amenitiesData.setAmenitiesId(rs.getString("AmenitiesId"));
				amenitiesData.setAmenitiesText(rs.getString("AmenitiesText"));
				amenitiesData.setAmenitiesIcon(rs.getString("AmenitiesIcon"));
				amenitiesData.setAmenitiesDescription(rs.getString("AmenitiesDescription"));
				amenitiesData.setSortOrder(rs.getInt("SortOrder"));
				amenitiesData.setShowInFilter(rs.getString("ShowInFilter"));
				amenitiesData.setIsActive(rs.getInt("IsActive"));

				amenitiesDataList.add(amenitiesData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return amenitiesDataList;
	}

	/**
	 * This method is used to Get details for a particular Amenities
	 *
	 * @return
	 */
	public AmenitiesData getAmenitiesData(String amenitiesId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Amenities where AmenitiesId = ? AND IsActive= ?;";
		AmenitiesData amenitiesData = new AmenitiesData();

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		try {
			stmt.setString(1, amenitiesId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				amenitiesData.setAmenitiesId(rs.getString("AmenitiesId"));
				amenitiesData.setAmenitiesText(rs.getString("AmenitiesText"));
				amenitiesData.setAmenitiesIcon(rs.getString("AmenitiesIcon"));
				amenitiesData.setAmenitiesDescription(rs.getString("AmenitiesDescription"));
				amenitiesData.setSortOrder(rs.getInt("SortOrder"));
				amenitiesData.setShowInFilter(rs.getString("ShowInFilter"));
				amenitiesData.setIsActive(rs.getInt("IsActive"));
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}
		logger.debug("Exit");
		return amenitiesData;
	}

	/**
	 * This method is used to Delete the existing Amenities details
	 *
	 * @return
	 */
	public boolean deleteAmenitiesData(String amenitiesId) {

		logger.debug("Entry");

		String query = "UPDATE Amenities SET IsActive = ? where AmenitiesId = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;
		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, amenitiesId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");

		return count == 0 ? false : true;
	}

}
