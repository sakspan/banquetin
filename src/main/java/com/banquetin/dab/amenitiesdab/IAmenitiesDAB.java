package com.banquetin.dab.amenitiesdab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IAmenitiesDAB{

	/**
	 * This method is used to Create a new Amenities
	 *
	 * @return
	 */
	public String addAmenitiesData(AmenitiesData amenitiesData);

	/**
	 * This method is used to Update the Amenities details
	 *
	 * @return
	 */
	public boolean updateAmenitiesData(AmenitiesData amenitiesData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesList();

	/**
	 * This method is used to Get details for a particular Amenities
	 *
	 * @return
	 */
	public AmenitiesData getAmenitiesData(String amenitiesId);

	/**
	 * This method is used to Delete the existing Amenities details
	 *
	 * @return
	 */
	public boolean deleteAmenitiesData(String amenitiesId);

}
