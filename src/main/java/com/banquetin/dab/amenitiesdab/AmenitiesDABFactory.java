package com.banquetin.dab.amenitiesdab;

import com.banquetin.dab.BaseDABFactory;

public class AmenitiesDABFactory extends BaseDABFactory {

	public static IAmenitiesDAB getDABInstance() {
		return AmenitiesDAB.getInstance();
	}

}
