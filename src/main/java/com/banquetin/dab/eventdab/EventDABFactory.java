package com.banquetin.dab.eventdab;

import com.banquetin.dab.BaseDABFactory;
import com.banquetin.dataobjects.EventData;

public class EventDABFactory extends BaseDABFactory {
	public static IEventDAB getDABInstance() {
		return EventDAB.getInstance();
	}
}
