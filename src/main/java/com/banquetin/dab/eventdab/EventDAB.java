package com.banquetin.dab.eventdab;

import java.util.*;

import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.pab.eventpab.EventPABFactory;
import com.banquetin.util.Constants;

public class EventDAB extends BaseDAB implements IEventDAB {

	// Singleton Instance of the class
	private static IEventDAB INSTANCE = new EventDAB();

	private static final Logger logger = Logger.getLogger(EventDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private EventDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IEventDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Event
	 *
	 * @return
	 */
	public String addEventData(EventData eventData) {

		logger.debug("Entry");

		//TODO:Populate query and parameters
		String query = "INSERT INTO eventtype(EventTypeText,EventDescription,ImageId,SortOrder,IsActive) values(?,?,?,?,?);";
		String eventTypeId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		
		

		try {
			
			
			stmt.setString(1, eventData.getEventTypeText());
			stmt.setString(2, eventData.getEventDescription());
			stmt.setString(3, eventData.getImageId());
			stmt.setInt(4, eventData.getSortOrder());
			stmt.setInt(5 , Constants.IS_ACTIVE_YES);
			

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				eventTypeId = rs.getString(1);
			}

			eventData.setEventTypeId(eventTypeId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return eventTypeId;
	}

	/**
	 * This method is used to Update the Event details
	 *
	 * @return
	 */
	public boolean updateEventData(EventData eventData) {

		logger.debug("Entry");

		
		String query = "UPDATE eventtype SET EventTypeText = ? ,EventDescription = ? ,ImageId = ? ,SortOrder = ? where EventTypeId = ? and IsActive =? ;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			
			stmt.setString(1, eventData.getEventTypeText());
			stmt.setString(2, eventData.getEventDescription());
			stmt.setString(3, eventData.getImageId());
			stmt.setInt(4, eventData.getSortOrder());
			stmt.setString(5, eventData.getEventTypeId());
			stmt.setInt(6, Constants.IS_ACTIVE_YES);


			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active Events
	 *
	 * @return
	 */
	public List<EventData> getEventList() {

		logger.debug("Entry");

		
		String query = "SELECT * FROM eventtype where IsActive = ?;";
		List<EventData> eventDataList = new ArrayList<EventData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

			EventData eventData = new EventData();
			eventData.setEventTypeId(rs.getString("EventTypeId"));
			eventData.setEventTypeText(rs.getString("EventTypeText"));
			eventData.setEventDescription(rs.getString("EventDescription"));
			//eventData.setImageId(rs.getString("ImageId"));
			eventData.setSortOrder(rs.getInt("SortOrder"));
			
			eventDataList.add(eventData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return eventDataList;
	}

	/**
	 * This method is used to Get details for a particular Event
	 *
	 * @return
	 */
	public EventData getEventData(String eventTypeId) {

		logger.debug("Entry");

		
		String query = "SELECT * FROM eventtype where EventTypeId = ? and IsActive=?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		EventData eventData = new EventData();

		try {
			stmt.setString(1, eventTypeId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				
				eventData.setEventTypeId(rs.getString("EventTypeId"));
				eventData.setEventTypeText(rs.getString("EventTypeText"));
				eventData.setEventDescription(rs.getString("EventDescription"));
				eventData.setImageId(rs.getString("ImageId"));
				eventData.setSortOrder(rs.getInt("SortOrder"));
				
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return eventData;
	}

	/**
	 * This method is used to Delete the existing Event details
	 *
	 * @return
	 */
	public boolean deleteEventrData(String eventId) {

		logger.debug("Entry");

		
		String query = "UPDATE eventtype SET IsActive=? where EventTypeId = ?;";
		int count=0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, eventId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public boolean deleteEventData(String eventId) {
		// TODO Auto-generated method stub
		return false;
	}
	public List<ImageData> getAllImageFromEvent() {
		logger.debug("Entry");
		String query = "SELECT * FROM image I, eventtype vi WHERE I.ImageId=vi.ImageId AND vi.IsActive = ? AND I.IsActive = ?;";
		List<ImageData> imageDataList = new ArrayList<ImageData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				ImageData imageData = new ImageData();
				imageData.setImageId(rs.getString("ImageId"));
				imageData.setPath(rs.getString("Path"));
				imageData.setHeight(rs.getInt("Height"));
				imageData.setWidth(rs.getInt("Width"));
				imageData.setAltText(rs.getString("AltText"));
				imageData.setImageLink(rs.getString("ImageLink"));
				imageData.setLength(rs.getInt("Length"));		
				
				
				imageDataList.add(imageData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return imageDataList;
	}


}
