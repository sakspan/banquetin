package com.banquetin.dab.propertydab;

import com.banquetin.dab.BaseDABFactory;

public class PropertyDABFactory extends BaseDABFactory {

	public static IPropertyDAB getDABInstance() {
		return PropertyDAB.getInstance();
	}

}
