package com.banquetin.dab.propertydab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IPropertyDAB {

	/**
	 * This method is used to Create a new Property
	 *
	 * @return
	 */
	public String addPropertyData(PropertyData propertyData);

	/**
	 * This method is used to Update the Property details
	 *
	 * @return
	 */
	public boolean updatePropertyData(PropertyData propertyData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PropertyData> getPropertyList();

	/**
	 * This method is used to Get details for a particular Property
	 *
	 * @return
	 */
	public PropertyData getPropertyData(String propertyId);

	/**
	 * This method is used to Delete the existing Property details
	 *
	 * @return
	 */
	public boolean deletePropertyData(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addAddressToProperty(String propertyId, String addressId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAddressFromProperty(String propertyId, String addressId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<AddressData> getAddressFromProperty(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVenueToProperty(String propertyId, String venueId, String sortOrder);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVenueFromProperty(String propertyId, String venueId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VenueData> getVenueFromProperty(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addAmenitiesToProperty(String propertyId, String amenitiesId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAmenitiesFromProperty(String propertyId, String amenitiesId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesFromProperty(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVendorToProperty(String propertyId, String vendorId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVendorFromProperty(String propertyId, String vendorId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VendorData> getVendorFromProperty(String propertyId);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addEmailToProperty(String propertyId, String emailId);

	public boolean addInfoToProperty(PropertyInfoData propertyInfo);

	public boolean updateInfoForProperty(String propertyInfoId, PropertyInfoData propertyInfo);

	public boolean deleteInfoFromProperty(String propertyInfoId);

	public PropertyInfoData getInfoFromProperty(String propertyInfoId);

	public List<PropertyInfoData> getAllInfoFromProperty(String propertyId);
	
	public boolean addImageToProperty(String propertyId, String imageId, int isPrimary);
	
	public boolean updateImageOfProperty(String propertyId, String imageId, int isPrimary);
	
	public boolean deleteImageFromProperty(String propertyId, String imageId, boolean deletePrimaryFlag);
	
	public ImageData getImageFromProperty(String propertyId, String imageId);
	
	public List<ImageData> getAllImageFromProperty(String propertyId);
	
	public List<AddressData> getUniqueAddressFromProperty();
	
	/**
	 * This method is used to email for a property
	 *
	 * @return
	 */
	public List<EmailData> getEmailForProperty(String propertyId);
	
	/**
	 * This method is used to get address for a property
	 *
	 * @return
	 */
	public List<PhoneData> getPhoneNumbersForProperty(String propertyId);
	
	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteEmailFromProperty(String propertyId, String emailId);
	
	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllEmailFromProperty(String propertyId);
	
	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllAddressFromProperty(String propertyId);
	
	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllAmenitiesFromProperty(String propertyId);
}
