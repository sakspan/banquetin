package com.banquetin.dab.propertydab;

import java.util.*;
import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.util.Constants;

public class PropertyDAB extends BaseDAB implements IPropertyDAB {

	// Singleton Instance of the class
	private static IPropertyDAB INSTANCE = new PropertyDAB();

	private static final Logger logger = Logger.getLogger(PropertyDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private PropertyDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IPropertyDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Property
	 *
	 * @return
	 */
	public String addPropertyData(PropertyData propertyData) {

		logger.debug("Entry");

		String query = "INSERT INTO Property(PropertyName,StarRating,PropertyDescription,PropertyTypeId,OnlineInd,IsActive) values(?,?,?,?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		String propertyId = null;

		try {
			stmt.setString(1, propertyData.getPropertyName());
			stmt.setString(2, propertyData.getStarRating());
			stmt.setString(3, propertyData.getPropertyDescription());
			stmt.setString(4, propertyData.getPropertyType().getId());
			stmt.setString(5, propertyData.getOnlineInd());
			stmt.setInt(6, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				propertyId = rs.getString(1);
			}

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return propertyId;
	}

	/**
	 * This method is used to Update the Property details
	 *
	 * @return
	 */
	public boolean updatePropertyData(PropertyData propertyData) {

		logger.debug("Entry");

		String query = "UPDATE Property SET PropertyName = ? ,StarRating = ? ,PropertyDescription = ? ,PropertyTypeId = ? ,OnlineInd = ?  where PropertyId = ? AND IsActive=?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {

			stmt.setString(1, propertyData.getPropertyName());
			stmt.setString(2, propertyData.getStarRating());
			stmt.setString(3, propertyData.getPropertyDescription());
			stmt.setString(4, propertyData.getPropertyType().getId());
			stmt.setString(5, propertyData.getOnlineInd());
			stmt.setString(6, propertyData.getPropertyId());
			stmt.setInt(7, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PropertyData> getPropertyList() {

		logger.debug("Entry");

		String query = "SELECT * FROM Property where IsActive = ?;";
		List<PropertyData> propertyDataList = new ArrayList<PropertyData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				PropertyData propertyData = new PropertyData();
				propertyData.setPropertyId(rs.getString("PropertyId"));
				propertyData.setPropertyName(rs.getString("PropertyName"));
				propertyData.setStarRating(rs.getString("StarRating"));
				propertyData.setPropertyDescription(rs.getString("PropertyDescription"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("PropertyTypeId"));
				propertyData.setPropertyType(staticData);
				propertyData.setOnlineInd(rs.getString("OnlineInd"));
				propertyData.setAddressList(getAddressFromProperty(rs.getString("PropertyId")));
				propertyDataList.add(propertyData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return propertyDataList;
	}

	/**
	 * This method is used to Get details for a particular Property
	 *
	 * @return
	 */
	public PropertyData getPropertyData(String propertyId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Property where PropertyId = ? AND IsActive = ?;";
		PropertyData propertyData = new PropertyData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				propertyData.setPropertyId(rs.getString("PropertyId"));
				propertyData.setPropertyName(rs.getString("PropertyName"));
				propertyData.setStarRating(rs.getString("StarRating"));
				propertyData.setPropertyDescription(rs.getString("PropertyDescription"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("PropertyTypeId"));
				propertyData.setPropertyType(staticData);
				propertyData.setOnlineInd(rs.getString("OnlineInd"));
				propertyData.setAddressList(getAddressFromProperty(propertyId));

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return propertyData;
	}

	/**
	 * This method is used to Delete the existing Property details
	 *
	 * @return
	 */
	public boolean deletePropertyData(String propertyId) {

		logger.debug("Entry");

		String query = "UPDATE Property SET IsActive=? where PropertyId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVenueToProperty(String propertyId, String venueId, String sortOrder) {

		logger.debug("Entry");

		String query = "INSERT INTO Property_Venue(PropertyId,VenueId,SortOrder,IsActive) values(?,?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, venueId);
			stmt.setString(3, sortOrder);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVenueFromProperty(String propertyId, String venueId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Venue SET IsActive=? where PropertyId=? AND VenueId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			stmt.setString(3, venueId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VenueData> getVenueFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "Select * from venue v, property_venue pv, venueType vt where vt.venueTypeId = v.venueTypeId and pv.venueId = v.venueId "
				+ "and pv.propertyId = ? and pv.isactive = ? and v.isactive = ? and vt.isactive = ?;";
		List<VenueData> venueDataList = new ArrayList<VenueData>();

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				VenueData venueData = new VenueData();
				venueData.setVenueId(rs.getString("VenueId"));
				venueData.setVenueName(rs.getString("VenueName"));
				venueData.setVenueDescription(rs.getString("VenueDescription"));
				venueData.setLength(rs.getInt("Length"));
				venueData.setWidth(rs.getInt("Width"));
				venueData.setHeight(rs.getInt("Height"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VenueTypeId"));
				staticData.setText(rs.getString("VenueTypeText"));
				venueData.setVenueType(staticData);
				venueData.setMinBookingAmount(rs.getDouble("minBookingAmount"));

				venueDataList.add(venueData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return venueDataList;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addAddressToProperty(String propertyId, String addressId) {

		logger.debug("Entry");

		String query = "INSERT INTO Property_Address(PropertyId,AddressId,IsActive) values(?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, addressId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addEmailToProperty(String propertyId, String emailId) {

		logger.debug("Entry");

		String query = "INSERT INTO Property_Email(PropertyId,EmailId,IsActive) values(?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, emailId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteEmailFromProperty(String propertyId, String emailId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Email SET IsActive=? where PropertyId = ? AND EmailId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			stmt.setString(3, emailId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllEmailFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Email SET IsActive=? where PropertyId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}
	

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllAddressFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Address SET IsActive=? where PropertyId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAddressFromProperty(String propertyId, String addressId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Address SET IsActive=? where PropertyId = ? AND AddressId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			stmt.setString(3, addressId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to email for a property
	 *
	 * @return
	 */
	public List<EmailData> getEmailForProperty(String propertyId) {
		logger.debug("Entry");

		String query = "Select * from email e, property_email pe where pe.emailId = e.Id and pe.propertyId = ? "
				+ "and pe.isactive = ? and e.isactive = ?;";
		List<EmailData> emailDataList = new ArrayList<EmailData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				EmailData emailData = new EmailData();
				emailData.setEmailAddress(rs.getString("EmailId"));
				emailData.setEmailId(rs.getString("Id"));
				emailData.setEmailTypeId(rs.getString("EmailTypeId"));
				emailDataList.add(emailData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return emailDataList;

	}

	/**
	 * This method is used to get address for a property
	 *
	 * @return
	 */
	public List<AddressData> getAddressFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "Select * from address a, property_address pa where pa.addressId = a.addressId and pa.propertyId = ? "
				+ "and pa.isactive = ? and a.isactive = ?;";
		List<AddressData> addressDataList = new ArrayList<AddressData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				AddressData addressData = new AddressData();
				addressData.setAddressId(rs.getString("addressId"));
				addressData.setAddress1(rs.getString("Address1"));
				addressData.setAddress2(rs.getString("Address2"));
				addressData.setCity(rs.getString("City"));
				addressData.setStateId(rs.getString("StateId"));
				addressData.setCountryId(rs.getString("CountryId"));
				addressData.setAddressTypeId(rs.getString("addressTypeId"));
				addressData.setPincode(rs.getString("pincode"));
				addressDataList.add(addressData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return addressDataList;
	}

	/**
	 * This method is used to get address for a property
	 *
	 * @return
	 */
	public List<PhoneData> getPhoneNumbersForProperty(String propertyId) {

		logger.debug("Entry");

		String query = "Select * from address a, property_address pa, address_phone ap, phone p where pa.addressId=ap.addressId and pa.addressId = a.addressId "
				+ "and pa.propertyId = ? and p.phoneId = ap.phoneId " + "and pa.isactive = ? and a.isactive = ? and ap.isactive=?;";
		List<PhoneData> phoneDataList = new ArrayList<PhoneData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				PhoneData phoneData = new PhoneData();
				phoneData.setPhoneId(rs.getString("PhoneId"));
				phoneData.setPhoneNumber(rs.getString("PhoneNo"));
				phoneData.setPhoneTypeId(rs.getString("PhoneTypeId"));
				phoneDataList.add(phoneData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return phoneDataList;
	}

	public List<AddressData> getUniqueAddressFromProperty() {

		logger.debug("Entry");

		String query = "SELECT DISTINCT City from address where IsActive=?";
		List<AddressData> addressDataList = new ArrayList<AddressData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {

			stmt.setInt(1, Constants.IS_ACTIVE_YES);

			rs = getResultSet(stmt);
			while (rs.next()) {
				AddressData addressData = new AddressData();

				addressData.setCity(rs.getString("City"));

				addressDataList.add(addressData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return addressDataList;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addAmenitiesToProperty(String propertyId, String amenitiesId) {

		logger.debug("Entry");

		String query = "INSERT INTO Property_Amenities(PropertyId,AmenitiesId,IsActive) values(?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, amenitiesId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAmenitiesFromProperty(String propertyId, String amenitiesId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Amenities SET IsActive = ? where PropertyId = ? AND AmenitiesId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			stmt.setString(3, amenitiesId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteAllAmenitiesFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Amenities SET IsActive = ? where PropertyId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}
	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<AmenitiesData> getAmenitiesFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "Select * from amenities a, property_amenities pa where pa.amenitiesId = a.amenitiesId and pa.propertyId = ? and pa.isactive = ? and a.isactive = ?;";
		List<AmenitiesData> amenitiesDataList = new ArrayList<AmenitiesData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				AmenitiesData amenitiesData = new AmenitiesData();
				amenitiesData.setAmenitiesId(rs.getString("amenitiesId"));
				amenitiesData.setAmenitiesText(rs.getString("amenitiesText"));
				amenitiesData.setAmenitiesIcon(rs.getString("amenitiesIcon"));
				amenitiesData.setAmenitiesDescription(rs.getString("amenitiesDescription"));
				amenitiesData.setSortOrder(rs.getInt("sortOrder"));
				amenitiesData.setShowInFilter(rs.getString("showInFilter"));

				amenitiesDataList.add(amenitiesData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return amenitiesDataList;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addVendorToProperty(String propertyId, String vendorId) {

		logger.debug("Entry");

		String query = "INSERT INTO Property_Vendor(PropertyId,VendorId,IsActive) values(?,?,?);";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, vendorId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteVendorFromProperty(String propertyId, String vendorId) {

		logger.debug("Entry");

		String query = "UPDATE Property_Vendor SET IsActive=? where PropertyId = ? AND VendorId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			stmt.setString(3, vendorId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<VendorData> getVendorFromProperty(String propertyId) {

		logger.debug("Entry");

		String query = "Select * from vendor v, property_vendor pv where pv.vendorId = v.vendorId and pv.propertyId = ? and pv.isactive = ? and v.isactive = ?;";
		List<VendorData> vendorDataList = new ArrayList<VendorData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				VendorData vendorData = new VendorData();
				vendorData.setVendorId(rs.getString("vendorId"));
				vendorData.setVendorName(rs.getString("vendorName"));
				vendorData.setVendorDescription(rs.getString("vendorDescription"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VendorTypeId"));
				vendorData.setVendorType(staticData);
				vendorData.setImageId(rs.getString("imageId"));

				vendorDataList.add(vendorData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return vendorDataList;
	}

	public boolean addInfoToProperty(PropertyInfoData propertyInfo) {
		logger.debug("Entry");

		String query = "INSERT INTO property_information(PropertyId,Title,Value,Icon,IsActive) values(?,?,?,?,?);";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, propertyInfo.getPropertyId());
			stmt.setString(2, propertyInfo.getTitle());
			stmt.setString(3, propertyInfo.getValue());
			stmt.setString(4, propertyInfo.getIcon());
			stmt.setInt(5, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public boolean updateInfoForProperty(String propertyInfoId, PropertyInfoData propertyInfo) {
		logger.debug("Entry");
		String query = "UPDATE property_information SET Title = ? , Value = ? , Icon = ? where PropertyInfoId = ? AND IsActive=?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {

			stmt.setString(1, propertyInfo.getTitle());
			stmt.setString(2, propertyInfo.getValue());
			stmt.setString(3, propertyInfo.getIcon());
			stmt.setString(4, propertyInfoId);
			stmt.setInt(5, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public boolean deleteInfoFromProperty(String propertyInfoId) {
		logger.debug("Entry");

		String query = "UPDATE property_information SET IsActive=? where PropertyInfoId = ?";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyInfoId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public PropertyInfoData getInfoFromProperty(String propertyInfoId) {
		logger.debug("Entry");

		String query = "SELECT * FROM property_information where PropertyInfoId = ? IsActive = ?;";
		PropertyInfoData propertyInfoData = new PropertyInfoData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyInfoId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				propertyInfoData.setPropertyInfoId(propertyInfoId);
				propertyInfoData.setPropertyId(rs.getString("PropertyId"));
				propertyInfoData.setTitle(rs.getString("Title"));
				propertyInfoData.setValue(rs.getString("Value"));
				propertyInfoData.setIcon(rs.getString("Icon"));
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return propertyInfoData;
	}

	@Override
	public List<PropertyInfoData> getAllInfoFromProperty(String propertyId) {
		logger.debug("Entry");

		String query = "SELECT * FROM property_information where PropertyId = ? AND IsActive = ?;";
		List<PropertyInfoData> propertyInfoList = new ArrayList<PropertyInfoData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				PropertyInfoData propertyInfoData = new PropertyInfoData();
				propertyInfoData.setPropertyInfoId(rs.getString("PropertyInfoId"));
				propertyInfoData.setPropertyId(propertyId);
				propertyInfoData.setTitle(rs.getString("Title"));
				propertyInfoData.setValue(rs.getString("Value"));
				propertyInfoData.setIcon(rs.getString("Icon"));
				propertyInfoData.setIsActive(rs.getInt("IsActive"));

				propertyInfoList.add(propertyInfoData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return propertyInfoList;
	}

	@Override
	public boolean addImageToProperty(String propertyId, String imageId, int isPrimary) {
		logger.debug("Entry");
		String query = "INSERT INTO property_image(PropertyId, ImageId, IsPrimary, IsActive) VALUES (?, ?, ?, ?);";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, imageId);
			stmt.setInt(3, isPrimary);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public boolean updateImageOfProperty(String propertyId, String imageId, int isPrimary) {
		logger.debug("Entry");

		String query = "UPDATE property_image SET IsPrimary=? WHERE PropertyId = ? AND ImageId = ?";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, isPrimary);
			stmt.setString(2, propertyId);
			stmt.setString(3, imageId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public boolean deleteImageFromProperty(String propertyId, String imageId, boolean deletePrimaryFlag) {
		logger.debug("Entry");
		String query = "UPDATE property_image SET IsActive = ? WHERE PropertyId = ?";
		if (imageId != null) {
			query += " and imageId = ?";
			if (deletePrimaryFlag == true) {

			} else {
				query += " and IsPrimary != ?";
			}
		} else {
			if (deletePrimaryFlag == true) {

			} else {
				query += " and IsPrimary != ?";
			}
		}
		query += ";";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, propertyId);
			if (imageId != null) {
				stmt.setString(3, imageId);
				if (deletePrimaryFlag == true) {
				} else {
					stmt.setInt(4, Constants.IS_PRIMARY_YES);
				}
			} else {
				if (deletePrimaryFlag == true) {

				} else {
					stmt.setInt(3, Constants.IS_PRIMARY_YES);
				}
			}

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public ImageData getImageFromProperty(String propertyId, String imageId) {
		logger.debug("Entry");
		String query = "SELECT * FROM Image I, property_image pi WHERE pi.ImageId = I.ImageId AND pi.PropertyId = ? AND pi.ImageId = ? AND pi.IsActive = ? AND I.IsActive = ?;";
		ImageData imageData = new ImageData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, imageId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				imageData.setImageId(rs.getString("ImageId"));
				imageData.setPath(rs.getString("Path"));
				imageData.setHeight(rs.getInt("Height"));
				imageData.setWidth(rs.getInt("Width"));
				imageData.setAltText(rs.getString("AltText"));
				imageData.setImageLink(rs.getString("ImageLink"));
				imageData.setLength(rs.getInt("Length"));
				imageData.setIsPrimary(rs.getInt("IsPrimary"));
				imageData.setCaption(rs.getString("Caption"));
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return imageData;
	}

	@Override
	public List<ImageData> getAllImageFromProperty(String propertyId) {
		logger.debug("Entry");
		String query = "SELECT * FROM Image I, property_image pi WHERE pi.ImageId = I.ImageId AND pi.PropertyId = ? AND pi.IsActive = ? AND I.IsActive = ?;";
		List<ImageData> imageDataList = new ArrayList<ImageData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				ImageData imageData = new ImageData();
				imageData.setImageId(rs.getString("ImageId"));
				imageData.setPath(rs.getString("Path"));
				imageData.setHeight(rs.getInt("Height"));
				imageData.setWidth(rs.getInt("Width"));
				imageData.setAltText(rs.getString("AltText"));
				imageData.setImageLink(rs.getString("ImageLink"));
				imageData.setLength(rs.getInt("Length"));
				imageData.setIsPrimary(rs.getInt("IsPrimary"));
				imageData.setCaption(rs.getString("Caption"));

				imageDataList.add(imageData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return imageDataList;
	}

}
