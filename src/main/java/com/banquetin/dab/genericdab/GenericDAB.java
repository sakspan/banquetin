package com.banquetin.dab.genericdab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.dataobjects.EmailData;
import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.PhoneData;
import com.banquetin.dataobjects.PromoData;
import com.banquetin.dataobjects.SlotTypeData;
import com.banquetin.util.Constants;

public class GenericDAB extends BaseDAB implements IGenericDAB {

	// Singleton Instance of the class
	private static IGenericDAB INSTANCE = new GenericDAB();

	private static final Logger logger = Logger.getLogger(GenericDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private GenericDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 * 
	 * @return
	 */
	static IGenericDAB getInstance() {
		return INSTANCE;
	}

	public String getImagePath(String imageId) {
		logger.debug("Entry");

		String query = "SELECT Path from image where ImageId = ? and IsActive = ?";
		String path = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		try {
			stmt.setString(1, imageId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				path = rs.getString("Path");
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return path;
	}

	/**
	 * This method is used to get the Details about all the images in the system.
	 * 
	 * @return
	 */
	public Map<String, ImageData> getImageMap() {

		logger.debug("Entry");

		String query = "SELECT * from image";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		Map<String, ImageData> imageMap = new HashMap<String, ImageData>();

		try {
			rs = getResultSet(stmt);
			while (rs.next()) {
				ImageData imageData = new ImageData();
				imageData.setImageId(rs.getString("ImageId"));
				imageData.setPath(rs.getString("Path"));
				imageData.setHeight(rs.getInt("Height"));
				imageData.setWidth(rs.getInt("Width"));
				imageData.setAltText(rs.getString("AltText"));
				imageData.setImageLink(rs.getString("ImageLink"));
				imageMap.put(imageData.getImageId(), imageData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return imageMap;
	}

	/**
	 * This method is used to update the subscription for user
	 * 
	 * @return
	 */
	public boolean updateSubscription(String emailId, boolean subsribe) {

		logger.debug("Entry");

		String shouldSubscribe = "Y";

		if (!subsribe)
			shouldSubscribe = "N";

		String query = "UPDATE newsletter SET Subsribed = ?  WHERE EmailId = ?";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int rowsUpdated = 0;

		try {
			stmt.setString(1, shouldSubscribe);
			stmt.setString(2, emailId);
			rowsUpdated = stmt.executeUpdate();

			if (rowsUpdated == 0) {
				query = "INSERT INTO javacommerce.newsletter(EmailId,Subsribed) VALUES (?,?)";
				stmt = getPreparedStatement(conn, query);
				stmt.setString(1, emailId);
				stmt.setString(2, shouldSubscribe);
				rowsUpdated = stmt.executeUpdate();
			}

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		if (rowsUpdated > 0)
			return true;
		else
			return false;
	}

	/**
	 * This method is used to get the value for the setting
	 * 
	 * @return
	 */
	public String getSettings(String settingName) {

		logger.debug("Entry");

		String query = "SELECT *  FROM settings WHERE SettingText = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		String settingValue = "";

		try {
			stmt.setString(1, settingName);
			rs = getResultSet(stmt);
			while (rs.next()) {
				settingValue = rs.getString("SettingValue");
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return settingValue;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addPhoneToAddress(String addressId, String phoneId) {

		logger.debug("Entry");

		String query = "INSERT INTO address_phone(AddressId,PhoneId,IsActive) values(?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, addressId);
			stmt.setString(2, phoneId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to add email to property.
	 *
	 * @return
	 */
	public boolean addEmailToProperty(String emailId, String propertyId) {

		logger.debug("Entry");

		String query = "INSERT INTO property_email(propertyId,emailId,IsActive) values(?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, propertyId);
			stmt.setString(2, emailId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to adds the phone number
	 * 
	 * @return
	 */
	public String addPhoneData(PhoneData phoneData) {
		logger.debug("Entry");

		String query = "INSERT INTO phone(PhoneNo,PhoneTypeId,isActive) VALUES (?,?,?)";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);
		String phoneId = null;

		try {
			stmt.setString(1, phoneData.getPhoneNumber());
			stmt.setString(2, phoneData.getPhoneTypeId());
			stmt.setInt(3, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				phoneId = rs.getString(1);
			}

			phoneData.setPhoneId(phoneId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return phoneId;
	}

	/**
	 * This method is used to adds the address
	 * 
	 * @return
	 */
	public String addAddressData(AddressData addressData) {

		logger.debug("Entry");

		String query = "INSERT INTO address(Address1,Address2,City,StateId,CountryId,Pincode,AddressTypeId,isActive) VALUES (?,?,?,?,?,?,?,?)";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);
		String addressId = null;

		try {
			stmt.setString(1, addressData.getAddress1());
			stmt.setString(2, addressData.getAddress2());
			stmt.setString(3, addressData.getCity());
			stmt.setString(4, addressData.getStateId());
			stmt.setString(5, addressData.getCountryId());
			stmt.setString(6, addressData.getPincode());
			stmt.setString(7, addressData.getAddressTypeId());
			stmt.setInt(8, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				addressId = rs.getString(1);
			}

			addressData.setAddressId(addressId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return addressId;
	}

	public List<SlotTypeData> getSlotTypes() {

		logger.debug("Entry");

		List<SlotTypeData> slotTypeList = new ArrayList<SlotTypeData>();

		String query = "SELECT *  FROM slottype WHERE isActive = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				SlotTypeData slotTypeData = new SlotTypeData();
				slotTypeData.setSlotTypeId(rs.getString("SlotTypeId"));
				slotTypeData.setSlotEndTime(rs.getString("SlotEndTime"));
				slotTypeData.setSlotStartTime(rs.getString("SlotStartTime"));
				slotTypeData.setSlotTypeText(rs.getString("SlotTypeText"));

				slotTypeList.add(slotTypeData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return slotTypeList;
	}

	/**
	 * This method is used to adds the address
	 * 
	 * @return
	 */
	public String addEmailData(EmailData emailData) {

		logger.debug("Entry");

		String query = "INSERT INTO email(EmailId,EmailTypeId,isActive) VALUES (?,?,?)";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);
		String emailId = null;

		try {
			stmt.setString(1, emailData.getEmailAddress());
			stmt.setString(2, emailData.getEmailTypeId());
			stmt.setInt(3, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				emailId = rs.getString(1);
			}

			emailData.setEmailId(emailId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return emailId;
	}

	/**
	 * This method is used to validate email exist
	 * 
	 * @return
	 */
	public boolean validateEmailExist(String emailId) {

		logger.debug("Entry");

		String query = "select * from email where emailid = ? and isActive = ?";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		boolean recordFound = false;
		ResultSet rs = null;

		try {
			stmt.setString(1, emailId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);

			rs = stmt.executeQuery();

			if (rs.next())
				recordFound = true;

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return recordFound;
	}

	/**
	 * This method is used to validate phone exist
	 * 
	 * @return
	 */
	public boolean validatePhoneExist(String phoneNum) {

		logger.debug("Entry");

		String query = "select * from phone where phoneNo = ? and isActive = ?";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		boolean recordFound = false;
		ResultSet rs = null;
		
		try {
			stmt.setString(1, phoneNum);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);

			rs = stmt.executeQuery();
			
			if (rs.next())
				recordFound = true;

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return recordFound;
	}

	@Override
	public boolean addConfiguration(String propertyId, PromoData promoData) {
		logger.debug("Entry");
		int rowCount = 0;
		String query = "INSERT INTO Configuration(PropertyId,PromoName,PromoDesc,PromoVal,IsActive) values(?,?,?,?,?);";
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {
			stmt.setInt(1, Integer.valueOf(propertyId));
			stmt.setString(2, promoData.getPromoName());
			stmt.setString(3, promoData.getPromoDescription());
			stmt.setString(4, promoData.getPromoValue());
			stmt.setInt(5, Constants.IS_ACTIVE_YES);
			rowCount = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return rowCount > 0 ? true : false;
	}

	@Override
	public boolean updateConfiguration(PromoData promoData) {
		logger.debug("Entry");
		String query = "UPDATE Configuration SET PromoName = ? , PromoDesc = ? , PromoVal = ? where PromoId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, promoData.getPromoName());
			stmt.setString(2, promoData.getPromoDescription());
			stmt.setString(3, promoData.getPromoValue());
			stmt.setString(4, promoData.getPromoId());
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}
		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public boolean deleteConfiguration(String promoId) {
		logger.debug("Entry");
		String query = "UPDATE configuration SET IsActive=? where promoId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setInt(2, Integer.valueOf(promoId));
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 1 ? true : false;
	}

	@Override
	public List<PromoData> getConfigurations(String propertyId) {
		logger.debug("Entry");
		List<PromoData> promoList = new ArrayList<>();
		String query = "SELECT PromoId,PromoName,PromoDesc,PromoVal,IsActive FROM configuration WHERE PropertyId = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Integer.valueOf(propertyId));
			rs = getResultSet(stmt);
			while (rs.next()) {
				PromoData promoData = new PromoData();
				promoData.setPromoId((rs.getString("PromoId")));
				promoData.setPromoName(rs.getString("PromoName"));
				promoData.setPromoDescription(rs.getString("PromoDesc"));
				promoData.setPromoValue(rs.getString("PromoVal"));
				promoData.setIsActive(rs.getInt("IsActive"));
				promoList.add(promoData);
			}
		} catch (Exception e) {
			logger.error("Exception Occured while retrieving", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return promoList;
	}

	@Override
	public boolean addUserAddressData(BookingData bookingData) {
		logger.debug("Entry");
		String query = "INSERT INTO user_address(UserId,AddressId,IsActive) VALUES (?,?,?)";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);
		int rowCount = 0;
		try {
			stmt.setString(1, bookingData.getUserId());
			stmt.setString(2, bookingData.getAddressId());
			stmt.setInt(3, Constants.IS_ACTIVE_YES);

			rowCount = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}
		logger.debug("Exit");
		return rowCount > 0 ? true : false;
	}

	@Override
	public AddressData getAddressData(String UserId) {
		logger.debug("Entry");
		AddressData addressData = new AddressData();
		String query = "select AddressId,Address1,Address2,City,StateId,PinCode from address WHERE addressId in (select AddressId from user_address where UserId = ?) ;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		try {
			stmt.setInt(1, Integer.valueOf(UserId));
			rs = getResultSet(stmt);
			while (rs.next()) {
				
				addressData.setAddressId(String.valueOf(rs.getInt("AddressId")));
				addressData.setAddress1(rs.getString("Address1"));
				addressData.setAddress2(rs.getString("Address2"));
				addressData.setCity(rs.getString("City"));
				addressData.setStateId(rs.getString("StateId"));
				addressData.setPincode(rs.getString("PinCode"));
				
			}
		} catch (Exception e) {
			logger.error("Exception Occured while retrieving", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return addressData;
	}
}
