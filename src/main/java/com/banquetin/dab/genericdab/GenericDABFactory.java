package com.banquetin.dab.genericdab;

import com.banquetin.dab.BaseDABFactory;

public class GenericDABFactory extends BaseDABFactory {

	public static IGenericDAB getDABInstance() {
		return GenericDAB.getInstance();
	}

}
