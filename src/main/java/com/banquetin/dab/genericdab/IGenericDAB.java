package com.banquetin.dab.genericdab;

import java.util.List;
import java.util.Map;

import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.dataobjects.EmailData;
import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.PhoneData;
import com.banquetin.dataobjects.PromoData;
import com.banquetin.dataobjects.SlotTypeData;

public interface IGenericDAB {

	/**
	 * This method is used to update the subscription for user
	 * 
	 * @return
	 */
	public boolean updateSubscription(String emailId, boolean subsribe);

	/**
	 * This method is used to get the Details about all the images in the system.
	 * 
	 * @return
	 */
	public Map<String, ImageData> getImageMap();

	/**
	 * This method is used to get the value for the setting
	 * 
	 * @return
	 */
	public String getSettings(String settingName);

	/**
	 * This method is used to adds the address
	 * 
	 * @return
	 */
	public String addAddressData(AddressData addressData);

	/**
	 * This method is used to adds the address
	 * 
	 * @return
	 */
	public String addEmailData(EmailData emailData);

	public String getImagePath(String amenitiesIcon);

	public List<SlotTypeData> getSlotTypes();

	/**
	 * This method is used to adds the phone number
	 * 
	 * @return
	 */
	public String addPhoneData(PhoneData phoneData);

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addPhoneToAddress(String addressId, String phoneId);

	public boolean addEmailToProperty(String emailId, String propertyId);

	public boolean addConfiguration(String propertyId, PromoData promoData);

	public boolean updateConfiguration(PromoData promoData);

	public boolean deleteConfiguration(String promoId);

	public List<PromoData> getConfigurations(String propertyId);

	/**
	 * This method is used to validate phone exist
	 * 
	 * @return
	 */
	public boolean validatePhoneExist(String phoneNum);

	/**
	 * This method is used to validate email exist
	 * 
	 * @return
	 */
	public boolean validateEmailExist(String emailId);
	
	public boolean addUserAddressData(BookingData bookingData);
	
	public AddressData getAddressData(String addressId);
}
