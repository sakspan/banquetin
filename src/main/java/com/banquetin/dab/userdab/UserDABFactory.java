package com.banquetin.dab.userdab;

import com.banquetin.dab.BaseDABFactory;

public class UserDABFactory extends BaseDABFactory {
	public static IUserDAB getDABInstance() {
		return UserDAB.getInstance();
	}
}
