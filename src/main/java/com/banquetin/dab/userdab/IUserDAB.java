package com.banquetin.dab.userdab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IUserDAB {

	/**
	 * Check if user already exist
	 * 
	 * @return
	 */
	public boolean checkIfCustomerExist(CustomerData customerData);
	/**
	 * This method is used to create a new customer record
	 * 
	 * @return
	 */
	public boolean createNewCustomer(CustomerData customerData);


	/**
	 * This method is used to update existing customer record
	 * 
	 * @return
	 */
	public boolean updateCustomer(CustomerData customerData);

	/**
	 * This method is used to validate the login details of the customer
	 * 
	 * @return
	 */
	public boolean customerLogin(CustomerData customerData);

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addUserData(UserData userData);

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updateUserData(UserData userData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<UserData> getUserList();

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public UserData getUserData(String userId);

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deleteUserData(String userId);

	public List<UserData> getFeedBackList();

	public String userLogin(UserData userData);

	public boolean addPropertyToUser(String userId, String propertyId);

	public boolean updateUserProperty(String userId, String propertyId);

	public PropertyData getUserPropertyData(String userId, String propertyId);

	public List<PropertyData> getUserPropertyList(String userId);

	public boolean deletePropertyFromUser(String userId, String propertyId);
}
