package com.banquetin.dab.userdab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.BaseDAB;
import com.banquetin.dab.propertydab.PropertyDABFactory;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.CustomerData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.util.Constants;
import com.banquetin.util.PasswordEncryptionService;

public class UserDAB extends BaseDAB implements IUserDAB {

	// Singleton Instance of the class
	private static IUserDAB INSTANCE = new UserDAB();

	private static final Logger logger = Logger.getLogger(UserDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private UserDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IUserDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * Check if user already exist
	 * 
	 * @return
	 */
	public boolean checkIfCustomerExist(CustomerData customerData) {

		logger.debug("Entry");

		String query = "SELECT * FROM customer WHERE EmailId = ? or Telephone = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		boolean isSuccess = false;
		ResultSet rs = null;

		try {

			stmt.setString(1, customerData.getEmailId());
			stmt.setString(2, customerData.getTelephone());
			rs = stmt.executeQuery();

			if (rs.next()) {
				isSuccess = true;
			}

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return isSuccess;
	}

	/**
	 * This method is used to create a new customer record
	 * 
	 * @return
	 */
	public boolean createNewCustomer(CustomerData customerData) {

		logger.debug("Entry");

		String query = "INSERT INTO customer(FirstName,LastName,EmailId,Telephone,Password,CustomerIP) VALUES (?,?,?,?,?,?)";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);
		boolean customerCreated = false;

		String encryptedPassword = PasswordEncryptionService.getEncryptedPassword(customerData.getPassword());

		try {
			stmt.setString(1, customerData.getFirstName());
			stmt.setString(2, customerData.getLastName());
			stmt.setString(3, customerData.getEmailId());
			stmt.setString(4, customerData.getTelephone());
			stmt.setString(5, encryptedPassword);
			stmt.setString(6, customerData.getCustomerIP());
			stmt.executeUpdate();
			customerCreated = true;

			ResultSet rs = stmt.getGeneratedKeys();
			String customerId = null;

			if (rs.next()) {
				customerId = rs.getString(1);
			}

			if (customerId != null)
				customerData.setCustomerId(customerId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return customerCreated;
	}

	/**
	 * This method is used to update existing customer record
	 * 
	 * @return
	 */
	public boolean updateCustomer(CustomerData customerData) {

		logger.debug("Entry");

		// TODO:Populate query and parameters
		String query = null;

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return true;
	}

	/**
	 * This method is used to validate the login details of the customer
	 * 
	 * @return
	 */
	public boolean customerLogin(CustomerData customerData) {

		logger.debug("Entry");

		String query = "SELECT CustomerId, FirstName, LastName, EmailId, Telephone, Password, CustomerIP FROM customer WHERE EmailId = ? and Password = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		boolean isSuccess = false;
		ResultSet rs = null;

		String encryptedPassword = PasswordEncryptionService.getEncryptedPassword(customerData.getPassword());

		try {

			stmt.setString(1, customerData.getEmailId());
			stmt.setString(2, encryptedPassword);
			rs = stmt.executeQuery();

			while (rs.next()) {
				customerData.setCustomerId(rs.getString("CustomerId"));
				customerData.setFirstName(rs.getString("FirstName"));
				customerData.setLastName(rs.getString("LastName"));
				customerData.setEmailId(rs.getString("EmailId"));
				customerData.setTelephone(rs.getString("Telephone"));
				customerData.setPassword(rs.getString("Password"));
				customerData.setCustomerIP(rs.getString("CustomerIP"));
				isSuccess = true;
			}

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return isSuccess;
	}

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addUserData(UserData userData) {

		logger.debug("Entry");

		// TODO:Populate query and parameters
		String query = "INSERT INTO User(UserName,UserPassword,UserEmail,UserRole,UserMobile,IsActive) values(?,?,?,?,?,?);";
		String userId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {

			stmt.setString(1, userData.getUserName());
			stmt.setString(2, userData.getUserPassword());
			stmt.setString(3, userData.getUserEmail());

			if (userData.getUserRoleTypeId() != null)
				stmt.setString(4, userData.getUserRoleTypeId());
			else
				stmt.setString(4, userData.getUserRoleType().getId());

			stmt.setString(5, userData.getUserMobile());
			stmt.setInt(6, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				userId = rs.getString(1);
			}

			userData.setUserId(userId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return userId;
	}

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updateUserData(UserData userData) {

		logger.debug("Entry");

		String query = "UPDATE User SET UserName = ? ,UserPassword = ? ,UserEmail = ? ,UserRole = ? ,UserMobile = ?  where UserId = ? and IsActive =? ;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {

			stmt.setString(1, userData.getUserName());
			stmt.setString(2, userData.getUserPassword());
			stmt.setString(3, userData.getUserEmail());
			stmt.setString(5, userData.getUserMobile());
			stmt.setString(4, userData.getUserRoleType().getId());
			stmt.setString(6, userData.getUserId());
			stmt.setInt(7, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<UserData> getUserList() {

		logger.debug("Entry");

		String query = "SELECT * FROM User where IsActive = ?;";
		List<UserData> userDataList = new ArrayList<UserData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				UserData userData = new UserData();
				userData.setUserId(rs.getString("UserId"));
				userData.setUserName(rs.getString("UserName"));
				userData.setUserPassword(rs.getString("UserPassword"));
				userData.setUserEmail(rs.getString("UserEmail"));
				userData.setUserMobile(rs.getString("UserMobile"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("UserRole"));
				userData.setUserRoleType(staticData);
				userDataList.add(userData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return userDataList;
	}

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public UserData getUserData(String userId) {

		logger.debug("Entry");

		String query = "SELECT * FROM User where UserId = ? and IsActive=?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		UserData userData = new UserData();

		try {
			stmt.setString(1, userId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				userData.setUserId(rs.getString("UserId"));
				userData.setUserName(rs.getString("UserName"));
				userData.setUserPassword(rs.getString("UserPassword"));
				userData.setUserEmail(rs.getString("UserEmail"));
				userData.setUserMobile(rs.getString("UserMobile"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("UserRole"));
				userData.setUserRoleType(staticData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return userData;
	}

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deleteUserData(String userId) {

		logger.debug("Entry");

		String query = "UPDATE User SET IsActive=? where UserId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, userId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public List<UserData> getFeedBackList() {

		logger.debug("Entry");

		String query = "SELECT * FROM user_feedback natural join user where IsActive = ?;";
		List<UserData> feedbackList = new ArrayList<UserData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				UserData feedbackData = new UserData();
				feedbackData.setUserId(rs.getString("UserId"));
				feedbackData.setUserFeedback(rs.getString("Feedback"));
				feedbackData.setUserName(rs.getString("UserName"));
				feedbackData.setUserPassword(rs.getString("UserPassword"));
				feedbackData.setUserEmail(rs.getString("UserEmail"));
				feedbackData.setUserMobile(rs.getString("UserMobile"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("UserRole"));
				feedbackData.setUserRoleType(staticData);

				feedbackList.add(feedbackData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return feedbackList;
	}

	public String userLogin(UserData userData) {

		logger.debug("Entry");

		String query = "SELECT * FROM user WHERE UserName = ? and UserPassword = ?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		String userId = null;
		ResultSet rs = null;

		String encryptedPassword = PasswordEncryptionService.getEncryptedPassword(userData.getUserPassword());

		try {

			stmt.setString(1, userData.getUserName());
			stmt.setString(2, userData.getUserPassword());
			rs = stmt.executeQuery();

			if (rs.next()) {
				userId = rs.getString("userId");
			}

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return userId;
	}

	@Override
	public boolean addPropertyToUser(String userId, String propertyId) {
		logger.debug("Entry");

		String query = "INSERT INTO user_property(userId,propertyId,IsActive) values(?,?,?);";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, userId);
			stmt.setString(2, propertyId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public boolean updateUserProperty(String userId, String propertyId) {
		logger.debug("Entry");

		String query = "UPDATE user_property SET propertyId = ? where UserId = ? and IsActive =? ;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {

			stmt.setString(1, propertyId);
			stmt.setString(2, userId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	@Override
	public PropertyData getUserPropertyData(String userId, String propertyId) {
		logger.debug("Entry");

		String query = "SELECT * FROM property p, user_property up where up.propertyId = ? and up.IsActive=? and p.IsActive = ? and up.userId=? and up.propertyId=p.PropertyId;";
		PropertyData propertyData = new PropertyData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			stmt.setString(4, userId);
			rs = getResultSet(stmt);
			while (rs.next()) {
				propertyData.setPropertyId(rs.getString("PropertyId"));
				propertyData.setPropertyName(rs.getString("PropertyName"));
				propertyData.setStarRating(rs.getString("StarRating"));
				propertyData.setPropertyDescription(rs.getString("PropertyDescription"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("PropertyTypeId"));
				propertyData.setPropertyType(staticData);
				propertyData.setOnlineInd(rs.getString("OnlineInd"));
				propertyData.setAddressList(PropertyDABFactory.getDABInstance().getAddressFromProperty(propertyId));
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return propertyData;
	}

	@Override
	public List<PropertyData> getUserPropertyList(String userId) {
		logger.debug("Entry");

		String query = "SELECT * FROM property p, user_property up where up.IsActive=? and p.IsActive = ? and up.propertyId=p.PropertyId and up.userId = ?;";
		List<PropertyData> propertyDataList = new ArrayList<PropertyData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setString(3, userId);
			rs = getResultSet(stmt);
			while (rs.next()) {
				PropertyData propertyData = new PropertyData();
				propertyData.setPropertyId(rs.getString("PropertyId"));
				propertyData.setPropertyName(rs.getString("PropertyName"));
				propertyData.setStarRating(rs.getString("StarRating"));
				propertyData.setPropertyDescription(rs.getString("PropertyDescription"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("PropertyTypeId"));
				propertyData.setPropertyType(staticData);
				propertyData.setOnlineInd(rs.getString("OnlineInd"));
				propertyData.setAddressList(
						PropertyDABFactory.getDABInstance().getAddressFromProperty(rs.getString("PropertyId")));

				propertyDataList.add(propertyData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return propertyDataList;
	}

	@Override
	public boolean deletePropertyFromUser(String userId, String propertyId) {
		logger.debug("Entry");

		String query = "UPDATE user_property SET IsActive=? where UserId = ? and propertyId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, userId);
			stmt.setString(3, propertyId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

}
