package com.banquetin.dab.venuedab;

import com.banquetin.dab.BaseDABFactory;

public class VenueDABFactory extends BaseDABFactory {

	public static IVenueDAB getDABInstance() {
		return VenueDAB.getInstance();
	}

}
