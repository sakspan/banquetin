package com.banquetin.dab.venuedab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.BaseDAB;
import com.banquetin.dab.propertydab.PropertyDABFactory;
import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.SeatingTypeData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.VenueData;
import com.banquetin.pab.propertypab.IPropertyPAB;
import com.banquetin.pab.propertypab.PropertyPABFactory;
import com.banquetin.util.CommonUtil;
import com.banquetin.util.Constants;

public class VenueDAB extends BaseDAB implements IVenueDAB {

	// Singleton Instance of the class
	private static IVenueDAB INSTANCE = new VenueDAB();
	private static IPropertyPAB PROP_INSTANCE = PropertyPABFactory.getPABInstance();
	private static final Logger logger = Logger.getLogger(VenueDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private VenueDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IVenueDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Venue
	 *
	 * @return
	 */
	public String addVenueData(VenueData venueData) {

		logger.debug("Entry");

		String query = "INSERT INTO Venue(VenueName,VenueDescription,Length,Width,Height,VenueTypeId,MinBookingAmount,IsActive) values(?,?,?,?,?,?,?,?);";
		String venueId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {
			stmt.setString(1, venueData.getVenueName());
			stmt.setString(2, venueData.getVenueDescription());
			stmt.setDouble(3, venueData.getLength());
			stmt.setDouble(4, venueData.getWidth());
			stmt.setDouble(5, venueData.getHeight());
			stmt.setString(6, venueData.getVenueType().getId());
			stmt.setDouble(7, venueData.getMinBookingAmount());
			stmt.setInt(8, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				venueId = rs.getString(1);
			}

			venueData.setVenueId(venueId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return venueId;
	}

	/**
	 * This method is used to Update the Venue details
	 *
	 * @return
	 */
	public boolean updateVenueData(VenueData venueData) {

		logger.debug("Entry");

		String query = "UPDATE Venue SET VenueName=? ,VenueDescription = ? ,Length = ? ,Width = ? ,Height = ? ,VenueTypeId = ? ,MinBookingAmount = ?   where VenueId = ? and IsActive =?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, venueData.getVenueName());
			stmt.setString(2, venueData.getVenueDescription());
			stmt.setInt(3, venueData.getLength());
			stmt.setInt(4, venueData.getWidth());
			stmt.setInt(5, venueData.getHeight());
			stmt.setString(6, venueData.getVenueType().getId());
			stmt.setDouble(7, venueData.getMinBookingAmount());
			stmt.setString(8, venueData.getVenueId());
			stmt.setInt(9, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VenueData> getVenueList() {

		logger.debug("Entry");

		String query = "SELECT * FROM Venue v, property_venue pv, venuetype vt where pv.venueId=v.venueId AND vt.VenueTypeId = v.VenueTypeId AND v.IsActive = ? AND pv.IsActive=?;";
		List<VenueData> venueDataList = new ArrayList<VenueData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				VenueData venueData = new VenueData();
				venueData.setVenueId(rs.getString("VenueId"));
				venueData.setPropertyId(rs.getString("propertyId"));
				venueData.setVenueName(rs.getString("VenueName"));
				venueData.setVenueDescription(rs.getString("VenueDescription"));
				venueData.setLength(rs.getInt("Length"));
				venueData.setWidth(rs.getInt("Width"));
				venueData.setHeight(rs.getInt("Height"));
				venueData.setMinBookingAmount(rs.getDouble("MinBookingAmount"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VenueTypeId"));
				staticData.setText(rs.getString("VenueTypeText"));
				venueData.setVenueType(staticData);
				venueData.setSeatingType(getSeatingTypeFromVenue(rs.getString("VenueId")));

				venueDataList.add(venueData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return venueDataList;
	}

	public List<VenueData> getFilteredVenueList(String startDate, String location, String pax) {
		logger.debug("Entry");
				 
		String query = "SELECT * FROM venue v, property_venue pv, property p, property_address pa, address a, venue_seatingtype vst, seatingtype st, "
				+ "venuetype vt WHERE (v.VenueId=pv.VenueId and pv.PropertyId=p.PropertyId) and (p.PropertyId=pa.PropertyId and pa.AddressId=a.AddressId "
				+ "and a.City = ?) and (v.VenueId=vst.VenueId and vst.SeatingTypeId=st.SeatingTypeId and ? BETWEEN vst.MinCapacity and vst.MaxCapacity) "
				+ "and v.VenueId NOT IN (SELECT blockdates.VenueId FROM blockdates WHERE ? >= blockdates.StartDate and ? <= blockdates.EndDate) "
				+ "and v.VenueId NOT IN (SELECT booking.VenueId FROM booking WHERE ? >= booking.StartDate and ? <= booking.EndDate and booking.IsActive = 1 "
				+ "and booking.StartTypeId =1 and booking.StartTypeId=2 ) "
				+ "and vt.VenueTypeId=v.VenueTypeId and v.IsActive = ? and pv.IsActive = ? and p.IsActive = ? and pa.isActive = ? and a.IsActive = ? "
				+ "and vst.IsActive = ? and st.IsActive = ?;";

		List<VenueData> venueDataList = new ArrayList<VenueData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, location);
			stmt.setString(2, pax);
			stmt.setString(3, CommonUtil.convertStringTDate(startDate));
			stmt.setString(4, CommonUtil.convertStringTDate(startDate));
			stmt.setString(5, CommonUtil.convertStringTDate(startDate));
			stmt.setString(6, CommonUtil.convertStringTDate(startDate));
			stmt.setInt(7, Constants.IS_ACTIVE_YES);
			stmt.setInt(8, Constants.IS_ACTIVE_YES);
			stmt.setInt(9, Constants.IS_ACTIVE_YES);
			stmt.setInt(10, Constants.IS_ACTIVE_YES);
			stmt.setInt(11, Constants.IS_ACTIVE_YES);
			stmt.setInt(12, Constants.IS_ACTIVE_YES);
			stmt.setInt(13, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				VenueData venueData = new VenueData();
				venueData.setVenueId(rs.getString("VenueId"));
				venueData.setPropertyId(rs.getString("propertyId"));
				venueData.setVenueName(rs.getString("VenueName"));
				venueData.setVenueDescription(rs.getString("VenueDescription"));
				venueData.setLength(rs.getInt("Length"));
				venueData.setWidth(rs.getInt("Width"));
				venueData.setHeight(rs.getInt("Height"));
				venueData.setMinBookingAmount(rs.getDouble("MinBookingAmount"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VenueTypeId"));
				staticData.setText(rs.getString("VenueTypeText"));
				venueData.setVenueType(staticData);
				venueData.setPropertyData(PROP_INSTANCE.getPropertyData(rs.getString("PropertyId")));
				venueData.setVenueImageList(getAllImageFromVenue(rs.getString("VenueId")));
				venueData.setSeatingType(getSeatingTypeFromVenue(rs.getString("VenueId")));
				venueData.setMorningAvailable(getSlotDetails(Constants.MORNING_SLOT, rs.getString("VenueId"), CommonUtil.convertStringTDate(startDate)));
				venueData.setEveningAvailable(getSlotDetails(Constants.EVENING_SLOT, rs.getString("VenueId"), CommonUtil.convertStringTDate(startDate)));
				venueDataList.add(venueData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return venueDataList;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VenueData> getVenueListForProperty(String propertyId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Venue V, Property_Venue pv, venuetype vt where vt.VenueTypeId=V.VenueTypeId and V.IsActive = ? and pv.IsActive = ? and pv.venueId = V.venueId and pv.propertyId=?;";
		List<VenueData> venueDataList = new ArrayList<VenueData>();

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setString(3, propertyId);

			rs = getResultSet(stmt);
			while (rs.next()) {

				VenueData venueData = new VenueData();
				venueData.setVenueId(rs.getString("VenueId"));
				venueData.setVenueName(rs.getString("VenueName"));
				venueData.setVenueDescription(rs.getString("VenueDescription"));
				venueData.setLength(rs.getInt("Length"));
				venueData.setWidth(rs.getInt("Width"));
				venueData.setHeight(rs.getInt("Height"));
				venueData.setMinBookingAmount(rs.getDouble("MinBookingAmount"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VenueTypeId"));
				staticData.setText(rs.getString("VenueTypeText"));
				venueData.setVenueType(staticData);

				venueDataList.add(venueData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return venueDataList;
	}

	/**
	 * This method is used to Get details for a particular Venue
	 *
	 * @return
	 */
	public VenueData getVenueData(String venueId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Venue V, Property_Venue pv where V.IsActive = ? and pv.IsActive = ? and pv.venueId = V.venueId and V.venueId=?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		VenueData venueData = new VenueData();
		int count = 0;

		try {
			stmt.setString(3, venueId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				System.out.println(count++);
				venueData.setVenueId(rs.getString("VenueId"));
				venueData.setPropertyId(rs.getString("propertyId"));
				venueData.setVenueName(rs.getString("VenueName"));
				venueData.setVenueDescription(rs.getString("VenueDescription"));
				venueData.setLength(rs.getInt("Length"));
				venueData.setWidth(rs.getInt("Width"));
				venueData.setHeight(rs.getInt("Height"));
				venueData.setMinBookingAmount(rs.getDouble("MinBookingAmount"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VenueType"));
				venueData.setVenueType(staticData);

			}
		} catch (Exception e) {

			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return venueData;
	}

	/**
	 * This method is used to Delete the existing Venue details
	 *
	 * @return
	 */
	public boolean deleteVenueData(String venueId) {

		logger.debug("Entry");

		String query = "UPDATE Venue SET IsActive=? where VenueId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {

			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, venueId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addEventTypeToVenue(String venueId, String eventTypeId, String sortOrder) {

		logger.debug("Entry");

		String query = "INSERT INTO Venue_EventType(VenueId,EventTypeId,SortOrder,IsActive) values(?,?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {

			stmt.setString(1, venueId);
			stmt.setString(2, eventTypeId);
			stmt.setString(3, sortOrder);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteEventTypeFromVenue(String venueId, String eventTypeId) {

		logger.debug("Entry");

		String query = "UPDATE Venue_EventType SET IsActive=? where venueId=? and EventTypeId=?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(2, venueId);
			stmt.setString(3, eventTypeId);

			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<StaticData> getEventTypeFromVenue(String venueId) {

		logger.debug("Entry");

		String query = "Select * from eventtype v, venue_eventtype pv where pv.EventTypeId = v.EventTypeId and pv.venueId = ? and pv.isactive = ? and v.isactive = ?;";
		List<StaticData> staticDataList = new ArrayList<StaticData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, venueId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				System.out.println(rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3) + "  "
						+ rs.getString(4) + "  " + rs.getString(5) + "  " + rs.getString(6) + "  " + rs.getString(7)
						+ "  " + rs.getString(8) + "  ");
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("EventTypeId"));
				staticData.setText(rs.getString("EventTypeText"));
				staticDataList.add(staticData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return staticDataList;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean addSeatingTypeToVenue(String venueId, SeatingTypeData seatingTypeData) {

		logger.debug("Entry");

		String query = "INSERT INTO Venue_SeatingType(VenueId,SeatingTypeId,MinCapacity,MaxCapacity,IsActive) values(?,?,?,?,?);";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		int count = 0;

		try {
			stmt.setString(1, venueId);
			stmt.setString(2, seatingTypeData.getSeatingTypeId());
			stmt.setString(3, seatingTypeData.getMinCapacity());
			stmt.setString(4, seatingTypeData.getMaxCapacity());
			stmt.setInt(5, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public boolean deleteSeatingTypeFromVenue(String venueId, String seatingTypeId) {

		logger.debug("Entry");

		String query = "UPDATE Venue_SeatingType SET IsActive=? where venueId=? and VenueSeatingTypeId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, venueId);
			stmt.setString(3, seatingTypeId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to
	 *
	 * @return
	 */
	public List<SeatingTypeData> getSeatingTypeFromVenue(String venueId) {

		logger.debug("Entry");
		String query = "SELECT * FROM Venue_SeatingType vs, seatingtype st where vs.VenueId = ? AND vs.SeatingTypeId=st.SeatingTypeId AND st.IsActive=? AND vs.IsActive = ?;";
		List<SeatingTypeData> seatingTypeDataList = new ArrayList<SeatingTypeData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, venueId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				SeatingTypeData seatingDataType = new SeatingTypeData();
				seatingDataType.setVenueSeatingTypeId(rs.getString("VenueSeatingTypeId"));
				seatingDataType.setSeatingTypeId(rs.getString("SeatingTypeId"));
				seatingDataType.setMaxCapacity(rs.getString("MaxCapacity"));
				seatingDataType.setMinCapacity(rs.getString("MinCapacity"));
				seatingDataType.setSeatingTypeText(rs.getString("SeatingTypeText"));
				seatingTypeDataList.add(seatingDataType);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return seatingTypeDataList;
	}

	/**
	 * This method is used to Create a new Image
	 *
	 * @return
	 */
	public boolean addImageTovenue(String venueId, String imageId, int isPrimary) {
		logger.debug("Entry");
		String query = "INSERT INTO venue_image(venueId, ImageId, IsPrimary, IsActive) VALUES (?, ?, ?, ?);";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, venueId);
			stmt.setString(2, imageId);
			stmt.setInt(3, isPrimary);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public boolean updateImageOfVenue(String venueId, String imageId, int isPrimary) {
		logger.debug("Entry");

		String query = "UPDATE venue_image SET IsPrimary=? WHERE venueId = ? AND ImageId = ?";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, isPrimary);
			stmt.setString(2, venueId);
			stmt.setString(3, imageId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public boolean deleteImageFromVenue(String venueId, String imageId, boolean deletePrimaryFlag) {
		logger.debug("Entry");
		String query = "UPDATE venue_image SET IsActive = ? WHERE venueId = ?";
		if (imageId != null) {
			query += " and imageId = ?";
			if (deletePrimaryFlag == true) {

			} else {
				query += " and IsPrimary != ?";
			}
		} else {
			if (deletePrimaryFlag == true) {

			} else {
				query += " and IsPrimary != ?";
			}
		}
		query += ";";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, venueId);
			if (imageId != null) {
				stmt.setString(3, imageId);
				if (deletePrimaryFlag == true) {
				} else {
					stmt.setInt(4, Constants.IS_PRIMARY_YES);
				}
			} else {
				if (deletePrimaryFlag == true) {

				} else {
					stmt.setInt(3, Constants.IS_PRIMARY_YES);
				}
			}

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public ImageData getImageFromVenue(String venueId, String imageId) {
		logger.debug("Entry");
		String query = "SELECT * FROM Image I, venue_image pi WHERE pi.ImageId = I.ImageId AND pi.venueId = ? AND pi.ImageId = ? AND pi.IsActive = ? AND I.IsActive = ?;";
		ImageData imageData = new ImageData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, venueId);
			stmt.setString(2, imageId);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				imageData.setImageId(rs.getString("ImageId"));
				imageData.setPath(rs.getString("Path"));
				imageData.setHeight(rs.getInt("Height"));
				imageData.setWidth(rs.getInt("Width"));
				imageData.setAltText(rs.getString("AltText"));
				imageData.setImageLink(rs.getString("ImageLink"));
				imageData.setLength(rs.getInt("Length"));
				imageData.setIsPrimary(rs.getInt("IsPrimary"));
				imageData.setCaption(rs.getString("Caption"));
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return imageData;
	}

	public List<ImageData> getAllImageFromVenue(String venueId) {
		logger.debug("Entry");
		String query = "SELECT * FROM Image I, venue_image pi WHERE pi.ImageId = I.ImageId AND pi.venueId = ? AND pi.IsActive = ? AND I.IsActive = ?;";
		List<ImageData> imageDataList = new ArrayList<ImageData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, venueId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				ImageData imageData = new ImageData();
				imageData.setImageId(rs.getString("ImageId"));
				imageData.setPath(rs.getString("Path"));
				imageData.setHeight(rs.getInt("Height"));
				imageData.setWidth(rs.getInt("Width"));
				imageData.setAltText(rs.getString("AltText"));
				imageData.setImageLink(rs.getString("ImageLink"));
				imageData.setLength(rs.getInt("Length"));
				imageData.setIsPrimary(rs.getInt("IsPrimary"));
				imageData.setCaption(rs.getString("Caption"));

				imageDataList.add(imageData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return imageDataList;
	}
	
	public boolean getSlotDetails(int slotId , String venueId, String date) {
		logger.debug("Entry");
		String query = "SELECT * FROM booking WHERE VenueId=? and StartTypeId =? and EndDate = ?;";
		boolean isAvail = false;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Integer.valueOf(venueId));
			stmt.setInt(2, slotId);
			stmt.setString(3, date);
			rs = getResultSet(stmt);
			if (!rs.isBeforeFirst() ) {    
				isAvail =  true; 
			} 
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}
		logger.debug("Exit");
		return isAvail;
	}
}
