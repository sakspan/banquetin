package com.banquetin.dab.partnerdab;

import java.util.*;

import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.pab.userpab.UserPABFactory;
import com.banquetin.util.Constants;

public class PartnerDAB extends BaseDAB implements IPartnerDAB {

	// Singleton Instance of the class
	private static IPartnerDAB INSTANCE = new PartnerDAB();

	private static final Logger logger = Logger.getLogger(PartnerDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private PartnerDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IPartnerDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new User
	 *
	 * @return
	 */
	public String addPartnerData(PartnerData partnerData) {

		logger.debug("Entry");

		
		String query = "INSERT INTO partner(partnerName,imageId,IsActive) values(?,?,?,?,?,?);";
		String partnerId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		
		

		try {
			
			
			stmt.setString(1, partnerData.getPartnerName());
			stmt.setString(2, partnerData.getImageId());
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				partnerId = rs.getString(1);
			}

			partnerData.setPartnerId(partnerId);

		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return partnerId;
	}

	/**
	 * This method is used to Update the User details
	 *
	 * @return
	 */
	public boolean updatePartnerData(PartnerData partnerData) {

		logger.debug("Entry");

		
		String query = "UPDATE User SET partnerName = ? ,imageId = ? where UserId = ? and IsActive =? ;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			
			stmt.setString(1, partnerData.getPartnerName());
			stmt.setString(2, partnerData.getImageId());
			stmt.setString(3, partnerData.getPartnerId());
			stmt.setInt(4, Constants.IS_ACTIVE_YES);


			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<PartnerData> getPartnerList() {

		logger.debug("Entry");

		
		String query = "SELECT * FROM partner V, image i where V.IsActive = ? and i.IsActive = ? and i.ImageId = V.imageId";
		List<PartnerData> partnerDataList = new ArrayList<PartnerData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

			PartnerData partnerData = new PartnerData();
			partnerData.setPartnerId(rs.getString("partnerId"));
			partnerData.setPartnerName(rs.getString("partnerName"));
			partnerData.setImageId(rs.getString("imageId"));
			partnerData.setPath(rs.getString("path"));
			partnerDataList.add(partnerData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return partnerDataList;
	}

	/**
	 * This method is used to Get details for a particular User
	 *
	 * @return
	 */
	public PartnerData getPartnerData(String partnerId) {

		logger.debug("Entry");

		
		String query = "SELECT * FROM User where partnerId = ? and IsActive=?;";

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;
		PartnerData partnerData = new PartnerData();

		try {
			stmt.setString(1, partnerId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				partnerData.setPartnerId(rs.getString("partnerId"));
				partnerData.setPartnerName(rs.getString("partnerName"));
				partnerData.setImageId(rs.getString("imageId"));

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return partnerData;
	}

	/**
	 * This method is used to Delete the existing User details
	 *
	 * @return
	 */
	public boolean deletePartnerData(String partnerId) {

		logger.debug("Entry");

		
		String query = "UPDATE partner SET IsActive=? where partnerId = ?;";
		int count=0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, partnerId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}
	

}
