package com.banquetin.dab.partnerdab;

import com.banquetin.dab.BaseDABFactory;

public class PartnerDABFactory extends BaseDABFactory {
	public static IPartnerDAB getDABInstance() {
		return PartnerDAB.getInstance();
	}
}
