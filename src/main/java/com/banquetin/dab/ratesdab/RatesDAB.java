package com.banquetin.dab.ratesdab;

import java.util.*;
import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.util.CommonUtil;
import com.banquetin.util.Constants;

public class RatesDAB extends BaseDAB implements IRatesDAB {

	// Singleton Instance of the class
	private static IRatesDAB INSTANCE = new RatesDAB();

	private static final Logger logger = Logger.getLogger(RatesDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private RatesDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IRatesDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Rates
	 *
	 * @return
	 */
	public String addRatesData(RatesData ratesData) {

		logger.debug("Entry");

		String query = "INSERT INTO Rates(VenueId,StartDate,EndDate,Rate,RateTypeId,slotTypeId,BookingAmount,IsActive) values(?,?,?,?,?,?,?,?);";
		String ratesDataId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {
			stmt.setString(1, ratesData.getVenueId());
			stmt.setDate(2, CommonUtil.getSQLDate(ratesData.getStartDate()));
			stmt.setDate(3, CommonUtil.getSQLDate(ratesData.getEndDate()));
			stmt.setDouble(4, ratesData.getRate());
			stmt.setString(5, ratesData.getRatesType().getId());
			stmt.setString(6, ratesData.getSlotTypeId());
			stmt.setDouble(7, ratesData.getBookingAmount());
			stmt.setInt(8, Constants.IS_ACTIVE_YES);

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				ratesDataId = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return ratesDataId;
	}

	/**
	 * This method is used to Update the Rates details
	 *
	 * @return
	 */
	public boolean updateRatesData(RatesData ratesData) {

		logger.debug("Entry");

		String query = "UPDATE Rates SET VenueId = ? ,StartDate = ? ,EndDate = ? ,Rate = ? ,RateTypeId = ? ,BookingAmount = ?, slotTypeId= ? where RatesId = ? and IsActive=?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, ratesData.getVenueId());
			stmt.setDate(2, CommonUtil.getSQLDate(ratesData.getStartDate()));
			stmt.setDate(3, CommonUtil.getSQLDate(ratesData.getEndDate()));
			stmt.setDouble(4, ratesData.getRate());
			stmt.setString(5, ratesData.getRatesType().getId());
			stmt.setDouble(6, ratesData.getBookingAmount());
			stmt.setString(7, ratesData.getSlotTypeId());
			stmt.setString(8, ratesData.getRatesId());
			stmt.setInt(9, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<RatesData> getRatesList(String venueId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Rates r, rateType rt, slottype st where r.VenueId = ? and r.IsActive=? and r.slotTypeId = st.slotTypeId "
				+ "and r.rateTypeId = rt.rateTypeId";
		List<RatesData> ratesDatalist = new ArrayList<RatesData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, venueId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				RatesData ratesData = new RatesData();
				ratesData.setVenueId(rs.getString("VenueId"));
				
				StaticData slotType = new StaticData();
				slotType.setId(rs.getString("SlotTypeId"));
				slotType.setText(rs.getString("SlotTypeText"));
				ratesData.setSlotType(slotType);
				
				ratesData.setSlotTypeId(rs.getString("SlotTypeId"));
				ratesData.setRatesId(rs.getString("RatesId"));
				ratesData.setVenueId(rs.getString("VenueId"));
				ratesData.setStartDate(rs.getDate("StartDate"));
				ratesData.setEndDate(rs.getDate("EndDate"));
				ratesData.setRate(rs.getDouble("Rate"));
				ratesData.setBookingAmount(rs.getDouble("BookingAmount"));

				StaticData rateType = new StaticData();
				rateType.setId(rs.getString("RateTypeId"));
				rateType.setText(rs.getString("RateTypeText"));
				ratesData.setRatesType(rateType);				
				
				ratesData.getRatesType().setId(rs.getString("RateTypeId"));

				ratesDatalist.add(ratesData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return ratesDatalist;
	}

	/**
	 * This method is used to Get details for a particular Rates
	 *
	 * @return
	 */
	public RatesData getRatesData(String ratesId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Rates where RatesId = ? and IsActive=?;";
		RatesData ratesData = new RatesData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, ratesId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				ratesData.setRatesId(rs.getString("RatesId"));
				ratesData.setVenueId(rs.getString("VenueId"));
				ratesData.setStartDate(rs.getDate("StartDate"));
				ratesData.setEndDate(rs.getDate("EndDate"));
				ratesData.setRate(rs.getDouble("Rate"));

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return ratesData;
	}

	/**
	 * This method is used to Delete the existing Rates details
	 *
	 * @return
	 */
	public boolean deleteRatesData(String ratesId) {

		logger.debug("Entry");

		String query = "UPDATE Rates SET IsActive=? where RatesId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, ratesId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

}
