package com.banquetin.dab.ratesdab;

import com.banquetin.dab.BaseDABFactory;

public class RatesDABFactory extends BaseDABFactory {

	public static IRatesDAB getDABInstance() {
		return RatesDAB.getInstance();
	}

}
