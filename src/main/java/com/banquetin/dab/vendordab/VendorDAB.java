package com.banquetin.dab.vendordab;

import java.util.*;
import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.util.Constants;

public class VendorDAB extends BaseDAB implements IVendorDAB {

	// Singleton Instance of the class
	private static IVendorDAB INSTANCE = new VendorDAB();

	private static final Logger logger = Logger.getLogger(VendorDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private VendorDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IVendorDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Vendor
	 *
	 * @return
	 */
	public String addVendorData(VendorData vendorData) {

		logger.debug("Entry");

		//TODO:Populate query and parameters
		String query = "INSERT INTO Vendor(VendorName,VendorDescription,VendorTypeId,ImageId,IsActive) values(?,?,?,?,?);";
		String vendorId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		
		

		try {
			
			stmt.setString(1, vendorData.getVendorName());
			stmt.setString(2, vendorData.getVendorDescription());
			stmt.setString(3, vendorData.getVendorType().getId());
			stmt.setString(4, vendorData.getImageId());
			stmt.setInt(5, Constants.IS_ACTIVE_YES);
			
			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				vendorId = rs.getString(1);
			}

			vendorData.setVendorId(vendorId);
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return vendorId;
	}

	/**
	 * This method is used to Update the Vendor details
	 *
	 * @return
	 */
	public boolean updateVendorData(VendorData vendorData) {

		logger.debug("Entry");

		
		String query = "UPDATE Vendor SET VendorName = ? ,VendorDescription = ? ,VendorTypeId = ? ,ImageId = ?   where VendorId = ? and IsActive =?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			
			stmt.setString(1, vendorData.getVendorName());
			stmt.setString(2, vendorData.getVendorDescription());
			stmt.setString(3, vendorData.getVendorType().getId());
			stmt.setString(4, vendorData.getImageId());
			stmt.setString(5, vendorData.getVendorId());
			stmt.setInt(6, Constants.IS_ACTIVE_YES);

			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VendorData> getVendorList() {

		logger.debug("Entry");

		
		String query = "SELECT * FROM Vendor where IsActive = ?;";
		List<VendorData> vendorDataList = new ArrayList<VendorData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs =null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

			VendorData vendorData = new VendorData();

			vendorData.setVendorId(rs.getString("VendorId"));
			vendorData.setVendorName(rs.getString("VendorName"));
			vendorData.setVendorDescription(rs.getString("VendorDescription"));
			StaticData staticData = new StaticData();
			staticData.setId(rs.getString("VendorTypeId"));
			vendorData.setVendorType(staticData);
			vendorData.setImageId(rs.getString("ImageId"));
			vendorDataList.add(vendorData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return vendorDataList;
	}

	/**
	 * This method is used to Get details for a particular Vendor
	 *
	 * @return
	 */
	public VendorData getVendorData(String vendorId) {

		logger.debug("Entry");

		
		String query = "SELECT * FROM Vendor where VendorId = ? and IsActive=?;";
		VendorData vendorData = new VendorData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, vendorId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

			
				vendorData.setVendorId(rs.getString("VendorId"));
				vendorData.setVendorName(rs.getString("VendorName"));
				vendorData.setVendorDescription(rs.getString("VendorDescription"));
				StaticData staticData = new StaticData();
				staticData.setId(rs.getString("VednorTypeId"));
				vendorData.setVendorType(staticData);
				vendorData.setImageId(rs.getString("ImageId"));
				

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return vendorData;
	}

	/**
	 * This method is used to Delete the existing Vendor details
	 *
	 * @return
	 */
	public boolean deleteVendorData(String vendorId) {

		logger.debug("Entry");

		
		String query = "UPDATE Vendor SET IsActive=? where VendorId = ?;";
		int count=0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, vendorId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

}
