package com.banquetin.dab.vendordab;

import com.banquetin.dab.BaseDABFactory;

public class VendorDABFactory extends BaseDABFactory {

	public static IVendorDAB getDABInstance() {
		return VendorDAB.getInstance();
	}

}
