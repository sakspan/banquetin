package com.banquetin.dab.vendordab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IVendorDAB{

	/**
	 * This method is used to Create a new Vendor
	 *
	 * @return
	 */
	public String addVendorData(VendorData vendorData);

	/**
	 * This method is used to Update the Vendor details
	 *
	 * @return
	 */
	public boolean updateVendorData(VendorData vendorData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<VendorData> getVendorList();

	/**
	 * This method is used to Get details for a particular Vendor
	 *
	 * @return
	 */
	public VendorData getVendorData(String vendorId);

	/**
	 * This method is used to Delete the existing Vendor details
	 *
	 * @return
	 */
	public boolean deleteVendorData(String vendorId);

}
