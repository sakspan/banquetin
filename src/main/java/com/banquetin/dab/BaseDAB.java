package com.banquetin.dab;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class BaseDAB {

	public static boolean USE_DIRECT_CONNECTION = true;

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static String DB_URL = "jdbc:mysql://127.0.0.1:3306/banquetindb"; // jdbc:mysql://OPENSHIFT_MYSQL_DB_HOST:OPENSHIFT_MYSQL_DB_PORT/OPENSHIFT_APP_NAME,
	private static String USER = "root";
	private static String PASS = "Apr@2018";

	static {

	}

	private static final Logger logger = Logger.getLogger(BaseDAB.class.getName());

	private Connection getTomcatConnection() {
		logger.debug("Entry");
		Connection conn = null;
		try {
			// Connection
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/payroll");

			conn = ds.getConnection();
		} catch (Exception e) {
			logger.error("Exception", e);
		}

		logger.debug("Exit");
		return conn;

	}

	private static Connection getDirectConnection() {
		logger.debug("Entry");
		Connection conn = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
		}

		logger.debug("Exit");
		return conn;

	}

	public static void main(String[] args) {
		getDirectConnection();

	}

	protected Connection getConnection() {
		logger.debug("Entry");

		Connection conn = null;
		if (USE_DIRECT_CONNECTION)
			conn = getDirectConnection();
		else
			conn = getTomcatConnection();

		logger.debug("Exit");
		return conn;
	}

	protected Statement getStatement(Connection conn) {
		logger.debug("Entry");
		Statement stmt = null;

		if (conn == null) {
			conn = getConnection();
		}

		try {
			stmt = conn.createStatement();
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		logger.debug("Exit");
		return stmt;
	}

	protected PreparedStatement getPreparedStatement(Connection conn, String sql) {
		logger.debug("Entry");
		PreparedStatement stmt = null;

		if (conn == null) {
			conn = getConnection();
		}

		try {
			stmt = conn.prepareStatement(sql);
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		logger.debug("Exit");
		return stmt;
	}

	protected PreparedStatement getPreparedStatementWithIncrementId(Connection conn, String sql) {
		logger.debug("Entry");
		PreparedStatement stmt = null;

		if (conn == null) {
			conn = getConnection();
		}

		try {
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		logger.debug("Exit");
		return stmt;
	}

	protected ResultSet getResultSet(Statement st, String sql) {
		logger.debug("Entry");
		ResultSet rs = null;

		try {
			rs = st.executeQuery(sql);
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		logger.debug("Exit");
		return rs;
	}

	protected ResultSet getResultSet(PreparedStatement st) {
		logger.debug("Entry");
		ResultSet rs = null;

		try {
			rs = st.executeQuery();
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		logger.debug("Exit");
		return rs;
	}

	protected void releaseResources(Connection conn, Statement st, ResultSet rs) {
		logger.debug("Entry");
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			logger.error("Exception", e);
		}
		logger.debug("Exit");

	}


}