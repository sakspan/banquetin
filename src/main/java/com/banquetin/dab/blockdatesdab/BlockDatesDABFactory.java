package com.banquetin.dab.blockdatesdab;

import com.banquetin.dab.BaseDABFactory;

public class BlockDatesDABFactory extends BaseDABFactory {

	public static IBlockDatesDAB getDABInstance() {
		return BlockDatesDAB.getInstance();
	}

}
