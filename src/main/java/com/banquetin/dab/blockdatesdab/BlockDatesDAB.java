package com.banquetin.dab.blockdatesdab;

import java.util.*;
import java.util.Date;
import java.sql.*;
import org.apache.log4j.*;
import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.*;
import com.banquetin.util.CommonUtil;
import com.banquetin.util.Constants;

public class BlockDatesDAB extends BaseDAB implements IBlockDatesDAB {

	// Singleton Instance of the class
	private static IBlockDatesDAB INSTANCE = new BlockDatesDAB();

	private static final Logger logger = Logger.getLogger(BlockDatesDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private BlockDatesDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IBlockDatesDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new BlockDates
	 *
	 * @return
	 */
	public String addBlockDatesData(BlockDatesData blockDatesData) {

		logger.debug("Entry");

		String query = "INSERT INTO BlockDates(VenueId,StartDate,EndDate,StartTypeId,EndTypeId,IsActive) values(?,?,?,?,?,?);";
		String blockDatesId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {
			stmt.setString(1, blockDatesData.getVenueId());
			stmt.setDate(2, CommonUtil.getSQLDate(blockDatesData.getStartDate()));
			stmt.setDate(3, CommonUtil.getSQLDate(blockDatesData.getEndDate()));
			stmt.setString(4, blockDatesData.getStartSlotTypeData().getSlotTypeId());
			stmt.setString(5, blockDatesData.getEndSlotTypeData().getSlotTypeId());
			stmt.setInt(6, Constants.IS_ACTIVE_YES);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				blockDatesId = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return blockDatesId;
	}

	/**
	 * This method is used to Update the BlockDates details
	 *
	 * @return
	 */
	public boolean updateBlockDatesData(BlockDatesData blockDatesData) {

		logger.debug("Entry");

		String query = "UPDATE BlockDates SET VenueId = ? ,StartDate = ? ,EndDate = ? ,StartTypeId = ? ,EndTypeId = ? where BlockDatesId = ? and IsActive=?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setString(1, blockDatesData.getVenueId());
			stmt.setDate(2, CommonUtil.getSQLDate(blockDatesData.getStartDate()));
			stmt.setDate(3, CommonUtil.getSQLDate(blockDatesData.getEndDate()));
			stmt.setString(4, blockDatesData.getStartSlotTypeData().getSlotTypeId());
			stmt.setString(5, blockDatesData.getEndSlotTypeData().getSlotTypeId());
			stmt.setString(6, blockDatesData.getBlockDatesId());
			stmt.setInt(7, Constants.IS_ACTIVE_YES);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<BlockDatesData> getBlockDatesList(String venueId, Date startDate, Date endDate) {

		logger.debug("Entry");

		String query = "SELECT * FROM BlockDates where VenueId = ? and StartDate>=? and EndDate <= ? and IsActive=?;";
		List<BlockDatesData> blockDatesDataList = new ArrayList<BlockDatesData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, venueId);
			stmt.setDate(2, CommonUtil.getSQLDate(startDate));
			stmt.setDate(3, CommonUtil.getSQLDate(endDate));
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				BlockDatesData blockDatesData = new BlockDatesData();
				blockDatesData.setBlockDatesId(rs.getString("BlockDatesId"));
				blockDatesData.setVenueId(rs.getString("VenueId"));
				blockDatesData.setStartDate(rs.getDate("StartDate"));
				blockDatesData.setEndDate(rs.getDate("EndDate"));
				SlotTypeData startSlotTypeData = new SlotTypeData();
				startSlotTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				blockDatesData.setStartSlotTypeData(startSlotTypeData);
				SlotTypeData endSlotTypeData = new SlotTypeData();
				endSlotTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				blockDatesData.setEndSlotTypeData(endSlotTypeData);

				blockDatesDataList.add(blockDatesData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return blockDatesDataList;
	}

	/**
	 * This method is used to Get details for a particular BlockDates
	 *
	 * @return
	 */
	public BlockDatesData getBlockDatesData(String blockDatesId) {

		logger.debug("Entry");

		String query = "SELECT * FROM BlockDates where BlockDatesId = ? and IsActive=?;";
		BlockDatesData blockDatesData = new BlockDatesData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, blockDatesId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {
				blockDatesData.setBlockDatesId(rs.getString("BlockDatesId"));
				blockDatesData.setVenueId(rs.getString("VenueId"));
				blockDatesData.setStartDate(rs.getDate("StartDate"));
				blockDatesData.setEndDate(rs.getDate("EndDate"));
				SlotTypeData startSlotTypeData = new SlotTypeData();
				startSlotTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				blockDatesData.setStartSlotTypeData(startSlotTypeData);
				SlotTypeData endSlotTypeData = new SlotTypeData();
				endSlotTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				blockDatesData.setEndSlotTypeData(endSlotTypeData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return blockDatesData;
	}

	/**
	 * This method is used to Delete the existing BlockDates details
	 *
	 * @return
	 */
	public boolean deleteBlockDatesData(String blockDatesId) {

		logger.debug("Entry");

		String query = "UPDATE BlockDates SET IsActive=? where BlockDatesId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, blockDatesId);
			;
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public List<BlockDatesData> getBlockDatesForProperty(String propertyId, Date startDate, Date endDate) {
		logger.debug("Entry");

		String query = "SELECT * FROM `blockdates`, property_venue WHERE property_venue.PropertyId = ? AND property_venue.VenueId = blockdates.VenueId AND property_venue.IsActive=? AND blockdates.IsActive=? AND StartDate>=? AND EndDate<=?;";
		List<BlockDatesData> blockDatesDataList = new ArrayList<BlockDatesData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			stmt.setInt(3, Constants.IS_ACTIVE_YES);
			stmt.setDate(4, CommonUtil.getSQLDate(startDate));
			stmt.setDate(5, CommonUtil.getSQLDate(endDate));
			rs = getResultSet(stmt);
			while (rs.next()) {
				BlockDatesData blockDatesData = new BlockDatesData();
				blockDatesData.setBlockDatesId(rs.getString("BlockDatesId"));
				blockDatesData.setVenueId(rs.getString("VenueId"));
				blockDatesData.setStartDate(rs.getDate("StartDate"));
				blockDatesData.setEndDate(rs.getDate("EndDate"));
				SlotTypeData startSlotTypeData = new SlotTypeData();
				startSlotTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				blockDatesData.setStartSlotTypeData(startSlotTypeData);
				SlotTypeData endSlotTypeData = new SlotTypeData();
				endSlotTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				blockDatesData.setEndSlotTypeData(endSlotTypeData);

				blockDatesDataList.add(blockDatesData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return blockDatesDataList;
	}
}
