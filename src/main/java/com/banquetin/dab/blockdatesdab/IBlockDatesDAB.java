package com.banquetin.dab.blockdatesdab;

import java.util.*;
import com.banquetin.dataobjects.*;

public interface IBlockDatesDAB {

	/**
	 * This method is used to Create a new BlockDates
	 *
	 * @return
	 */
	public String addBlockDatesData(BlockDatesData blockDatesData);

	/**
	 * This method is used to Update the BlockDates details
	 *
	 * @return
	 */
	public boolean updateBlockDatesData(BlockDatesData blockDatesData);

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<BlockDatesData> getBlockDatesList(String venueId, Date startDate, Date endDate);

	/**
	 * This method is used to Get details for a particular BlockDates
	 *
	 * @return
	 */
	public BlockDatesData getBlockDatesData(String blockDatesId);

	/**
	 * This method is used to Delete the existing BlockDates details
	 *
	 * @return
	 */
	public boolean deleteBlockDatesData(String blockDatesId);

	public List<BlockDatesData> getBlockDatesForProperty(String propertyId, Date startDate, Date endDate);

}
