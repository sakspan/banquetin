package com.banquetin.dab.lookupdab;

import com.banquetin.dab.BaseDABFactory;

public class LookupDABFactory extends BaseDABFactory {

	public static ILookupDAB getDABInstance() {
		return LookupDAB.getInstance();
	}
}
