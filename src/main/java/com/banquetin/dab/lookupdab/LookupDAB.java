package com.banquetin.dab.lookupdab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.StaticData;

public class LookupDAB extends BaseDAB implements ILookupDAB {

	// Singleton Instance of the class
	private static ILookupDAB INSTANCE = new LookupDAB();

	private static final Logger logger = Logger.getLogger(LookupDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private LookupDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static ILookupDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to get the list of static objects
	 *
	 * @return
	 */
	public List<StaticData> getLookUpData(String tableName) {

		logger.debug("Entry");

		String query = "SELECT * from " + tableName;

		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = getResultSet(stmt);

		List<StaticData> staticDataList = new ArrayList<StaticData>();
		
		try {
			while (rs.next()) {
				StaticData staticData = new StaticData();
				
				staticData.setId(rs.getString(1));
				staticData.setText(rs.getString(2));
				
				if(rs.getMetaData().getColumnCount() > 3)
					staticData.setDesc(rs.getString(3));
				
				staticDataList.add(staticData);
				
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return staticDataList;
	}

}
