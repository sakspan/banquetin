package com.banquetin.dab.lookupdab;

import java.util.List;

import com.banquetin.dataobjects.StaticData;

public interface ILookupDAB{

	/**
	 * This method is used to get the list of static objects
	 *
	 * @return
	 */
	public List<StaticData> getLookUpData(String tableName);

}
