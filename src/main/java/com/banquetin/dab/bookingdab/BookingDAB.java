package com.banquetin.dab.bookingdab;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.banquetin.dab.BaseDAB;
import com.banquetin.dataobjects.AddressData;
import com.banquetin.dataobjects.BookingData;
import com.banquetin.dataobjects.SlotTypeData;
import com.banquetin.dataobjects.StaticData;
import com.banquetin.dataobjects.UserData;
import com.banquetin.util.CommonUtil;
import com.banquetin.util.Constants;

public class BookingDAB extends BaseDAB implements IBookingDAB {

	// Singleton Instance of the class
	private static IBookingDAB INSTANCE = new BookingDAB();

	private static final Logger logger = Logger.getLogger(BookingDAB.class.getName());

	// Private constructor to avoid creating multiple instance of same class.
	private BookingDAB() {

	}

	/**
	 * The method returns the singleton instance of the class
	 *
	 * @return
	 */
	static IBookingDAB getInstance() {
		return INSTANCE;
	}

	/**
	 * This method is used to Create a new Booking
	 *
	 * @return
	 */
	public String addBookingData(BookingData bookingData) {

		logger.debug("Entry");

		String query = "INSERT INTO Booking(VenueId,StartDate,EndDate,Amount,Transactionid,PaymentTypeId,BookingStatusId,UserId,EventTypeId,StartTypeId,EndTypeId,PaxCount,IsActive,AdvanceAmount) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		String bookingId = null;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatementWithIncrementId(conn, query);

		try {
			stmt.setString(1, bookingData.getVenueId());
			stmt.setDate(2, CommonUtil.getSQLDate(bookingData.getStartDate()));
			stmt.setDate(3, CommonUtil.getSQLDate(bookingData.getEndDate()));
			stmt.setDouble(4, bookingData.getAmount());
			stmt.setString(5, bookingData.getTransactionid());
			stmt.setString(6, bookingData.getPaymentTypeId());
			stmt.setString(7, bookingData.getBookingStatusId());
			stmt.setString(8, bookingData.getUserId());
			stmt.setString(9, bookingData.getEventType());
			stmt.setString(10, bookingData.getStartSlotTypeId());
			stmt.setString(11, bookingData.getEndSlotTypeId());
			stmt.setString(12, bookingData.getPaxCount());
			stmt.setInt(13, Constants.IS_ACTIVE_YES);
			stmt.setDouble(14, bookingData.getAdvanceAmount());

			stmt.executeUpdate();

			ResultSet rs = stmt.getGeneratedKeys();

			if (rs.next()) {
				bookingId = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return bookingId;
	}

	/**
	 * This method is used to Update the Booking details
	 *
	 * @return
	 */
	public String updateBookingData(BookingData bookingData, UserData userData, AddressData addressData) {

		logger.debug("Entry");

		String query = "UPDATE booking b, user u, address a, user_address ua" + 
				" SET u.UserName = ?,u.UserEmail = ?,u.UserMobile = ?,a.Address1 = ?,a.Address2 = ?,a.City = ?,a.StateId = ?,a.PinCode = ?," + 
				" b.Amount = ?,b.AdvanceAmount = ?  where b.BookingId = ?  and b.IsActive = ?  and u.UserId= ?  and a.AddressId= ? ";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {

			stmt.setString(1, userData.getUserName());
			stmt.setString(2, userData.getUserEmail());
			stmt.setString(3, userData.getUserMobile());
			stmt.setString(4, addressData.getAddress1());
			stmt.setString(5, addressData.getAddress2());
			stmt.setString(6, addressData.getCity());
			stmt.setInt(7, Integer.valueOf(addressData.getStateId()));
			stmt.setString(8, addressData.getPincode());
			stmt.setDouble(9, bookingData.getAmount());
			stmt.setDouble(10, bookingData.getAdvanceAmount());
			stmt.setInt(11, Integer.valueOf(bookingData.getBookingId()));
			stmt.setInt(12, Constants.IS_ACTIVE_YES);
			stmt.setInt(13, Integer.valueOf(bookingData.getUserId()));
			stmt.setInt(14, Integer.valueOf(addressData.getAddressId()));
			

			count = stmt.executeUpdate();
			System.out.println(stmt.getUpdateCount());
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}
		
		logger.debug("Exit");
		return (count>0?bookingData.getBookingId():null);
	}

	/**
	 * This method is used to Get list of all active companies
	 *
	 * @return
	 */
	public List<BookingData> getBookingList(String venueId, String userId, Date startDate, Date endDate) {

		logger.debug("Entry");

		String query = "SELECT * FROM Booking b, bookingstatus bs, venue v where v.venueId=b.venueId and b.bookingstatusid = bs.bookingstatusid and IsActive = ?";
		if (venueId != null) {
			query += " and VenueId = ?";
		}
		if (userId != null) {
			query += " and UserId = ?";
		}
		if (startDate != null) {
			query += " and StartDate >= ?";
		}
		if (endDate != null) {
			query += " and EndDate <= ?";
		}
		query += ";";

		int count = 1;
		List<BookingData> bookingDataList = new ArrayList<BookingData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(count++, Constants.IS_ACTIVE_YES);
			if (venueId != null) {
				stmt.setString(count++, venueId);
			}
			if (userId != null) {
				stmt.setString(count++, userId);
			}
			if (startDate != null) {
				stmt.setDate(count++, CommonUtil.getSQLDate(startDate));
			}
			if (endDate != null) {
				stmt.setDate(count++, CommonUtil.getSQLDate(endDate));
			}
			rs = getResultSet(stmt);
			while (rs.next()) {

				BookingData bookingData = new BookingData();
				bookingData.setBookingId(rs.getString("BookingId"));
				bookingData.setVenueId(rs.getString("VenueId"));
				bookingData.setVenueText(rs.getString("v.venueName"));
				bookingData.setStartDate(rs.getDate("StartDate"));
				bookingData.setEndDate(rs.getDate("EndDate"));
				bookingData.setAmount(rs.getDouble("Amount"));
				bookingData.setTransactionid(rs.getString("Transactionid"));

				StaticData paymentType = new StaticData();
				paymentType.setId(rs.getString("PaymentTypeId"));
				bookingData.setPaymentType(paymentType);

				StaticData bookingStatus = new StaticData();
				bookingStatus.setId(rs.getString("BookingStatusId"));
				bookingStatus.setText(rs.getString("BookingStatusText"));
				bookingData.setBookingStatus(bookingStatus);

				bookingData.setUserId(rs.getString("UserId"));
				bookingData.setEventType(rs.getString("EventTypeId"));

				SlotTypeData startTypeData = new SlotTypeData();
				startTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				bookingData.setStartSlotTypeData(startTypeData);

				SlotTypeData endTypeData = new SlotTypeData();
				endTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				bookingData.setEndSlotTypeData(endTypeData);

				bookingDataList.add(bookingData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return bookingDataList;
	}

	/**
	 * This method is used to Get details for a particular Booking
	 *
	 * @return
	 */
	public BookingData getBookingData(String bookingId) {

		logger.debug("Entry");

		String query = "SELECT * FROM Booking b, bookingstatus bs, venue v where BookingId = ? and b.IsActive=? and b.venueId=v.venueId "
				+ "and b.bookingstatusid = bs.bookingstatusid;";
		BookingData bookingData = new BookingData();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, bookingId);
			stmt.setInt(2, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				bookingData.setBookingId(rs.getString("BookingId"));
				bookingData.setVenueId(rs.getString("VenueId"));
				bookingData.setVenueText(rs.getString("v.venueName"));
				bookingData.setStartDate(rs.getDate("StartDate"));
				bookingData.setEndDate(rs.getDate("EndDate"));
				bookingData.setAmount(rs.getDouble("Amount"));
				bookingData.setTransactionid(rs.getString("Transactionid"));
				StaticData paymentType = new StaticData();
				paymentType.setId(rs.getString("PaymentTypeId"));
				bookingData.setPaymentType(paymentType);
				bookingData.setPaymentTypeId(rs.getString("PaymentTypeId"));
				StaticData bookingStatus = new StaticData();
				bookingStatus.setId(rs.getString("BookingStatusId"));
				bookingStatus.setText(rs.getString("BookingStatusText"));
				bookingData.setPaxCount(rs.getString("PaxCount"));
				bookingData.setBookingStatus(bookingStatus);
				bookingData.setUserId(rs.getString("UserId"));
				bookingData.setEventType(rs.getString("EventTypeId"));
				SlotTypeData startSlotTypeData = new SlotTypeData();
				startSlotTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				bookingData.setStartSlotTypeData(startSlotTypeData);
				SlotTypeData endSlotTypeData = new SlotTypeData();
				endSlotTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				bookingData.setStartSlotTypeData(endSlotTypeData);
				bookingData.setAdvanceAmount(rs.getDouble("AdvanceAmount"));
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return bookingData;
	}

	/**
	 * This method is used to Delete the existing Booking details
	 *
	 * @return
	 */
	public boolean deleteBookingData(String bookingId) {

		logger.debug("Entry");

		String query = "UPDATE Booking SET IsActive=?, BookingStatusId=? where BookingId = ?;";
		int count = 0;
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_NO);
			stmt.setString(2, Constants.BOOKING_STATUS_CANCELLED);
			stmt.setString(3, bookingId);
			count = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, null);
		}

		logger.debug("Exit");
		return count == 0 ? false : true;
	}

	public List<BookingData> getBookingsForProperty(String propertyId, Date startDate, Date endDate) {

		logger.debug("Entry");

		String query = "SELECT * FROM Booking b, bookingstatus bs, venue v, property_venue pv where pv.propertyId = ? and pv.VenueId = b.VenueId and  "
				+ "startDate>=? and endDate<=? and b.IsActive =? and pv.IsActive=? and b.venueId=v.venueId and b.bookingstatusid = bs.bookingstatusid;";
		List<BookingData> bookingDataList = new ArrayList<BookingData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setString(1, propertyId);
			stmt.setDate(2, CommonUtil.getSQLDate(startDate));
			stmt.setDate(3, CommonUtil.getSQLDate(endDate));
			stmt.setInt(4, Constants.IS_ACTIVE_YES);
			stmt.setInt(5, Constants.IS_ACTIVE_YES);
			rs = getResultSet(stmt);
			while (rs.next()) {

				BookingData bookingData = new BookingData();
				bookingData.setBookingId(rs.getString("BookingId"));
				bookingData.setVenueId(rs.getString("VenueId"));
				bookingData.setVenueText(rs.getString("v.venueName"));
				bookingData.setStartDate(rs.getDate("StartDate"));
				bookingData.setEndDate(rs.getDate("EndDate"));
				bookingData.setAmount(rs.getDouble("Amount"));
				bookingData.setTransactionid(rs.getString("Transactionid"));
				StaticData paymentType = new StaticData();
				paymentType.setId(rs.getString("PaymentTypeId"));
				bookingData.setPaymentType(paymentType);

				StaticData bookingStatus = new StaticData();
				bookingStatus.setId(rs.getString("BookingStatusId"));
				bookingStatus.setText(rs.getString("BookingStatusText"));

				bookingData.setBookingStatus(bookingStatus);
				bookingData.setUserId(rs.getString("UserId"));
				bookingData.setEventType(rs.getString("EventTypeId"));
				SlotTypeData startSlotTypeData = new SlotTypeData();
				startSlotTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				bookingData.setStartSlotTypeData(startSlotTypeData);
				SlotTypeData endSlotTypeData = new SlotTypeData();
				endSlotTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				bookingData.setStartSlotTypeData(endSlotTypeData);
				bookingData.setAdvanceAmount(rs.getDouble("AdvanceAmount"));
				bookingDataList.add(bookingData);

			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return bookingDataList;
	}
	
	public List<BookingData> getBlockedDatesList(String venueId) {

		logger.debug("Entry");

		String query = "SELECT StartDate,EndDate,StartTypeId,EndTypeId,VenueId FROM blockdates where isActive = ? and VenueId = ?;";
		
		List<BookingData> blockedBookingList = new ArrayList<BookingData>();
		Connection conn = getConnection();
		PreparedStatement stmt = getPreparedStatement(conn, query);
		ResultSet rs = null;

		try {
			stmt.setInt(1, Constants.IS_ACTIVE_YES);
			stmt.setInt(2, Integer.valueOf(venueId));
			rs = getResultSet(stmt);
			
			while (rs.next()) {
				BookingData bookingData = new BookingData();
				bookingData.setVenueId(rs.getString("VenueId"));
				bookingData.setStartDate(rs.getDate("StartDate"));
				bookingData.setEndDate(rs.getDate("EndDate"));
				SlotTypeData startSlotTypeData = new SlotTypeData();
				startSlotTypeData.setSlotTypeId(rs.getString("StartTypeId"));
				bookingData.setStartSlotTypeData(startSlotTypeData);
				SlotTypeData endSlotTypeData = new SlotTypeData();
				endSlotTypeData.setSlotTypeId(rs.getString("EndTypeId"));
				bookingData.setStartSlotTypeData(endSlotTypeData);
				blockedBookingList.add(bookingData);
			}
		} catch (Exception e) {
			logger.error("Exception", e);
		} finally {
			releaseResources(conn, stmt, rs);
		}

		logger.debug("Exit");
		return blockedBookingList;
	}

}
