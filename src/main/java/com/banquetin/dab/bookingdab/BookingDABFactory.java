package com.banquetin.dab.bookingdab;

import com.banquetin.dab.BaseDABFactory;

public class BookingDABFactory extends BaseDABFactory {

	public static IBookingDAB getDABInstance() {
		return BookingDAB.getInstance();
	}

}
