package com.banquetin.dataobjects;

public class PromoData extends BaseData{
	
	private String promoId;
	private String promoName;
	private String promoDescription;
	private String promoValue;
	private int isActive;
	
	public String getPromoId() {
		return promoId;
	}
	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	public String getPromoDescription() {
		return promoDescription;
	}
	public void setPromoDescription(String promoDescription) {
		this.promoDescription = promoDescription;
	}
	public String getPromoValue() {
		return promoValue;
	}
	public void setPromoValue(String promoValue) {
		this.promoValue = promoValue;
	}
	
	@Override
	public String toString() {
		return "PromoData [promoId=" + promoId + ", promoName=" + promoName + ", promoDescription=" + promoDescription
				+ ", promoValue=" + promoValue +", isActive=" + isActive + "]";
	}
	
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	
}
