package com.banquetin.dataobjects;

public class StaticData extends BaseData {

	private String id;
	private String text;
	private String desc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String toString() {
		String retString = " Id : "+id+", Text : "+text+"\n";
		return retString;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
