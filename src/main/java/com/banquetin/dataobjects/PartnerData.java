package com.banquetin.dataobjects;

import java.util.*;


import com.banquetin.dab.partnerdab.IPartnerDAB;

public class PartnerData extends BaseData {

	private String partnerId;
	private String partnerName;
	private String ImageId;
	private String Path;


	public String getImageId() {
		return ImageId;
	}

	public void setImageId(String imageId) {
		this.ImageId = imageId;
	}

	public String toString() {
		String retString = " partnerId : "+partnerId+", partnerName : "+partnerName+", ImageId : "+ImageId+", Path : "+Path+"\n";
		return retString;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPath() {
		return Path;
	}

	public void setPath(String path) {
		Path = path;
	}

	

}
