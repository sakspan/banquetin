package com.banquetin.dataobjects;

public class EmailData extends BaseData {

	private String emailId;
	private String emailAddress;
	private String emailTypeId;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailTypeId() {
		return emailTypeId;
	}

	public void setEmailTypeId(String emailTypeId) {
		this.emailTypeId = emailTypeId;
	}

	public String toString() {
		String retString = " EmailId : "+emailId+", EmailAddress : "+emailAddress+", EmailTypeId : "+emailTypeId+"\n";
		return retString;
	}
}
