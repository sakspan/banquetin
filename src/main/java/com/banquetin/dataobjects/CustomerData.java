package com.banquetin.dataobjects;

import java.util.*;

public class CustomerData extends BaseData {

	private String customerId;
	private String firstName;
	private String lastName;
	private String emailId;
	private String telephone;
	private String password;
	private String customerIP;
	private List<AddressData> addressList;
	private List<String> customerTypeList;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCustomerIP() {
		return customerIP;
	}

	public void setCustomerIP(String customerIP) {
		this.customerIP = customerIP;
	}

	public List<AddressData> getAddressList() {
		if (addressList == null)
			addressList = new ArrayList<AddressData>();
		return addressList;
	}

	public void setAddressList(List<AddressData> addressList) {
		this.addressList = addressList;
	}

	public List<String> getCustomerTypeList() {
		return customerTypeList;
	}

	public void setCustomerTypeList(List<String> customerTypeList) {
		this.customerTypeList = customerTypeList;
	}

	public String toString() {
		String retString = " CustomerId : " + customerId + ", FirstName : " + firstName + ", LastName : " + lastName
				+ ", EmailId : " + emailId + ", Telephone : " + telephone + ", Password : " + password
				+ ", CustomerIP : " + customerIP + ", AddressList : " + addressList + ", CustomerTypeList : "
				+ customerTypeList + "\n";
		return retString;
	}
}
