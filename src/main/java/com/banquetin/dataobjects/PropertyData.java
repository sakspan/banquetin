package com.banquetin.dataobjects;

import java.util.*;

public class PropertyData extends BaseData {

	private String propertyId;
	private String propertyName;
	private String starRating;
	private String propertyDescription;
	private StaticData propertyType;
	private String onlineInd;
	private List<VenueData> venueList;
	private List<AmenitiesData> amenitiesList;
	private List<AddressData> addressList;
	private List<PhoneData> phoneList;
	private List<EmailData> emailList;
	private List<PropertyInfoData> propertyInfo;
	private List<ImageData> propertyImageList;
	private String propertyTypeId;
	private List<String> amenitiesIdList;
	private List<String> imageIdList;
	private String userId;

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public List<AddressData> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<AddressData> addressList) {
		this.addressList = addressList;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getStarRating() {
		return starRating;
	}

	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public StaticData getPropertyType() {
		if (propertyType == null) {
			propertyType = new StaticData();
			propertyType.setId(propertyTypeId);
		}

		return propertyType;
	}

	public void setPropertyType(StaticData propertyType) {
		this.propertyType = propertyType;
	}

	public String getOnlineInd() {
		return onlineInd;
	}

	public void setOnlineInd(String onlineInd) {
		this.onlineInd = onlineInd;
	}

	public List<VenueData> getVenueList() {
		return venueList;
	}

	public void setVenueList(List<VenueData> venueList) {
		this.venueList = venueList;
	}

	public List<AmenitiesData> getAmenitiesList() {
		return amenitiesList;
	}

	public void setAmenitiesList(List<AmenitiesData> amenitiesList) {
		this.amenitiesList = amenitiesList;
	}

	public List<PropertyInfoData> getPropertyInfo() {
		return propertyInfo;
	}

	public void setPropertyInfo(List<PropertyInfoData> propertyInfo) {
		this.propertyInfo = propertyInfo;
	}

	public List<ImageData> getPropertyImageList() {
		return propertyImageList;
	}

	public void setPropertyImageList(List<ImageData> propertyImageList) {
		this.propertyImageList = propertyImageList;
	}

	public String getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(String propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	public List<String> getAmenitiesIdList() {
		return amenitiesIdList;
	}

	public void setAmenitiesIdList(List<String> amenitiesIdList) {
		this.amenitiesIdList = amenitiesIdList;
	}

	public List<String> getImageIdList() {
		if (imageIdList == null) {
			imageIdList = new ArrayList<String>();
		}
		return imageIdList;
	}

	public void setImageIdList(List<String> imageIdList) {
		this.imageIdList = imageIdList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
	public List<EmailData> getEmailList() {
		return emailList;
	}

	public void setEmailList(List<EmailData> emailList) {
		this.emailList = emailList;
	}

	public List<PhoneData> getPhoneList() {
		return phoneList;
	}

	public void setPhoneList(List<PhoneData> phoneList) {
		this.phoneList = phoneList;
	}

	@Override
	public String toString() {
		return "PropertyData [propertyId=" + propertyId + ", propertyName=" + propertyName + ", starRating="
				+ starRating + ", propertyDescription=" + propertyDescription + ", propertyType=" + propertyType
				+ ", onlineInd=" + onlineInd + ", venueList=" + venueList + ", amenitiesList=" + amenitiesList
				+ ", addressList=" + addressList + ", phoneList=" + phoneList + ", emailList=" + emailList
				+ ", propertyInfo=" + propertyInfo + ", propertyImageList=" + propertyImageList + ", propertyTypeId="
				+ propertyTypeId + ", amenitiesIdList=" + amenitiesIdList + ", imageIdList=" + imageIdList + ", userId="
				+ userId + "]";
	}

}
