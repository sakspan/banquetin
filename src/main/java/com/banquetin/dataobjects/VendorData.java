package com.banquetin.dataobjects;

import java.util.*;

public class VendorData extends BaseData {

	private String vendorId;
	private String vendorName;
	private String vendorDescription;
	private StaticData vendorType;
	private String imageId;
	private List< AddressData> addressData;
	private List< EmailData> emailList;

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorDescription() {
		return vendorDescription;
	}

	public void setVendorDescription(String vendorDescription) {
		this.vendorDescription = vendorDescription;
	}

	public StaticData getVendorType() {
		return vendorType;
	}

	public void setVendorType(StaticData vendorType) {
		this.vendorType = vendorType;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public List< AddressData> getAddressData() {
		return addressData;
	}

	public void setAddressData(List< AddressData> addressData) {
		this.addressData = addressData;
	}

	public List< EmailData> getEmailList() {
		return emailList;
	}

	public void setEmailList(List< EmailData> emailList) {
		this.emailList = emailList;
	}

	public String toString() {
		String retString = " VendorId : "+vendorId+", VendorName : "+vendorName+", VendorDescription : "+vendorDescription+", VendorType : "+vendorType+", ImageId : "+imageId+", AddressData : "+addressData+", EmailList : "+emailList+"\n";
		return retString;
	}
}
