package com.banquetin.dataobjects;

public class BookingHistoryData extends BaseData {

	private String bookingHistoryId;
	private String fromStatus;
	private String toStatus;
	private String comments;
	private String userId;

	public String getBookingHistoryId() {
		return bookingHistoryId;
	}

	public void setBookingHistoryId(String bookingHistoryId) {
		this.bookingHistoryId = bookingHistoryId;
	}

	public String getFromStatus() {
		return fromStatus;
	}

	public void setFromStatus(String fromStatus) {
		this.fromStatus = fromStatus;
	}

	public String getToStatus() {
		return toStatus;
	}

	public void setToStatus(String toStatus) {
		this.toStatus = toStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String toString() {
		String retString = " BookingHistoryId : "+bookingHistoryId+", FromStatus : "+fromStatus+", ToStatus : "+toStatus+", Comments : "+comments+", UserId : "+userId+"\n";
		return retString;
	}
}
