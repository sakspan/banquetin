package com.banquetin.dataobjects;

public class PhoneData extends BaseData {

	private String phoneId;
	private String phoneNumber;
	private String phoneTypeId;

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneTypeId() {
		return phoneTypeId;
	}

	public void setPhoneTypeId(String phoneTypeId) {
		this.phoneTypeId = phoneTypeId;
	}

	public String toString() {
		String retString = " PhoneId : "+phoneId+", PhoneNumber : "+phoneNumber+", PhoneTypeId : "+phoneTypeId+"\n";
		return retString;
	}
}
