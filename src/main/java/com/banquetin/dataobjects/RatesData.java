package com.banquetin.dataobjects;

import java.util.*;

public class RatesData extends BaseData {

	private String ratesId;
	private String venueId;
	private Date startDate;
	private Date endDate;
	private double rate;
	private String slotTypeId;
	private double bookingAmount;
	private StaticData ratesType;
	private StaticData slotType;

	public String getRatesId() {
		return ratesId;
	}

	public void setRatesId(String ratesId) {
		this.ratesId = ratesId;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(double bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public StaticData getRatesType() {
		if (ratesType == null)
			ratesType = new StaticData();
		return ratesType;
	}

	public void setRatesType(StaticData ratesType) {
		this.ratesType = ratesType;
	}

	public String getSlotTypeId() {
		return slotTypeId;
	}

	public void setSlotTypeId(String slotTypeId) {
		this.slotTypeId = slotTypeId;
	}

	@Override
	public String toString() {
		return "RatesData [ratesId=" + ratesId + ", venueId=" + venueId + ", startDate=" + startDate + ", endDate="
				+ endDate + ", rate=" + rate + ", slotTypeId=" + slotTypeId + ", bookingAmount=" + bookingAmount
				+ ", ratesType=" + ratesType + "]";
	}

	public StaticData getSlotType() {
		return slotType;
	}

	public void setSlotType(StaticData slotType) {
		this.slotType = slotType;
	}

}
