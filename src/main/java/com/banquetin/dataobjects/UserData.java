package com.banquetin.dataobjects;

import java.util.*;

public class UserData extends BaseData {

	private String userId;
	private String userName;
	private String userPassword;
	private String userEmail;
	private String userMobile;
	private String userFeedback;
	private List<AddressData> addressList;
	private StaticData userRoleType;
	private String userRoleTypeId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public List<AddressData> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<AddressData> addressList) {
		this.addressList = addressList;
	}

	public StaticData getUserRoleType() {
		return userRoleType;
	}

	public void setUserRoleType(StaticData userRoleType) {
		this.userRoleType = userRoleType;
	}

	public String toString() {
		String retString = " UserId : " + userId + ", UserName : " + userName + ", UserPassword : " + userPassword
				+ ", UserEmail : " + userEmail + ", UserMobile : " + userMobile + ", AddressList : " + addressList
				+ ", UserRoleType : " + userRoleType + ", userFeedback : " + userFeedback + "\n";
		return retString;
	}

	public String getUserFeedback() {
		return userFeedback;
	}

	public void setUserFeedback(String userFeedback) {
		this.userFeedback = userFeedback;
	}

	public String getUserRoleTypeId() {
		return userRoleTypeId;
	}

	public void setUserRoleTypeId(String userRoleTypeId) {
		this.userRoleTypeId = userRoleTypeId;
	}
}
