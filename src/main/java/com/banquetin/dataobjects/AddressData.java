package com.banquetin.dataobjects;

import java.util.*;

public class AddressData extends BaseData {

	private String addressId;
	private String address1;
	private String address2;
	private String city;
	private String stateId;
	private String countryId;
	private String pincode;
	private String addressTypeId;
	private List<PhoneData> phoneList;

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getAddressTypeId() {
		return addressTypeId;
	}

	public void setAddressTypeId(String addressTypeId) {
		this.addressTypeId = addressTypeId;
	}

	public List<PhoneData> getPhoneList() {
		return phoneList;
	}

	public void setPhoneList(List<PhoneData> phoneList) {
		this.phoneList = phoneList;
	}

	public String toString() {
		String retString = " AddressId : "+addressId+", Address1 : "+address1+", Address2 : "+address2+", City : "+city+", StateId : "+stateId+", CountryId : "+countryId+", Pincode : "+pincode+", AddressTypeId : "+addressTypeId+", PhoneList : "+phoneList+"\n";
		return retString;
	}
}
