package com.banquetin.dataobjects;

import java.util.*;

public class VenueData extends BaseData {

	private String venueId;
	private String propertyId;
	private String venueDescription;
	private String venueName;
	private int length;
	private int width;
	private int height;
	private int sortOrder;
	private List<StaticData> eventTypeList;
	private List<SeatingTypeData> seatingType;
	private SeatingTypeData[] seatingTypeList;
	private double minBookingAmount;
	private StaticData venueType;
	private List<ImageData> venueImageList;
	private PropertyData propertyData;

	private String venueTypeId;
	private String[] eventTypeId;
	private boolean morningAvailable = true;
	private boolean eveningAvailable = true;

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public String getVenueDescription() {
		return venueDescription;
	}

	public void setVenueDescription(String venueDescription) {
		this.venueDescription = venueDescription;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public double getMinBookingAmount() {
		return minBookingAmount;
	}

	public void setMinBookingAmount(double minBookingAmount) {
		this.minBookingAmount = minBookingAmount;
	}

	public StaticData getVenueType() {

		if (venueType == null) {
			venueType = new StaticData();
			venueType.setId(venueTypeId);
		}
		return venueType;
	}

	public void setVenueType(StaticData venueType) {
		this.venueType = venueType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	// @Override
	// public String toString() {
	// return "VenueData [venueId=" + venueId + ", propertyId=" + propertyId + ",
	// venueDescription=" + venueDescription
	// + ", venueName=" + venueName + ", length=" + length + ", width=" + width + ",
	// height=" + height
	// + ", sortOrder=" + sortOrder + ", eventType=" + eventType + ",
	// seatingTypeList=" + seatingTypeList
	// + ", minBookingAmount=" + minBookingAmount + ", venueType=" + venueType + ",
	// venueImageList="
	// + venueImageList + ", propertyData=" + propertyData + "]";
	// }

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public List<ImageData> getVenueImageList() {
		return venueImageList;
	}

	public void setVenueImageList(List<ImageData> venueImageList) {
		this.venueImageList = venueImageList;
	}

	public PropertyData getPropertyData() {
		return propertyData;
	}

	public void setPropertyData(PropertyData propertyData) {
		this.propertyData = propertyData;
	}

	public List<SeatingTypeData> getSeatingType() {

		if (seatingType == null)
			seatingType = Arrays.asList(seatingTypeList);

		return seatingType;
	}

	public void setSeatingType(List<SeatingTypeData> seatingType) {
		this.seatingType = seatingType;
	}

	public SeatingTypeData[] getSeatingTypeList() {
		return seatingTypeList;
	}

	public void setSeatingTypeList(SeatingTypeData[] seatingTypeList) {
		this.seatingTypeList = seatingTypeList;
	}

	public String getVenueTypeId() {
		return venueTypeId;
	}

	public void setVenueTypeId(String venueTypeId) {
		if (venueType == null) {
			venueType = new StaticData();
			venueType.setId(venueTypeId);
		}
		this.venueTypeId = venueTypeId;
	}

	public String[] getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(String[] eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	@Override
	public String toString() {
		return "VenueData [venueId=" + venueId + ", propertyId=" + propertyId + ", venueDescription=" + venueDescription
				+ ", venueName=" + venueName + ", length=" + length + ", width=" + width + ", height=" + height
				+ ", sortOrder=" + sortOrder + ", eventTypeList=" + eventTypeList + ", seatingType=" + seatingType
				+ ", seatingTypeList=" + Arrays.toString(seatingTypeList) + ", minBookingAmount=" + minBookingAmount
				+ ", venueType=" + venueType + ", venueImageList=" + venueImageList + ", propertyData=" + propertyData
				+ ", venueTypeId=" + venueTypeId + ", eventTypeId=" + eventTypeId + "]";
	}

	public List<StaticData> getEventTypeList() {
		return eventTypeList;
	}

	public void setEventTypeList(List<StaticData> eventTypeList) {
		this.eventTypeList = eventTypeList;
	}

	public boolean isMorningAvailable() {
		return morningAvailable;
	}

	public void setMorningAvailable(boolean morningAvailable) {
		this.morningAvailable = morningAvailable;
	}

	public boolean isEveningAvailable() {
		return eveningAvailable;
	}

	public void setEveningAvailable(boolean eveningAvailable) {
		this.eveningAvailable = eveningAvailable;
	}

}
