package com.banquetin.dataobjects;

import java.util.*;

public class BlockDatesData extends BaseData {

	private String blockDatesId;
	private String venueId;
	private Date startDate;
	private Date endDate;
	private SlotTypeData startSlotTypeData;
	private SlotTypeData endSlotTypeData;

	public String getBlockDatesId() {
		return blockDatesId;
	}

	public void setBlockDatesId(String blockDatesId) {
		this.blockDatesId = blockDatesId;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public SlotTypeData getStartSlotTypeData() {
		return startSlotTypeData;
	}

	public void setStartSlotTypeData(SlotTypeData startSlotTypeData) {
		this.startSlotTypeData = startSlotTypeData;
	}

	public SlotTypeData getEndSlotTypeData() {
		return endSlotTypeData;
	}

	public void setEndSlotTypeData(SlotTypeData endSlotTypeData) {
		this.endSlotTypeData = endSlotTypeData;
	}

	public String toString() {
		String retString = " BlockDatesId : "+blockDatesId+", VenueId : "+venueId+", StartDate : "+startDate+", EndDate : "+endDate+", StartSlotTypeData : "+startSlotTypeData+", EndSlotTypeData : "+endSlotTypeData+"\n";
		return retString;
	}
}
