package com.banquetin.dataobjects;

public class SeatingTypeData extends BaseData {

	private String seatingTypeId;
	private String seatingTypeText;
	private String minCapacity;
	private String maxCapacity;
	private String venueSeatingTypeId;

	public String getSeatingTypeId() {
		return seatingTypeId;
	}

	public void setSeatingTypeId(String seatingTypeId) {
		this.seatingTypeId = seatingTypeId;
	}

	public String getMinCapacity() {
		return minCapacity;
	}

	public void setMinCapacity(String minCapacity) {
		this.minCapacity = minCapacity;
	}

	public String getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(String maxCapacity) {
		this.maxCapacity = maxCapacity;
	}


	public String getSeatingTypeText() {
		return seatingTypeText;
	}

	public void setSeatingTypeText(String seatingTypeText) {
		this.seatingTypeText = seatingTypeText;
	}

	@Override
	public String toString() {
		return "SeatingTypeData [seatingTypeId=" + seatingTypeId + ", seatingTypeText=" + seatingTypeText
				+ ", minCapacity=" + minCapacity + ", maxCapacity=" + maxCapacity + ", venueSeatingTypeId="
				+ venueSeatingTypeId + "]";
	}

	public String getVenueSeatingTypeId() {
		return venueSeatingTypeId;
	}

	public void setVenueSeatingTypeId(String venueSeatingTypeId) {
		this.venueSeatingTypeId = venueSeatingTypeId;
	}
}
