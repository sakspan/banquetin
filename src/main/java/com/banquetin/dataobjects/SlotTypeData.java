package com.banquetin.dataobjects;

public class SlotTypeData extends BaseData {

	private String slotTypeId;
	private String slotTypeText;
	private String slotStartTime;
	private String slotEndTime;

	public String getSlotTypeId() {
		return slotTypeId;
	}

	public void setSlotTypeId(String slotTypeId) {
		this.slotTypeId = slotTypeId;
	}

	public String getSlotTypeText() {
		return slotTypeText;
	}

	public void setSlotTypeText(String slotTypeText) {
		this.slotTypeText = slotTypeText;
	}

	public String getSlotStartTime() {
		return slotStartTime;
	}

	public void setSlotStartTime(String slotStartTime) {
		this.slotStartTime = slotStartTime;
	}

	public String getSlotEndTime() {
		return slotEndTime;
	}

	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}

	public String toString() {
		String retString = " SlotTypeId : "+slotTypeId+", SlotTypeText : "+slotTypeText+", SlotStartTime : "+slotStartTime+", SlotEndTime : "+slotEndTime+"\n";
		return retString;
	}
}
