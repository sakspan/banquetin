package com.banquetin.dataobjects;

public class AmenitiesData extends BaseData {

	private String amenitiesId;
	private String amenitiesText;
	private String amenitiesIcon;
	private String amenitiesDescription;
	private String showInFilter;
	private int isActive;
	private boolean isEnabled;
	private int sortOrder;
	
	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getAmenitiesId() {
		return amenitiesId;
	}

	public void setAmenitiesId(String amenitiesId) {
		this.amenitiesId = amenitiesId;
	}

	public String getAmenitiesText() {
		return amenitiesText;
	}

	public void setAmenitiesText(String amenitiesText) {
		this.amenitiesText = amenitiesText;
	}

	public String getAmenitiesIcon() {
		return amenitiesIcon;
	}

	public void setAmenitiesIcon(String amenitiesIcon) {
		this.amenitiesIcon = amenitiesIcon;
	}

	public String getAmenitiesDescription() {
		return amenitiesDescription;
	}

	public void setAmenitiesDescription(String amenitiesDescription) {
		this.amenitiesDescription = amenitiesDescription;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String toString() {
		String retString = " AmenitiesId : "+amenitiesId+", AmenitiesText : "+amenitiesText+", AmenitiesIcon : "+amenitiesIcon+", AmenitiesDescription : "+amenitiesDescription+", SortOrder : "+sortOrder+ ", isEnabled :"+ isEnabled + "\n";
		return retString;
	}

	public String getShowInFilter() {
		// TODO Auto-generated method stub
		return this.showInFilter;
	}

	public void setShowInFilter(String showInFilter) {
		// TODO Auto-generated method stub
		this.showInFilter = showInFilter;
		
	}

	public void setIsActive(int isActive) {
		// TODO Auto-generated method stub
		this.isActive = isActive;
	}
}
