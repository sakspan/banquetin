package com.banquetin.dataobjects;

import java.util.*;

public class BookingData extends BaseData {

	private String bookingId;
	private String venueId;
	private String venueText;
	private Date startDate;
	private Date endDate;
	private double amount;
	private String transactionid;
	private StaticData paymentType;
	private StaticData bookingStatus;
	private String userId;
	private String eventType;
	private SlotTypeData startSlotTypeData;
	private SlotTypeData endSlotTypeData;
	private String paxCount;
	private double advanceAmount;
	private String paymentTypeId;
	private String bookingStatusId;
	private String startSlotTypeId;
	private String endSlotTypeId;
	private String addressId;

	public String getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public String getBookingStatusId() {
		return bookingStatusId;
	}

	public void setBookingStatusId(String bookingStatusId) {
		this.bookingStatusId = bookingStatusId;
	}

	public String getStartSlotTypeId() {
		return startSlotTypeId;
	}

	public void setStartSlotTypeId(String startSlotTypeId) {
		this.startSlotTypeId = startSlotTypeId;
	}

	public String getEndSlotTypeId() {
		return endSlotTypeId;
	}

	public void setEndSlotTypeId(String endSlotTypeId) {
		this.endSlotTypeId = endSlotTypeId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	public StaticData getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(StaticData paymentType) {
		this.paymentType = paymentType;
	}

	public StaticData getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(StaticData bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public SlotTypeData getStartSlotTypeData() {
		return startSlotTypeData;
	}

	public void setStartSlotTypeData(SlotTypeData startSlotTypeData) {
		this.startSlotTypeData = startSlotTypeData;
	}

	public SlotTypeData getEndSlotTypeData() {
		return endSlotTypeData;
	}

	public void setEndSlotTypeData(SlotTypeData endSlotTypeData) {
		this.endSlotTypeData = endSlotTypeData;
	}

	public String getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}

	@Override
	public String toString() {
		return "BookingData [bookingId=" + bookingId + ", venueId=" + venueId + ", venueText=" + venueText
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", amount=" + amount + ", transactionid="
				+ transactionid + ", paymentType=" + paymentType + ", bookingStatus=" + bookingStatus + ", userId="
				+ userId + ", eventType=" + eventType + ", startSlotTypeData=" + startSlotTypeData
				+ ", endSlotTypeData=" + endSlotTypeData + ", paxCount=" + paxCount + ", advanceAmount=" + advanceAmount
				+ ", paymentTypeId=" + paymentTypeId + ", bookingStatusId=" + bookingStatusId + ", startSlotTypeId="
				+ startSlotTypeId + ", endSlotTypeId=" + endSlotTypeId + ", addressId=" + addressId + "]";
	}

	public String getVenueText() {
		return venueText;
	}

	public void setVenueText(String venueText) {
		this.venueText = venueText;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

}
