package com.banquetin.dataobjects;

import java.util.*;

import com.banquetin.dab.eventdab.IEventDAB;

public class EventData extends BaseData {

	private String EventTypeId;
	private String EventTypeText;
	private String EventDescription;
	private String ImageId;
	private int sortOrder;


	public String getEventTypeId() {
		return EventTypeId;
	}

	public void setEventTypeId(String eventTypeId) {
		this.EventTypeId = eventTypeId;
	}

	public String getEventTypeText() {
		return EventTypeText;
	}

	public void setEventTypeText(String eventText) {
		this.EventTypeText = eventText;
	}
	
	public String getEventDescription() {
		return EventDescription;
	}

	public void setEventDescription(String eventDesc) {
		this.EventDescription = eventDesc;
	}

	public String getImageId() {
		return ImageId;
	}

	public void setImageId(String imageId) {
		this.ImageId = imageId;
	}
	
	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	@Override
	public String toString() {
		String retString = " EventTypeId : "+EventTypeId+", EventTypeText : "+EventTypeText+", EventDescription : "+EventDescription+", ImageId : "+ImageId+", SortOrder : "+sortOrder+"\n";
		return retString;
	}

	

}
