package com.banquetin.dataobjects;

public class PropertyInfoData extends BaseData {
	
	private String propertyInfoId;
	private String propertyId;
	private String title;
	private String value;
	private String icon;
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PropertyInfoData [propertyInfoId=" + propertyInfoId + ", propertyId=" + propertyId + ", title=" + title
				+ ", value=" + value + ", icon=" + icon + ", isActive=" + isActive + "]";
	}
	public String getPropertyInfoId() {
		return propertyInfoId;
	}
	public void setPropertyInfoId(String propertyInfoId) {
		this.propertyInfoId = propertyInfoId;
	}
	private int isActive;
	public String getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
}
