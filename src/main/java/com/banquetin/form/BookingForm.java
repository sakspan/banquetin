package com.banquetin.form;

import org.apache.struts.action.ActionForm;

public class BookingForm extends ActionForm {

	private static final long serialVersionUID = 1L;

	private String venueId;
	private String bookingDate;
	private String slotType;
	private double rate;
	private String guestName;
	private String emailId;
	private String phoneNum;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String pincode;
	private String comments;
	private String estAmount;
	private String bookingAmount;
	private String advancePaid;
	private String paymentType;
	private String eventType;
	private String transactionId;
	private String paxCount;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getEstAmount() {
		return estAmount;
	}

	public void setEstAmount(String estAmount) {
		this.estAmount = estAmount;
	}

	public String getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(String bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public String getAdvancePaid() {
		return advancePaid;
	}

	public void setAdvancePaid(String advancePaid) {
		this.advancePaid = advancePaid;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getSlotType() {
		return slotType;
	}

	public void setSlotType(String slotType) {
		this.slotType = slotType;
	}

	@Override
	public String toString() {
		return "BookingForm [venueId=" + venueId + ", bookingDate=" + bookingDate + ", slotType=" + slotType + ", rate="
				+ rate + ", guestName=" + guestName + ", emailId=" + emailId + ", phoneNum=" + phoneNum + ", address1="
				+ address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state + ", pincode=" + pincode
				+ ", comments=" + comments + ", estAmount=" + estAmount + ", bookingAmount=" + bookingAmount
				+ ", advancePaid=" + advancePaid + ", paymentType=" + paymentType + ", bookingId=" + bookingId + "]";
	}

	public String getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(String paxCount) {
		this.paxCount = paxCount;
	}
	
	private String bookingId;

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	private String operationType;
	
	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

}