package com.banquetin.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class PropertyForm extends ActionForm {

	private static final long serialVersionUID = 1L;

	private String propertyId;
	private String propertyName;
	private String starRating;
	private String propertyDescription;
	private String propertyTypeId;
	private String onlineInd;
	private String emailId;
	private String phoneNumber;
	private String altPhoneNumber1;
	private String altPhoneNumber2;
	
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zipCode;
	private List<VenueForm> venueList;
	private List<String> amenitiesList;
	private String venueListArray;
	private String infoListArray;
	private String amenitiesListArray;

	public String getAmenitiesListArray() {
		return amenitiesListArray;
	}

	public void setAmenitiesListArray(String amenitiesListArray) {
		this.amenitiesListArray = amenitiesListArray;
	}

	public String getInfoListArray() {
		return infoListArray;
	}

	public void setInfoListArray(String infoListArray) {
		this.infoListArray = infoListArray;
	}

	public String getVenueListArray() {
		return venueListArray;
	}

	public void setVenueListArray(String venueListArray) {
		this.venueListArray = venueListArray;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getStarRating() {
		return starRating;
	}

	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyType() {
		return propertyTypeId;
	}

	public void setPropertyType(String propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	public String getOnlineInd() {
		return onlineInd;
	}

	public void setOnlineInd(String onlineInd) {
		this.onlineInd = onlineInd;
	}

	public List<VenueForm> getVenueList() {
		return venueList;
	}

	public void setVenueList(List<VenueForm> venueList) {
		this.venueList = venueList;
	}

	public List<String> getAmenitiesList() {
		return amenitiesList;
	}

	public void setAmenitiesList(List<String> amenitiesList) {
		this.amenitiesList = amenitiesList;
	}

	public String getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(String propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return "PropertyForm [propertyId=" + propertyId + ", propertyName=" + propertyName + ", starRating="
				+ starRating + ", propertyDescription=" + propertyDescription + ", propertyTypeId=" + propertyTypeId
				+ ", onlineInd=" + onlineInd + ", emailId=" + emailId + ", phoneNumber=" + phoneNumber + ", address1="
				+ address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode
				+ ", venueList=" + venueList + ", amenitiesList=" + amenitiesList + ", venueListArray=" + venueListArray
				+ "]";
	}

	public String getAltPhoneNumber1() {
		return altPhoneNumber1;
	}

	public void setAltPhoneNumber1(String altPhoneNumber1) {
		this.altPhoneNumber1 = altPhoneNumber1;
	}

	public String getAltPhoneNumber2() {
		return altPhoneNumber2;
	}

	public void setAltPhoneNumber2(String altPhoneNumber2) {
		this.altPhoneNumber2 = altPhoneNumber2;
	}

}