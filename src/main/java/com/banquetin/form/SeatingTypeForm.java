package com.banquetin.form;

import org.apache.struts.action.ActionForm;

public class SeatingTypeForm extends ActionForm {

	private static final long serialVersionUID = 5538425667248463644L;
	private String seatingTypeId;
	private String minCapacity;
	private String maxCapacity;

	public String getSeatingTypeId() {
		return seatingTypeId;
	}

	public void setSeatingTypeId(String seatingTypeId) {
		this.seatingTypeId = seatingTypeId;
	}

	public String getMinCapacity() {
		return minCapacity;
	}

	public void setMinCapacity(String minCapacity) {
		this.minCapacity = minCapacity;
	}

	public String getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(String maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

}