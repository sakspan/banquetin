package com.banquetin.form;

import org.apache.struts.action.ActionForm;

public class PromoForm extends ActionForm{

	private static final long serialVersionUID = 1L;

	private String promoId;
	private String promoName;
	private String promoDescription;
	private String promoValue;
	private String operationType;
	private int isActive;
	
	public String getPromoId() {
		return promoId;
	}
	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	public String getPromoDescription() {
		return promoDescription;
	}
	public void setPromoDescription(String promoDescription) {
		this.promoDescription = promoDescription;
	}
	public String getPromoValue() {
		return promoValue;
	}
	public void setPromoValue(String promoValue) {
		this.promoValue = promoValue;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	@Override
	public String toString() {
		return "PromoForm [promoId=" + promoId + ", promoName=" + promoName + ", promoDescription=" + promoDescription
				+ ", promoValue=" + promoValue + ", operationType=" + operationType + ", isActive=" + isActive +"]";
	}
	
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
}
