package com.banquetin.form;

import org.apache.struts.action.ActionForm;

public class AmenitiesForm extends ActionForm {

	private static final long serialVersionUID = 8977274393504908770L;

	private String amenitiesId;
	private String amenitiesText;
	private String amenitiesIcon;
	private String amenitiesDescription;
	private int sortOrder;

	public String getAmenitiesId() {
		return amenitiesId;
	}

	public void setAmenitiesId(String amenitiesId) {
		this.amenitiesId = amenitiesId;
	}

	public String getAmenitiesText() {
		return amenitiesText;
	}

	public void setAmenitiesText(String amenitiesText) {
		this.amenitiesText = amenitiesText;
	}

	public String getAmenitiesIcon() {
		return amenitiesIcon;
	}

	public void setAmenitiesIcon(String amenitiesIcon) {
		this.amenitiesIcon = amenitiesIcon;
	}

	public String getAmenitiesDescription() {
		return amenitiesDescription;
	}

	public void setAmenitiesDescription(String amenitiesDescription) {
		this.amenitiesDescription = amenitiesDescription;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
}