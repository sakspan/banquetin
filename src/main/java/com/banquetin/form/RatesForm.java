package com.banquetin.form;

import org.apache.struts.action.ActionForm;

public class RatesForm extends ActionForm {

	private static final long serialVersionUID = 1L;

	private String ratesId;
	private String venueId;
	private String startDate;
	private String endDate;
	private double rate;
	private String slotTypeId;
	private double bookingAmount;
	private String rateType;
	private String operationType;

	public String getRatesId() {
		return ratesId;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public void setRatesId(String ratesId) {
		this.ratesId = ratesId;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(double bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public String getSlotTypeId() {
		return slotTypeId;
	}

	public void setSlotTypeId(String slotTypeId) {
		this.slotTypeId = slotTypeId;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	@Override
	public String toString() {
		return "RatesForm [ratesId=" + ratesId + ", venueId=" + venueId + ", startDate=" + startDate + ", endDate="
				+ endDate + ", rate=" + rate + ", slotTypeId=" + slotTypeId + ", bookingAmount=" + bookingAmount
				+ ", rateType=" + rateType + "]";
	}

}