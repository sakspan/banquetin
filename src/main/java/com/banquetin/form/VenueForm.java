package com.banquetin.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import com.banquetin.dataobjects.ImageData;
import com.banquetin.dataobjects.PropertyData;
import com.banquetin.dataobjects.SeatingTypeData;
import com.banquetin.dataobjects.StaticData;

public class VenueForm extends ActionForm {

	private static final long serialVersionUID = 7818531118769669435L;

	private String venueId;
	private String venueDescription;
	private String venueName;
	private int length;
	private int width;
	private int height;
	private int sortOrder;
	private StaticData eventType;
	private List<SeatingTypeData> seatingType;
	private double minBookingAmount;
	private StaticData venueType;
	private List<ImageData> venueImageList;
	private PropertyData propertyData;

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public String getVenueDescription() {
		return venueDescription;
	}

	public void setVenueDescription(String venueDescription) {
		this.venueDescription = venueDescription;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public StaticData getEventType() {
		return eventType;
	}

	public void setEventType(StaticData eventType) {
		this.eventType = eventType;
	}

	public List<SeatingTypeData> getSeatingType() {
		return seatingType;
	}

	public void setSeatingType(List<SeatingTypeData> seatingType) {
		this.seatingType = seatingType;
	}

	public double getMinBookingAmount() {
		return minBookingAmount;
	}

	public void setMinBookingAmount(double minBookingAmount) {
		this.minBookingAmount = minBookingAmount;
	}

	public StaticData getVenueType() {
		return venueType;
	}

	public void setVenueType(StaticData venueType) {
		this.venueType = venueType;
	}

	public List<ImageData> getVenueImageList() {
		return venueImageList;
	}

	public void setVenueImageList(List<ImageData> venueImageList) {
		this.venueImageList = venueImageList;
	}

	public PropertyData getPropertyData() {
		return propertyData;
	}

	public void setPropertyData(PropertyData propertyData) {
		this.propertyData = propertyData;
	}

	@Override
	public String toString() {
		return "VenueForm [venueId=" + venueId + ", venueDescription=" + venueDescription + ", venueName=" + venueName
				+ ", length=" + length + ", width=" + width + ", height=" + height + ", sortOrder=" + sortOrder
				+ ", eventType=" + eventType + ", seatingType=" + seatingType + ", minBookingAmount=" + minBookingAmount
				+ ", venueType=" + venueType + ", venueImageList=" + venueImageList + ", propertyData=" + propertyData
				+ "]";
	}

}