
<section class="page-header widthBgimg"
	style="background-image: url(images/banner-img/service-bannerImg.jpg);">
	<div class="container">
		<h1>Decor & Florists</h1>
	</div>
</section>
<section class="content">
	<div class="container">
		<div class="service-view">
			<div class="row">
				<!-- <div class="col-md-3">
					<div class="left-side">
						<div class="filter-view">
							<div class="filter-block">
								<div class="title">
									<h2>Decor & Florists</h2>
								</div>
								<div class="check-slide">
									<label class="label_check" for="checkbox-1"> <input
										name="sample-checkbox-01" id="checkbox-1" value="1"
										type="checkbox">Wedding Flowers
									</label>
								</div>
								<div class="check-slide">
									<label class="label_check" for="checkbox-2"> <input
										name="sample-checkbox-01" id="checkbox-2" value="1"
										type="checkbox">Balloon Decorations
									</label>
								</div>
								<div class="check-slide">
									<label class="label_check" for="checkbox-3"> <input
										name="sample-checkbox-01" id="checkbox-3" value="1"
										type="checkbox">Room Decor
									</label>
								</div>
								<div class="check-slide">
									<label class="label_check" for="checkbox-4"> <input
										name="sample-checkbox-01" id="checkbox-4" value="1"
										type="checkbox">Car Decoration
									</label>
								</div>
							</div>
						</div>
						<div class="filter-view">
							<div class="filter-block booking-form">
								<div class="title">
									<h2>Enter Booking Details</h2>
								</div>
								<div class="form-filde">
									<div class="row">
										<div class="col-sm-12">
											<div class="input-box">
												<input type="text" placeholder="Name">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="input-box">
												<input type="text" placeholder="Email">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="input-box">
												<input type="text" placeholder="Mobile">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-box">
												<input type="text" placeholder="Date">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-box">
												<input type="text" placeholder="Time">
											</div>
										</div>
										<div class="col-sm-12">
											<div class="submit-box">
												<input type="submit" value="Book Now" class="btn">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="col-md-12">
					<div class="service-right">
						<div class="event-info">
							<h2>Wedding Flowers</h2>
							<p>Lorem Ipsum is simply dummy text of the printing and
								typesetting industry. Lorem Ipsum has been the industry's
								standard dummy text ever since the 1500s, when an unknown
								printer took a galley of type and scrambled it to make a type
								specimen book. It has survived not only five centuries, but also
								the leap into electronic typesetting, remaining essentially
								unchanged. It was popularised in the 1960s with the release of
								Letraset sheets containing Lorem Ipsum passages, and more
								recently with desktop publishing software like Aldus PageMaker
								including versions of Lorem Ipsum.</p>
							<p>It is a long established fact that a reader will be
								distracted by the readable content of a page when looking at its
								layout. The point of using Lorem Ipsum is that it has a
								more-or-less normal distribution of letters, as opposed to using
								'Content here, content here', making it look like readable
								English.</p>
							<div class="service-details">
								<div class="row">
									<div class="col-sm-4">
										<div class="img">
											<img src="images/property-img/locationImg5.jpg" alt="">
										</div>
									</div>
									<div class="col-sm-8">
										<h3>Ideal to be availed for:</h3>
										<ul class="customList">
											<li>Real Wedding Flowers</li>
											<li>Christmas Arrangements</li>
											<li>Local Florists</li>
											<li>Engagement Ceremony</li>
											<li>Social Events</li>
											<li>Flowers Advice & Etiquette</li>
											<li>Preparing bouquets</li>
										</ul>
										<table>
											<td align="left" valign="top"></td>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="event-galler">
							<h2>Photo Gallery</h2>
							<div class="event-gallerSlider">
								<div class="item">
									<img src="images/gallery-img/service-gallerImg1.jpg" alt="">
								</div>
								<div class="item">
									<img src="images/gallery-img/service-gallerImg2.jpg" alt="">
								</div>
								<div class="item">
									<img src="images/gallery-img/service-gallerImg3.jpg" alt="">
								</div>
								<div class="item">
									<img src="images/gallery-img/service-gallerImg1.jpg" alt="">
								</div>
								<div class="item">
									<img src="images/gallery-img/service-gallerImg2.jpg" alt="">
								</div>
								<div class="item">
									<img src="images/gallery-img/service-gallerImg3.jpg" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
