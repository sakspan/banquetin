
<header id="header">

	<nav id="nav-main">
		<div class="container">
			<div class="navbar navbar-inverse">

				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">

						<li><a href="propertyDetail.html">Property</a></li>
						<li><a href="addRates.html">Rates</a></li>
						<li><a href="search.html">Booking</a></li>
						<li><a href="addPromo.html">Configuration</a></li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</header>
