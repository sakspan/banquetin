<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header id="header">
	<div class="quck-link">
		<div class="container">
			<div class="mail">
				<a href="MailTo:contact@banquetin.com"><span
					class="icon icon-envelope"></span>contactus@banquetin.com</a>
			</div>
			<div class="right-link">
				<ul>
					<!-- <li><a href="adminlogin.jsp"><span
							class="icon icon-multi-user"></span>Become a Vendor</a></li> -->
					<li class="registration"><a href="register.jsp"
						data-toggle="modal" data-target="#registrationModal">Registration</a></li>

					<c:choose>
						<c:when test="${empty userDataSession}">
							<!-- <li><a href="login.jsp" data-toggle="modal"
								data-target="#loginModal">Login</a></li> -->
							<li><a href="adminLogin.html">Login</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="logout.html">Logout</a></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
	</div>
	<nav id="nav-main">
		<div class="container">
			<div class="navbar navbar-inverse">
				<div class="navbar-header">
					<a href="index.html" class="navbar-brand"><img
						src="images/logo.png" alt=""></a>
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon1-barMenu"></span> <span class="icon1-barMenu"></span>
						<span class="icon1-barMenu"></span>
					</button>

				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">

						<li><a href="index.html">Home</a></li>
						<li><a href="about.html">About Us</a></li>
						<!-- 	<li><a href="events.html">Events <span
								class="icon icon-arrow-down"></span></a>
							<ul>
								<li><a href="services.html">Caterers</a></li>
								<li><a href="services.html">Mehndi</a></li>
								<li><a href="services.html">Decor &amp; Florists</a></li>
								<li><a href="services.html">Cakes</a></li>
								<li><a href="services.html">Wedding Planner</a></li>
								<li><a href="services.html">Gifts and Flowers</a></li>
								<li><a href="services.html">Make-up and Hair</a></li>
								<li><a href="services.html">Entertainment</a></li>
								<li><a href="services.html">Photographers/
										Videographers</a></li>
								<li><a href="services.html">DJ</a></li>
								<li><a href="services.html">Wedding Cards</a></li>
							</ul></li>
						 -->

						<li><a href="faq.html">FAQ�</a></li>
						<li><a href="contact.html">Contact us</a></li>
					</ul>
					<div class="search-box">
						<div class="search-icon">
							<span class="icon icon-search"></span>
						</div>
						<div class="search-view">
							<div class="input-box">
								<form>
									<input type="text" placeholder="Search here"> <input
										type="submit" value="">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>
