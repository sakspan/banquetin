<div class="content">
	<div class="container">
		<div class="bookin-info">
			<div class="bookin-infoRow">
				<div class="bookin-id">
					Booking ID : <span> <%=(String)request.getParameter("bookingId") %></span>
				</div>
			</div>
			<div class="thanks-msg">
				<div class="icon icon-right-sign"></div>
				<h3>Booking has been cancelled successfully</h3>
				<p>The booking is cancelled. Please refer to cancellation policy. You can book another using the option below.</p>
			</div>

			<a href="search.html" class="btn">Book Another</a>
		</div>
	</div>
</div>
