<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="searchFilter-main">
	<section class="searchFormTop">
		<div class="container">
			<div class="searchCenter">
				<div class="refineCenter">
					<span class="icon icon-filter"></span> <span>Refine Results</span>
				</div>
				<div class="searchFilter row">
					<form action="searchresult.html" method="get">
						<div class="input-box col-lg-3">
							<div class="icon icon-grid-view"></div>
							<input type="text" placeholder="Event Type" value="${eventType}"
								name="eventtype">
						</div>
						<div class="input-box pax col-lg-3">
							<div class="icon icon-grid-view"></div>
							<input type="text" placeholder="No. of People" value="${pax}"
								name="pax">
						</div>
						<div class="input-box searchlocation col-lg-3">
							<div class="icon icon-location-1"></div>
							<input type="text" placeholder="City" value="${location}"
								name="location">
						</div>
						<div class="input-box date col-lg-3">
							<div class="icon icon-calander-month"></div>
							<input type="text" placeholder="dd/mm/yyyy" value="${startDate}"
								id="datepicker2" name="date">
						</div>
						<input type="submit" value="Search" class="btn">
					</form>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="#">Home</a>/</li>
					<li><a href="#">${eventType}</a>/</li>
					<li class="active"><a href="#">${location}</a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="venues-view">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-12">
						<div class="left-side">
							<div class="search">
								<div class="search-icon">
									<div class="icon icon-search"></div>
								</div>
								<input type="text" placeholder="Search by name">
							</div>
							<div class="filter-view">

								<div class="filter-block">
									<div class="title">
										<h2>Number of Guests</h2>
										<div class="reste-filter">
											<a href="javascript:reset();"><span class="icon icon-reset"></span>Reset</a>
										</div>
									</div>
									<div class="check-slide">
										<label class="label_check" for="checkbox-22"><input
											type="checkbox" name="sample-checkbox-01" id="checkbox-22"
											value="1">&lt; 10</label>
									</div>
									<div class="check-slide">
										<label class="label_check" for="checkbox-23"><input
											type="checkbox" name="sample-checkbox-02" id="checkbox-23"
											value="1">10 - 100</label>
									</div>
									<div class="check-slide">
										<label class="label_check" for="checkbox-24"><input
											type="checkbox" name="sample-checkbox-03" id="checkbox-24"
											value="1">100 - 200</label>
									</div>
									<div class="check-slide">
										<label class="label_check" for="checkbox-25"><input
											type="checkbox" name="sample-checkbox-04" id="checkbox-25"
											value="1" >200 - 500</label>
									</div>
									<div class="check-slide">
										<label class="label_check" for="checkbox-26"><input
											type="checkbox" name="sample-checkbox-05" id="checkbox-26"
											value="1">&gt; 500</label>
									</div>
								</div>

								<div class="filter-block">
									<div class="title">
										<h2>Price</h2>
									</div>
									<div class="check-slide">Price Filter to come here</div>
								</div>

								<div class="filter-block">
									<div class="title">
										<h2>Rating</h2>
									</div>
									<div class="check-slide">Rating Scale to come here</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-9 col-lg-9 col-sm-12">
						<div class="right-side">
							<div class="toolbar">
								<div class="finde-count">${venueDataList.size()}&nbsp;Venue(s)
									found.</div>
								<div class="right-tool">
									<!-- <a href="#" class="shortlist-btn"><span
										class="icon icon-heart-filled"></span>7 Shortlist</a> -->
									<!-- <div class="link">
										<ul>
											<li><a href="#">Map</a></li>
											<li class="active"><a href="#">List</a></li>
										</ul>
									</div>  -->
								</div>

							</div>
							<c:forEach var="venue" items="${venueDataList}">
								<div class="venues-slide first">
									<div class="img">
										<c:forEach var="venueImage" items="${venue.venueImageList}">
											<c:if test="${venueImage.isPrimary eq 1}">
												<img src="${venueImage.path}" alt="${venueImage.altText}">
											</c:if>
										</c:forEach>
									</div>
									<div class="text">
										<a href="searchdetail.html?venueId=${venue.venueId}"><h3>${venue.venueName}
												- by ${venue.propertyData.propertyName}</h3></a>
										<div class="description">${venue.venueDescription}</div>
										<div class="address">
											${venue.propertyData.addressList[0].address1},
											${venue.propertyData.addressList[0].address2},
											${venue.propertyData.addressList[0].city},
											${venue.propertyData.addressList[0].stateId} -
											${venue.propertyData.addressList[0].pincode},
											${venue.propertyData.addressList[0].countryId}</div>
										<div class="outher-info">

											<div class="info-slide first">
												<label>Dimension</label> <span>${venue.length}ft x ${venue.width}ft x ${venue.height}ft</span>
											</div>
											<div class="info-slide">
												<label>Venue Type</label> <span>${venue.venueType.text}</span>
											</div>
											<div class="info-slide">
												<label>Price Range</label> <span>Rs. ${venue.minBookingAmount * 10} <small>(onwards)</small></span>
											</div>
											<div class="info-slide">
												<label>Min. Booking Amount</label> <span>Rs.
													${venue.minBookingAmount} <small>(onwards)</small>
												</span>
											</div>
											<br />
											 <label>Seating Capacity</label>
											<c:forEach var="seatingType" items="${venue.seatingType}">
												<div class="info-slide">
													<label>${seatingType.seatingTypeText}</label> <span>
														${seatingType.minCapacity} - ${seatingType.maxCapacity} </span>
												</div>
											</c:forEach>



										</div>
										<div class="button">
											<a href="#" class="btn">Book Now</a> <a href="javascript:;"
												class="btn gray">View Amenities <span
												class="icon icon-arrow-down"></span></a>
										</div>
									</div>
									<div class="amenities-view">
										<h2>All Amenities :</h2>
										<c:forEach var="amenity"
											items="${venue.propertyData.amenitiesList}">
											<c:set var="className" value="a" />
											<c:if test="${amenity.isEnabled() eq false}">
												<c:set var="className" value="disabled" />
											</c:if>
											<div class="amenities-box ${className}">
												<div class="icon ${amenity.amenitiesIcon}"></div>
												<span>${amenity.amenitiesText}</span>
											</div>
										</c:forEach>
									</div>
									<div class="modal fade modal-vcenter" id="contactModal1"
										tabindex="-1" role="dialog">
										<div class="modal-dialog contactvendor-popup" role="document">
											<div class="modal-content">
												<div class="close-icon" aria-label="Close"
													data-dismiss="modal">
													<img src="images/close-icon.png" alt="">
												</div>
												<h1>Mariom Banquet</h1>
												<div class="note">Lorem Ipsum has been the industry's
													standard dummy text ever since the 1500s.</div>
												<div class="row">
													<div class="col-sm-6">
														<div class="input-slide">
															<input type="text" placeholder="First Name">
														</div>
													</div>
													<div class="col-sm-6">
														<div class="input-slide">
															<input type="text" placeholder="Last Name ">
														</div>
													</div>
													<div class="col-sm-6">
														<div class="input-slide">
															<input type="text" placeholder="Email ID">
														</div>
													</div>
													<div class="col-sm-6">
														<div class="input-slide">
															<input type="text" placeholder="Phone Number">
														</div>
													</div>
													<div class="col-sm-12">
														<div class="input-slide">
															<textarea placeholder="Message"></textarea>
														</div>
													</div>
													<div class="col-sm-12">
														<div class="submit-slide">
															<input type="submit" value="Send" class="btn">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>

						<div class="pagination">
							<ul>
								<li class="prev disabled"><a href="#">Prev</a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li class="next"><a href="#">Next</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
if(${pax}<10){
	document.getElementById("checkbox-22").checked = true;
	$('.label_check input:checked').each(function(){ 
		$(this).parent('label').addClass('c_on');
	});
}
if(${pax}>=10 && ${pax}<100){
	document.getElementById("checkbox-23").checked = true;
	$('.label_check input:checked').each(function(){ 
		$(this).parent('label').addClass('c_on');
	});
}
if(${pax}>=100 && ${pax}<200){
	document.getElementById("checkbox-24").checked = true;
	$('.label_check input:checked').each(function(){ 
		$(this).parent('label').addClass('c_on');
	});
}
if(${pax}>=200 && ${pax}<500){
	document.getElementById("checkbox-25").checked = true;
	$('.label_check input:checked').each(function(){ 
		$(this).parent('label').addClass('c_on');
	});
}
if(${pax}>=500){
	document.getElementById("checkbox-26").checked = true;
	$('.label_check input:checked').each(function(){ 
		$(this).parent('label').addClass('c_on');
	});
}
</script>
