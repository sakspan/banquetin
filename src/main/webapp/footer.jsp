
<footer id="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-sm-6 col-md-3 text-center">
					<div class="footer-link">
						<h5>Company</h5>
						<ul>
							<li><a href="aboutUs.html">About Us</a></li>
					
						<!--  	<li><a href="privacy_policy.html">Privacy Policy</a></li> 
 							<li><a href="career.html">Careers</a></li>		
							<li><a href="privacy_policy.html">Privacy Policy</a></li>
						<li><a href="career.html">Careers</a></li>
							<li><a href="blog.html">Blogs</a></li> -->
 						<li><a href="contact.html">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-md-3">
					<div class="footer-contact">
						<h5>Contact us</h5>
						<div class="contact-slide">
							<div class="icon icon-location-1"></div>
							<p>Bangalore, Bangalore</p>
						</div>
						<div class="contact-slide">
							<div class="icon icon-phone"></div>
							<p>1234567890</p>
						</div>

						<div class="contact-slide">
							<div class="icon icon-message"></div>
							<a href="MailTo:contact@banquetin.com">contact@banquetin.com</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6 col-md-3">
					<div class="contact-form">
						<h5>Connect with us</h5>
						<div class="sosal-midiya">
							<ul>
								<li><a href="#"><span class="icon icon-facebook"></span></a></li>
								<li><a href="#"><span class="icon icon-twitter"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<p>
				Copyright &copy; <span></span> - Banquetin.com | All Rights Reserved
			</p>
		</div>
	</div>
</footer>