<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- <c:set var="isReadOnly" value=' readonly="readonly" '/>
<c:set var="isDisabled" value=' disabled="disabled" '/> --%>



<c:if test="${empty propertyList}">	
	<c:set var="isReadOnly" value="" />
	<c:set var="isDisabled" value="" />
</c:if>

<form  id="msform" method="post">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Basic Details</li>
		<li>Contact</li>
		<li>Venues</li>
		<li>Amenities, Info & Others</li>
	</ul>
	<!-- fieldsets -->
	<fieldset id="property-details">
		<h2 class="fs-title">Basic Details</h2>

		<div class="col-xs-6 form-group">
			<input type="hidden" id="propertyId"
				value="${propertyList[0].propertyId}" /> <input type="text"
				class="placeholder-field" name="propertyName" id="propertyName"
				${isReadOnly} value="${propertyList[0].propertyName}" /> <label
				alt='Placeholder' placeholder='Property Name'></label>
			<div class="error-msg property-name-error">Please enter
				property name</div>
		</div>
		<div class="col-xs-6 form-group">
			<input type="text" class="placeholder-field" name="starRating"
				id="starRating" ${isReadOnly} value="${propertyList[0].starRating}" />
			<label alt='Placeholder' placeholder='Rating'></label>
			<div class="error-msg ratings-error">Please enter Star rating
				from 1-5</div>
		</div>
		<div class="col-xs-12 form-group">
			<input type="text" name="propertyDescription"
				class="placeholder-field" id="propertyDescription" ${isReadOnly}
				value="${propertyList[0].propertyDescription}" /> <label
				alt='Placeholder' placeholder='Description'></label>
			<div class="error-msg property-description-error">Please enter
				property description</div>
		</div>
		<div class="col-xs-6 form-group">
			<select class="form-control" name="propertyType" id="propertyTypeId"
				${isDisabled}>
				<option value="-1">Select Property Type</option>
				<c:forEach var="propertyType" items="${propertyTypeList}">
					<option value="${propertyType.id}"<c:if test="${propertyType.id == propertyList[0].propertyType.id}">selected ="selected"</c:if>>${propertyType.text}</option>
				</c:forEach>
			</select>
			<div class="error-msg property-type-error">Please select property type</div>
		</div>
		<div class="col-xs-6 form-group">
			<select class="form-control" name="onlineInd" id="onlineInd" ${isDisabled}>
				<option value="-1">Want to show online?</option>
				<option value="Y" <c:if test="${propertyList[0].onlineInd == 'Y'}">selected ="selected"</c:if> >Yes</option>
				<option value="N" <c:if test="${propertyList[0].onlineInd == 'N'}">selected ="selected"</c:if> >No</option>
			</select>
			<div class="error-msg online-ind-error">Please select online indicator</div>
			
		</div>
		<div class="col-xs-12 form-group">
			<div class="col-xs-6">
				<button class="propertyUpdate save-button" onclick="!validateBasicDetailsForm();"/>Save</button>
			</div>
			<div class="col-xs-6">
				<input type="button" name="next" class="next action-button" value="Next" />
			</div>
		</div>
	</fieldset>
	<fieldset id="contact-details">
		<h2 class="fs-title">Contact Form</h2>
		<div class="col-xs-4 form-group">
			<input type="text" name="emailId" placeholder="Email" id="emailId" value="${propertyList[0].emailList[0].emailAddress}"/>
			<div class="error-msg email-error">Please enter valid email</div>
		</div>
		<div class="col-xs-4 form-group">
			<input type="text" name="phoneNumber" placeholder="Phone Number" id="phoneNumber" value="${propertyList[0].phoneList[0].phoneNumber}"/>
			<div class="error-msg phone-error">Please enter valid Phone Number</div>
		</div>
		<div class="col-xs-4 form-group">
			<input type="text" name="altPhoneNumber1" placeholder="Alternate Number" id="altPhoneNumber1" value="${propertyList[0].phoneList[1].phoneNumber}"/>
			<div class="error-msg alt-phone-error">Please enter valid Phone Number</div>
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-6 form-group">
			<input type="text" name="address1" placeholder="Address Line1" id="address1" value="${propertyList[0].addressList[0].address1}"/>
			<div class="error-msg address-error">Please enter valid address</div>
		</div>
		<div class="col-xs-6 form-group">
			<input type="text" name="address2" placeholder="Addres Line2" id="address2" value="${propertyList[0].addressList[0].address2}"/>
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-6 form-group">
			<input type="text" name="city" placeholder="City" id="city" value="${propertyList[0].addressList[0].city}"/>
			<div class="error-msg city-error">Please enter valid city</div>
		</div>
		<div class="col-xs-6 form-group">
			<input type="text" name="zipCode" placeholder="Zip Code" id="zipCode" value="${propertyList[0].addressList[0].pincode}"/>
			<div class="error-msg zipCode-error">Please enter valid Zip Code</div>
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-6 form-group">
			<select class="form-control" name="state" id="state">
				<option value="-1">Select State</option>
				<c:forEach var="state" items="${statesList}">
					<option value="${state.id}" <c:if test="${state.id == propertyList[0].addressList[0].stateId}">selected ="selected"</c:if>>${state.desc}</option>
				</c:forEach>
			</select>
			<div class="error-msg state-error">Please select valid State</div>
		</div>
		<div class="col-xs-12 form-group">
			<div class="col-xs-4 form-group">
				<button class="propertyUpdate save-button" onclick="!validateContactDetailsForm()">Save</button>
			</div>
			<div class="col-xs-4 form-group">
				<input type="button" name="previous" class="previous action-button"
					value="Previous" />
			</div>
			<div class="col-xs-4 form-group">
				<input type="button" name="next" class="next action-button"
					value="Next" />
			</div>
		</div>
	</fieldset>
	<fieldset id="venue-details">
		<h2 class="fs-title">Add Venues</h2>
		<div class="tableContainer" style="background-color: white;">
			<table class="table table-responsive table-hover" id="venueTable">
				<thead>
					<tr>
						<th>Venue Name</th>
						<th>Venue Description</th>
						<th>Length</th>
						<th>Width</th>
						<th>Height</th>
						<th>Venue Type</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${not empty propertyList}">
					<c:forEach var="venueList" items="${propertyList[0].venueList}" varStatus="index">
					<tr>
						<td>${venueList.venueName}</td>
						<td>${venueList.venueDescription}</td>
						<td>${venueList.length}</td>
						<td>${venueList.width}</td>
						<td >${venueList.height}</td>
						<td>${venueList.venueType.text}</td>
						<td>
							<button data-toggle="modal" data-target="#venueModal" class="btn btn-sm btn-success editVenue"
									data-venueId="${venueList.venueId}"
									data-venueName="${venueList.venueName}" data-venueId="${venueList.venueId}"
									 data-venueDesc="${venueList.venueDescription}" data-length="${venueList.length}" 
									 data-width="${venueList.width}" data-height="${venueList.height}" 
									 data-venueTypeId="${venueList.venueType.id}" data-eventTypeId=""
									 data-seatingTypeOneMin='${venueList.seatingType[0].minCapacity}'
									 data-seatingTypeOneMax='${venueList.seatingType[0].maxCapacity}'
									 data-seatingTypeTwoMin='${venueList.seatingType[1].minCapacity}'
									 data-seatingTypeTwoMax='${venueList.seatingType[1].maxCapacity}'
									 data-seatingTypeThreeMin='${venueList.seatingType[2].minCapacity}'
									 data-seatingTypeThreeMax='${venueList.seatingType[2].maxCapacity}'
									 data-seatingTypeFourMin='${venueList.seatingType[3].minCapacity}'
									 data-seatingTypeFourMax='${venueList.seatingType[3].maxCapacity}'
									 data-seatingTypeFiveMin='${venueList.seatingType[4].minCapacity}'
									 data-seatingTypeFiveMax='${venueList.seatingType[4].maxCapacity}'>Edit</button>
							
							<button class="btn btn-sm btn-danger deleteVenue" data-venueId="${venueList.venueId}">Delete</button></td>
					</tr>
					</c:forEach>
				</c:if>
				</tbody>	
			</table>
		</div>
		<div class="row">
			<!-- Modal -->
			<div class="modal fade" id="venueModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Venue</h4>
						</div>
						<div class="modal-body">
							<section class="scrollable wrapper w-f venueDataForm">
								<div id="venueForm">
									<section class="panel panel-default">
										<div class="panel-body">
											<div class="venueInfo">
												<div class="col-xs-12 form-group">
													<input type="hidden" id="venueId" class="form-control"
														name="venueId" placeholder="venueId">
												</div>
												<div class="col-xs-12 form-group">
													<input type="text" id="venueName" class="form-control"
														name="venueName" placeholder="Name">
													<div class="error-msg venue-name-error">Please enter Venue name</div>
												</div>
												<div class="col-xs-12 form-group">
													<input type="text" id="venueDescription"
														class="form-control" name="venueDescription"
														placeholder="Description">
													<div class="error-msg venue-desc-error">Please enter Venue description</div>
												</div>
												<div class="col-xs-4 form-group">
													<input type="text" id="length" class="form-control"
														name="length" placeholder="Length"><span class="feets">ft</span>
												</div>
												<div class="col-xs-4 form-group">
													<input type="text" id="width" class="form-control"
														name="width" placeholder="Width"><span class="feets">ft</span>
												</div>
												<div class="col-xs-4 form-group">
													<input type="text" id="height" class="form-control"
														name="height" placeholder="Height"><span class="feets">ft</span>
												</div>
												<div class="col-xs-12 form-group">
													<div class="error-msg dimensions-error">Please enter valid dimensions</div>
												</div>
												<!-- <div class="col-xs-12 form-group">
													<input type="hidden" id="minBookingAmount" value="0"
														class="form-control" name="minBookingAmount"
														placeholder="Minimum Booking Amount">
													<div class="error-msg minBookingAmount-error">Please enter valid amount</div>
												</div> -->
											</div>
											
											<div class="col-xs-6 form-group">
												<select class="form-control" id="venueTypeId" style="height: 35px; padding: 0 0 0 10px;">
													<option value="-1">Select Venue Type</option>
					<c:forEach var="venueType" items="${venueTypeList}">
						<option value="${venueType.id}"
							<c:if test="${venueType.id == propertyList[0].venueList[0].venueType.id}">selected ="selected"</c:if>>${venueType.text}</option>
													</c:forEach>
												</select>
												<div class="error-msg venue-type-error">Please select valid venue type</div>
											</div>
											<div class="col-xs-6 form-group">
												<select class="form-control" id="eventTypeId"
													style="height: 80px; padding: 0 0 0 10px;" multiple>
													<option value="-1">Select Event Type:</option>
													<c:forEach var="eventType" items="${eventTypeList}">
														<option value="${eventType.eventTypeId}">${eventType.eventTypeText}</option>
													</c:forEach>
												</select>
												<div class="error-msg event-type-error">Please select valid event type</div>
											</div>

											<div class="col-xs-12 form-group">
												<label class="col-sm-3" style="margin-bottom: 15px;">Seating
													Type:</label>
												<c:forEach var="seatingType" items="${seatingTypeList}" varStatus="index">
													<div class="col-sm-12 seatingTypeList">
														<div class="col-xs-4 form-group" style="margin-top: 5px;">
															<span data-seating-id="${seatingType.id}"
																style="margin-top: 10px;">${seatingType.text}</span>
														</div>
														<div class="col-xs-4 form-group">
															<input id="${seatingType.id}_minCapacity"
																data-min-name="minCapacity" placeholder="Min Capacity"
																style="height: 35px;"/>
														</div>
														<div class="col-xs-4 form-group">
															<input id="${seatingType.id}_maxCapacity"
																data-max-name="maxCapacity" placeholder="Max Capacity"
																style="height: 35px;" value="${propertyList[0].venueList[0].seatingTypeList[index].maxCapacity}"/>
														</div>
													</div>
												</c:forEach>
												<div class="col-sm-12">
													<div class="error-msg seating-type-error">Please enter valid min and max capacity</div>
												</div>
											</div>
										</div>
								</div>
							</section>
							
							<button class="btn btn-sm btn-success venueSave showVenueSubmit "
								data-dismiss="modal">Submit</button>
							<button class="btn btn-sm btn-primary venueSave showVenueSave" data-dismiss="modal">Save</button>
							<button class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="buttonSection">
			<input type="button" name="add" class="action-button addVenue"
				data-toggle="modal" data-target="#venueModal" value="Add Venue" /><br />
			<input type="button" name="previous" class="previous action-button"
				value="Previous" /> <input type="button" name="next"
				class="next action-button" value="Next" />
		</div>
	</fieldset>
	<fieldset>
		<h2 class="fs-title">Amenities, Info & Others</h2>
		<div class="col-xs-12 form-group">
			<label class="col-sm-2">Amenities:</label>
				<c:forEach var="amenitiesType" items="${propertyList[0].amenitiesList}" varStatus="list">
					<div class="selectBox<c:if test="${amenitiesType.enabled}"> selected</c:if>" data-amenities-id="${amenitiesType.amenitiesId}">
						<span class="amenitiesIcon ${amenitiesType.amenitiesIcon}"></span><br>
						<span class="icon-text">${amenitiesType.amenitiesText}</span>
					</div>	
				</c:forEach>
		</div>
		<div class="infoSection panel panel-default">
			<div class="panel-body">
				<!-- Table -->
				<div class="tableContainer" style="background-color: white;">
					<table class="table table-responsive table-hover" id="infoTable">
						<thead>
							<tr>
								<th>Name</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty propertyList}">
								<c:forEach var="infoList" items="${propertyList[0].propertyInfo}" varStatus="index">
									<tr>
										<td>${infoList.title}</td>
										<td>${infoList.value}</td>
										<td><button class='btn btn-sm btn-success editInfo' data-propertyTitle="${infoList.title}"
										 		data-propertyValue="${infoList.value}"
										 		data-propertyInfoId="${infoList.propertyInfoId}"
										 		data-toggle="modal" data-target="#infoModal">Edit</button>
										<button class='btn btn-sm btn-danger deleteInfo' data-propertyInfoId="${infoList.propertyInfoId}">Delete</button></td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
				<input type="button" name="add" class="action-button addInfo"
					data-toggle="modal" data-target="#infoModal" value="Add Info" /><br />
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="infoModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Info</h4>
					</div>
					<div class="modal-body">
						<section class="scrollable wrapper w-f infoDataForm">
							<div id="venueForm">
								<section class="panel panel-default">
									<div class="panel-body">
										<div class="col-xs-12 form-group">
											<input type="hidden" id="propertyInfoId" class="form-control" name="propertyInfoId">
										</div>
										<div class="col-xs-12 form-group">
											<input type="text" id="title" class="form-control"
												name="title" placeholder="Name" value="" value="${propertyList[0].propertyInfo[0].title}">
										</div>
										<div class="col-xs-12 form-group">
											<textarea id="value" class="form-control" name="value" value="${propertyList[0].propertyInfo[0].value}"></textarea>
										</div>
									</div>
								</section>
								<button class="btn btn-sm btn-primary infoSave" data-dismiss="modal">Save</button>
								<button class="btn btn-sm btn-success updateInfoSubmit" data-dismiss="modal">Submit</button>
								<button class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
							</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="others" class="form-control" name="others" placeholder="Others"> 
		<input type="button" name="previous" class="previous action-button" value="Previous" /> 
		<button class="save-button amenitiesSave"/>Save</button>
	</fieldset>
	<input type="hidden" name="venueListArray" id="venueList" /> 
	<input type="hidden" name="infoListArray" id="infoList" /> 
	<input type="hidden" name="amenitiesListArray" id="amenitiesList" />

</form>