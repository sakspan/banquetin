<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	function searchResults() {
		var selDate = document.getElementById("selectedDate").value;
		var paxCount = document.getElementById("pax").value;
		location.href = "bookings.html?selectedDate=" + selDate + "&paxCount="
				+ paxCount;
	}
</script>
<form action="bookingSubmit.do" id="bookingForm" method="post">


	<div class="content">
		<div class="container">

			<div class="bookin-info">
				<div class="bookinRow">
				<jsp:include page="BookedCalendar.jsp" />
					<div class="col-xs-4 form-group">
						<label>Date:</label> <input type="text" id="selectedDate"
							name="bookingDate" class="datepicker" placeholder="Date"
							value="${param.selectedDate}">
					</div>
					<div class="col-xs-4 form-group">
						<label>Pax:</label> <input type="text" id="pax" placeholder="Pax"
							name="paxCount" value="${param.paxCount}">
					</div>

					<a href="javascript:searchResults();" class="btn">Search</a>
				</div>


				<div id="step0">
					<table class="bookin-table">
						<tr>
							<th colspan="6" class="table-heading first">Venue1 <a
								href="#" class="icon icon-delete"></a></th>
						</tr>
						<tr>
							<th class="first Theading">Details</th>
							<th class="Theading">Dimension (L * W * H)</th>
							<th class="Theading">Seating Types (Capacity)</th>
							<th class="Theading">Price</th>
							<th class="Theading">Booking Amount</th>
							<th class="Theading last">Availability</th>
						</tr>

						<c:forEach var="venue" items="${venueList}">
							<tr>
								<td class="first">
									<p>${venue.venueDescription}</p>
								</td>
								<td>
									<p>${venue.length}*${venue.width}*${venue.height}</p>
								</td>
								<td><c:forEach var="seatingTypeObj"
										items="${venue.seatingType}">
										<p>${seatingTypeObj.seatingTypeText}
											(${seatingTypeObj.minCapacity} -
											${seatingTypeObj.maxCapacity})</p>
									</c:forEach></td>
								<td><label>Price Range</label>
									<p>
										Rs. ${venue.minBookingAmount * 10}</small>
									</p></td>
								<td>
									<p>Rs. ${venue.minBookingAmount}</p>
								</td>
								<td class="last">
								<c:if test="${venue.morningAvailable}">
								<a href="javascript:showStep1(${venue.venueId},'1',${venue.minBookingAmount},${venue.minBookingAmount *10},'${venue.venueName}','${venue.venueDescription}');">Morning</a>
								</c:if>
								
								<c:if test="${venue.eveningAvailable}">
								&nbsp;<a href="javascript:showStep1(${venue.venueId},'2',${venue.minBookingAmount},${venue.minBookingAmount *10},'${venue.venueName}','${venue.venueDescription}');">Evening</a>
								</c:if>
								<c:if test="${!venue.morningAvailable}">
								Morning
								</c:if>
								
								<c:if test="${!venue.eveningAvailable}">
								Evening
								</c:if>
								
								</td>
								
									
							</tr>
							<input type="hidden" id="venueName" name="venueName" value=""/>
							<input type="hidden" id="venueDescription" name="venueDescription" value=""/>
						</c:forEach>
					</table>
				</div>

				<div id="bookingSteps">

					<div class="step-nav">
						<div class="container">
							<div class="inner-nav">
								<ul>
									<li id="firstStep" class="first active"><a href="#"><span
											class="number">1</span><span class="text">Booking
												Details</span></a></li>
									<li id="secondStep"><a href="#"><span class="number">2</span><span
											class="text">Payment Details</span></a></li>
									<li id="thirdStep" class="last"><a href="#"><span
											class="number">3</span><span class="text">Order
												Confirm</span></a></li>
								</ul>
							</div>
						</div>
					</div>

					<div id="step1">

							<div class="bookinRow">
							<div class="col-xs-4 form-group">
								<input id="guestName" name="guestName" type="text" class="placeholder-field">
								<label alt='Placeholder' placeholder='Guest name' class="bookingDetails"></label>
								<div class="error-msg booking-guestName-error">Please enter Guest Name</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="emailId" name="emailId" class="placeholder-field">
								<label alt='Placeholder' placeholder='Email ID' class="bookingDetails"></label>
								<div class="error-msg booking-email-error">Please enter valid email</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="phoneNum" name="phoneNum" class="placeholder-field">
								<label alt='Placeholder' placeholder='Phone' class="bookingDetails"></label>
								<div class="error-msg booking-phone-error">Please enter valid Phone Number</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="address1" name="address1" class="placeholder-field">
								<label alt='Placeholder' placeholder='Address1' class="bookingDetails"></label>
								<div class="error-msg booking-address-error">Please enter Address</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="address2" name="address2" class="placeholder-field">
								<label alt='Placeholder' placeholder='Address2' class="bookingDetails"></label>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="city" name="city" class="placeholder-field">
								<label alt='Placeholder' placeholder='City name' class="bookingDetails"></label>
								<div class="error-msg booking-city-error">Please enter City</div>
							</div>
							<div class="col-xs-4 form-group">
								<select class="form-control" id="state" name="state">
									<option value="-1">Select State</option>
									<c:forEach var="state" items="${statesList}">
										<option value="${state.id}">${state.desc}</option>
									</c:forEach>
								</select>
								<div class="error-msg booking-state-error">Please Select State</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="pincode" name="pincode" class="placeholder-field">
								<label alt='Placeholder' placeholder='Pin Code' class="bookingDetails"></label>
								<div class="error-msg booking-pinCode-error">Please enter valid pincode</div>
							</div>
							<div class="col-xs-12 form-group">
								<input type="text" id="comments" name="comments" class="placeholder-field">
								<label alt='Placeholder' placeholder='Comments' class="bookingDetails"></label>
							</div>

							<input id="venueId" name="venueId" value="" type="hidden" /> <input
								id="slotType" name="slotType" value="" type="hidden" />


						</div>
						<a href="javascript:showStep0();" class="btn btns btn-prev">Prev</a>
						<a href="javascript:showStep2();" class="btn btns btn-next">Next</a>
					</div>

					<div id="step2">
						<div class="bookinRow">
							<div class="col-xs-6 form-group">
								<label>Estimated Amount: </label><br> 
								<input type="text" id="estAmount" name="estAmount" placeholder="Estimated Amount">
								<div class="error-msg payment-estAmount-error">Please enter Estimated Amount</div>
							</div>
							<div class="col-xs-6 form-group">
								<label>Booking Amount: </label><br> <input type="text"
									id="bookingAmount" name="bookingAmount"
									placeholder="Booking Amount">
									<div class="error-msg payment-bookingAmount-error">Please enter Booking Amount</div>
							</div>
							<div class="col-xs-6 form-group">
								<input type="text" id="advancePaid" name="advancePaid" class="placeholder-field">
								<label alt='Placeholder' placeholder='Advance Paid' class="bookingDetails"></label>
								<div class="error-msg payment-advancedPaid-error">Please enter Advance amount paid</div>
							</div>
							<div class="col-xs-6 form-group">
								<select class="form-control" id="paymentType" name="paymentType">
									<option value="-1">Select Payment Type</option>
									<c:forEach var="paymentTypeData" items="${paymentTypeList}">
										<option value="${paymentTypeData.id}">${paymentTypeData.text}</option>
									</c:forEach>
								</select>
								<div class="error-msg payment-paymentType-error">Please select Payment Type</div>
							</div>
						</div>
						<a href="javascript:showStep1();" class="btn btns btn-prev">Prev</a>
						<a href="javascript:showStep3();" class="btn btns btn-next">Next</a>
					</div>

					<div id="step3">

						<div class="bookinRow">Please verify the details below and
							click on CONFIRM to complete the booking</div>

						<div class="bookinRow">

							<div class="col-xs-4 form-group">
								Venue: <label id="venueDisplay">venueDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Date: <label id="dateDisplay">dateDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Pax: <label id="paxDisplay">paxDisplay</label>
							</div>

							<div class="col-xs-4 form-group">
								Guest Name: <label id="guestNameDisplay">DisplayGuestName</label>
							</div>
							<div class="col-xs-4 form-group">
								Email Id: <label id="emailIdDisplay">DisplayEmailID</label>
							</div>
							<div class="col-xs-4 form-group">
								Phone Number: <label id="phoneNumDisplay">phoneNumDisplay</label>
							</div>
							<div class="col-xs-8 form-group">
								Address: <label id="address1Display">address1Display</label>
							</div>
						
							<div class="col-xs-4 form-group">
								Payment Type: <label id="paymentTypeDisplay">paymentTypeDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Estimated Amount: <label id="estAmountDisplay">estAmountDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Booking Amount: <label id="bookingAmountDisplay">bookingAmountDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Advance Paid: <label id="advancePaidDisplay">advancePaidDisplay</label>
							</div>
							<div class="col-xs-8 form-group">
								Comments: <label id="commentsDisplay">commentsDisplay</label>
							</div>
							
						</div>
						<a href="javascript:showStep2();" class="btn btns btn-prev">Prev</a>
						<a href="javascript:submitBookingForm();"
							class="btn btns btn-confirm">Confirm</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
