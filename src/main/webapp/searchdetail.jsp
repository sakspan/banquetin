<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="simple-banner">
	<div class="banner-img">
		<c:forEach var="propertyImage"
			items="${venueData.propertyData.propertyImageList}">
			<c:if test="${propertyImage.isPrimary eq 1}">
				<img src="${propertyImage.path}" alt="${propertyImage.altText}">
			</c:if>
		</c:forEach>
	</div>
	<div class="text">
		<div class="inner-text">
			<h2>${venueData.venueName}</h2>
			<p>
				${venueData.propertyData.addressList[0].address1},
				${venueData.propertyData.addressList[0].address2},
				${venueData.propertyData.addressList[0].city} <br />
				${venueData.propertyData.addressList[0].stateId} -
				${venueData.propertyData.addressList[0].pincode},
				${venueData.propertyData.addressList[0].countryId}
			</p>
		</div>
	</div>
</div>
<section class="content">
	<div class="breadcrumb">
		<div class="container">
			<ul>
				<li><a href="#">Home</a>/</li>
				<li class="active"><a href="#">${venueData.venueName}</a></li>
			</ul>
		</div>
	</div>
	<div class="search-resultPage">
		<div class="fiexd-nav">
			<div class="container">
				<div class="back-link">
					<a href="searchresult.html"><span class="icon icon-back-filled"></span>Back</a>
				</div>
				<ul>
					<li><a href="javascript:;"> <span class="icon icon-info"></span>
							<span class="text">Information</span>
					</a></li>
					<li><a href="javascript:;"> <span class="icon icon-hands"></span>
							<span class="text">Amenities</span>
					</a></li>
					<li><a href="javascript:;"> <span
							class="icon icon-thumb-image"></span> <span class="text">Photo
								Gallery</span>
					</a></li>
					<c:forEach var="info" items="${venueData.propertyData.propertyInfo}">
						<li><a href="javascript:;"> <span
							class="icon ${info.icon}"></span> 
							<span class="text">
								${info.title}
							</span>
					</a></li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="container">
	

			<div class="row">
				<div class="col-lg-9 col-sm-9 col-md-9">
					<div class="hotel-info tab-content">
						<h2>About the Venue</h2>
						<div class="inner-info">
							<p>${venueData.venueDescription}</p>
							<div class="address">
								<div class="address-view">
									<h3>Address :</h3>
									<div class="address-slide full">
										<div class="icon icon-location-2"></div>
										<label>${venueData.venueName} by ${venueData.propertyData.propertyName }</label>
										<p>
											${venueData.propertyData.addressList[0].address1},
											${venueData.propertyData.addressList[0].address2} <br />
											${venueData.propertyData.addressList[0].stateId} -
											${venueData.propertyData.addressList[0].pincode},
											${venueData.propertyData.addressList[0].countryId}
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="amenities-list tab-content">
						<h2>Amenities for You and Guests</h2>
						<div class="amenities-view">
							<c:forEach var="amenity" items="${venueData.propertyData.amenitiesList}">
								<c:set var="className" value="a" />
								<c:if test="${amenity.isEnabled() eq false}">
									<c:set var="className" value="disabled" />
								</c:if>
								<div class="amenities-box ${className}">
									<div class="icon ${amenity.amenitiesIcon}"></div>
									<span>${amenity.amenitiesText}</span>
								</div>
							</c:forEach>
						</div>
					</div>
					<div class="photo-gallery tab-content">
						<h2>Photo Gallery</h2>
						<div class="gallery-view">
							<div class="row">
								<c:forEach var="propertyImage"
									items="${venueData.propertyData.propertyImageList}">
									<c:if test="${propertyImage.isPrimary eq 0}">
										<div class="col-sm-4 col-xs-6">
											<div class="img">
												<a href="${propertyImage.path}" data-lightbox="example-set"
													data-title="${propertyImage.caption}"> <img
													src="${propertyImage.path}" alt="${propertyImage.altText}">
												</a>
											</div>
											<div class="name">${propertyImage.caption}</div>
										</div>
									</c:if>
								</c:forEach>
								<c:forEach var="venueImage"
									items="${venueData.venueImageList}">
									<c:if test="${venueImage.isPrimary eq 0}">
										<div class="col-sm-4 col-xs-6">
											<div class="img">
												<a href="${venueImage.path}" data-lightbox="example-set"
													data-title="${venueImage.caption}"> <img
													src="${venueImage.path}" alt="${venueImage.altText}">
												</a>
											</div>
											<div class="name">${venueImage.caption}</div>
										</div>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>

					<c:forEach var="info" items="${venueData.propertyData.propertyInfo}">
						<div class="terms-conditions tab-content">
							<h2>${info.title}</h2>
							<div class="conditions">${info.value}</div>
						</div>
					</c:forEach>

				</div>
				<div class="col-lg-3 col-sm-3 col-md-3">
					<%-- <div class="booking-formMain">
						<div class="book-title">Venue Details</div>
						<div class="book-form">
							${propertyData.venueList}
						</div>
					</div>  --%>
				</div>
			</div>
		</div>
	</div>
</section>
