<!DOCTYPE html>
<%@ taglib prefix="custom" uri="customTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!-- Mobile Specific Metas -->
<title>Banquetin</title>

<!-- favicon icon -->
<link rel="shortcut icon" href="images/Favicon.ico">
<!-- CSS Inclusion Start -->
<tiles:useAttribute id="cssList" name="globalCSSDev"
	classname="java.util.List" />
<c:forEach var="cssfile" items="${cssList}">
	<link rel="stylesheet" href="${cssfile}">
</c:forEach>
<tiles:useAttribute id="cssList" name="localCSS"
	classname="java.util.List" />
<c:forEach var="cssfile" items="${cssList}">
	<link rel="stylesheet" href="${cssfile}">
</c:forEach>
<!-- CSS Inclusion End -->
<link rel="shortcut icon" href="images/favicon/favicon.ico">
<link rel="icon" sizes="16x16 32x32 64x64"
	href="images/favicon/favicon.ico">


</head>
<body>
	<div class="wrapper">
		<!-- Preloader Start -->
		<div id="preloader">
			<div id="preloader-status"></div>
		</div>
		<!-- Preloader End -->

		<tiles:insert attribute="header" />
		

		<!-- JSP Inclusion Start -->
		<tiles:useAttribute id="jsplist" name="body"
			classname="java.util.List" />
		<div class="page-content">
			<c:forEach var="jspfile" items="${jsplist}">
				<jsp:include page="${jspfile}"></jsp:include>
			</c:forEach>
		</div>
		<!-- JSP Inclusion End -->
		<%-- 		<tiles:insert attribute="newsletter" />
 --%>
		<tiles:insert attribute="footer" />
	</div>
	<a class="goto-top" href="#gotop"></a>
	<div class="loader"></div>

	<tiles:useAttribute id="jsList" name="globalJSDev"
		classname="java.util.List" />
	<c:forEach var="jsfile" items="${jsList}">
		<script src="${jsfile}"></script>
	</c:forEach>
	<tiles:useAttribute id="jsList" name="localJS"
		classname="java.util.List" />
	<c:forEach var="jsfile" items="${jsList}">
		<script src="${jsfile}"></script>
	</c:forEach>
	<!-- JS Inclusion End -->
</body>
</html>