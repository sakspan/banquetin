<div class="content">
	<div class="container">
		<div class="bookin-info">
			<div class="bookin-infoRow">
				<div class="bookin-id">
					Booking ID : <span> ${bookingId}</span><input type="hidden" name="bookingId" id="bookingId" value="${bookingId}" />
					<%request.setAttribute("printBookingId", request.getAttribute("bookingId")); %>
				</div>
			</div>
			<div class="thanks-msg">
				<div class="icon icon-right-sign"></div>
				<h3>Thank you for booking</h3>
				<p>The booking is confirmed. Email will be shortly sent out to
					the registered email id. You can print the booking details using
					the option below.</p>
			</div>
			<a href="downloadPdf.html" class="btn" >Print</a>
			<a href="search.html" class="btn" >Book Another</a>
		</div>
	</div>
</div>
