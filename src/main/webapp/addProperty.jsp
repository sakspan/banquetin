<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%-- <c:set var="isReadOnly" value=' readonly="readonly" '/>
<c:set var="isDisabled" value=' disabled="disabled" '/> --%>

<c:if test="${empty propertyList}">
<c:set var="isReadOnly" value=""/>
<c:set var="isDisabled" value=""/>
</c:if>

<form action="addPropertySubmit.do" id="msform" method="post">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Basic Details</li>
		<li>Contact</li>
		<li>Venues</li>
		<li>Amenities, Info & Others</li>
	</ul>
	<!-- fieldsets -->
	<fieldset id="property-details">
		<h2 class="fs-title">Basic Details</h2>

		<div class="col-xs-6 form-group">
			<input type="text" class="placeholder-field" name="propertyName" id="propertyName" ${isReadOnly} value="${propertyList[0].propertyName}" />
			<label alt='Placeholder' placeholder='Property Name'></label>
			<div class="error-msg property-name-error">Please enter property name</div>
		</div>
		<div class="col-xs-6 form-group">
			<input type="text" class="placeholder-field" name="starRating" id="starRating" ${isReadOnly} value="${propertyList[0].starRating}" />
			<label alt='Placeholder' placeholder='Rating'></label>
			<div class="error-msg ratings-error">Please enter Star rating from 1-5</div>
		</div>
		<div class="col-xs-12 form-group">
			<input type="text" name="propertyDescription" class="placeholder-field"  id="propertyDescription" ${isReadOnly} value="${propertyList[0].propertyDescription}" />
			<label alt='Placeholder' placeholder='Description'></label>
			<div class="error-msg property-description-error">Please enter property description</div>
		</div>
		<div class="col-xs-6 form-group">
			<select class="form-control" name="propertyType" id="propertyType" ${isDisabled}>
				<option value="-1">Select Property Type</option>
				<c:forEach var="propertyType" items="${propertyTypeList}">
					<option value="${propertyType.id}" <c:if test="${propertyType.id == propertyList[0].propertyType.id}">selected ="selected"</c:if>>${propertyType.text}</option>
				</c:forEach>
			</select>
			<div class="error-msg property-type-error">Please select property type</div>
		</div>
		<div class="col-xs-6 form-group">
			<select class="form-control" name="onlineInd" id="onlineInd" ${isDisabled}>
				<option value="-1">Want to show online?</option>
				<option value="Y" <c:if test="${propertyList[0].onlineInd == 'Y'}">selected ="selected"</c:if> >Yes</option>
				<option value="N" <c:if test="${propertyList[0].onlineInd == 'N'}">selected ="selected"</c:if> >No</option>
			</select>
			<div class="error-msg online-ind-error">Please select online indicator</div>
			
		</div>
		<div class="col-xs-12 form-group">
			<input type="button" name="next" class="next action-button"
				value="Next" />
		</div>
	</fieldset>
	<fieldset id="contact-details">
		<h2 class="fs-title">Contact Form</h2>
		<div class="col-xs-4 form-group">
			<input type="text" class="placeholder-field" name="emailId" id="emailId" value="${propertyList[0].emailList[0].emailAddress}"/>
			<label alt='Placeholder' placeholder='Email'></label>
			<div class="error-msg email-error">Please enter valid email</div>
			<div class="error-msg email-error-duplicate">You are already registered. Please log in.</div>
		</div>
		<div class="col-xs-4 form-group">
			<input type="text" name="phoneNumber" class="placeholder-field" id="phoneNumber" value="${propertyList[0].phoneList[0].phoneNumber}"/>
			<label alt='Placeholder' placeholder='Phone Number'></label>
			<div class="error-msg phone-error">Please enter valid Phone Number</div>
			<div class="error-msg phone-error-duplicate">You are already registered. Please log in.</div>
		</div>
		<div class="col-xs-4 form-group">
			<input type="text" name="altPhoneNumber1" class="placeholder-field" id="altPhoneNumber1" value="${propertyList[0].phoneList[1].phoneNumber}"/>
			<label alt='Placeholder' placeholder='Alternate Number'></label>
			<div class="error-msg alt-phone-error">Please enter valid Phone Number</div>
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-6 form-group">
			<input type="text" name="address1" class="placeholder-field" id="address1" value="${propertyList[0].addressList[0].address1}"/>
			<label alt='Placeholder' placeholder='Address Line1'></label>
			<div class="error-msg address-error">Please enter valid address</div>
		</div>
		<div class="col-xs-6 form-group">
			<input type="text" name="address2" class="placeholder-field" value="${propertyList[0].addressList[0].address2}"/>
			<label alt='Placeholder' placeholder='Addres Line2'></label>
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-6 form-group">
			<input type="text" name="city" class="placeholder-field" id="city" value="${propertyList[0].addressList[0].city}"/>
			<label alt='Placeholder' placeholder='City'></label>
			<div class="error-msg city-error">Please enter valid city</div>
		</div>
		<div class="col-xs-6 form-group">
			<input type="text" name="zipCode" class="placeholder-field" id="zipCode" value="${propertyList[0].addressList[0].pincode}"/>
			<label alt='Placeholder' placeholder='Zip Code'></label>
			<div class="error-msg zipCode-error">Please enter valid Zip Code</div>
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-6 form-group">
			<select class="form-control" name="state" id="state">
				<option value="-1">Select State</option>
				<c:forEach var="state" items="${statesList}">
					<option value="${state.id}" <c:if test="${state.id == propertyList[0].addressList[0].stateId}">selected ="selected"</c:if>>${state.desc}</option>
				</c:forEach>
			</select>
			<div class="error-msg state-error">Please select valid State</div>
		</div>
		<div class="col-xs-12 form-group">
			<div class="col-xs-6 form-group">
				<input type="button" name="previous" class="previous action-button"
					value="Previous" />
			</div>
			<div class="col-xs-6 form-group">
				<input type="button" name="next" class="next action-button"
					value="Next" />
			</div>
		</div>

	</fieldset>
	<fieldset id="venue-details">
		<h2 class="fs-title">Add Venues</h2>
		<div class="tableContainer" style="background-color: white;">
			<table class="table table-responsive table-hover" id="venueTable">
				<thead>
					<tr>
						<th>Venue Name</th>
						<th>Venue Description</th>
						<th>Length</th>
						<th>Width</th>
						<th>Height</th>
						<th>Venue Type</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="row">
			<!-- Modal -->
			<div class="modal fade" id="venueModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Add Venue</h4>
						</div>
						<div class="modal-body">
							<section class="scrollable wrapper w-f venueDataForm">
								<div id="venueForm">
									<section class="panel panel-default">
										<div class="panel-body">
											<div class="venueInfo">
												<div class="col-xs-12 form-group">
													<input type="text" id="venueName" class="form-control placeholder-field"
														name="venueName">
														<label alt='Placeholder' placeholder='Name' style="padding-top: 5px;"></label>
													<div class="error-msg venue-name-error">Please enter Venue name</div>
												</div>
												<div class="col-xs-12 form-group">
													<input type="text" id="venueDescription"
														class="form-control placeholder-field" name="venueDescription">
														<label alt='Placeholder' placeholder='Description' style="padding-top: 5px;"></label>
													<div class="error-msg venue-desc-error">Please enter Venue description</div>
												</div>
												<div class="col-xs-4 form-group">
													<input type="text" id="length" class="form-control placeholder-field"
														name="length">
														<span class="feets">ft</span>
												</div>
												<div class="col-xs-4 form-group">
													<input type="text" id="width" class="form-control placeholder-field"
														name="width">
														<span class="feets">ft</span>
												</div>
												<div class="col-xs-4 form-group">
													<input type="text" id="height" class="form-control placeholder-field"
														name="height">
														<span class="feets">ft</span>
												</div>
												<div class="col-xs-12 form-group">
													<div class="error-msg dimensions-error">Please enter valid dimensions</div>
												</div>
												<!-- <div class="col-xs-12 form-group">
													<input type="hidden" id="minBookingAmount" value="0"
														class="form-control" name="minBookingAmount">
													<div class="error-msg minBookingAmount-error">Please enter valid amount</div>
												</div> -->
											</div>
											<div class="col-xs-6 form-group">
												<select class="form-control" id="venueTypeId"
													style="height: 35px; padding: 0 0 0 10px;">
													<option value="-1">Select Venue Type</option>
													<c:forEach var="venueType" items="${venueTypeList}">
														<option value="${venueType.id}" <c:if test="${venueType.id == propertyList[0].venueList[0].venueType.id}">selected ="selected"</c:if>>${venueType.text}</option>
													</c:forEach>
												</select>
												<div class="error-msg venue-type-error">Please select valid venue type</div>
											</div>
											<div class="col-xs-6 form-group">
												<select class="form-control" id="eventTypeId"
													style="height: 80px; padding: 0 0 0 10px;" multiple>
													<option value="-1">Select Event Type</option>
													<c:forEach var="eventType" items="${eventTypeList}">
														<option value="${eventType.eventTypeId}">${eventType.eventTypeText}</option>
													</c:forEach>
												</select>
												<div class="error-msg event-type-error">Please select valid event type</div>
											</div>

											<div class="col-xs-12 form-group">
												<label class="col-sm-3" style="margin-bottom: 15px;">Seating
													Type:</label>
												<c:forEach var="seatingType" items="${seatingTypeList}" varStatus="index">
													<div class="col-sm-12 seatingTypeList">
														<div class="col-xs-4 form-group" style="margin-top: 5px;">
															<span data-seating-id="${seatingType.id}"
																style="margin-top: 10px;">${seatingType.text}</span>
														</div>
														<div class="col-xs-4 form-group">
															<input id="${seatingType.id}_minCapacity"
																data-min-name="minCapacity" style="height: 35px;padding-top: 5px;" class="placeholder-field"/>
																<label alt='Placeholder' placeholder='Min Capacity'></label>
														</div>
														<div class="col-xs-4 form-group">
															<input id="${seatingType.id}_maxCapacity"
																data-max-name="maxCapacity" style="height: 35px;padding-top: 5px" class="placeholder-field" value="${propertyList[0].venueList[0].seatingTypeList[index].maxCapacity}"/>
																<label alt='Placeholder' placeholder='Max Capacity'></label>
														</div>
													</div>
												</c:forEach>
												<div class="col-sm-12">
													<div class="error-msg seating-type-error">Please enter valid min and max capacity</div>
												</div>
											</div>
										</div>
								</div>
							</section>
							<button class="btn btn-sm btn-success venueSubmit" data-dismiss="modal">Submit</button>
							<button class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="buttonSection">
			<input type="button" name="add" class="action-button addVenue"
				data-toggle="modal" data-target="#venueModal" value="Add Venue" /><br />
			<input type="button" name="previous" class="previous action-button"
				value="Previous" /> <input type="button" name="next"
				class="next action-button" value="Next" />
		</div>
	</fieldset>
	
	<fieldset>
		<h2 class="fs-title">Amenities, Info & Others</h2>
		<div class="col-xs-12 form-group">
			<label class="col-sm-2">Amenities:</label>
				<c:forEach var="amenitiesType" items="${amenitiesList}" varStatus="list">
					<div class="selectBox" data-amenities-id="${amenitiesType.amenitiesId}">
						<span class="amenitiesIcon ${amenitiesType.amenitiesIcon}"></span><br>
						<span class="icon-text" data-amenities-id="${amenitiesType.amenitiesId}">${amenitiesType.amenitiesText}</span>
					</div>
				</c:forEach>
		</div>
		<div class="infoSection panel panel-default">
			<div class="panel-body">
				<!-- Table -->
				<div class="tableContainer" style="background-color: white;">
					<table class="table table-responsive table-hover" id="infoTable">
						<thead>
							<tr>
								<th>Name</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<input type="button" name="add" class="action-button"
					data-toggle="modal" data-target="#infoModal" value="Add Info" /><br />
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="infoModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Info</h4>
					</div>
					<div class="modal-body">
						<section class="scrollable wrapper w-f infoDataForm">
							<div id="venueForm">
								<section class="panel panel-default">
									<div class="panel-body">
										<div class="col-xs-12 form-group">
											<input type="text" id="title" class="form-control placeholder-field"
												name="title" value="${propertyList[0].propertyInfo[0].title}">
												<label alt='Placeholder' placeholder='Name'></label>
												<div class="error-msg amenity-title-error">Please enter tile for Amenity Info</div>
										</div>
										<div class="col-xs-12 form-group">
											<textarea id="value" class="form-control" name="value" value="${propertyList[0].propertyInfo[0].value}"></textarea>
											<div class="error-msg amenity-value-error">Please enter Description</div>
										</div>
									</div>
								</section>
								<button class="btn btn-sm btn-success infoSubmit"
									data-dismiss="modal">Submit</button>
								<button class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
							</div>
					</div>

				</div>
			</div>
		</div>
		<input type="hidden" id="others" class="form-control" name="others"
			placeholder="Others"> <input type="button" name="previous"
			class="previous action-button" value="Previous" /> <input
			type="submit" name="submit" class="submit action-button"
			value="Submit" />
	</fieldset>
	<input type="hidden" name="venueListArray" id="venueList" /> 
	<input	type="hidden" name="infoListArray" id="infoList" /> 
	<input type="hidden" name="amenitiesListArray" id="amenitiesList" />

</form>