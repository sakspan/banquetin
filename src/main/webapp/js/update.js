$(".propertyUpdate").click(function(e){
	e.preventDefault();
	var propertyId = $("#propertyId").val();
	var propertyName = $("#propertyName").val();
	var starRating = $("#starRating").val();
	var propertyDescription = $("#propertyDescription").val();
	var propertyTypeId = $("#propertyTypeId").val();
	var onlineInd = $("#onlineInd").val();
	var emailId = $("#emailId").val();
	var phoneNumber = $("#phoneNumber").val();
	var altPhoneNumber1 = $("#altPhoneNumber1").val();
	var address1 = $("#address1").val();
	var address2 = $("#address2").val();
	var city = $("#city").val();
	var zipCode = $("#zipCode").val();
	var state = $("#state").val();
	
	var operation = "update";

	var postData = {
			propertyId: propertyId,
			propertyName: propertyName,
			starRating: starRating,
			propertyDescription : propertyDescription,
			propertyTypeId : propertyTypeId,
			onlineInd : onlineInd,
			emailId: emailId,
			phoneNumber : phoneNumber,
			altPhoneNumber1: altPhoneNumber1,
			address1: address1,
			address2: address2,
			city: city,
			zipCode: zipCode,
			state: state,
			operationType : operation
	};
	submitForm("updateProperty.do", postData);
	
});

$(".editVenue").click(function(e){
	e.preventDefault();
	$('.error-msg').hide();
	error = false;	
	$(".showVenueSave").css({'display': 'inline'});
	$(".showVenueSubmit").css({'display': 'none'});
	var venueId = $(this).data('venueid');
    var venueName = $(this).data('venuename');   
    var venueDesc = $(this).data('venuedesc');
    var length = $(this).data('length');
    var width = $(this).data('width');
    var height = $(this).data('height');
    var venueType = $(this).data('venuetypeid');
    var eventType = $(this).data('eventtypeid');
    var seatingTypeOneMin = $(this).data('seatingtypeonemin');
    var seatingTypeOneMax = $(this).data('seatingtypeonemax');
    var seatingTypeTwoMin = $(this).data('seatingtypetwomin');
    var seatingTypeTwoMax = $(this).data('seatingtypetwomax');
    var seatingTypeThreeMin = $(this).data('seatingtypethreemin');
    var seatingTypeThreeMax = $(this).data('seatingtypethreemax');
    var seatingTypeFourMin = $(this).data('seatingtypefourmin');
    var seatingTypeFourMax = $(this).data('seatingtypefourmax');
    var seatingTypeFiveMin = $(this).data('seatingtypefivemin');
    var seatingTypeFiveMax = $(this).data('seatingtypefivemax');
    $('#venueId').val(venueId); 
	$("#venueName").val(venueName);
	$("#venueDescription").val(venueDesc);
	$("#length").val(length);
	$("#width").val(width);
	$("#height").val(height);
	$("#venueType").val(venueType);
	$("#eventTypeId").val(eventType);
	$("#1_minCapacity").val(seatingTypeOneMin);
	$("#1_maxCapacity").val(seatingTypeOneMax);
	$("#2_minCapacity").val(seatingTypeTwoMin);
	$("#2_maxCapacity").val(seatingTypeTwoMax);
	$("#3_minCapacity").val(seatingTypeThreeMin);
	$("#3_maxCapacity").val(seatingTypeThreeMax);
	$("#4_minCapacity").val(seatingTypeFourMin);
	$("#4_maxCapacity").val(seatingTypeFourMax);
	$("#5_minCapacity").val(seatingTypeFiveMin);
	$("#5_maxCapacity").val(seatingTypeFiveMax);
});

$(".editVenueSubmit").click(function(e){
	e.preventDefault();
	console.log(this);
});

$(".addInfo").click(function(e){
	$(".infoSave").css({'display': 'none'});
	$(".updateInfoSubmit").css({'display': 'inline'});
	
	$("#title").val("");
	$("#tinymce").val("");  
	
});


$(".venueSave").click(function(e){
	e.preventDefault();
	if(!validateVenueDetailsForm()) {
		return false;
	}
		var name;
		var venueObj= {};
		var seatingTypeList=[];
		var tableBody = $("#venueTable tbody");
		var venueType = $("#venueType").val();
		count++;
		e.preventDefault();
		$(".venueInfo input[id]").each(function(i){
			 name = $(this).attr("id");
			 value = $(this).val();
			 venueObj[name] = value;
		});
		$(".venueDataForm select[id]").each(function(i){
			 name = $(this).attr("id");
			 value = $(this).val();
			 text = $(this).find('option:selected').text();
			 venueObj[name] = value;	 	
		});
		$(".seatingTypeList").each(function(i){
			 var seatingObj= {};
			 var seatingId = $(this).find('span').attr("data-seating-id");
			 seatingObj['seatingTypeId']=seatingId;
			 name = $(this).find("input[id*='minCapacity']").attr("data-min-name");
			 value = $(this).find("input[id*='minCapacity']").val();
			 seatingObj[name] = value;
			 maxname = $(this).find("input[id*='maxCapacity']").attr("data-max-name");
			 value = $(this).find("input[id*='maxCapacity']").val();
			 seatingObj[maxname] = value;
			 seatingTypeList.push(seatingObj);
		});
		venueObj.seatingTypeList = seatingTypeList;
		venueList.push(venueObj);
		venueList = JSON.stringify(venueList);
		var operation;
		if($('#venueId').val().length === 0){
			operation = "add";
		} else {
			operation = "update";
		}
		

	var postData = {
			venueData: venueList,
			operationType : operation
	};
	submitForm("venue.do", postData);
});

$(".amenitiesSave").click(function(e){ 
	fetchAmenities();
	var amenitiesList = JSON.stringify(amenitiesArr);

	var postData = {
			amenities: amenitiesList
	};
	submitForm("updatePropertyAmenities.do", postData);
});

$(".editInfo").click(function(e){
	e.preventDefault();
	$(".infoSave").css({'display': 'inline'});
	$(".updateInfoSubmit").css({'display': 'none'});
	var propertyTitle = $(this).data('propertytitle');
    var propertyValue = $(this).data('propertyvalue'); 
    var propertyInfoId = $(this).data('propertyinfoid'); 
    
    $('#propertyInfoId').val(propertyInfoId);
	$("#title").val(propertyTitle);
	var s= $("#tinymce");
	$("#tinymce").val(propertyValue);  
    
});

$(".deleteInfo").click(function(e){ 
	var propertyInfoId = $(this).data('propertyinfoid');
	var postData = {
			propertyInfoId: propertyInfoId,
			operationType : "delete"
	};
	submitForm("propertyInfo.do", postData);
});


$(".infoSave").click(function(e){ 
	if(!validateAmenityDetailsForm()) {
		return false;
	}
	var name;
	var infoList = [];
	var infoObj= {};
	count++;
	e.preventDefault();
	$.each($('input, textarea', '.infoDataForm'),function(i){
		 name = $(this).attr("id");
		 value = $(this).val();
		 infoObj[name] = value;
		 if(name == 'value') {
			 value = value.replace(/<(?:.|\n)*?>/gm, '');
		 }
	});
	infoList.push(infoObj);
	var infoList = JSON.stringify(infoList);
	var postData = {
			propertyInfoData: infoList,
			operationType : "update"
	};
	submitForm("propertyInfo.do", postData);
});

$(".updateInfoSubmit").click(function(e){ 
	fetchPropertyInfo();
	var infoListData = JSON.stringify(infoList);

	var postData = {
			propertyInfoData: infoListData,
			operationType : "add"
	};
	submitForm("propertyInfo.do", postData);
	
});

$(".deleteVenue").click(function(e){ 
	var venueId = $(this).data('venueid');
	var postData = {
			venueId: venueId,
			operationType : "delete"
	};
	submitForm("venue.do", postData);
});
//Common For all
function submitForm(url, postData) {
	if(error) {
		return false;
	} else {
		$.ajax({
			type : "POST",
			url : url,
			data : postData,
			success : function(response) {
				setTimeout(function() {
					location.reload();
				}, 1000);
			},
			error : function(e) {
				alert("OOPS!! Some Error");
			}
		});
	}
}


