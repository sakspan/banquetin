function onLoad() {
	showStep1();
}

function submitBookingForm() {
	document.getElementById("bookingForm").submit();
}

function showStep0() {
	hideStep();
	removeStepsHighlight();
	document.getElementById("bookingSteps").style.display = "none";
	document.getElementById("step0").style.display = "block";
}

function showStep1(venueId, slotType, bookingamount, amount, name, desc) {
	clearErrorMessage();
	hideStep();
	removeStepsHighlight();
	document.getElementById("venueId").value = venueId;
	document.getElementById("slotType").value = slotType;
	document.getElementById("estAmount").value = amount;
	document.getElementById("bookingAmount").value = bookingamount;

	document.getElementById("bookingSteps").style.display = "block";
	document.getElementById("step1").style.display = "block";
	document.getElementById('firstStep').classList.add('active');
	document.getElementById("venueName").value = name;
	document.getElementById("venueDescription").value = desc;
}

function showStep2() {
	if(!validateBookingDetailsForm()){
		return;
	}
	hideStep();
	removeStepsHighlight();
	document.getElementById("step2").style.display = "block";
	document.getElementById('secondStep').classList.add('active');
}

function showStep3() {
	if(!validatePaymentDetailsForm()){
		return;
	}
	hideStep();
	removeStepsHighlight();

	var stateSelect = document.getElementById("state");
	var state = stateSelect.options[stateSelect.selectedIndex].text;
	
	var paymentSelect = document.getElementById("paymentType");
	var payment = paymentSelect.options[paymentSelect.selectedIndex].text;
	
	document.getElementById("guestNameDisplay").innerHTML = document
			.getElementById("guestName").value;
	document.getElementById("emailIdDisplay").innerHTML = document
			.getElementById("emailId").value;
	document.getElementById("phoneNumDisplay").innerHTML = document
			.getElementById("phoneNum").value;

	document.getElementById("address1Display").innerHTML = document
			.getElementById("address1").value
			+ ", "
			+ document.getElementById("address2").value
			+ ", "
			+ document.getElementById("city").value
			+ ", "
			+ state
			+ ", "
			+ document.getElementById("pincode").value;
	

	document.getElementById("paymentTypeDisplay").innerHTML = payment;
	document.getElementById("estAmountDisplay").innerHTML = "Rs."
			+ document.getElementById("estAmount").value;
	document.getElementById("bookingAmountDisplay").innerHTML = "Rs."
			+ document.getElementById("bookingAmount").value;
	document.getElementById("advancePaidDisplay").innerHTML = "Rs."
			+ document.getElementById("advancePaid").value;
	document.getElementById("commentsDisplay").innerHTML = document
			.getElementById("comments").value;

	document.getElementById("venueDisplay").innerHTML = document.getElementById("venueName").value + ", " + document.getElementById("venueDescription").value;
	document.getElementById("dateDisplay").innerHTML = document
			.getElementById("selectedDate").value;
	document.getElementById("paxDisplay").innerHTML = document
			.getElementById("pax").value;

	document.getElementById("step3").style.display = "block";
	document.getElementById('thirdStep').classList.add('active');
}

function hideStep() {

	document.getElementById("step0").style.display = "none";
	document.getElementById("step1").style.display = "none";
	document.getElementById("step2").style.display = "none";
	document.getElementById("step3").style.display = "none";
}

function removeStepsHighlight() {
	document.getElementById('firstStep').classList.remove('active');
	document.getElementById('secondStep').classList.remove('active');
	document.getElementById('thirdStep').classList.remove('active');
}

$(document).ready(function() {
	$('.datepicker').datepicker({
		format : 'dd-mm-yyyy'
	});
});

function clearErrorMessage(){
	$(".error-msg.booking-guestName-error").hide();
	$(".error-msg.booking-email-error").hide();
	$(".error-msg.booking-phone-error").hide();
	$(".error-msg.booking-address-error").hide();
	$(".error-msg.booking-city-error").hide();
	$(".error-msg.booking-pinCode-error").hide();
	$(".error-msg.booking-state-error").hide();
}
