
$(document).on("click", ".editButton", function () {
	clearErrorMessage();
    var ratesId = $(this).data('id');
    var venueId = $(this).data('venueid');
    
    var startDate = $(this).data('startdate');
    var endDate = $(this).data('enddate');
    var slotTypeId = $(this).data('slottypeid');
    var rate = $(this).data('rate');
    var rateTypeId = $(this).data('ratetypeid');
    var bookingAmount = $(this).data('bookingamount');
        
	document.getElementById("ratesId").value = ratesId;
	document.getElementById("updateVenueId").value = venueId;
	document.getElementById("startDate").value = startDate;
	document.getElementById("endDate").value = endDate;
	document.getElementById("slotType").value = slotTypeId;
	document.getElementById("rate").value = rate;
	document.getElementById("bookingAmount").value = bookingAmount;
	document.getElementById("rateType").value = rateTypeId;

});

function showAddRatesPanel() {
	document.getElementById("pnlAdd").style.display = "block";
}

function hideAddRatesPanel() {
	document.getElementById("pnlAdd").style.display = "none";
}

function fetchRates(){
	
	var venueId = document.getElementById("venueId").value;
	
	var postData = {
			venueId : venueId
		};

		submitForm("addRates.do", postData);
}

function deleteRates(rateId){
	
	
	var postData = {
			ratesId : rateId,
			operationType : "delete"
		};

		submitForm("addRatesSubmit.do", postData);
}

// Common For all
function submitFormAndRefresh(url, postData) {
	$.ajax({
		type : "POST",
		url : url,
		data : postData,
		success : function(response) {
			setTimeout(function() {
				location.reload();
			}, 1000);
		},
		error : function(e) {
			alert("OOPS!! Some Error");
		}
	});
}


// Common For all
function submitForm(url, postData) {
	$.ajax({
		type : "POST",
		url : url,
		data : postData,
		success : function(response) {
			setTimeout(function() {
				location.reload();
			}, 1000);
		},
		error : function(e) {
			alert("OOPS!! Some Error");
		}
	});
}

// Designation Crux Functions
function editRatesDisplay(ratesId,venueId, startDate,endDate,slotType,rate,rateType,bookingAmount) {

	var operation = "update";

	
	document.getElementById("ratesId").value = ratesId;
	document.getElementById("updateVenueId").value = venueId;
	document.getElementById("startDate").value = startDate;
	document.getElementById("endDate").value = endDate;
	document.getElementById("slotType").value = slotType;
	document.getElementById("rate").value = rate;
	document.getElementById("bookingAmount").value = bookingAmount;
	document.getElementById("rateType").value = rateType;

	showAddRatesPanel();
	// submitForm("designationSubmit.do",postData);
}

// Designation Crux Functions
function addRatesDisplay(){

	document.getElementById("ratesId").value = "";
	document.getElementById("updateVenueId").value = "";
	document.getElementById("startDate").value = "";
	document.getElementById("endDate").value = "";
	document.getElementById("slotType").value = "";
	document.getElementById("rate").value = "";
	document.getElementById("bookingAmount").value = "";
	document.getElementById("rateType").value = "";

	showAddRatesPanel();
	// submitForm("designationSubmit.do",postData);
}


function addUpdateRates() {
	if(!validateRatesDetailsForm()){
		return false;
	}
	var ratesId = document.getElementById("ratesId").value;
	var venueId = document.getElementById("updateVenueId").value;
	var startDate = document.getElementById("startDate").value;
	var endDate = document.getElementById("endDate").value;
	var slotType = document.getElementById("slotType").value;
	var rate = document.getElementById("rate").value;
	var bookingAmount = document.getElementById("bookingAmount").value;
	var rateType = document.getElementById("rateType").value;
	
	var operation = "add";

	if (ratesId == 0) {
		var postData = {
			ratesId: ratesId,
			venueId: venueId,
			startDate : startDate,
			endDate : endDate,
			slotTypeId : slotType,
			rate : rate,
			bookingAmount : bookingAmount,
			operationType : operation,
			rateType : rateType
		};

		submitForm("addRatesSubmit.do", postData);
	} else {
		var postData = {
				ratesId: ratesId,
				venueId: venueId,
				startDate : startDate,
				endDate : endDate,
				slotTypeId : slotType,
				rate : rate,
				bookingAmount : bookingAmount,
				operationType : "update",
				rateType : rateType
		};

		submitForm("addRatesSubmit.do", postData);
	}
}

function validateRatesDetailsForm() {
	$('.error-msg').hide();
	var error = false;
	var updateVenueId = $("#updateVenueId").val();
	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	var slotType = $("#slotType").val();
	var rate = $("#rate").val();
	var bookingAmount = $("#bookingAmount").val();
	if(updateVenueId === '-1') {
		$(".rates-updateVenueId-error").show();
		$("#updateVenueId").focus();
		error = true;
	}
	if(startDate == '') {
		$(".rates-startDate-error").show();
		if(!error) { $("#startDate").focus(); }
		error = true;
	}
	if(endDate == '') {
		$(".rates-endDate-error").show();
		if(!error) { $("#endDate").focus(); }
		error = true;
	}
	if(slotType === '-1') {
		$(".rates-slotType-error").show();
		if(!error) { $("#slotType").focus(); }
		error = true;
	}
	if(rate == '') {
		$(".rates-rate-error").show();
		if(!error) { $("#rate").focus(); }
		error = true;
	}
	if(bookingAmount == '' || bookingAmount < 0) {
		$(".rates-bookingAmount-error").show();
		if(!error) { $("#bookingAmount").focus(); }
		error = true;
	}
	if(error) {
		return false;
	} else {
		return true;
	}
}

function clearErrorMessage(){
	$(".error-msg.rates-updateVenueId-error").hide();
	$(".error-msg.rates-startDate-error").hide();
	$(".error-msg.rates-endDate-error").hide();
	$(".error-msg.rates-slotType-error").hide();
	$(".error-msg.rates-rate-error").hide();
	$(".error-msg.rates-bookingAmount-error").hide();
}

$(document).ready(function() {
	$('.datepicker').datepicker({
		format : 'mm/dd/yyyy'
	});
});