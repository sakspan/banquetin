$(function($) {
	$('.error-msg').hide();
	var error = false;
});

function isInt(value) {
	return !isNaN(value) && 
         parseInt(Number(value)) == value && 
         !isNaN(parseInt(value, 10));
}

function validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(email) == false) {
        return false;
    }
    return true;
}

function validatePhone(phone) {
    var reg = /^[0]?[6789]\d{9}$/;

    if (reg.test(phone) == false) {
        return false;
    }
    return true;
}

function validateZipCode(zipcode) {
    var reg = /^\d{6}$/;

    if (reg.test(zipcode) == false) {
        return false;
    }
    return true;
}

function validateCity(city){
	var reg = /^[a-zA-Z]+$/;
	
	if (reg.test(city) == false) {
        return false;
    }
    return true;
}


function validateBasicDetailsForm() {
	$('.error-msg').hide();
	error = false;
	var propertyName = $("#propertyName").val();
	var starRating = $("#starRating").val();
	var propertyDescription = $("#propertyDescription").val();
	var propertyType = $("#propertyType").val();
	var onlineInd = $("#onlineInd").val();
	if(propertyName === '') {
		$(".property-name-error").show();
		$("#propertyName").focus();
		error = true;
	}
	if(starRating === '') {
		$(".ratings-error").show();
		if(!error) { $("#starRating").focus(); }
		error = true;
	}
	if(!isInt(starRating)) {
		$(".ratings-error").show();
		if(!error) { $("#starRating").focus(); }
		error = true;
	}
	if(starRating > 5 || starRating < 0) {
		$(".ratings-error").show();
		if(!error) { $("#starRating").focus(); }
		error = true;
	}
	if(propertyDescription === '') {
		$(".property-description-error").show();
		if(!error) { $("#propertyDescription").focus(); }
		error = true;
	}
	if(propertyType === '-1') {
		$(".property-type-error").show();
		if(!error) { $("#propertyType").focus(); }
		error = true;
	}
	if(onlineInd === '-1') {
		$(".online-ind-error").show();
		if(!error) { $("#onlineInd").focus(); }
		error = true;
	}
	if(error) {
		return false;
	} else {
		return true;
	}
	
}

function validateContactDetailsForm() {
	var emailExistsError = $(".email-error-duplicate").css('display');
	var phoneExistsError = $(".phone-error-duplicate").css('display');
	$('.error-msg').hide();
	error = false;
	var emailId = $("#emailId").val();
	var phoneNumber = $("#phoneNumber").val();
	var address1 = $("#address1").val();
	var city = $("#city").val();
	var state = $("#state").val();
	var zipCode = $("#zipCode").val();
	var altPhoneNumber1 = $("#altPhoneNumber1").val();
	
	if(emailId == '') {
		$(".email-error").show();
		$("#emailId").focus();
		error = true;
	}
	if(!validateEmail(emailId)) {
		$(".email-error").show();
		if(!error) { $("#emailId").focus(); }
		error = true;
	}
	if(emailExistsError == 'block'){
		$(".email-error").hide();
		$(".email-error-duplicate").show();
		if(!error) { $("#emailId").focus(); }
		error = true;
	}
	if(phoneNumber == '') {
		$(".phone-error").show();
		if(!error) { $("#phoneNumber").focus(); }
		error = true;
	}
	if(!validatePhone(phoneNumber)) {
		$(".phone-error").show();
		if(!error) { $("#phoneNumber").focus(); }
		error = true;
	}
	if(!validatePhone(altPhoneNumber1)) {
		$(".alt-phone-error").show();
		if(!error) { $("#altPhoneNumber1").focus(); }
		error = true;
	}
	if(phoneExistsError == 'block'){
		$(".phone-error").hide();
		$(".phone-error-duplicate").show();
		if(!error) { $("#phoneNumber").focus(); }
		error = true;
	}
	if(address1 === '') {
		$(".address-error").show();
		if(!error) { $("#address1").focus(); }
		error = true;
	}
	if(city === '') {
		$(".city-error").show();
		if(!error) { $("#city").focus(); }
		error = true;
	}
	if(!validateCity(city)) {
		$(".city-error").show();
		if(!error) { $("#city").focus(); }
		error = true;
	}
	if(zipCode == '') {
		$(".zipCode-error").show();
		if(!error) { $("#zipCode").focus(); }
		error = true;
	}
	if(!validateZipCode(zipCode)) {
		$(".zipCode-error").show();
		if(!error) { $("#zipCode").focus(); }
		error = true;
	}
	if(state === '-1') {
		$(".state-error").show();
		if(!error) { $("#state").focus(); }
		error = true;
	}
	if(error) {
		return false;
	} else {
		return true;
	}
}

function validateVenueDetailsForm() {
	$('.error-msg').hide();
	error = false;
	var venueName = $("#venueName").val();
	var venueDescription = $("#venueDescription").val();
	var length = $("#length").val();
	var width = $("#width").val();
	var height = $("#height").val();
	var venueType = $("#venueTypeId").val();
	var eventType = $("#eventTypeId").val();
	var minBookingAmount = $("#minBookingAmount").val();
	if(venueName === '') {
		$(".venue-name-error").show();
		if(!error) { $("#venueName").focus(); }
		error = true;
	}
	if(venueDescription === '') {
		$(".venue-desc-error").show();
		if(!error) { $("#venueDescription").focus(); }
		error = true;
	}
	if(length === '' || width === '' || height === '' || !isInt(length) || !isInt(width) || !isInt(height)) {
		$(".dimensions-error").show();
		if(!error) { $("#length").focus(); }
		error = true;
	}
	if(minBookingAmount === '') {
		$(".minBookingAmount-error").show();
		if(!error) { $("#minBookingAmount").focus(); }
		error = true;
	}
	if(venueType === '-1') {
		$(".venue-type-error").show();
		if(!error) { $("#venueType").focus(); }
		error = true;
	}
	if(eventType === '-1') {
		$(".event-type-error").show();
		if(!error) { $("#eventType").focus(); }
		error = true;
	}
	$(".seatingTypeList").each(function(i){
		 var minvalue = $(this).find("input[id*='minCapacity']").val();
		 var maxvalue = $(this).find("input[id*='maxCapacity']").val();
		 if(minvalue == "" || maxvalue == "" || parseInt(minvalue) > parseInt(maxvalue) || minvalue<=0 || maxvalue<=0) {
			$(".seating-type-error").show();
			error = true;
		 }
	});
	if(error) {
		return false;
	} else {
		return true;
	}
}

function validateAmenityDetailsForm() {
	$('.error-msg').hide();
	error = false;
	var title = $("#title").val();
	var value = $("#value").val();
	
	if(title == '') {
		$(".amenity-title-error").show();
		$("#title").focus();
		error = true;
	}
	if(value == '') {
		$(".amenity-value-error").show();
		if(!error) { $("#value").focus(); }
		error = true;
	}
	if(error) {
		return false;
	} else {
		return true;
	}
}

function validateBookingDetailsForm() {
	$('.error-msg').hide();
	error = false;
	var guestName = $("#guestName").val();
	var emailId = $("#emailId").val();
	var phoneNum = $("#phoneNum").val();
	var address1 = $("#address1").val();
	var city = $('#city').val();
	var state = $("#state").val();
	var pincode = $("#pincode").val();
	
	if(guestName == '') {
		$(".booking-guestName-error").show();
		$("#guestName").focus();
		error = true;
	}
	if(emailId == '') {
		$(".booking-email-error").show();
		if(!error) { $("#emailId").focus(); }
		error = true;
	}
	if(!validateEmail(emailId)) {
		$(".booking-email-error").show();
		if(!error) { $("#emailId").focus(); }
		error = true;
	}
	if(phoneNum == '') {
		$(".booking-phone-error").show();
		if(!error) { $("#phoneNum").focus(); }
		error = true;
	}
	if(!validatePhone(phoneNum)) {
		$(".booking-phone-error").show();
		if(!error) { $("#phoneNum").focus(); }
		error = true;
	}
	if(address1 === '') {
		$(".booking-address-error").show();
		if(!error) { $("#address1").focus(); }
		error = true;
	}
	if(city === '') {
		$(".booking-city-error").show();
		if(!error) { $("#city").focus(); }
		error = true;
	}
	if(pincode == '') {
		$(".booking-pinCode-error").show();
		if(!error) { $("#pinCode").focus(); }
		error = true;
	}
	if(!validateZipCode(pincode)) {
		$(".booking-pinCode-error").show();
		if(!error) { $("#pinCode").focus(); }
		error = true;
	}
	if(state === '-1') {
		$(".booking-state-error").show();
		if(!error) { $("#state").focus(); }
		error = true;
	}
	if(error) {
		return false;
	} else {
		return true;
	}
}

function validatePaymentDetailsForm() {
	$('.error-msg').hide();
	error = false;
	var estAmount = $("#estAmount").val();
	var bookingAmount = $("#bookingAmount").val();
	var advancePaid = $("#advancePaid").val();
	var paymentType = $("#paymentType").val();
	if(estAmount == '') {
		$(".payment-estAmount-error").show();
		$("#estAmount").focus();
		error = true;
	}
	if(bookingAmount == '') {
		$(".payment-bookingAmount-error").show();
		if(!error) { $("#bookingAmount").focus(); }
		error = true;
	}
	if(advancePaid == '') {
		$(".payment-advancedPaid-error").show();
		if(!error) { $("#advancePaid").focus(); }
		error = true;
	}
	if(paymentType === '-1') {
		$(".payment-paymentType-error").show();
		if(!error) { $("#paymentType").focus(); }
		error = true;
	}
	if(error) {
		return false;
	} else {
		return true;
	}
}
