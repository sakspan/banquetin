function searchResults() {
	if (!validateBookingSearch()) {
		return;
	}
	var selDate = document.getElementById("selectedDate").value;
	var paxCount = document.getElementById("pax").value;
	location.href = "bookings.html?selectedDate=" + selDate + "&paxCount="
			+ paxCount;
}

function validateBookingSearch() {
	$('.error-msg').hide();
	var error = false;
	var selectedDate = $("#selectedDate").val();
	var pax = $("#pax").val();
	if (selectedDate == '') {
		$(".selectedDate-error").show();
		if (!error) {
			$("#selectedDate").focus();
		}
		error = true;
	}
	if (pax === '') {
		$(".pax-error").show();
		if (!error) {
			$("#pax").focus();
		}
		error = true;
	}
	if (error) {
		return false;
	} else {
		return true;
	}
}

function clearErrorMessage() {
	$(".error-msg.selectedDate-error").hide();
	$(".error-msg.pax-error").hide();
}

$(document).ready(function() {
	$('.datepicker').datepicker({
		format : 'dd-mm-yyyy'
	});

	if(bookedDatesList){
		var startDate = new Date();
		startDate.setDate(startDate.getDate()-1);
		var datesArray = bookedDatesList.toString();
		var JSONObj = bookedDatesMap;
		var jsonObj = JSON.parse(bookedDatesMap);
		$('#bookingCalendarView').datepicker({
			startDate:startDate,
			inline: true,
			numberOfMonths: 3,
			beforeShowDay: function (date) {
				var theday = (("0" + (date.getMonth() + 1)).slice(-2)) +'/'+ ("0" + date.getDate()).slice(-2) + '/' + date.getFullYear();
				var n = datesArray.indexOf(theday);
				if (n >=0) {
					var day = "_" + theday.replace(/\//g, "");	
					var highlightText = jsonObj[day];
					if(highlightText.indexOf("Booked")>=0 && highlightText.indexOf("Morning")>=0 && highlightText.indexOf("Evening")>=0){
						return [true, "fullBookedEvent", highlightText];
					}else if (highlightText.indexOf("Booked")>=0 && highlightText.indexOf("Morning")>=0){
						return [true, "morningBookedEvent", highlightText];
					}else if (highlightText.indexOf("Booked")>=0 && highlightText.indexOf("Evening")>=0){
						return [true, "eveningBookedEvent", highlightText];
					}
					
					else if(highlightText.indexOf("Blocked")>=0 && highlightText.indexOf("Morning")>=0 && highlightText.indexOf("Evening")>=0){
						return [true, "blockedFullBookedEvent", highlightText];
					}
					else if (highlightText.indexOf("Blocked")>=0 && highlightText.indexOf("Morning")>=0){
						return [true, "blockedMorningBookedEvent", highlightText];
					}else if (highlightText.indexOf("Blocked")>=0 && highlightText.indexOf("Evening")>=0){
						return [true, "blockedEveningBookedEvent", highlightText];
					}
				}
				else {
					return [true, 'whiteBackground', ''];
				}
			}
		});
		
		/*$("#bookingCalendarView").datepicker('show');
		$("#bookingCalendarView").datepicker("setDate", new Date());*/
	}
	
	/*if(blockedDatesList){
		var datesArray = blockedDatesList.toString();
		var JSONObj = blockedBookingDateMap;
		var jsonObj = JSON.parse(blockedBookingDateMap);
		$('#bookingCalendarView').datepicker({
			inline: true,
			numberOfMonths: 3,
			beforeShowDay: function (date) {
				var theday = (("0" + (date.getMonth() + 1)).slice(-2)) +'/'+ ("0" + date.getDate()).slice(-2) + '/' + date.getFullYear();
				var n = datesArray.indexOf(theday);
				if (n >=0) {
					var day = "_" + theday.replace(/\//g, "");	
					var highlightText = jsonObj[day];
					if(highlightText.indexOf("Morning")>=0 && highlightText.indexOf("Evening")>=0){
						return [true, "blockedFullBookedEvent", highlightText];
					}else if (highlightText.indexOf("Morning")>=0){
						return [true, "blockedMorningBookedEvent", highlightText];
					}else{
						return [true, "blockedEveningBookedEvent", highlightText];
					}
				}
				else {
					return [true, 'whiteBackground', ''];
				}
			}
		});
		
		$("#bookingCalendarView").datepicker('show');
		$("#bookingCalendarView").datepicker("setDate", new Date());
	}*/
	$("#bookingCalendarView").datepicker('show');
	$("#bookingCalendarView").datepicker("setDate", new Date());
	//$("td.bookedEvent").children("a").first().text("1");
	//$(".bookedEvent.ui-state-default").html("<b>1</b>");
	//alert($(".bookedEvent").html());
});

function deleteBooking(bookingId){
	var postData = {
			bookingId : bookingId,
	};
	location.href = "bookingCancel.html?bookingId=" + bookingId +"&operationType=delete";
}

function editBooking(bookingId) {
	var postData = {
			bookingId : bookingId,
	};
	location.href = "bookingEdit.html?bookingId=" + bookingId +"&operationType=update";
}

function reloadCalendar() {
	var venueId = document.getElementById("venueId").value;
	location.href = "search.html?venueId=" + venueId;
}
