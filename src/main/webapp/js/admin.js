//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var venueList = [];
var infoList = [];
var venueArray = [];
var amenitiesArr =[];

$(".next").click(function(){
	current_fs = $(this).parents("fieldset");
	next_fs = $(this).parents("fieldset").next();
	
	if(current_fs.attr('id') == 'property-details') {
		if(!validateBasicDetailsForm()) {
			return false;
		}
	}
	if(current_fs.attr('id') == 'contact-details') {
		if(!validateContactDetailsForm()) {
			return false;
		}
	}
	
	if(animating) return false;
	animating = true;
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){			
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parents("fieldset");
	previous_fs = $(this).parents("fieldset").prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$("#emailId").blur(function(){
	var email = $("#emailId").val();
	var url = "validateData.do";
	var emailError = $(".email-error-duplicate");
	var postData = {
			type: "email",
			data: email
	};
	
	$.ajax({
		type : "POST",
		url : url,
		data : postData,
		success : function(response) {
			if(response == "success"){
				$(".email-error").hide();
				emailError.show();
			} else {
				emailError.hide();
			}
		},
		error : function(e) {
			alert("OOPS!! Some Error");
		}
	}); 	
});

$("#phoneNumber").blur(function(){
	var phone = $("#phoneNumber").val();
	var url = "validateData.do";
	var phoneError = $(".phone-error-duplicate");
	var postData = {
			type: "phone",
			data: phone
	};
	
	$.ajax({
		type : "POST",
		url : url,
		data : postData,
		success : function(response) {
			if(response == "success"){
				$(".phone-error").hide();
				phoneError.show();
			} else {
				phoneError.hide();
			}
		},
		error : function(e) {
			alert("OOPS!! Some Error");
		}
	}); 	
});


$(document).on("click", ".addVenue", function (e) {    
	$('.error-msg').hide();
	error = false;
	$("#venueName").val("");
	$("#venueDescription").val("");
	$("#length").val("");
	$("#width").val("");
	$("#height").val("");
	$("#venueTypeId").val("-1");
	$("#eventTypeId").val("-1");
	$("#1_minCapacity").val("");
	$("#1_maxCapacity").val("");
	$("#2_minCapacity").val("");
	$("#2_maxCapacity").val("");
	$("#3_minCapacity").val("");
	$("#3_maxCapacity").val("");
	$("#4_minCapacity").val("");
	$("#4_maxCapacity").val("");
	$("#5_minCapacity").val("");
	$("#5_maxCapacity").val("");
	
	$(".showVenueSave").css({'display': 'none'});
	$(".showVenueSubmit").css({'display': 'inline'});
});


$(".venueSubmit").click(function(e){
	if(!validateVenueDetailsForm()) {
		return false;
	}
	var name;
	var venueObj= {};
	var seatingTypeList=[];
	var tableBody = $("#venueTable tbody");
	var venueType = $("#venueType").val();
	var tableRow = $("<tr></tr>").appendTo(tableBody);	
	count++;
	e.preventDefault();
	$(".venueInfo input[id]").each(function(i){
		 name = $(this).attr("id");
		 value = $(this).val();
		 venueObj[name] = value;
		 $("<td></td>").text(value).appendTo(tableRow);	
	});
	$(".venueDataForm select[id]").each(function(i){
		 name = $(this).attr("id");
		 value = $(this).val();
		 text = $(this).find('option:selected').text();
		 venueObj[name] = value;
		 if (!$.isArray(value)){
			 $("<td></td>").text(text).appendTo(tableRow);
		 }		 	
	});
	$(".seatingTypeList").each(function(i){
		 var seatingObj= {};
		 var seatingId = $(this).find('span').attr("data-seating-id");
		 seatingObj['seatingTypeId']=seatingId;
		 name = $(this).find("input[id*='minCapacity']").attr("data-min-name");
		 value = $(this).find("input[id*='minCapacity']").val();
		 seatingObj[name] = value;
		 maxname = $(this).find("input[id*='maxCapacity']").attr("data-max-name");
		 value = $(this).find("input[id*='maxCapacity']").val();
		 seatingObj[maxname] = value;
		 seatingTypeList.push(seatingObj);
	});
	venueObj.seatingTypeList = seatingTypeList;
	venueList.push(venueObj);	
	});

$(".infoSubmit").click(function(e){
	fetchPropertyInfo();
		
});

function fetchPropertyInfo() {
		if(!validateAmenityDetailsForm()) {
			return false;
		}
		var name;
		var infoObj= {};
		var tableBody = $("#infoTable tbody");
		var tableRow = $("<tr></tr>").appendTo(tableBody);	
		count++;
		$.each($('input, textarea', '.infoDataForm'),function(i){
			 name = $(this).attr("id");
			 value = $(this).val();
			 infoObj[name] = value;
			 if(name == 'value') {
				 value = value.replace(/<(?:.|\n)*?>/gm, '');
			 }
			 $("<td></td>").text(value).appendTo(tableRow);	
		});
		infoList.push(infoObj);
};

$(".selectBox").click(function(e){
	var selectBox = $(this);
	if(selectBox.hasClass('selected')){
		selectBox.css({'background': '#E4DEDC'});
		selectBox.removeClass('selected');
	} else {
		selectBox.css({'background': '#74F71E'});
		selectBox.addClass('selected');
	}
});

function fetchAmenities(){
	$('.selected').each(function(){
		var amenitiesId = $(this).attr("data-amenities-id");
		 amenitiesArr.push(amenitiesId);
	});
};

$(".submit").click(function(e){
	fetchAmenities();
	$("form#msform" ).submit(function(){
		$("#venueList").val(JSON.stringify(venueList));
		$("#infoList").val(JSON.stringify(infoList));
		$("#amenitiesList").val(JSON.stringify(amenitiesArr));
	});	
});



$(document).ready(function() {
    count = 0;
    $('.datepicker').datepicker({
    	format: 'dd-mm-yyyy'
    });
    tinymce.init({
        selector: '#value',
        menubar: false,
        toolbar: [
            'undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright'
          ],
	    setup: function (editor) {
          editor.on('change', function () {
              tinymce.triggerSave();
          });
	    }
      });
    
});