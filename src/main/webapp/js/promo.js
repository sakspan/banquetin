$(document).on("click", ".promoEditButton", function() {
	clearErrorMessage();
	var promoName = $(this).data('name');
	var promoDescription = $(this).data('promodesc');
	var promoValue = $(this).data('promoval');
	var promoId = $(this).data('promoid');

	document.getElementById("promoName").value = promoName;
	document.getElementById("promoDescription").value = promoDescription;
	document.getElementById("promoValue").value = promoValue;
	document.getElementById("promoId").value = promoId;
});

function addPromo() {
	if (!validatePromoDetails()) {
		return false;
	}
	var promoName = document.getElementById("promoName").value;
	var promoDescription = document.getElementById("promoDescription").value;
	var promoValue = document.getElementById("promoValue").value;
	var promoId = document.getElementById("promoId").value;
	var operation = "add";

	if (promoId == 0) {
		var postData = {
				promoId : promoId,
				promoName : promoName,
				promoDescription : promoDescription,
				promoValue : promoValue,
				operationType : operation
		};
		submitForm("addPromoSubmit.do", postData);
	} else {
		var postData = {
				promoId : promoId,
				promoName : promoName,
				promoDescription : promoDescription,
				promoValue : promoValue,
				operationType : "update"
		};

		submitForm("addPromoSubmit.do", postData);
	}
}

function submitForm(url, postData) {
	$.ajax({
		type : "POST",
		url : url,
		data : postData,
		success : function(response) {
			setTimeout(function() {
				location.reload();
			}, 1000);
		},
		error : function(e) {
			alert("OOPS!! Some Error");
		}
	});
}

function validatePromoDetails() {
	$('.error-msg').hide();
	var error = false;
	var promoName = $("#promoName").val();
	var promoDescription = $("#promoDescription").val();
	var promoValue = $("#promoValue").val();

	if (promoName == '') {
		$(".promoName-error").show();
		$("#promoName").focus();
		error = true;
	}
	if (promoDescription == '') {
		$(".promoDescription-error").show();
		if (!error) {
			$("#promoDescription").focus();
		}
		error = true;
	}
	if (promoValue == '') {
		$(".promoValue-error").show();
		if (!error) {
			$("#promoValue").focus();
		}
		error = true;
	}
	if (error) {
		return false;
	} else {
		return true;
	}
}

function clearErrorMessage() {
	$(".error-msg.promoName-error").hide();
	$(".error-msg.promoDescription-error").hide();
	$(".error-msg.promoValue-error").hide();
}

function deletePromo(promoId){
	var postData = {
			promoId : promoId,
			operationType : "delete"
	};
	submitForm("addPromoSubmit.do", postData);
}