<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <%@ taglib prefix="display" uri="http://displaytag.sf.net" %> --%>
<script>
	function searchResults() {
		var selDate = document.getElementById("selectedDate").value;
		var paxCount = document.getElementById("pax").value;
		location.href = "bookings.html?selectedDate=" + selDate + "&paxCount="
				+ paxCount;
	}
	
	var bookedDatesList = <%=request.getAttribute("bookedDatesList")%>;
	var bookedDatesMap = '<%=request.getAttribute("bookedDatesMap")%>';
	
</script>
<style>
#parent{width:100%;}
#parent div{float:left !important;width:33.33% !important;font-size: 20px !important;}
</style>
<div id="bookingRow">
<header class="header header-rates bg-white b-b b-light">
	<div id="parent">
		<div><label>Booking</label></div>
		<div>
			<label>Select Venue : </label> <select id="venueId" onchange="javascript:reloadCalendar();">
			<option value="-1">Select Venue</option>
				<c:forEach var="venue" items="${propertyVenueList}">
					<option value="${venue.venueId}" 
									<c:if test="${venue.venueId == filteredVenueId}">selected ="selected"</c:if>>${venue.venueName}
					</option>
				</c:forEach>
			</select></div>
		<div>
			<button class="btn btn-sm btn-success createBooking"
				data-toggle="modal" data-target="#searchModal"
				style="margin-right: 25px;" onclick="clearErrorMessage();">Create
				New Booking</button>
		</div>
		<br style="clear: both">
	</div>
</header>
<br>

<!-- Div for List Showing -->

<div class="row">
	<section class="scrollable wrapper w-f">
		<div class="modal fade" id="searchModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add Booking</h4>
					</div>
					<div class="modal-body">
						<section class="scrollable wrapper w-f infoDataForm">
							<div class="bookin-info" style="padding: 0px;">
								<div class="bookinRow">
									<jsp:include page="BookedCalendar.jsp" />
									<div class="input-box">
										<label>Date :</label> <input type="text" id="selectedDate"
											class="datepicker" placeholder="Date">
											<div class="error-msg selectedDate-error">Please enter date</div>
									</div>
									<div class="input-box">
										<label>Pax :</label> <input type="text" id="pax"
											placeholder="Pax">
											<div class="error-msg pax-error">Please enter pax</div>
									</div>
									<br>
									<br> <a href="javascript:searchResults();" class="btn">Search</a>
								</div>

							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="container" style="background-color: white;">
		<div id="bookingCalendarOuterView">
		<div id="bookingCalendarView" style="margin: 0px auto; display: table;">
		</div>
		</div>
		<br>
		<div id="bookingCalendarOuterView">
		<div style="margin: 0px auto; display: table;">
		<table style="width:100%">
  			<tr>
	    		<td style="padding-right: 10px"><img src="images/morning.gif">Morning Event Booked</td>
	    		<td style="padding-right: 10px"><img src="images/Evening.gif">Morning Event Booked</td>
	    		<td style="padding-right: 10px"><img src="images/morning.gif">Morning Event Blocked</td>
	    		<td ><img src="images/Evening.gif">Morning Event Blocked</td>
  			</tr>
		</table>
		</div>
		</div>
		<br>
			<table class="table table-responsive table-hover booking-table">
				<thead>
					<tr>
						<th>Booking Id</th>
						<th>Venue Id</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Booking Amount</th>
						<th>Booking Status</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="bookingData" items="${bookingList}">
						<tr>
							<td>${bookingData.bookingId}</td>
							<td>${bookingData.venueText}</td>
							<td>${bookingData.startDate}</td>
							<td>${bookingData.endDate}</td>
							<td>${bookingData.amount}</td>
							<td>${bookingData.bookingStatus.text}</td>

							<td><button class="btn btn-sm btn-success" onclick="javascript:editBooking(${bookingData.bookingId});">Edit</button>
								<button class="btn btn-sm btn-danger" onclick="javascript:deleteBooking(${bookingData.bookingId});">Delete</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<!-- <div class="col-xs-12 form-group text-right">
				<button class="btn btn-sm btn-success createBooking" data-toggle="modal"
					data-target="#searchModal" style="margin-right: 25px;" onclick="clearErrorMessage();">Create New Booking</button>
			</div> -->
		</div>
	</section>
</div>
</div>