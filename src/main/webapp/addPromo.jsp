<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<header class="header header-rates bg-white b-b b-light">
	<p>
		<b> Add Promo Codes</b>
	</p>
</header>
<style>
.center {
	left: 0;
	right: 0;
	top: 0;
	bottom: 0;
	margin: auto;
	/*this to solve "the content will not be cut when the window is smaller than the content": */
	max-width: 100%;
	max-height: 100%;
	overflow: auto;
}
</style>
<br>
<div class="row">
	<section class="scrollable wrapper w-f">
		<div class="modal fade" id="promoModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Promo Add / Modify Panel</h4>
					</div>
					<div class="modal-body">
						<div id="pnlAdd promo-form">
							<section class="panel panel-default">
								<div class="panel-body">
									<div class="col-xs-12 form-group">
										<input type="hidden" id="promoId" class="form-control"
											disabled="disabled">
									</div>

									<div class="col-xs-12 form-group">
										<input type="text" id="promoName"
											class="form-control placeholder-field" height="45px">
										<label alt='Placeholder' placeholder='Promo Name'></label>
										<div class="error-msg promoName-error">Please enter Promo Name</div>
									</div>

									<div class="col-xs-12 form-group">
										<input type="text" id="promoDescription"
											class="form-control placeholder-field" height="45px">
										<label alt='Placeholder' placeholder='Promo Description'></label>
										<div class="error-msg promoDescription-error">Please enter Promo Description</div>
									</div>

									<div class="col-xs-12 form-group">
										<input type="text" id="promoValue"
											class="form-control placeholder-field" height="45px">
										<label alt='Placeholder' placeholder='Promo Value'></label>
										<div class="error-msg promoValue-error">Please enter Promo Value</div>
										<input type="hidden" id="isActive" name="isActive">
									</div>

									<div class="col-xs-12 form-group text-center">
										<button onclick="javascript:addPromo();"
											class="btn btn-sm btn-success">Submit</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container" style="background-color: white;">
			<table class="table table-responsive table-hover rates-table">
				<thead>
					<tr>
						<th>Promo ID</th>
						<th>Promo Name</th>
						<th>Promo Description</th>
						<th>Promo Value</th>
						<th>Promo Active</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="promoData" items="${promoList}">
						<tr>
							<td>${promoData.promoId}</td>
							<td>${promoData.promoName}</td>
							<td>${promoData.promoDescription}</td>
							<td>${promoData.promoValue}</td>
							<td><c:if test="${promoData.isActive == 1}">Yes</c:if><c:if test="${promoData.isActive == 0}">No</c:if></td>
							<td>
								<button data-toggle="modal" data-target="#promoModal"
									class="btn btn-sm btn-success promoEditButton"
									data-promoid="${promoData.promoId}"
									data-name="${promoData.promoName}"
									data-promodesc="${promoData.promoDescription}"
									data-promoval="${promoData.promoValue}" >Edit</button>
								<button class="btn btn-sm btn-danger"
									onclick="javascript:deletePromo(${promoData.promoId});">Delete</button>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<div class="col-xs-12 form-group text-right">
			<button class="btn btn-sm btn-success" data-toggle="modal"
				data-target="#promoModal" style="margin-right: 25px;"
				onclick="javascript:clearErrorMessage();">Add Promo</button>
		</div>
	</section>
</div>