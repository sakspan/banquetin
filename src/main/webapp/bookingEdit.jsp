<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form action="bookingSubmit.do" id="bookingForm" method="post">
	<div class="content">
		<div class="container">

			<div class="bookin-info">
				<div id="bookingSteps">

					<div class="step-nav">
						<div class="container">
							<div class="inner-nav">
								<ul>
									<li id="firstStep" class="first active"><a href="#">
									<span class="number">1</span><span class="text">Booking Details</span></a></li>
									<li id="secondStep"><a href="#"><span class="number">2</span>
									<span class="text">Payment Details</span></a></li>
									<li id="thirdStep" class="last"><a href="#">
									<span class="number">3</span><span class="text">Edit Confirm</span></a></li>
								</ul>
							</div>
						</div>
					</div>

					<div id="step1">

						<div class="bookinRow">
							<div class="col-xs-4 form-group">
								<input id="guestName" name="guestName" type="text"
									class="placeholder-field" value="${editUserData.userName}"> <label alt='Placeholder'
									placeholder='Guest name' class="bookingDetails"></label>
								<div class="error-msg booking-guestName-error">Please
									enter Guest Name</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="emailId" name="emailId"
									class="placeholder-field" value="${editUserData.userEmail}"> <label alt='Placeholder'
									placeholder='Email ID' class="bookingDetails"></label>
								<div class="error-msg booking-email-error">Please enter
									valid email</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="phoneNum" name="phoneNum"
									class="placeholder-field" value="${editUserData.userMobile}"> <label alt='Placeholder'
									placeholder='Phone' class="bookingDetails"></label>
								<div class="error-msg booking-phone-error">Please enter
									valid Phone Number</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="address1" name="address1"
									class="placeholder-field" value="${editAddressData.address1}"> <label alt='Placeholder'
									placeholder='Address1' class="bookingDetails"></label>
								<div class="error-msg booking-address-error">Please enter
									Address</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="address2" name="address2"
									class="placeholder-field" value="${editAddressData.address2}"> <label alt='Placeholder'
									placeholder='Address2' class="bookingDetails"></label>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="city" name="city"
									class="placeholder-field" value="${editAddressData.city}"> <label alt='Placeholder'
									placeholder='City name' class="bookingDetails"></label>
								<div class="error-msg booking-city-error">Please enter
									City</div>
							</div>
							<div class="col-xs-4 form-group">
								<select class="form-control" id="state" name="state">
									<option value="-1">Select State</option>
									<c:forEach var="state" items="${statesList}">
									<option value="${state.id}" 
									<c:if test="${state.id == editAddressData.stateId}">selected ="selected"</c:if>>${state.desc}
									</option>
									</c:forEach>
								</select>
								<div class="error-msg booking-state-error">Please Select
									State</div>
							</div>
							<div class="col-xs-4 form-group">
								<input type="text" id="pincode" name="pincode"
									class="placeholder-field" value="${editAddressData.pincode}"> <label alt='Placeholder'
									placeholder='Pin Code' class="bookingDetails"></label>
								<div class="error-msg booking-pinCode-error">Please enter
									valid pincode</div>
							</div>
							<input id="venueId" name="venueId" value="${bookingData.venueId}" type="hidden" />
							<input type="hidden" id="venueName" name="venueName" value="${bookingData.venueText}"/>
							<input type="hidden" id="venueDescription" name="venueDescription" value=""/>
							<input type="hidden" id="selectedDate" name="bookingDate" value="${bookingData.startDate}">
							<input type="hidden" id="pax" name="paxCount" value="${bookingData.paxCount}">
							<!-- <div class="col-xs-12 form-group">
								<input type="text" id="comments" name="comments"
									class="placeholder-field"> <label alt='Placeholder'
									placeholder='Comments' class="bookingDetails"></label>
							</div> -->

							<input id="venueId" name="venueId" value="" type="hidden" /> <input
								id="slotType" name="slotType" value="" type="hidden" />


						</div>
						<a href="javascript:showStep0();" class="btn btns btn-prev">Prev</a>
						<a href="javascript:showStep2();" class="btn btns btn-next">Next</a>
					</div>

					<div id="step2">
						<div class="bookinRow">
							<div class="col-xs-6 form-group">
								<label>Estimated Amount: </label><br> <input type="text"
									id="estAmount" name="estAmount" placeholder="Estimated Amount" value="${bookingData.amount*10}">
								<div class="error-msg payment-estAmount-error">Please
									enter Estimated Amount</div>
							</div>
							<div class="col-xs-6 form-group">
								<label>Booking Amount: </label><br> <input type="text"
									id="bookingAmount" name="bookingAmount"
									placeholder="Booking Amount" value="${bookingData.amount}">
								<div class="error-msg payment-bookingAmount-error">Please
									enter Booking Amount</div>
							</div>
							<div class="col-xs-6 form-group">
								<input type="text" id="advancePaid" name="advancePaid"
									class="placeholder-field" value="${bookingData.advanceAmount}"> <label alt='Placeholder'
									placeholder='Advance Paid' class="bookingDetails"></label>
								<div class="error-msg payment-advancedPaid-error">Please
									enter Advance amount paid</div>
							</div>
							<div class="col-xs-6 form-group">
								<select class="form-control" id="paymentType" name="paymentType">
									<option value="-1">Select Payment Type</option>
									<c:forEach var="paymentTypeData" items="${paymentTypeList}">
									<option value="${paymentTypeData.id}" 
									<c:if test="${paymentTypeData.id == bookingData.paymentTypeId}">selected ="selected"</c:if>>${paymentTypeData.text}
									</option>
									</c:forEach>
								</select>
								<div class="error-msg payment-paymentType-error">Please
									select Payment Type</div>
							</div>
						</div>
						<a href="javascript:showStep1();" class="btn btns btn-prev">Prev</a>
						<a href="javascript:showStep3();" class="btn btns btn-next">Next</a>
					</div>

					<div id="step3">

						<div class="bookinRow">Please verify the details below and
							click on CONFIRM to complete the booking</div>

						<div class="bookinRow">

							<div class="col-xs-4 form-group">
								Venue: <label id="venueDisplay">venueDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Date: <label id="dateDisplay">dateDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Pax: <label id="paxDisplay">paxDisplay</label>
							</div>

							<div class="col-xs-4 form-group">
								Guest Name: <label id="guestNameDisplay">DisplayGuestName</label>
							</div>
							<div class="col-xs-4 form-group">
								Email Id: <label id="emailIdDisplay">DisplayEmailID</label>
							</div>
							<div class="col-xs-4 form-group">
								Phone Number: <label id="phoneNumDisplay">phoneNumDisplay</label>
							</div>
							<div class="col-xs-8 form-group">
								Address: <label id="address1Display">address1Display</label>
							</div>

							<div class="col-xs-4 form-group">
								Payment Type: <label id="paymentTypeDisplay">paymentTypeDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Estimated Amount: <label id="estAmountDisplay">estAmountDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Booking Amount: <label id="bookingAmountDisplay">bookingAmountDisplay</label>
							</div>
							<div class="col-xs-4 form-group">
								Advance Paid: <label id="advancePaidDisplay">advancePaidDisplay</label>
							</div>
							<div class="col-xs-8 form-group">
								Comments: <label id="commentsDisplay">commentsDisplay</label>
							</div>

						</div>
						<a href="javascript:showStep2();" class="btn btns btn-prev">Prev</a>
						<a href="javascript:submitBookingForm();"
							class="btn btns btn-confirm">Confirm</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	document.getElementById("step1").style.display = "block";
	document.getElementById("step2").style.display = "none";
	document.getElementById("step3").style.display = "none";
	document.getElementById("bookingSteps").style.display = "block";
	document.getElementById('firstStep').classList.add('active');
	
	function showStep2() {
		if(!validateBookingDetailsForm()){
			return;
		}
		hideStep();
		removeStepsHighlight();
		document.getElementById("step2").style.display = "block";
		document.getElementById('secondStep').classList.add('active');
	}
	function hideStep() {
		document.getElementById("step1").style.display = "none";
		document.getElementById("step2").style.display = "none";
		document.getElementById("step3").style.display = "none";
	}
	function removeStepsHighlight() {
		document.getElementById('firstStep').classList.remove('active');
		document.getElementById('secondStep').classList.remove('active');
		document.getElementById('thirdStep').classList.remove('active');
	}
	function showStep1() {
		clearErrorMessage();
		hideStep();
		removeStepsHighlight();
		document.getElementById("bookingSteps").style.display = "block";
		document.getElementById("step1").style.display = "block";
		document.getElementById('firstStep').classList.add('active');
	}
	function showStep3() {
		if(!validatePaymentDetailsForm()){
			return;
		}
		hideStep();
		removeStepsHighlight();

		var stateSelect = document.getElementById("state");
		var state = stateSelect.options[stateSelect.selectedIndex].text;
		var paymentSelect = document.getElementById("paymentType");
		var payment = paymentSelect.options[paymentSelect.selectedIndex].text;
		
		document.getElementById("guestNameDisplay").innerHTML = document.getElementById("guestName").value;
		document.getElementById("emailIdDisplay").innerHTML = document.getElementById("emailId").value;
		document.getElementById("phoneNumDisplay").innerHTML = document.getElementById("phoneNum").value;

		document.getElementById("address1Display").innerHTML = document.getElementById("address1").value
				+ ", "+ document.getElementById("address2").value+ ", "+ document.getElementById("city").value+ ", "
				+ state+ ", "+ document.getElementById("pincode").value;
		

		document.getElementById("paymentTypeDisplay").innerHTML = payment;
		document.getElementById("estAmountDisplay").innerHTML = "Rs."+ document.getElementById("estAmount").value;
		document.getElementById("bookingAmountDisplay").innerHTML = "Rs."+ document.getElementById("bookingAmount").value;
		document.getElementById("advancePaidDisplay").innerHTML = "Rs."+ document.getElementById("advancePaid").value;
		//document.getElementById("commentsDisplay").innerHTML = document.getElementById("comments").value;

		document.getElementById("venueDisplay").innerHTML = document.getElementById("venueName").value + ", " + document.getElementById("venueDescription").value;
		document.getElementById("dateDisplay").innerHTML = document.getElementById("selectedDate").value;
		document.getElementById("paxDisplay").innerHTML = document.getElementById("pax").value;

		document.getElementById("step3").style.display = "block";
		document.getElementById('thirdStep').classList.add('active');
	}
	function submitBookingForm() {
		var locationParam = $("<input>").attr("type", "hidden").attr("name", "operationType").val("edit");
		$('#bookingForm').append($(locationParam));
		$('#bookingForm').submit();
	}
	function showStep0() {
		hideStep();
		removeStepsHighlight();
		document.getElementById("bookingSteps").style.display = "none";
		location.href = "search.html";
	}
	function clearErrorMessage(){
		$(".error-msg.booking-guestName-error").hide();
		$(".error-msg.booking-email-error").hide();
		$(".error-msg.booking-phone-error").hide();
		$(".error-msg.booking-address-error").hide();
		$(".error-msg.booking-city-error").hide();
		$(".error-msg.booking-pinCode-error").hide();
		$(".error-msg.booking-state-error").hide();
	}
</script>

1 . date field and pax field in both create and edit flow for booking
2. 