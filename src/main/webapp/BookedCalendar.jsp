<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach items="${hashMap}" var="hmap">
	<c:forEach items="${hashMap.value}" var="hmapValue">
		<script type="text/javascript">
			function unavailable(date) {
				var unavailableDates = new Array();
				<c:set var="i" value="0" />
				<c:forEach items="${hmapValue}" var="current" varStatus="status">
				unavailableDates[<c:out value='${i}' />] = '<c:out value='${current.startDate}'/>';
				<c:set var="i" value="${i+1}" />
				</c:forEach>
				var dmy = date.getFullYear()
						+ "-"
						+ ((date.getMonth() + 1) < 10 ? ('0' + (date.getMonth() + 1))
								: (date.getMonth() + 1))
						+ "-"
						+ ((date.getDate() + 1) < 10 ? ('0' + date.getDate())
								: date.getDate());
				if ($.inArray(dmy, unavailableDates) == -1) {
					return [ true, "" ];
				} else {
					return [ false, "", "unavailable" ];
				}
			}
			function initCalendar() {
				$("#bookedCalendar<c:out value='${hmap.key}' />").datepicker({
					dateFormat : 'yy-mm-dd',
					beforeShowDay : unavailable,
					changeMonth : true,
					changeYear : true,
					altField : "#selectedDate",
					altFormat : "yy-mm-dd"
				});
			}
		</script>
		<div id="bookedCalendar<c:out value='${hmap.key}' />"></div>
	</c:forEach>
</c:forEach>
<button onclick="initCalendar()">Show Calendar</button>

 --%>