<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>

</script>
<header class="header header-rates bg-white b-b b-light">
	<p>
		<b> Add Rates to Venue</b>
	</p>
</header>
<br>
<div class="row" style="margin: 15px;">
	<div class="col-xs-12 form-group text-center venue-details">
		<label>Select Venue : </label> <select id="venueId">
			<c:forEach var="venue" items="${venueList}">
				<option value="${venue.venueId}">${venue.venueDescription}</option>
			</c:forEach>
		</select>

		<button onclick="javascript:fetchRates();"
			class="btn btn-sm btn-success">Fetch Rates</button>
	</div>
</div>
<div class="row">
	<section class="scrollable wrapper w-f">
		<div class="modal fade" id="ratesModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Rates Add / Modify Panel</h4>
					</div>
					<div class="modal-body">
						<div id="pnlAdd rates-form">
							<section class="panel panel-default">
								<div class="panel-body">
									<div class="col-xs-12 form-group">
										<input type="hidden" id="ratesId" class="form-control"
											placeholder="Enter ratesId" disabled="disabled">
											<!-- <div class="error-msg amenity-title-error">Please enter tile for Amenity Info</div> -->
									</div>

									<div class="col-xs-12 form-group">
										<select id="updateVenueId">
											<option value="-1">Select Venue</option>
											<c:forEach var="venue" items="${venueList}">
												<option value="${venue.venueId}">${venue.venueDescription}</option>
											</c:forEach>
										</select>
										<div class="error-msg rates-updateVenueId-error">Please Select Venue</div>
									</div>
									<div class="col-xs-6 form-group">
										<input type="text" id="startDate"
											class="form-control datepicker placeholder-field" height="45px">
											<label alt='Placeholder' placeholder='Select Start Date'></label>
											<div class="error-msg rates-startDate-error">Please enter start date</div>
									</div>
									<div class="col-xs-6 form-group">
										<input type="text" id="endDate"
											class="form-control datepicker placeholder-field" height="45px">
											<label alt='Placeholder' placeholder='Select End Date'></label>
											<div class="error-msg rates-endDate-error">Please enter end date</div>
									</div>
									<div class="col-xs-12 form-group">
										<select id="slotType">
											<option value="-1">Select Slot</option>
											<c:forEach var="slotTypeData" items="${slotTypeList}">
												<option value="${slotTypeData.slotTypeId}">${slotTypeData.slotTypeText}(${slotTypeData.slotStartTime}
													- ${slotTypeData.slotEndTime})</option>
											</c:forEach>
										</select>
										<div class="error-msg rates-slotType-error">Please select a rate slot</div>
									</div>
									<div class="col-xs-6 form-group">
										<input type="text" id="rate" class="form-control placeholder-field" height="45px">
											<label alt='Placeholder' placeholder='Enter Rate'></label>
											<div class="error-msg rates-rate-error">Please enter rate</div>
									</div>
									<input type="hidden" value="1" id="rateType"
										class="form-control" placeholder="Enter Rate Type">
									<div class="col-xs-6 form-group">
										<input type="text" id="bookingAmount" class="form-control placeholder-field" height="45px">
											<label alt='Placeholder' placeholder='Minimum Booking Amount'></label>
											<div class="error-msg rates-bookingAmount-error">Please enter booking amount</div>
									</div>
									<div class="col-xs-12 form-group text-center">
										<button onclick="javascript:addUpdateRates();"
											class="btn btn-sm btn-success">Submit</button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<button class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Div for List Showing -->
		<div class="clearfix"></div>
		<div class="container" style="background-color: white;">
			<%-- <% if (session.getAttribute("StartDateError") == null & session.getAttribute("EndDateError") == null) { %>
				<div class="alert alert-success alert-dismissible show"
					role="alert">
					<p>SUCCESS!!</p>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<% } %> --%>
			<%
				if (session.getAttribute("StartDateError") != null) {
			%>
			<div class="alert alert-danger alert-dismissible show" role="alert">
				${StartDateError}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<%
				}
			%>
			<%
				if (session.getAttribute("EndDateError") != null) {
			%>
			<div class="alert alert-danger alert-dismissible show" role="alert">
				${EndDateError}
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<%
				}
			%>
			<table class="table table-responsive table-hover rates-table">
				<thead>
					<tr>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Slot Type</th>
						<th>Rate</th>
						<!-- <th>Rate Type</th>  -->
						<th>Booking Amount</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rateData" items="${rateList}">
						<tr>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${rateData.startDate}" /></td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${rateData.endDate}" /></td>
							<td>${rateData.slotType.text}</td>
							<td>${rateData.rate}</td>
							<!-- <td>${rateData.ratesType.text}</td>  -->
							<td>${rateData.bookingAmount}</td>

							<td>
								<button data-toggle="modal" data-target="#ratesModal" class="btn btn-sm btn-success editButton"
									data-id="${rateData.ratesId}" data-venueId="${rateData.venueId}" data-startDate="<fmt:formatDate type = 'date' value = '${rateData.startDate}' pattern='dd-MM-yyyy'/>" data-endDate="<fmt:formatDate type = 'date' value = '${rateData.endDate}' pattern='dd-MM-yyyy'/>" data-slotTypeId="${rateData.slotTypeId}" data-rate="${rateData.rate}" data-rateTypeId="${rateData.ratesType.id}" data-bookingAmount="${rateData.bookingAmount}">Edit</button>
								<button class="btn btn-sm btn-danger"
									onclick="javascript:deleteRates(${rateData.ratesId});">Delete</button></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="col-xs-12 form-group text-right">
			<button class="btn btn-sm btn-success" data-toggle="modal"data-target="#ratesModal" style="margin-right: 25px;" onclick="javascript:clearErrorMessage();">Add New Rate</button>
		</div>
	</section>
</div>
